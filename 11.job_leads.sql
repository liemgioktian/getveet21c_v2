-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 14, 2016 at 09:59 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `getveet21c_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `job_leads`
--

CREATE TABLE `job_leads` (
  `lid` int(11) NOT NULL,
  `date_leads` varchar(255) NOT NULL,
  `suburb_leads` varchar(255) NOT NULL,
  `qty_leads` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_leads`
--

INSERT INTO `job_leads` (`lid`, `date_leads`, `suburb_leads`, `qty_leads`) VALUES
(1457856470, '01-03-2016', 'smg', '3'),
(1457868144, '13-03-2016', 'smg', '34'),
(1457871104, '15-03-2016', 'kota', '12'),
(1457871399, '16-03-2016', 'suburb', '34');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
