CREATE TABLE `lead_company` (
  `id` int(11) NOT NULL,
  `lid` int(11) NOT NULL,
  `cid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `lead_company`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `lead_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;