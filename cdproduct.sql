-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 24, 2015 at 12:39 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `getveet21c_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `cd_product`
--

CREATE TABLE `cd_product` (
  `item_id` int(11) NOT NULL,
  `id` varchar(255) NOT NULL,
  `type` char(1) NOT NULL DEFAULT 'C',
  `value` int(11) NOT NULL DEFAULT '1',
  `lifetime` varchar(255) NOT NULL DEFAULT '20000+',
  `output` int(11) NOT NULL DEFAULT '350',
  `efficacy` int(11) NOT NULL DEFAULT '62',
  `factors` float NOT NULL DEFAULT '0.8715'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cd_product`
--

INSERT INTO `cd_product` (`item_id`, `id`, `type`, `value`, `lifetime`, `output`, `efficacy`, `factors`) VALUES
(1, 'PRIMSAL PMR166WWHPF', 'C', 1, '20000+', 350, 62, 0.8715),
(2, 'PRIMSAL PMR166WWHPFB', 'C', 1, '20000+', 350, 62, 0.8715),
(3, 'Primsal PLV10WWK (Driver PD1000)', 'D', 1, '20000+', 350, 69, 0.924),
(4, 'Primsal PLV6WWK (Driver PD400)', 'D', 1, '20000+', 350, 69, 0.924),
(5, 'Primsal PGU106WWDHPF', 'D', 1, '20000+', 350, 69, 0.924);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cd_product`
--
ALTER TABLE `cd_product`
  ADD PRIMARY KEY (`item_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cd_product`
--
ALTER TABLE `cd_product`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;