<?php

class getveetModel extends CI_Model {
    
    var $table;
    var $id;
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function save($data){
        if(isset($data[$this->id])){
        	$this->db->where($this->id, $data[$this->id])->update($this->table, $data);
        }else{
            $data[$this->id] = time();
            $this->db->insert($this->table, $data);
        }
        return $data[$this->id];
    }
    
    function retrieve($id){
        return $this->db->get_where($this->table, array($this->id => $id))->row_array();
    }
    
    function get_list($conditions = array()){
    	if(!isset($this->db->ar_select[0])) $this->db->select('*');
		$this->db->from($this->table);
        if(count($conditions) > 2)
            foreach ($conditions as $field => $value) $this->db->where($field, $value);
        else $this->db->where($conditions);
        return 
        $this->db->get()->result();
        // die($this->db->last_query());
    }
    
    function delete($id){
        $this->db->where($this->id, $id)->delete($this->table);
    }

    function getveet_send_mail($to = null, $htmlMessage, $subject, $attachment = null) {
        $sender['name'] = 'My Electrician';
        $sender['email'] = 'admin@myelec.services';

        $to = null === $to ? $sender['email'] : $to;
        $list = is_array($to) ? $to : array($to);

        $this->load->library('email');
        $this->email->clear();
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender['email'], $sender['name']);
        $this->email->to($list);
        $this->email->subject($subject);
        $this->email->message($htmlMessage);
				if (null !== $attachment) $this->email->attach($attachment);

        if (true === $this->email->send()) {
          $this->email->clear(TRUE);
          return true;
        } else return $this->email->print_debugger();
    }
}
