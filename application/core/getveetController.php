<?php

class getveetController extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function loadView($parameter = null, $view = 'mainView') {
        if (!$this->session->userdata('uid'))
            $this->load->view('loginView');
        else {
        	$parameter['current_user'] = $this->session->all_userdata();
            if ($this->session->userdata('is_admin') == 1){
                $parameter['menus'][] = array('title'=>'SETTINGS','url'=>site_url('settingscontroller'));
                $parameter['menus'][] = array('title' => 'REGISTERED USERS', 'url' => site_url('maincontroller/user_list'));
                $parameter['menus'][] = array('title' => 'STOCK MANAGER','url' => site_url('stockcontroller/installer'));
            	$parameter['menus'][] = array('title'=>'LEAD MANAGER','url'=>site_url('lead-manager'));
                $parameter['menus'][] = array('title'=>'PROFIT CALCULATOR','url'=>site_url('settingscontroller/profit'));
                $parameter['menus'][] = array('title' => 'PRODUCTS','url' => site_url('maincontroller/subform/product'));
            }else if ($this->session->userdata('is_admin') == 2){
            	$parameter['menus'][] = array('title'=>'SETTINGS','url'=>site_url('settingscontroller/edit/'.$this->session->userdata('cid')));
							$parameter['menus'][] = array('title' => 'REGISTERED USERS', 'url' => site_url('maincontroller/user_list'));
                $parameter['menus'][] = array('title'=>'LEAD MANAGER','url'=>site_url('lead-manager'));
								$cid = $parameter['current_user']['cid'];
								$parameter['menus'][] = array('title' => 'STOCK MANAGER','url' => site_url("stockcontroller/installer/$cid/company"));
            }
            
            if ($parameter['current_user']['is_admin'] != 3) {
	            $parameter['menus'][] = array('title'=>'STORE','url'=>site_url('maincontroller/store'));
	            $parameter['menus'][] = array('title' => 'RESOURCES','url' => site_url('maincontroller/documents'));
	            $parameter['menus'][] = array('title'=>'NEW FORM','url'=>site_url('maincontroller/subform'));
                $parameter['menus'][] = array('title'=>'SAVED FORMS','url'=>site_url('maincontroller/subform/list'));
            }

			if (!isset($parameter['logo'])) {
				$company_logo = $this->db->get_where('company',array('cid'=>$this->session->userdata('cid')))->row_array();
				$company_logo = isset($company_logo['logo'])?$company_logo['logo']:'';
				$parameter['logo'] = empty($company_logo)?'logo_2.png':$company_logo;
			}
			$parameter['logo'] = base_url('asset/css/images/'.$parameter['logo']);

            $this->load->view($view, $parameter);
        }
    }

}
