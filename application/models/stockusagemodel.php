<?php

class stockusagemodel extends getveetModel {
	
    function __construct() {
        parent::__construct();
		date_default_timezone_set('Australia/Victoria');
        $this->table = 'stock_usage';
        $this->id = 'stock_usage_id';
    }
	
	function save($data){
		$date = date('Y-m-d');
		$data['time'] = strtotime("$date 00:00:00");
		
		$exists = $this->db->get_where('stock_usage',array(
			'stock_id' => $data['stock_id'],
			'job_refference' => $data['job_refference'],
		))->result();
		if(count($exists)>0){
			$data['stock_usage_id'] = $exists[0]->stock_usage_id;
			$id = parent::save($data);
			$stock = $this->db->get_where('stock',array('stock_id'=>$data['stock_id']))->row_array();
			$data['stock'] = -1 * ($exists[0]->stock - $data['stock']);
			// die($stock['stock'].' - ( -1 * ('.$exists[0]->stock .' - '. $data['stock'].'))');
		}else{
			$data['stock_usage_id'] = null;
			$this->db->insert('stock_usage',$data);
			$id = $this->db->insert_id();
		}
		
		$this->db
				->where('stock_id', $data['stock_id'])
				->set('stock','stock-'.$data['stock'], false)
				->update('stock');
		return $id;
	}
	
	function getData($installer_id){
		return $this->db
			->select('stock_usage.time')
			->select('stock_usage.job_refference')
			->select("GROUP_CONCAT(CONCAT(stock_usage.stock_id,'|',stock_usage.stock)) as items",false)
			->join('stock','stock_usage.stock_id=stock.stock_id')
			->where('stock.installer_id',$installer_id)
			->group_by('stock_usage.job_refference')
			->get('stock_usage')
			->result();
	}
}