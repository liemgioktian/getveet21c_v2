<?php

Class installermodel extends getveetModel {
    function __construct() {
        parent::__construct();
        $this->table = 'installer';
        $this->id = 'iid';
    }
	
	function retrieve($iid){
		$this->db->join('company','company.cid=installer.cid');
		return 
		parent::retrieve($iid);
	}
	
	function get_admin_installers(){
		return $this->db->query(
		"SELECT * FROM installer WHERE cid IN (
			SELECT cid FROM user WHERE is_admin = 1
		)"
		)->result();
	}
}