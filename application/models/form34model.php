<?php

class form34model extends getveetModel {
  
  function __construct() {
    parent::__construct();
    $this->table = 'form34';
    $this->id = 'fid';
  }
  
  function save($data) {
    if (!isset($data['fid'])) {
      if ($this->session->userdata('appId')) {
        $appId = $this->session->userdata('appId');
        $appAdmin = $this->session->userdata('appId') == $this->session->userdata('cid') ? 1 : 0;
      } else {
        $appId = 1;
        $appAdmin = 0;
      }

      $data['uid'] = $this->session->userdata('uid');
      $data['cid'] = $this->session->userdata('cid');
      $data['appId'] = $appId;
      $data['appAdmin'] = $appAdmin;

      $f34 = end(parent::get_list(array()));
      if (!isset($f34->job_reference)) $data['job_reference'] = 3400000001; 
      else $data['job_reference'] = (int) $f34->job_reference + 1 ;
    }

    foreach ($data['data'] as $po => $st) parse_str($st, $data['data'][$po]);
    $data['data'] = json_encode($data['data']);
    // echo'<pre>';print_r($data['data']['quote']);die();
    return parent::save($data);
  }

  function getFileList () {
    $this->db
      ->select('form34.*')
      ->select('user.email as user', false)
      ->select('company.electricians_company_name as company', false)
      ->select('app.electricians_company_name as application', false)
      ->join('user', 'user.uid = form34.uid', 'LEFT')
      ->join('company', 'company.cid = form34.cid', 'LEFT')
      ->join('company as app', 'app.cid = form34.appId', 'LEFT');
    return parent::get_list(array());
  }

  function delete ($fid) {
    parent::delete($fid);

    $this->load->model('evidence34model');
    foreach ($this->evidence34model->get_list(array('fid34'=>$fid)) as $evi)
      $this->evidence34model->deleteTree($evi->eid);

    return true;
  }
}