<?php

class jobleadsmodel extends getveetModel {
  
  function __construct() {
    parent::__construct();
    $this->table = 'job_leads';
    $this->id = 'lid';
  }

  function getJson ($conditions) {
    $this->db
      ->select("$this->table.*")
      ->select("lid as id", false);
    return parent::get_list($conditions);
  }

  function joinLeadCompany ($where) {
    $this->db
      ->select('job_leads.*')
      ->select ("COUNT(lead_company.id) AS companyRequest", false)
      ->select ("GROUP_CONCAT(lead_company.cid) AS cids", false)
      ->join ('lead_company', 'job_leads.lid = lead_company.lid', 'left')
      ->group_by ('job_leads.lid');
    return parent::get_list($where);
  }

  function bindLeadCompany ($lid, $cid) {
    $table = 'lead_company';
    $obj = array (
      'lid' => $lid,
      'cid' => $cid
    );
    $exists = $this->db->get_where($table, $obj)->row_array();
    if (isset ($exists['id'])) return true;
    return $this->db->insert($table, $obj);
  }
}