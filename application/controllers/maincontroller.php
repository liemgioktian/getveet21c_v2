<?php

class maincontroller extends getveetController {

    function __construct() {
        parent::__construct();
        $this->load->model('fileModel');
        $this->load->model('jobleadsmodel');
		date_default_timezone_set('Australia/Victoria');
    }

    function index($uid = 0) {
		$conditions = array();
		$sort = array();
		$loadView = true;
		$post = $this->input->post();

    if (!$post) { // DEFAULT CONDITION, ACCESSED FROM MENU
      $conditions['file.lodge_draft'] = 'LODGE FORM';
    }

		switch ($this->session->userdata('is_admin')) {
			case '0':
				$conditions['file.uid'] = $this->session->userdata('uid');
				break;
			case '1':
				if (!$post) $conditions['file.lodge_draft'] = 'LODGE FORM';
			break;
			case '2':
				$conditions['user.cid'] = $this->session->userdata('cid');
				break;
		}

		if(isset($post['since']) && $post['since'] !=''){
			$param['since'] = $post['since'];
			$conditions['file.fid >='] = strtotime($post['since']);
		}if(isset($post['until']) && $post['until'] !=''){
			$param['until'] = $post['until'];
			$conditions['file.fid <='] = strtotime($post['until']);
		}

		if (isset($post['file_list_dropdown'])) {
			switch ($post['file_list_dropdown']) {
				case 'LODGE':
					$conditions['file.lodge_draft'] = 'LODGE FORM';
					break;
				case 'DRAFT':
					$conditions['file.lodge_draft'] = 'SAVE DRAFT';
					break;
				case 'COMPLETE':
          $sort[] = 'completed_date';
					$conditions['file.complete'] = 1;
					break;
				case 'INCOMPLETE':
					$conditions['file.complete'] = 0;
					break;
				case 'CLIENTNUMBER':
					$sort[] = 'file.clientNumber';
					break;
				case 'GET':
					$conditions['file.admin3_status'] = 'incomplete';
					break;
				default:break;
			}
		}

		$condition_like = array();
		if(isset($post['namelike']) && $post['namelike']!=''){
			$param['namelike'] = $post['namelike'];
			$condition_like['file.name'] = $post['namelike'];
		}

		if(isset($post['cid'])){
			if($post['cid']!='all')
				$param['selected_company'] = $conditions['user.cid'] = $post['cid'];
		}

		if (isset($post['filter_lodge_draft']) && $post['filter_lodge_draft'] !== 'ALL') {
			$conditions['file.lodge_draft'] = $post['filter_lodge_draft'];
		}

		if (isset($post['filter_complete']) && $post['filter_complete'] !== 'ALL') {
			$conditions['file.complete'] = $post['filter_complete'];
		}
		
		if($this->session->userdata('is_admin')==1){
			$this->load->model('settingsModel');
			$param['companies'] = $this->settingsModel->get_list();
		}
		/*
		if (isset($post['set_admin3_incomplete'])) {
			$form = $this->fileModel->retrieve($post['fid']);
			$form['admin3_status'] = 'incomplete';
			$this->fileModel->save($form);
		}
		*/

		if (isset($post['set_admin3_complete'])) {
			$form = $this->fileModel->retrieve($post['fid']);
			$form['admin3_status'] = 'confirmed';
			$form['checked_date'] = time();
			$form['info_submitted'] = 0;
			$this->fileModel->save($form);
		}

		if (isset($post['set_admin3_msg'])) {
			$form = $this->fileModel->retrieve($post['fid']);
			$form['admin3_status'] = 'incomplete';
			$form['admin3_msg'] = $post['admin3_msg'];
			$form['request_date'] = time();
			$form['info_submitted'] = 0;
			$this->fileModel->save($form);
			if ($this->session->userdata('is_admin')==3) {
				$this->fileModel->getveet_send_mail('forms@myelec.net.au', $post['admin3_msg'], 'GET FORM INCOMPLETE MESSAGE');
			}
		}

		if (isset($post['set_msg_submitted'])) {
			$form = $this->fileModel->retrieve($post['fid']);
			$form['info_submitted'] = 1;
			$form['submitted_date'] = time();
			$form['admin3_msg'] = $post['admin3_msg'];
			$this->fileModel->save($form);
			if ($this->session->userdata('is_admin')==1) {
				$this->fileModel->getveet_send_mail('forms@greenenergytrading.com.au', $post['admin3_msg'], 'Form Submitted');
			}
		}
		
		if(isset($post['set_complete'])){
			$form = $this->fileModel->retrieve($post['fid']);
			$form['complete'] = 1;
			$form['marked'] = 1;
			$form['completed_date'] = time();
			$this->fileModel->save($form);

			$formData = json_decode($form['data'], true);
			$customerEmail = $formData['C31'];
			$customerName = $formData['C22'];
			$clientNumber = $formData['H4'];
			$company = $formData['C124'];
			$companyPhone = $formData['O127'];
			$companyEmail = $formData['Z127'];
			$companyInstaller = $formData['C127'];
			$logo = FCPATH.'asset/images/logo-green.png';
			$subject = 'VEET 21 scheme assignment form '.$clientNumber.' halogen replacement program';
			$htmlMsg = 
			'Dear '.$customerName.', <br /><br />
			Congratulations on choosing to replace your halogen downlights with energy saving alternatives with '.$company.'. <br />
			Please find attached the assignment form for the installation in your home or commercial property.
			Be sure to keep the attached assignment form away for your records. It contains information about your installation and installer contact details. <br />
			Please check to make sure all information is true and correct. <br />
			For warranty claims or problems with your installation, please contact.
			'.$companyInstaller.' from '.$company.' on '.$companyPhone.' or by email '.$companyEmail.'. 

			 <br /> <br/>
			Kind Regards, <br />
			My Electrician Team <br />
			Office: 1300 660 042 <br />
			www.myelec.net.au <br />
			<img src="'.$logo.'" alt="" style="width:50px">

			'
			
			;

			$receivers = array(
				'forms@greenenergytrading.com.au',
				'forms@myelec.services',
				'forms@myelec.net.au',
				$customerEmail
			);
			$this->load->model('userModel');
			$author = $this->userModel->retrieve($form['uid']);
			$this->load->model('settingsModel');
			$settings = $this->settingsModel->retrieve($author['cid']);
			$formData['logo'] = isset($settings['logo'])?$settings['logo']:'';
			$formData['logo'] = empty($formData['logo'])?'logo_2.png':$formData['logo'];
			$formData['logo'] = 'asset/css/images/'.$formData['logo'];
			ob_start();
      		$this->load->library('dompdf_gen');
      		$html = $this->load->view('assignment', $formData, true);
      		$this->dompdf->load_html($html);
      		$this->dompdf->render();
			$fileName = time();
			$pdf_path = FCPATH.'temp/'.$fileName.'.pdf';
			file_put_contents($pdf_path, $this->dompdf->output());
			foreach ($receivers as $receiver) {
				$this->fileModel->getveet_send_mail($receiver, $htmlMsg, $subject, $pdf_path);
			}
			unlink($pdf_path);

			$this->smartsheet($form['data'], 'Completed Form');
		}
		if(isset($post['set_incomplete'])){
			$form = $this->fileModel->retrieve($post['fid']);
			$form['complete'] = 0;
			$form['marked'] = 1;
			$form['lodge_draft'] = 'SAVE DRAFT';
			$file_company = $this->fileModel->get_file_company($post['fid']);
			$data = json_decode($form['data'], true);
			$data['M51'] = $file_company['c_certificate'];
			$data['M55'] = $file_company['d_certificate'];
			$form['data'] = json_encode($data);
			if(isset($post['incompletion_msg'])){
				$form['incompletion_msg'] = $post['incompletion_msg'];
			}
			$this->fileModel->save($form);
			#$this->fileModel->getveet_send_mail($data['C31'], $form['incompletion_msg'], 'Please complete your form', null);

			$formData= json_decode($form['data'], true);
			$clientNumber = $formData['H4'];
			$companyEmail = $formData['C127'];
			$customerName = $formData['C22'];
			$incompleteMsg = $form['incompletion_msg'];
			$subject = 'Assignment form '.$clientNumber.' has been checked and is incomplete.';
			$htmlMsg = 
			
			'Dear '.$customerName.', <br /> <br />
			Assignment form '.$clientNumber.' has been checked and marked as incomplete due to: <br />
			'.$incompleteMsg.'
			<br />Please fix the reported issues and re-lodge the form. <br />
			Please note that whilst the form is incomplete, the VEEC price is unlocked and exposed to rise and fall.
			Make the required corrections to your assignment form and resubmit as soon as possible to reduce the risk of a VEEC price change affecting the anticipated financial benefit.
			<br /> <br/>
			Kind Regards, <br />
			My Electrician Team <br />
			Office: 1300 660 042 <br />
			www.myelec.net.au
			';
			$this->fileModel->getveet_send_mail($companyEmail, $htmlMsg, $subject, null);
			#$this->smartsheet_deleterow($form['data'], 'Completed Form');
			$this->smartsheet($form['data'], 'Draft Form');
		}
		if(isset($post['lodge_draft'])){
			if (!isset($post['confirm'])) {
				$confirm_param = array(
					'post_url' => current_url(),
					'post_params' => $post
				);
				$this->loadView ($confirm_param, 'confirmationView');
				$loadView = false;
			} else {
				$form = $this->fileModel->retrieve($post['fid']);
				$form['lodge_draft'] = $post['lodge_draft'];
				if ($post['lodge_draft'] === 'SAVE DRAFT') {
					$file_company = $this->fileModel->get_file_company($post['fid']);
					$data = json_decode($form['data'], true);
					$data['M51'] = $file_company['c_certificate'];
					$data['M55'] = $file_company['d_certificate'];
					$form['data'] = json_encode($data);
					$this->smartsheet($form['data'], 'Draft Form');
				} else $this->smartsheet($form['data'], 'Lodge Form');
				$this->fileModel->save($form);
			}
		}
		if(isset($post['reset_signature'])){
			$form = $this->fileModel->retrieve($post['fid']);
			$form_data = json_decode($form['data'], true);
			foreach (array('E153','E225') as $sign) {
				if (isset($form_data[$sign]) && !empty($form_data[$sign])) {
					if (file_exists($form_data[$sign])) unlink($form_data[$sign]);
					unset($form_data[$sign]);
				}
			}
			$form['data'] = json_encode($form_data);
			$this->fileModel->save($form);
		}
		
		$is_admin = $this->session->userdata('is_admin');
    if (isset($conditions['file.lodge_draft']) && $conditions['file.lodge_draft'] == 'LODGE FORM' && $is_admin != 3) $conditions['file.complete'] = 0;
		if ($loadView) {
			  $limit = isset($post['removelimit']) ? null : 100;
				$param['items'] = $this->fileModel->get_list($conditions, $condition_like, $sort, $limit);
				$this->loadView($param, $is_admin == 3 ? 'greenEnergyTradingFileListView':'fileListView');
			}
    }

    function create() {
        if($this->input->post()){
            $post = $this->input->post();
						$post['H4'] = $this->fileModel->getClientNumber();
            foreach ($_FILES as $key => $value){
                $post[$key] = 'photos/'.time().'@file@'.$key.'.jpg';
                move_uploaded_file($_FILES[$key]["tmp_name"], $post[$key]);
            }

            if($post['E153'] || $post['E225']) $this->load->library('signature');
            if($post['E153']){
                $E153 = $this->signature->createImage($post['E153']);
                $post['E153'] = 'photos/sign1'.time().'.png';
                imagepng($E153, $post['E153']);
            }
            if($post['E225']){
                $E225 = $this->signature->createImage($post['E225']);
                $post['E225'] = 'photos/sign2'.time().'.png';
                imagepng($E225, $post['E225']);
            }

            $out_of_stock = $this->generate_stock_usage($post);
            $fid = $this->fileModel->save(array(
                'uid' => $this->session->userdata('uid'),
                'name' => $post['file_name'],
                'clientNumber' => $post['H4'],
                'data' => json_encode($post),
                'out_of_stock' => $out_of_stock
            ));
						redirect(site_url('maincontroller/edit/'.$fid.'/upload'));
        }

		$param = array('clientNumber'=>$this->fileModel->getClientNumber());
		$this->load->model('settingsModel');
		$param['settings_app'] = $this->settingsModel->retrieve($this->session->userdata('cid'));

        $this->loadView($param);
    }

    function subform($page='create'){
    	// $this->load->model('settingsModel');
    	// $param['settings_app'] = $this->settingsModel->retrieve($this->session->userdata('cid'));
    	switch ($page) {
        case 'create':
          $param['link21'] = site_url('maincontroller/create');
          $param['link34'] = site_url('form34controller/subform');
          break;
        case 'list':
          $param['link21'] = site_url('maincontroller');
          $param['link34'] = site_url('form34controller');
          break;
        case 'product':
          $param['link21'] = site_url('product21controller');
          $param['link34'] = site_url('product34controller');
          break;
    	}
    	$this->loadView($param,'subform');
    }

    function edit($fid) {
        if($this->input->post()){
            $post = $this->input->post();
			$record = $this->fileModel->retrieve($fid);
            $olddata = json_decode($record['data'], true);

			// AVOID RE-SIGN
			if(isset($olddata['E153'])) $post['E153'] = $olddata['E153'];
			if(isset($olddata['E225'])) $post['E225'] = $olddata['E225'];
			if(isset($olddata['M51'])) $post['M51'] = $olddata['M51'];
			if(isset($olddata['M55'])) $post['M55'] = $olddata['M55'];
			if(isset($olddata['mapsrc'])) $post['mapsrc'] = $olddata['mapsrc'];// AVOID RE-LOCATE
			$post['file_name'] = $olddata['file_name'];// AVOID RENAME

      if($_FILES){
      	foreach ($_FILES as $key => $value){
        	if ($key == 'signature1' || $key == 'signature2') {
          	foreach (array('signature1', 'signature2') as $upsign) {
            	if (empty($_FILES[$upsign]["name"])) continue;
            		$location = "photos/".$_FILES[$upsign]["name"];
	            	move_uploaded_file($_FILES[$upsign]["tmp_name"], $location);
								$cellNumber = $upsign == 'signature1' ? 'E153' : 'E225';
	            	$post[$cellNumber] = $location;
								if (isset($olddata[$cellNumber]) && file_exists($olddata[$cellNumber])) unlink($olddata[$cellNumber]);
            	}
						} else {
          		if(strlen($value['tmp_name'])<1) $post[$key] = $olddata[$key];
              else{
              	if(isset($olddata[$key]) && file_exists($olddata[$key])) unlink($olddata[$key]);
                $post[$key] = 'photos/'.time().'@file@'.$key.'.jpg';
                move_uploaded_file($_FILES[$key]["tmp_name"], $post[$key]);
          		}							
						}
      	}
			}

			if(isset($post['delete-photos']))
				foreach($post['delete-photos'] as $field => $file){
					if(empty($file)) continue;
					if(file_exists($file)){
						unlink($file);
						$post[$field] = '';
					}
				}

			$saveMe = array(
                'fid' => $fid,
                'data' => json_encode($post),
                'out_of_stock' => $this->generate_stock_usage($post)
            );

			if (isset ($post['lodge_form'])) {
				$saveMe['lodge_draft'] = 'LODGE FORM';
				$this->smartsheet($post, 'Lodge Form');
			}
			if (isset ($post['save_draft'])) {
				$saveMe['lodge_draft'] = 'SAVE DRAFT';
				$this->smartsheet($post, 'Draft Form');
			}
            $fid = $this->fileModel->save($saveMe);
						$this->smartsheet_update($post);
            // redirect(site_url('maincontroller/edit/'.$fid.'/upload'));
            redirect(base_url());
        }
        $file = $this->fileModel->retrieve($fid);
        $data['stored'] = json_decode($file['data'], true);

        $this->load->model('userModel');
		$author = $this->userModel->retrieve($file['uid']);
		$this->load->model('settingsModel');
		$data['settings_app'] = $this->settingsModel->retrieve($author['cid']);
		$data['logo'] = $data['settings_app']['logo'];
		/*
        $this->load->model('cdproductmodel');
		$data['c_products'] = $this->cdproductmodel->get_list(array('type' => 'C'));
		$data['d_products'] = $this->cdproductmodel->get_list(array('type' => 'D'));
		*/
        $this->loadView($data);
    }

    function delete($fid, $confirm = null){
        if(is_null($confirm)){

			// retur stock
			$post = $this->fileModel->retrieve($fid);
			foreach(json_decode($post['data'], true) as $key => $value){
				$post[$key] = $value;
			}
			if(!isset($post['iid'])){
				$installer_email = $post['Z127'];
				if(strlen($installer_email)<1) continue;
				$query = "SELECT * FROM installer WHERE email_address = '$installer_email'";
				$installers = $this->db->query($query)->result();
				if(count($installers) < 1) continue;
				else if(count($installers) > 1){
					$user = $this->db->get_where('user', array('uid' => $post['uid']))->row_array();
					$query .= " AND cid = ".$user['cid'];
					$installers = $this->db->query($query)->result();
				}
				$post['iid'] = $installers[0]->iid;
			}
			for($sep = 74; $sep <= 80; $sep += 2){
				$post["AI$sep"] = 0;
			}
			$this->generate_stock_usage($post);

            $this->fileModel->delete($fid);
            redirect(base_url());
        }else $this->loadView (null, 'confirmationView');
    }

    function pdf($fid) {
        $db_data = $this->fileModel->retrieve($fid);
        $file_data = json_decode($db_data['data'], true);

		$this->load->model('userModel');
		$author = $this->userModel->retrieve($db_data['uid']);
		$this->load->model('settingsModel');
		$settings = $this->settingsModel->retrieve($author['cid']);
		$file_data['logo'] = isset($settings['logo'])?$settings['logo']:'';
		$file_data['logo'] = empty($file_data['logo'])?'logo_2.png':$file_data['logo'];
		$file_data['logo'] = 'asset/css/images/'.$file_data['logo'];
		// echo'<pre>';print_r($file_data);die();

        $this->load->view('assignment', $file_data);
        $this->load->library('dompdf_gen');
        $this->dompdf->load_html($this->output->get_output());
        $this->dompdf->render();
        $clientNumber = $db_data['clientNumber'];

        $this->dompdf->stream("Assignment form $clientNumber.pdf");
    }

    function user_list(){
        $this->load->model('userModel');
		$conditions = array();
		switch ($this->session->userdata('is_admin')) {
			case '0':
				$conditions['uid'] = $this->session->userdata('uid');
				break;
			case '1':
				$conditions['is_admin !='] = '1';
				break;
			case '2':
				$conditions['user.cid'] = $this->session->userdata('cid');
				$conditions['uid !='] = $this->session->userdata('uid');
				break;
		}

		if($this->input->post()){
			$post = $this->input->post();
			$this->load->model('userModel');
			$user = $this->userModel->retrieve($post['uid']);
			$user['is_admin'] = $post['is_admin'];
			$this->userModel->save($user);
		}
        $this->loadView(array(
            'items' => $this->userModel->get_list($conditions)
        ),'userListView');
    }

    function userDelete($uid, $confirm = null){
        if(is_null($confirm)){
            $this->load->model('userModel');
            $this->userModel->delete($uid);
            redirect(site_url('maincontroller/user_list'));
        }else $this->loadView (null, 'confirmationView');
    }

	function csv(){
		$ids = $this->input->get('ids');
		$ids = explode('|', $ids);
		$records = !empty($ids[0]) ?
			$this->fileModel->get_where_in($ids):
			$this->fileModel->get_list(array());
		$output = fopen("php://output",'w') or die("Can't open php://output");
		header("Content-Type:application/csv");
		header("Content-Disposition:attachment;filename=getveet21c.csv");
		fputcsv($output, array(
			"Activity",
			"Estimated VEECs",
			"Customer reference",
			"Installation Date",
			"Business/Company Name",
			"ABN / ACN",
			"Industry/Business Type",
			"Number of Levels",
			"Floor Space (m2)",
			// "Unit Type",
			"Unit Number",
			"Level Type",
			"Level Number",
			"Street Number",
			"Street Name",
			"Street Type",
			"Street Type Suffix",
			"Town / Suburb",
			"State",
			"Postcode",
			"Consumer First Name - OR - Authorised Signatory First Name",
			"Consumer Last Name - OR - Authorised Signatory Last Name",
			"Consumer phone number - OR - Authorised Signaotry Phone Number",
			"Consumer - OR - Authorised Signaotry Email ",
			"Product Brand 1",
			"Product Model 1",
			"Quantity 1",
			"Product Brand 2",
			"Product Model 2",
			"Quantity 2",
			"Certificate of Electrical Compliance Number (or just NA)",
			"Installer First Name",
			"Electrician Licence number",
			"Previous VEEC lighting installation? - if Yes give details"
		));
		foreach($records as $record){
			$data = json_decode($record->data);
			$cells = array($data->R74,$data->AS94,$data->H4,$data->C68,$data->Y22,$data->Y25,$data->Y31,$data->AE39,$data->AM39,/*$data->C36,*/$data->I36,'',$data->AE39,$data->P36,$data->W36,$data->AO36,'',$data->C39,'VIC',$data->Z39,$data->C22,$data->C23,'="'.$data->C28.'"',$data->C31,$data->ZA23,$data->L74,$data->AI74,$data->ZA24,$data->L76,$data->AI76,$data->AM117,$data->C127,$data->K130,$data->C42.' '.$data->L43);
			if($data->R74 != $data->R76){
				$cells[0] = $data->R74;
				$cells[1] = $data->R74 == '21C' ? $data->AS94 : $data->AS96;
				$cells[25] = $data->AI74;
				$cells[28] = 0;
				fputcsv($output, $cells);// WRITE ROW WITH AI74 ONLY, THEN
				
				$cells[0] = $data->R76;
				$cells[1] = $data->R76 == '21C' ? $data->AS94 : $data->AS96;
				$cells[25] = 0;
				$cells[28] = $data->AI76;
				fputcsv($output, $cells);// WRITE NEXT ROW WITH AI76 ONLY
			}else fputcsv($output, $cells);
		}
		fclose($output) or die("Can't close php://output");
	}

	function contractor_weekly_report(){
		if($this->input->post()){
			$post = $this->input->post();
			$conditions = array(
				'complete' => 1
			);
			$company = 'All Companies';
			$address = '';
			if(isset($post['since']) && $post['since'] !=''){
				$param['since'] = $post['since'];
				$conditions['file.fid >='] = strtotime($post['since']);
			}if(isset($post['until']) && $post['until'] !=''){
				$param['until'] = $post['until'];
				$conditions['file.fid <='] = strtotime($post['until']);
			}if(isset($post['cid']) && $post['cid'] > 0){
				$conditions['company.cid'] = $post['cid'];
				$this->load->model('settingsModel');
				$settings =  $this->settingsModel->retrieve($post['cid']);
				$company = $settings['electricians_company_name'];
				$address = $settings['company_full_address'];
			}
			$records = $this->fileModel->get_list($conditions);
		}

		$output = fopen("php://output",'w') or die("Can't open php://output");
		header("Content-Type:application/csv");
		header("Content-Disposition:attachment;filename=weekly_report.csv");
		fputcsv($output, array('My Electrician VEEC 21 Report'));
		fputcsv($output, array('Company : ', $company));
		fputcsv($output, array('Address : ', $address));
		fputcsv($output, array('Date Range : ', $post['since'].' - '.$post['until']));
		fputcsv($output, array(
			"Job No",
			"Job Date",
			"Company Name",
			"Customer Name",
			"Installation address",
			"21C Lamps Installed",
			"21C VEECS",
			"21D Lamps Installed",
			"21D VEECS",
			"21E Lamps Installed",
			"21E VEECS",
			"VEECs Total",
			"VEEC Price",
			"Amount Due (inc gst)",
			"Completion Status",
			"Completed Date"
		));
		foreach($records as $record){
			$data = json_decode($record->data);
			$qty_21C = 0;
			$qty_21D = 0;
			$qty_21E = 0;
			foreach (array(74,76,78,80) as $activity) {
				$qty = 'AI'.$activity;
				$activity = 'R'.$activity;
				if ($data->$activity == '21C') {
					$qty_21C += intval($data->$qty);
				} else if ($data->$activity == '21D') {
					$qty_21D += intval($data->$qty);
				} else if ($data->$activity == '21E') {
					$qty_21E += intval($data->$qty);
				}
			}
			if  (isset($data->AS98) && ($data->AS98 != 0)) {
					$AS98 = $data->AS98;
				}else{
					$AS98 = 0;
				} 
			$cells = array(
				$data->H4,
				$data->C68,
				$data->C124,
				$data->C228,
				$data->I36.' , '.$data->P36.' , '.$data->W36.'  '.$data->AO37.' , '.$data->C39.'  '.$data->Z40,
				$qty_21C,
				$data->AS94,
				$qty_21D,
				$data->AS96,
				$qty_21D,
				$AS98,
				$data->AS94 + $data->AS96 + $AS98,
				$data->M51,
				'$'.floatval(str_replace('$', '', $data->M51)) * ($data->AS94 + $data->AS96 + $AS98),
				$record->complete == 1 ? 'COMPLETED' : 'INCOMPLETE',
				$record->complete == 1 && $record->completed_date > 0 ? date('d.m.Y', $record->completed_date) : '',
			);
			fputcsv($output, $cells);
		}
		fclose($output) or die("Can't close php://output");
	}

    function userActive($uid, $confirm = null){
        if(is_null($confirm)){
			$this->load->model('userModel');
			$user = $this->userModel->retrieve($uid);
			$user['active'] = $user['active'] == 0 ? 1 : 0 ;
			$this->userModel->save($user);
			redirect(site_url('maincontroller/user_list'));
        }else $this->loadView (null, 'confirmationView');
    }

	function generateStockFromOldTransaction(){
		$query = "SELECT * FROM file WHERE clientNumber NOT IN (SELECT job_refference FROM stock_usage)";
		$limit = 0;
		foreach($this->db->query($query)->result() as $post){
			$limit++;
			// if($limit > 1) continue;
			$post = (array) $post;
			foreach(json_decode($post['data'], true) as $key => $value){
				$post[$key] = $value;
			}
			$installer_email = $post['Z127'];
			if(strlen($installer_email)<1) continue;
			$query = "SELECT * FROM installer WHERE email_address = '$installer_email'";
			$installers = $this->db->query($query)->result();
			if(count($installers) < 1) continue;
			else if(count($installers) > 1){
				$user = $this->db->get_where('user', array('uid' => $post['uid']))->row_array();
				$query .= " AND cid = ".$user['cid'];
				$installers = $this->db->query($query)->result();
				if(count($installers) > 1){
					$name = explode(' ', $post['C127']);
					$first_name = $name[0];
					$query .= " AND first_name = '$first_name'";
					$installers = $this->db->query($query)->result();
				}
			}
			$post['iid'] = $installers[0]->iid;
			$out_of_stock = $this->generate_stock_usage($post);
			$this->db->where('fid',$post['fid'])->set('out_of_stock', $out_of_stock)->update('file');
		}
	}

	private function generate_stock_usage($post){
		$out_of_stock = 0;
		$this->load->model(array(
			'stockusagemodel',
			'stockmodel',
			'cdproductmodel',
			'installermodel',
		));
		if(strlen($post['iid']) > 0){

			// if using company stock instead of installer stock
			$iids = array();
			foreach($this->installermodel->get_admin_installers() as $ins) $iids[] = $ins->iid;
			$installer = $this->installermodel->retrieve($post['iid']);
			$post['iid'] = in_array($installer['iid'], $iids) ? $installer['iid'] : $installer['cid'];

			$products = array();
			foreach($this->cdproductmodel->get_list() as $product)
				$products[$product->id] = $product->item_id;

			for($sep = 74; $sep <= 80; $sep += 2){
				$product_name = '';
			  	if($post["C$sep"]=='12V Halogen lamp only'){
			  		$product_name = $post["T$sep"];
			  		if ($post["AM$sep"]=='1') {
			  			$product_name = str_replace(' (Electronic driver)', '', $product_name);
			  		}else if ($post["AM$sep"]=='2') {
			  			$product_name = str_replace(' (Magnetic driver)', '', $product_name);
			  		}
			  	}else if($post["C$sep"]=='12V Halogen lamp & transfomer'){
			  		$pn = $sep-1;
			  		$product_name = $post["T$pn"];
			  	}else if($post["C$sep"]=='230V Halogen GU10 lamp only'){
			  		$e21 = $sep-1;  
			  		$product21e = $post["TT$e21"];
			  		$product_name = 'Primsal '.$product21e;
			  	}
			  	if(strlen($product_name)<1) continue;
				if(!isset($products[$product_name])) continue;
				$item_id = $products[$product_name];

				$stocks = $this->stockmodel->get_list(array(
					'installer_id' => $post['iid'],
					'item_id' => $item_id
				));
				if(count($stocks)>0){
					$stock_id = $stocks[0]->stock_id;
			        $usage = $this->stockusagemodel->get_list(array(
			        	'stock_id' => $stock_id,
			            'job_refference' => $post['H4']
			        ));
					if(count($usage)>0){
						// RETUR
						if($post["AI$sep"] > $usage[0]->stock + $stocks[0]->stock){
							$out_of_stock = 1;
						}
					}else if($stocks[0]->stock < $post["AI$sep"]) $out_of_stock = 1;
				}else{
					$stock_id = $this->stockmodel->save(array(
						'installer_id' => $post['iid'],
						'item_id' => $item_id,
					));
					$out_of_stock = 1;
				}
				$this->stockusagemodel->save(array(
					'stock_id' => $stock_id,
					'job_refference' => $post['H4'],
					'stock' => $post["AI$sep"],
				));
			}
		}
		return $out_of_stock;
	}

	/*
	*	RESOURCES PAGE
	*/

	function training(){

    	$this->loadView(null,'training');
    }

    function develompentPackDocument(){

    	$this->loadView(null,'development');
    }

    function instruction(){

    	$this->loadView(null,'instructions');
    }

    function store(){

    	$this->loadView(null,'store');
    }

		function marked ($fid) {
			$form = $this->fileModel->retrieve($fid);
			$form['marked'] = $form['marked'] == 0 ? 1 : 0 ;
			$this->fileModel->save($form);
		}

	function documents ($did = null) {
		$doc_dir = FCPATH.'documents/';
		if (null !== $did) {
			$document = $this->db->get_where('document',array('did'=>$did))->row_array();
			if (file_exists($doc_dir.$document['file'])) {
				$this->load->helper('download');
				force_download($document['file'], file_get_contents($doc_dir.$document['file']));
			}else{
				die("Sorry, we could not locate file ".$document['file']);
			}
		}
		
		if (isset ($_FILES) && !empty($_FILES)) {
			move_uploaded_file($_FILES['upload']['tmp_name'], $doc_dir.$_FILES['upload']['name']);
			$this->db->insert('document', array(
				'file' => $_FILES['upload']['name'],
				'description' => $this->input->post('description')
			));
		}else if ($this->input->post()) {
			$post = $this->input->post();
			$document = $this->db->get_where('document',array('did'=>$post['did']))->row_array();
			unlink($doc_dir.$document['file']);
			$this->db->delete('document', array('did' => $document['did']));
		}
		$param['documents'] = $this->db->get('document')->result();
		$this->loadView($param,'documentsView');
	}
	
	private function smartsheet ($post, $sheet) {
		// PERMISSION
		if (
			false !== strpos(current_url(), 'localhost')	|| 
			false !== strpos(current_url(), 'staging') 		||
			$this->session->userdata('is_admin') == 3)
		return true;
		
		// CONFIGURATIONS
		$API_ACCESS_TOKEN = '3tyflplbuf46lupspip245g4br';
		$sheets_ids = array(
			'Draft Form' => '5978642024032132',
			'Lodge Form' => '424459036321668',
			'Completed Form' => '7461780130686852',
			'weekly_report' => '4367204654311300'
		);
		$sheet_id = $sheets_ids[$sheet];
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$header = array();
		$header[] = 'Content-length: 0';
		$header[] = 'Content-type: application/json';
		$header[] = "Authorization: Bearer $API_ACCESS_TOKEN";
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

		/* curl https://api.smartsheet.com/2.0/sheets/5978642024032132/rows
		 * -H "Authorization: Bearer 3tyflplbuf46lupspip245g4br"
		 * -H "Content-Type: application/json"
		 * -X POST
		 * -d '[{"toTop":true, "cells":[{"columnId": 3928465121535876, "value": "henrisusanto"}]}]'
		 */
		$smartrows = array();
		$smartrow = new stdClass();
		$smartcells = array();
		$smartrow->toTop = true;

		// collect column ids
    curl_setopt($curl, CURLOPT_URL, "https://api.smartsheet.com/2.0/sheets/$sheet_id/columns/");
    $columns = curl_exec($curl);
		$columns = json_decode($columns);
		$column_ids = array();
		foreach ($columns->data as $column) {
			$column_ids[] = $column->id;
		}

		$data = $post;
		$data = is_array($data) ? (object) $data : $data;
		$data = is_string($data)? json_decode($data, false) : $data;

		switch ($sheet) {
			case 'Completed Form':
			case 'Lodge Form':
			case 'Draft Form':
				$this->smartsheet_deleterow($data, 'Lodge Form');
				$this->smartsheet_deleterow($data, 'Draft Form');
				$cells = array($data->R74,$data->AS94,$data->H4,$data->C68,$data->Y22,$data->Y25,$data->Y31,$data->AE39,$data->AM39,$data->I36,'',$data->AE39,$data->P36,$data->W36,$data->AO36,'',$data->C39,'VIC',$data->Z39,$data->C22,$data->C23,'="'.$data->C28.'"',$data->C31,$data->ZA23,$data->L74,$data->AI74,$data->ZA24,$data->L76,$data->AI76,$data->AM117,$data->C127,$data->K130,$data->C42.' '.$data->L43);
				$index = 0;
				foreach ($cells as $cell) {
					$smartcell = new stdClass();
					$smartcell->columnId = $column_ids[$index];
					if ($index === 3) {
						$date = new DateTime();
						$date->createFromFormat('d/m/Y', $cell);
						$cell = $date->format('Y-m-d');
					}
					$smartcell->value = $cell;
					$smartcells[] = $smartcell;
					$index++;
				}

				if ($data->R74 != $data->R76) {
					$smartcells[0]->value = $data->R74;
					$smartcells[1]->value = $data->R74 == '21C' ? $data->AS94 : $data->AS96;
					$smartcells[25]->value = $data->AI74;
					$smartcells[28]->value = 0;
					$smartrow->cells = $smartcells;
					$smartrows[] = $smartrow;

					$shadowrow = clone $smartrow;
					$shadowcells = $smartcells;
					foreach (array(0,1,25,28) as $clone) $shadowcells[$clone] = clone $smartcells[$clone];

					$shadowcells[0]->value = $data->R76;
					$shadowcells[1]->value = $data->R76 == '21C' ? $data->AS94 : $data->AS96;
					$shadowcells[25]->value = 0;
					$shadowcells[28]->value = $data->AI76;
					$shadowrow->cells = $shadowcells;
					$smartrows[] = $shadowrow;
				} else {
					$smartrow->cells = $smartcells;
					$smartrows[] = $smartrow;
				}
				break;
			case 'weekly_report' :
					$this->load->model('settingsModel');
					$companies = $this->settingsModel->get_super_admin_companies();
					$company = $companies[0];
					$super_admin_price = $company->c_certificate;
					
					$qty_21C = 0;
					$qty_21D = 0;
					$qty_21E = 0;
					foreach (array(74,76,78,80) as $activity) {
						$qty = 'AI'.$activity;
						$activity = 'R'.$activity;
						if ($data->$activity == '21C') {
							$qty_21C += intval($data->$qty);
						} else if ($data->$activity == '21D') {
							$qty_21D += intval($data->$qty);
						} else if ($data->$activity == '21E') {
							$qty_21E += intval($data->$qty);
						}
					}

					if  (isset($data->AS98) && ($data->AS98 != 0)) {
							$AS98 = $data->AS98;
						}else{
							$AS98 = 0;
					} 

					$cells = array(
						$data->H4,
						$data->C68,
						$data->C124,
						$data->C228,
						$data->I36.' , '.$data->P36.' , '.$data->W36.'  '.$data->AO37.' , '.$data->C39.'  '.$data->Z40,
						$qty_21C,
						$data->AS94,
						$qty_21D,
						$data->AS96,
						$qty_21E,
						$AS98,
						$data->AS94 + $data->AS96 + $AS98,
						$data->M51,
						'$'.floatval(str_replace('$', '', $data->M51)) * ($data->AS94 + $data->AS96 + $AS98),
						'COMPLETED',
						date('d.m.Y', time()),
						$super_admin_price
					);
					$index = 0;
					foreach ($cells as $cell) {
						$smartcell = new stdClass();
						$smartcell->columnId = $column_ids[$index];
						$smartcell->value = $cell;
						$smartcells[] = $smartcell;
						$index++;
					}
					$smartrow->cells = $smartcells;
					$smartrows[] = $smartrow;
			break;
		}
		$smartrows = json_encode($smartrows);

		$header[0] = "Content-Length: ". strlen($smartrows);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_URL, "https://api.smartsheet.com/2.0/sheets/$sheet_id/rows");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $smartrows);
		$result = curl_exec($curl);
    curl_close($curl);

		/*echo'<pre>';
		print_r($smartrows);
		die();*/

		if ($sheet == 'Completed Form') $this->smartsheet($post, 'weekly_report');
		return true;
	}

	private function smartsheet_deleterow($post, $sheet) {
		// PERMISSION
		if (
			false !== strpos(current_url(), 'localhost')	|| 
			false !== strpos(current_url(), 'staging') 		||
			$this->session->userdata('is_admin') == 3)
		return true;
		
		// CONFIGURATIONS
		$API_ACCESS_TOKEN = '3tyflplbuf46lupspip245g4br';
		$sheets_ids = array(
			'Draft Form' => '5978642024032132',
			'Lodge Form' => '424459036321668',
			'Completed Form' => '7461780130686852',
			'weekly_report' => '4367204654311300'
		);
		$sheet_id = $sheets_ids[$sheet];

		$post = is_object($post) ? (array) $post : $post;
		$post = is_string($post)? json_decode($post, true) : $post;

		$job_reference = $post['H4'];
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$header = array();
		$header[] = 'Content-length: 0';
		$header[] = 'Content-type: application/json';
		$header[] = "Authorization: Bearer $API_ACCESS_TOKEN";
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		
		// FIND RECORD
    curl_setopt($curl, CURLOPT_URL, "https://api.smartsheet.com/2.0/search?query=$job_reference");
    $search = curl_exec($curl);
		$search = json_decode($search, false);
		if (!isset($search->totalCount) || $search->totalCount == 0) return true;

		// FILTER SEARCH RESULT
		$row_id = '';
		foreach ($search->results as $sr) {
			if ($sr->parentObjectType == 'sheet' && $sr->parentObjectId == $sheet_id && $sr->objectType == 'row') {
				$row_id .= $row_id == '' ? $sr->objectId : ",$sr->objectId";
			}
		}

		// REMOVE ROW
		//curl https://api.smartsheet.com/2.0/sheets/{sheetId}/rows?ids={rowId1},{rowId2},{rowId3}&ignoreRowsNotFound=true -X DELETE
		curl_setopt($curl, CURLOPT_URL, "https://api.smartsheet.com/2.0/sheets/$sheet_id/rows?ids=$row_id");
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		$delete = curl_exec($curl);
		//echo'<pre>';print_r($delete);die("https://api.smartsheet.com/2.0/sheets/$sheet_id/rows?ids=$row_id");
		curl_close($curl);

		if ($sheet == 'Completed Form') $this->smartsheet_deleterow($post, 'weekly_report');
		return true;
	}

	private function smartsheet_update($post) {
		$API_ACCESS_TOKEN = '3tyflplbuf46lupspip245g4br';
		$sheets_ids = array(
			'5978642024032132' => 'Draft Form',
			'424459036321668' => 'Lodge Form',
			'7461780130686852' => 'Completed Form'
		);
		$sheet_id = '';
		$row_id = '';

		$post = is_object($post) ? (array) $post : $post;
		$post = is_string($post)? json_decode($post, true) : $post;

		$job_reference = $post['H4'];
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$header = array();
		$header[] = 'Content-length: 0';
		$header[] = 'Content-type: application/json';
		$header[] = "Authorization: Bearer $API_ACCESS_TOKEN";
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

		// FIND RECORD
    curl_setopt($curl, CURLOPT_URL, "https://api.smartsheet.com/2.0/search?query=$job_reference");
    $search = curl_exec($curl);
		$search = json_decode($search, false);
    if (!isset($search->totalCount) || $search->totalCount == 0) return true;

		// FILTER SEARCH RESULT
		foreach ($search->results as $sr) {
			if ($sr->parentObjectType == 'sheet' && in_array($sr->parentObjectId, array_keys($sheets_ids)) && $sr->objectType == 'row') {
				$sheet_id = $sr->parentObjectId;
			}
		}

		if ($sheet_id != '') {
			$sheet = $sheets_ids[$sheet_id];
			$this->smartsheet_deleterow($post, $sheet);
			$this->smartsheet($post, $sheet);
		}
		return true;
	}

	function mark_user($field, $uid) {
		$this->load->model('userModel');
		$user = $this->userModel->retrieve($uid);
		$user[$field] = $user[$field] == 1 ? 0 : 1;
		$this->userModel->save($user);
		redirect(site_url('maincontroller/user_list')); 
	}
	
	function redflag($fid) {
		$form = $this->fileModel->retrieve($fid);
		$form['redflag'] = $form['redflag'] == 1 ? 0 : 1;
		$this->fileModel->save($form);
	}
	
	function processed($fid) {
		$form = $this->fileModel->retrieve($fid);
		$form['processed'] = $form['processed'] == 1 ? 0 : 1;
		$form['processed_date'] = time();
		$this->fileModel->save($form);
		/*
		if ($this->session->userdata('is_admin')==1)
			$this->fileModel->getveet_send_mail('forms@greenenergytrading.com.au', 'incompleted form has been processed please confirm', 'incompleted form has been processed');*/
	}


	function job_leads () {
    $post = $this->input->post();
    if (isset($post['save_leads'])) {
	    $this->jobleadsmodel->save(array(
	        'date_leads' => $post['date_leads'],
	        'suburb_leads' => $post['suburb_leads'],
	        'qty_leads' => $post['qty_leads'],
	    ));
	    $date = $post['date_leads'];
	    $suburb = $post['suburb_leads'];
	    $qty = $post['qty_leads'];
	    $htmlMsg = ' New Job has been listed on the lead register <br /> Please find the details below : <br /> <br /> <br /> '.$date.', '.$suburb.', '.$qty;
	    $this->load->model('settingsModel');
	    foreach ($this->settingsModel->contractors_email() as $index => $list) {
	    	$email_address = $list->email_address;
	    	$this->jobleadsmodel->getveet_send_mail($email_address, $htmlMsg, 'New Job Has Been Listed', null);
	    }
        //$status = ($success)?'sending email success' : 'failed to send email';
        //echo "<html><head><script>alert('$status');history.back()</script></head></html>";
    }

		if (isset($post['req_leads'])) {
			$this->load->model('settingsModel');
			$leads = $this->jobleadsmodel->retrieve($post['lid']);
			//$form['admin3_status'] = 'incomplete';
			//$form['admin3_msg'] = $post['admin3_msg'];
			//$form['request_date'] = time();
			//$form['info_submitted'] = 0;
			//$this->jobleadsmodel->save($leads);
			$cid = $this->session->userdata('cid');
			$company = $this->settingsModel->retrieve($cid);
			$contractorName = $company['electricians_company_name'];
			$htmlMsg = $contractorName.' has requested <br /> <br />'.$leads['lid'].', '.$leads['date_leads'].', '.$leads['suburb_leads'].', '.$leads['qty_leads'];
			$param['status'] = $this->jobleadsmodel->getveet_send_mail('reception@myelec.net.au', $htmlMsg, 'Job Lead Requested', null);
  		$param['status'] = true === $param['status'] ? 'Thank you, Your request has been received and will be emailed to you shortly' : $param['status'];
  		$this->jobleadsmodel->bindLeadCompany($post['lid'], $cid);
		}

    $param['items'] = $this->jobleadsmodel->joinLeadCompany(array());
    $this->loadView($param, 'job_leads');
	}

	function leadsDelete($lid, $confirm = null){
        if(is_null($confirm)){
            $this->load->model('jobleadsmodel');
            $this->jobleadsmodel->delete($lid);
            redirect(site_url('maincontroller/job_leads'));
        }else $this->loadView (null, 'confirmationView');
    }
}
