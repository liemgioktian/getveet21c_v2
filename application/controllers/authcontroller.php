<?php

class authcontroller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('userModel');
        $this->load->model('settingsModel');
    }

    function login() {
        $user = $this->userModel->get_list(array(
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password'))
        ));
        if (count($user) === 1 && $user[0]->cid != 0 && $user[0]->active==1) { 
        	$this->session->set_userdata($user[0]);
					if ($user[0]->is_admin == 3) redirect(site_url('maincontroller'));
				} else $this->session->set_flashdata('error','Login Incorrect');
				redirect(base_url());
    }

    function register($next_step=0) {
    	if($this->input->post()) {
    	  $post = $this->input->post();
    	  switch ($next_step) {
            case 2;
                break;
    	  	case 3:
			    $post['item'] = array();
    	  	    $cid = $this->settingsModel->save($post);
			    $this->session->set_userdata('cid', $cid);

                ob_start();
                $this->load->library('dompdf_gen');
                $html = $this->load->view('comp_agreement_pdf', $post, true);
                $this->dompdf->load_html($html);
                
                $this->dompdf->render();
					$pdf_path = FCPATH.'temp/'.$post['electricians_company_name'].'.pdf';
					file_put_contents($pdf_path, $this->dompdf->output());
					$this->settingsModel->getveet_send_mail('daniel@myelec.net.au', '', 'Company Agreement', $pdf_path);
					unlink($pdf_path);
				$this->smartsheet($post, 'contractor list');
				
    	  	  break;
			case 4:
              $this->userModel->save(array(
                'is_admin' => 2,
                'active' => 0,
                'cid' => $this->session->userdata('cid'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password'))
              ));

			  $setting = $this->settingsModel->retrieve($this->session->userdata('cid'));
			  $email = "New Company Registration";
			  $email.= "<br/>";
			  $email.= "Electricians Company Name : " . $setting['electricians_company_name'] ;
			  $email.= "<br/>";
			  $email.= "ABN Number : " . $setting['abn_number'] ;
			  $email.= "<br/>";
			  $email.= "Company Address : " . $setting['company_full_address'] ;
			  $email.= "<br/>";
			  $email.= "Telephone Number : " . $setting['telephone_number'] ;
			  $email.= "<br/>";
			  $email.= "Web Site : " . $setting['website'] ;
			  $email.= "<br/>";
			  $email.= "Email Address : " . $setting['email_address'] ;
			  $email.= "<br/>";
			  $email.= "R.E.C Number : " . $setting['rec_number'] ;
			  $email.= "<br/>";

			  $email.= "Administrator Username : ". $this->input->post('email');
			  $this->settingsModel->getveet_send_mail('daniel@myelec.net.au', $email, "New Company Registration");

			  break;
		  }
    	}
        $this->load->view('signupView');
    }
    function company_agreement(){
        $this->load->view('company_agreement');
    }
    function installer_agreement(){
        $this->load->view('installer_agreement');
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }
    
    function forgot_password() {
        if ($this->input->post('email')) {
            $user = $this->userModel->get_list(array(
                'email' => $this->input->post('email'),
            ));
            if (count($user) == 1){
                $this->userModel->send_new_password($user[0]);
                redirect(base_url());
            }
        }
        $this->load->view('forgotPasswordView');
    }
    
	function reset_password($uid, $encpwd) {
			$users = $this->userModel->get_list(array(
				'uid' => $uid,
				'password' => $encpwd
			));
			if (count($users) < 1) {
				$this->session->set_flashdata('error', 'Incorrect reset password URL');
				redirect(base_url());
			} else {
				if ($this->input->post('password')) {
					$user = $this->userModel->retrieve($users[0]->uid);
					$user['password'] = md5($this->input->post('password'));
					$this->userModel->save($user);
					$this->session->set_flashdata('error', 'Please login with your new password');
					redirect(base_url());
				}
				$this->load->view('resetPasswordView');
			}
	}
		
	function installer_signup ($stripped_company_name, $step = 1) {
			$cids = explode('-', $stripped_company_name);
			$company = $this->settingsModel->retrieve($cids[0]);
			if (count($company) < 1) {
				echo '<p class="bg-danger">WARNING : INVALID COMPANY</p>';
			} else {
				if ($this->input->post()) {
					$post = $this->input->post();
					//$installer['iid'] = $post[''];
					//$installer['uid'] = $post[''];
					$installer['cid'] = $company['cid'];
					$installer['first_name' ] = $post['installer_first_name'];
					$installer['last_name' ] = $post['installer_last_name'];
					$installer['license_number' ] = $post['licence_number'];
					$installer['renewal_date' ] = $post['issue_date'];
					$installer['expiry_date' ] = $post['expaired_date'];
					$installer['email_address' ] = $post['email_address'];
					$installer['phone_number' ] = $post['phone_number'];
					
					$photo = $_FILES['installer_photo'];
					move_uploaded_file($photo['tmp_name'], FCPATH . 'settings_app_installer_photos/' . $photo['name']);
					$installer['photo' ] = $photo['name'];

			          $photo = $_FILES['installer_photo2'];
			          move_uploaded_file($photo['tmp_name'], FCPATH . 'settings_app_installer_photos/' . $photo['name']);
			          $installer['photo2' ] = $photo['name'];
			          								
					$this->load->model('installerModel');
					$this->installerModel->save($installer);

					//$user['uid'] = $post[''];
				  $user['cid'] = $company['cid'];
				  $user['email'] = $post['email_address'];
				  $user['password'] = md5($post['password']);
				  $user['is_admin'] = 0;
				  $user['active'] = 0;
					$this->load->model('userModel');
					$this->userModel->save($user);

				}				
			}
			$this->load->view('installerSignupView');
	}

	private function smartsheet ($post, $sheet) {
		// PERMISSION
		/*
		if (
			false !== strpos(current_url(), 'localhost')	|| 
			false !== strpos(current_url(), 'staging') 		||
			$this->session->userdata('is_admin') == 3)
		return true;
		*/
		
		// CONFIGURATIONS
		$API_ACCESS_TOKEN = '3tyflplbuf46lupspip245g4br';
		$sheets_ids = array(
			'contractor list' => '8087155286271876'
	
		);
		$sheet_id = $sheets_ids[$sheet];
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$header = array();
		$header[] = 'Content-length: 0';
		$header[] = 'Content-type: application/json';
		$header[] = "Authorization: Bearer $API_ACCESS_TOKEN";
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

		/* curl https://api.smartsheet.com/2.0/sheets/5978642024032132/rows
		 * -H "Authorization: Bearer 3tyflplbuf46lupspip245g4br"
		 * -H "Content-Type: application/json"
		 * -X POST
		 * -d '[{"toTop":true, "cells":[{"columnId": 3928465121535876, "value": "henrisusanto"}]}]'
		 */
		$smartrows = array();
		$smartrow = new stdClass();
		$smartcells = array();
		$smartrow->toTop = true;

		// collect column ids
    	curl_setopt($curl, CURLOPT_URL, "https://api.smartsheet.com/2.0/sheets/$sheet_id/columns/");
    	$columns = curl_exec($curl);
		$columns = json_decode($columns);
		$column_ids = array();
		foreach ($columns->data as $column) {
			$column_ids[] = $column->id;
		}

		$data = $post;
		$data = is_array($data) ? (object) $data : $data;
		$data = is_string($data)? json_decode($data, false) : $data;

		switch ($sheet) {
			case 'contractor list':
			$cells = array(
						$data->electricians_company_name,
						$data->author,
						$data->abn_number,
						$data->gst,
						$data->email_address,
						$data->telephone_number,
						$data->company_address,
						$data->suburb,
						$data->postal_code,
						$data->website,
						$data->rec_number
					);
					$index = 0;
					foreach ($cells as $cell) {
						$smartcell = new stdClass();
						$smartcell->columnId = $column_ids[$index];
						$smartcell->value = $cell;
						$smartcells[] = $smartcell;
						$index++;
					}
					$smartrow->cells = $smartcells;
					$smartrows[] = $smartrow;

			
			break;
		}
		$smartrows = json_encode($smartrows);

		$header[0] = "Content-Length: ". strlen($smartrows);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_URL, "https://api.smartsheet.com/2.0/sheets/$sheet_id/rows");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $smartrows);
		$result = curl_exec($curl);
    	curl_close($curl);

		/*echo'<pre>';
		print_r($smartrows);
		die();*/

		
		return true;
	}

}
