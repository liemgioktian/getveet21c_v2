<?php

class croncontroller extends  CI_Controller {

  function smartsheet_jobleads () {
    $this->load->library('smartsheet');
    $this->load->model(array('jobleadsmodel', 'settingsModel'));
    $param['method'] = 'sheets';
    $param['id'] = '7495236852180868';
    $param['datatype'] = 'object';
    $columns = $this->smartsheet->get($param);
    foreach ($columns->rows as $row) {
      $date = explode('-', $row->cells[1]->value);
      $date = $date[2] . '-' . $date[1] . '-' . $date[0];
      $record = array (
        'lid' => isset($row->cells[0]->displayValue) ? $row->cells[0]->displayValue : 0,
        'date_leads' => $date,
        'suburb_leads' => $row->cells[2]->displayValue,
        'qty_leads' => $row->cells[3]->displayValue, 
      );

      $exists = $this->jobleadsmodel->retrieve($record['lid']);
      if (!empty($exists)) $this->jobleadsmodel->save($record);//update
      else {// insert into db, set new lid into smartsheet
        unset($record['lid']);
        $dbid = $this->jobleadsmodel->save($record);

        $date = $record['date_leads'];
        $suburb = $record['suburb_leads'];
        $qty = $record['qty_leads'];
        $htmlMsg = ' New Job has been listed on the lead register <br /> Please find the details below : <br /> <br /> <br /> '.$date.', '.$suburb.', '.$qty;
        foreach ($this->settingsModel->contractors_email() as $index => $list) {
          $email_address = $list->email_address;
          $this->jobleadsmodel->getveet_send_mail($email_address, $htmlMsg, 'New Job Has Been Listed', null);
        }

        $cellback = new stdClass();
        $cellback->columnId = '3699825339328388';
        $cellback->value = $dbid;
        $rowback = new stdClass();
        $rowback->id = $row->id;
        $rowback->cells[] = $cellback;
        $setparam = array (
          'method' => 'sheets/7495236852180868/rows',
          'object' => array ($rowback)
        );
        $this->smartsheet->update($setparam);
      }

    }
  }

}
