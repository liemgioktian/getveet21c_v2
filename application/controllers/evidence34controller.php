<?php

class evidence34controller extends getveetController {

  function __construct() {
    parent::__construct();
    $this->load->model('evidence34Model');
  }

  function open ($fid34=null, $eid=null) {
    $eid = 0 == $eid ? null : $eid;
    $post = $this->input->post();

    $save = array(
      'eid' => $eid,
      'fid34' => $fid34,
      'name' => isset($post['name'])?$post['name']:'',
      'type' => $post['type'],
      'parent' => is_null($eid) ? '' : $eid
    );

    if (isset($post['create'])) {
      if (is_null($fid34)) {
        $this->load->model('form34model');
        $save['fid34'] = $this->form34model->save(array('data'=>array()));
      }
      unset($save['eid']);
      $fid34 = $this->evidence34Model->save($save);
      redirect(site_url('evidence34controller/open/'.$fid34));
    }
    
    if (isset($post['rename'])) {
      unset($save['parent']);
      $this->evidence34Model->save($save);
    }
    
    if (isset($post['upload']) && !empty($_FILES['name']['name'])) {
      $save['name'] = $_FILES['name']['name'];
      move_uploaded_file($_FILES['name']['tmp_name'], EVIDENCE34_DIR . $_FILES['name']['name']);
      unset($save['eid']);
      $this->evidence34Model->save($save);
      redirect(site_url("evidence34controller/open/$fid34/$eid"));// prevent reupload on refresh
    }

    $param = array('fid34' => $fid34);
    if (!is_null($eid)) {
      $param['self'] = $this->evidence34Model->retrieve($eid);
      $param['childs'] = $this->evidence34Model->get_list(array('fid34'=>$fid34, 'parent'=>$eid));
    }
    if (!is_null($fid34) && is_null($eid)) {
      $param['childs'] = $this->evidence34Model->get_list(array(
        'parent' => 0,
        'fid34' => $fid34
      ));
    }

    $this->loadView($param, 'evidence34');
  }

  function download ($eid) {
    $self = $this->evidence34Model->retrieve($eid);
    $this->load->helper('download');
    $data = file_get_contents(EVIDENCE34_DIR . $self['name']);
    $name = $self['name'];
    force_download($name, $data);
  }

  function delete($eid, $confirm = null){
    if(is_null($confirm)){
      $self = $this->evidence34Model->retrieve($eid);
      $this->evidence34Model->deleteTree($eid);
      redirect(isset($self['parent']) ?
        site_url('evidence34controller/open/' . $self['fid34'] . '/' . $self['parent']):
        site_url('form34controller/'));
    }else $this->loadView (null, 'confirmationView');
  }
}
