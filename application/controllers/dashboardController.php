<?php

class dashboardController extends getveetController
{
	function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('settingsModel');
    }

    public function index()
    {
        $user = $this->session->all_userdata();
        $data = $this->settingsModel->get_non_super_admin_companies();
    	$data = (array)$data[0];
		$param['setting'] = $data;
		$this->loadView($param, 'dashboardView');
    }
}

