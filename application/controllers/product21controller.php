<?php

class product21controller extends getveetController {

  function __construct() {
    parent::__construct();
    $this->load->model('cdproductmodel');
  }

  function index () {
    $param['items'] = $this->cdproductmodel->get_list(array());
    $this->loadView($param, 'product21ListView');
  }

  function create () {
    $post = $this->input->post();
    if ($post) {
      $this->cdproductmodel->save(array(
        'brand' => $post['brand'],
        'id' => $post['id'],
        'type' => $post['type'],
        'value' => $post['value'],
        'lifetime' => $post['lifetime'],
        'output' => $post['output'],
        'efficacy' => $post['efficacy'],
        'factors' => $post['factors'],
        'abatement' => $post['abatement'],
        'power' => $post['power'],
        'veec_vic' => $post['veec_vic'],
        'veec_metro' => $post['veec_metro'],
        'electronic' => $post['electronic'],
        'magnetic' => $post['magnetic'],
      ));
      redirect(site_url('product21controller'));
    }
    $this->loadView(null, 'product21DetailView');
  }
  
  function delete($pid, $confirm=null){
    if(is_null($confirm)){
      $this->cdproductmodel->delete($pid);
      redirect(site_url('product21controller'));
    }else $this->loadView (null, 'confirmationView');
  }
  
  function edit ($item_id) {
    $post = $this->input->post();
    if ($post) {
      $this->cdproductmodel->save(array(
        'item_id' => $item_id,
        'brand' => $post['brand'],
        'id' => $post['id'],
        'type' => $post['type'],
        'value' => $post['value'],
        'lifetime' => $post['lifetime'],
        'output' => $post['output'],
        'efficacy' => $post['efficacy'],
        'factors' => $post['factors'],
        'abatement' => $post['abatement'],
        'power' => $post['power'],
        'veec_vic' => $post['veec_vic'],
        'veec_metro' => $post['veec_metro'],
        'electronic' => $post['electronic'],
        'magnetic' => $post['magnetic'],
      ));
      redirect(site_url('product21controller'));
    }
    $param = $this->cdproductmodel->retrieve($item_id);
    $this->loadView($param, 'product21DetailView');
  }
}