<?php

class form34controller extends getveetController {

  function __construct() {
    parent::__construct();
    $this->load->model('form34Model');
    date_default_timezone_set('Australia/Victoria');
  }

  function index() {
    $form34s['items'] = $this->form34Model->getFileList();
    $this->loadView($form34s, 'form34ListView');
  }

  function create() {
    $post = $this->input->post();
    if ($post) {
      $fid = $this->form34Model->save(array('data'=>$post));
      $this->generate_stock_usage($post, $fid);
      exit("$fid");
    }
    $this->load->model('product34model');
    $param['product34'] = $this->product34model->getJson(array());
    $this->load->model('installermodel');
    $installers = $this->installermodel->get_list(array(
      'cid' => $this->session->userdata('cid'),
    )); 
    foreach ($installers as $ins) $param['settings_app']['installer'][] = array(
      'id' => "$ins->first_name $ins->last_name",
      'iid' => $ins->iid
    );
    $this->loadView($param,'34form');
  }

  function edit ($fid) {
    $post = $this->input->post();
    if ($post) {
      // echo'<pre>';print_r($post);die();
      $form34 = array (
        'fid' => $fid,
        'data' => $post
      );
      $fid = $this->form34Model->save($form34);
      $this->generate_stock_usage($post, $fid);
      exit("$fid");
    }
    $form34 = $this->form34Model->retrieve($fid);
    $form34['data'] = json_decode($form34['data'], true);
    // echo'<pre>';print_r($form34);die();
    $this->load->model('product34model');
    $form34['product34'] = $this->product34model->getJson(array());
    $this->load->model('installermodel');
    $installers = $this->installermodel->get_list(array(
      'cid' => $this->session->userdata('cid'),
    )); 
    foreach ($installers as $ins) $form34['settings_app']['installer'][] = array(
      'id' => "$ins->first_name $ins->last_name",
      'iid' => $ins->iid
    );
    $this->loadView($form34,'34form');
  }

  function delete ($fid, $confirmed='confirmed') {
    if ($confirmed=='confirmed') {
      $this->form34Model->delete($fid);
      redirect(site_url('form34controller'));
    } else {
      $this->loadView (null, 'confirmationView');
    }
  }

  function subform () {
    $param['link21'] = site_url('evidence34controller/open');
    $param['text21'] = 'CREATE EVIDENCE';
    $param['icon21'] = base_url('asset/images/folder_open.png');
    $param['link34'] = site_url('form34controller/create');
    $param['text34'] = 'CREATE FORM';
    $this->loadView($param,'subform');
  }

  private function generate_stock_usage($post, $fid){
    $past = $post;
    $post = array();
    foreach ($past as $section => $string) {
      $pist = explode('&', $string);
      foreach ($pist as $p) {
        $get = explode('=', $p);
        $post[$section][$get[0]] = $get[1];
      }
    }
    $iid = $post['assignment34']['iid'];
    $out_of_stock = 0;
    $this->load->model(array(
      'stockusagemodel',
      'stockmodel',
      'installermodel',
    ));
    if(strlen($iid) > 0){
      $f34 = $this->form34Model->retrieve($fid);
      $job_reference = $f34['job_reference'];
      // if using company stock instead of installer stock
      $iids = array();
      foreach($this->installermodel->get_admin_installers() as $ins) $iids[] = $ins->iid;
      $installer = $this->installermodel->retrieve($iid);
      $iid = in_array($installer['iid'], $iids) ? $installer['iid'] : $installer['cid'];

      $pi = 15;
      while (isset($post['survey']["AA$pi"])) {
        $item_id = $post['survey']["PID$pi"];
        $qty = $post['survey']["AS$pi"];
        $stocks = $this->stockmodel->get_list(array(
          'installer_id' => $iid,
          'item_id' => $item_id
        ));
        if(count($stocks)>0){
          $usage = $this->stockusagemodel->get_list(array(
            'stock_id' => $stocks[0]->stock_id,
            'job_refference' => $job_reference
          ));
          if(count($usage)>0){
            // RETUR
            if($qty > $usage[0]->stock + $stocks[0]->stock){
              $out_of_stock = 1;
            }
          }else if($stocks[0]->stock < $qty) $out_of_stock = 1;
        }else{
          $stock_id = $this->stockmodel->save(array(
            'installer_id' => $iid,
            'item_id' => $item_id,
          ));
          $out_of_stock = 1;
        }
        $this->stockusagemodel->save(array(
          'stock_id' => $stock_id,
          'job_refference' => $job_reference,
          'stock' => $qty,
        ));
        $pi++;
      }
    }
    return $out_of_stock;
  }
}
