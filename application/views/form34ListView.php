<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Form 34 List</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <input type="hidden" name="action" id="action">
                            <input name="action" id="action" type="hidden">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>34FORMS</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page user-->
                            <div class="page" id="">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                    <table id="tcodes_tbl" cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                        <thead>
                                                            <tr class="skyblue">
                                                            		<th>FORM ID</th>
                                                                <th>USER</th>
                                                                <th>COMPANY</th>
                                                                <th>APPLICATION</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($items as $item): ?>
                                                                <tr>
                                                                  <td><?= $item->fid ?></td>
                                                                  <td><?= $item->user ?></td>
                                                                  <td><?= $item->company ?></td>
                                                                  <td><?= $item->application ?></td>
                                                                  <td>
                                                                    <a class="btn btn-warning" href="<?= site_url("form34controller/edit/$item->fid") ?>">EDIT</a>
                                                                    <a class="btn btn-danger" href="<?= site_url("form34controller/delete/$item->fid/confirm") ?>">DELETE</a>
                                                                    <a class="btn" href="<?= site_url('evidence34controller/open/'.$item->fid) ?>">EVIDENCE</a>
                                                                  </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--close user-->
                        </div>
                    </div>
                        
                        
                </div>
            </div>      
        </div>


        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
    </body>
</html>