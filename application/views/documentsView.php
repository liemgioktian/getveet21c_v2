<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Site Instructions</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
				
                <div class="col-md-12">
                    <div class="panel">
                        <!--<?php include APPPATH.'/views/resourcesSubmenu.php'; ?>-->
                        <div class="box_label text-center">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>RESOURCES</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="page" id="">
                                <div class="row">
                                    <div class="col-sm-12">
                                    	
                                    	<table class="table table-bordered table-condesed table-hover">
                                    		<thead>
	                                    		<tr class="skyblue">
	                                    			<th>FILE NAME</th>
	                                    			<th>DESCRIPTION</th>
	                                    			<th></th>
	                                    		</tr>
                                    		</thead>
                                    		<tbody>
                                    	<?php foreach ($documents as $docs) : ?>
                                    		<tr>
                                    			<td><?= $docs->file ?></td>
                                    			<td><?= $docs->description ?></td>
	                                    		<td>
	                                    		<?php if ('1' === $current_user['is_admin']): ?>
	                                    			<form method="POST" style="display: inline">
	                                    				<input type="hidden" name="did" value="<?= $docs->did ?>" />
	                                    				<input type="submit" class="btn btn-sm" name="delete" value="<?= "DELETE" ?>" />
	                                    			</form>
	                                    		<?php endif; ?>
	                                    		<a class="btn btn-sm" href="<?= site_url("maincontroller/documents/$docs->did") ?>" target="_blank">DOWNLOAD</a>
	                                    		</td>
                                    		</tr>
                                    	<?php endforeach; ?>
                                    	</tbody>
                                    	
                                    	<?php if ('1' === $current_user['is_admin']): ?>
                                    		<tfoot>
                                    			<tr>
		                                    		<form enctype="multipart/form-data" method="POST">
		                                    			<th><input type="file" name="upload" /></th>
		                                    			<th><input type="text" name="description" /></th>
		                                    			<th><input type="submit" value="UPLOAD" class="btn btn-sm" /></th>
		                                    		</form>
                                    			</tr>
                                    		</tfoot>
                                    	<?php endif; ?>
                                    	
                                    	</table>
                                    	
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>      
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
        <script type="text/javascript">
        	jQuery('[name="delete"]').click(function (){
        		if(!confirm('Are you sure ?')) return false;
        	})
        </script>
    </body>
</html>