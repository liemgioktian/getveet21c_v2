<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>assignment form</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/select2.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/select2-bootstrap.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <link rel="stylesheet" type="text/css" href="<?= base_url('asset/css/jquery.signaturepad.css') ?>" />
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">

    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="content">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-header text-right">
                                    <div class="btn-group text-right" role="group" aria-label="...">
                                        <a class="btn 34form-menu btn-xs" href="#lighting-survey" data-toggle="tab">Lighting Survey</a>
                                        <a class="btn 34form-menu btn-xs" href="#quote-page" data-toggle="tab">Quote Page</a>
                                        <a class="btn 34form-menu btn-xs" href="#assignment-form" data-toggle="tab">Assignment Form</a>
                                        <a class="btn 34form-menu btn-xs" href="#doc-require" data-toggle="tab">VEEC Document Requirement</a>
                                        <a class="btn btn-xs btn-34-submit">Submit Form</a>
                                    </div>
                                </div>
                                <!--
                                <div class="tab-pane active" id="34pack">
                                    <ul class="nav text-center">
                                        <li class="col-sm-3">
                                            <a href="#lighting-survey" data-toggle="tab" class="btn 34form-menu">
                                                <img src="<?= base_url('asset/images/new_form.png') ?>" class="dashboard-icon" alt=""> <br>
                                                <span class="bold">Lighting Survey</span>
                                            </a>
                                        </li>
                                        <li class="col-sm-3">
                                            <a href="#quote-page" data-toggle="tab" class="btn 34form-menu">
                                                <img src="<?= base_url('asset/images/new_form.png') ?>" class="dashboard-icon" alt=""> <br>
                                                <span class="bold">Quote Page</span>
                                            </a>
                                        </li>
                                        <li class="col-sm-3">
                                            <a href="#assignment-form" data-toggle="tab" class="btn 34form-menu">
                                                <img src="<?= base_url('asset/images/new_form.png') ?>" class="dashboard-icon" alt=""> <br>
                                                <span class="bold">Assignment Form</span>
                                            </a>
                                        </li>
                                        <li class="col-sm-3">
                                            <a href="#doc-require" data-toggle="tab" class="btn 34form-menu">
                                                <img src="<?= base_url('asset/images/new_form.png') ?>" class="dashboard-icon" alt=""> <br>
                                                <span class="bold">VEEC Document Requirement</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                -->
                                <div class="tab-pane active" id="lighting-survey">
                                    <?php include APPPATH.'/views/survey.php'; ?>
                                </div>
                                <div class="tab-pane" id="quote-page">
                                    <?php include APPPATH.'/views/quote-page.php'; ?>
                                </div>
                                <div class="tab-pane" id="assignment-form">
                                    <form enctype="multipart/form-data" action="" class="form-horizontal" method="post" role="form" id="assignment34">
                                        <input type="hidden" name="mapsrc" id="mapsrc"
                                        value="<?= isset($data['assignment34']['mapsrc'])?$data['assignment34']['mapsrc']:'' ?>">
                                        <input name="action" id="action" type="hidden"
                                        value="<?= isset($data['assignment34']['action'])?$data['assignment34']['action']:'' ?>">
                                        <div class="page">
                                            <div class="hidden" id="map-canvas"></div>
                                            <div class="row">
                                                <div class="col-sm-4 hidden-xs">
                                                    <img src="<?= $logo ?>" style="height:80px" alt="">
                                                </div>
                                                <div class="col-sm-4">
                                                    <h2 class="text-center bold">CL VEEC</h2>
                                                    <h5 class="text-center bold">Assignment Form <br> Version 4.03</h5>
                                                </div>
                                                <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                                <div class="col-sm-3 hidden-xs">
                                                    <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                                </div>
                                            </div>
                                            <!--
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-6 col-xs-8">Job Reference</label>
                                                        <div class="col-sm-6 col-xs-4">
                                                            <input type="text" style="border:solid 1px grey" class="form-control input-sm text-right bold" id="reference" name="reference" data-cell="H4">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-6 col-xs-8">Client number</label>
                                                        <div class="col-sm-6 col-xs-4">
                                                          <input type="text" style="border:solid 1px grey" class="form-control input-sm text-right bold text-center" id="client" name="client" data-cell="H5" data-formula="'C21448'">
                                                        </div>
                                                      </div>
                                                </div>
                                                <div class="col-sm-3 hidden-xs">
                                                    <p class="text-justify">
                                                        2 Domville Avenue, Hawthorn VIC 3122 <br>
                                                        T:1300 077 784    F:03 9815 1066
                                                    </p>
                                                    <p class="text-justify" style="color:green">
                                                        Forms@greenenergytrading.com.au <br>
                                                    </p>
                                                    <p class="text-justify">
                                                        ABN 21 128 476 406
                                                    </p>
                                                </div>
                                            </div>
                                            -->

                                            <div class="row">
                                                <div id="row_up1" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button1" class="btn btn-danger btn-xs row_up_button" data-button="1"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down1" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button1" class="btn btn-success btn-xs row_down_button" data-button="1"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">About the Victorian Energy Efficiency Target</h5>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 box_desc row_class1">
                                                    <p class="text-justify">
                                                        Consumers in respect of whom a prescribed activity is undertaken can create Victorian Energy Efficiency Certificates (VEECs)
                                                        under the Victorian Energy Efficiency Target Act 2007. One VEEC represents one tonne of carbon dioxide equivalent (CO2-e) to
                                                        be reduced by the prescribed activity undertaken by the consumer.
                                                        Consumers or their authorised signatories are able to assign their right to create VEECs to an Accredited Person. In assigning
                                                        their right to an Accredited Person, the Accredited Person will be entitled to create and own certificates in respect of the
                                                        prescribed activity undertaken by the consumer. In return, the Accredited Person should provide consumers with an identifiable
                                                        benefit for the assignation, such as a price reduction on a product, free installation or a cash-back arrangement. Consumers
                                                        should be aware that it is their responsibility to negotiate satisfactory terms with the Accredited Person in return for assigning their
                                                        right to create VEECs.
                                                    </p>
                                                    <p class="text-center red bold">
                                                        Yellow fields are mandatory. <br>
                                                        Depending on answers, some fields may change
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up2" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button2" class="btn btn-danger btn-xs row_up_button" data-button="2"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down2" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button2" class="btn btn-success btn-xs row_down_button" data-button="2"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Energy Consumer details (i.e. where the installation was performed)</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class2"> 
                                                <div class="col-sm-12 box_desc">
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-7">
                                                            <label for="C14">Company name</label>
                                                            <input type="text" class="form-control input-sm" id="C14" name="C14" data-cell="C14" data-formula="IF(#survey!C6=0,' ',#survey!C6)"
                                                            value="<?= isset($data['assignment34']['C14'])?$data['assignment34']['C14']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <label for="S14">ABN of Energy Consumer</label>
                                                            <input type="text" class="form-control input-sm yellow" id="S14" name="S14" data-cell="S14"
                                                            value="<?= isset($data['assignment34']['S14'])?$data['assignment34']['S14']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-7">
                                                            <label for="C16">Installation street address</label>
                                                            <input type="text" class="form-control input-sm" id="C16" name="C16" data-cell="C16" data-formula="IF(#survey!C9=0,' ',#survey!C9&', '&#survey!M9&'  '&#survey!W9&'  '&#survey!Y9)"
                                                            value="<?= isset($data['assignment34']['C16'])?$data['assignment34']['C16']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <label for="S16">Industry type</label>
                                                            <input type="text" class="form-control input-sm yellow industry" id="S16" name="S16" data-cell="S16"
                                                            value="<?= isset($data['assignment34']['S16'])?$data['assignment34']['S16']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-7">
                                                            <label for="C18">Postal address (if different)</label>
                                                            <input type="text" class="form-control input-sm" id="C18" name="C18" data-cell="C18"
                                                            value="<?= isset($data['assignment34']['C18'])?$data['assignment34']['C18']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <label for="S18">Tenant/Landlord/Owner?</label>
                                                            <select class="input-sm form-control yellow" name="S18" id="S18" data-cell="S18">
                                                                <option value="0" <?= isset($data['assignment34']['S18'])&&$data['assignment34']['S18']==0?'selected':'' ?>></option>
                                                                <option value="1" <?= isset($data['assignment34']['S18'])&&$data['assignment34']['S18']==1?'selected':'' ?>>Tenant</option>
                                                                <option value="2" <?= isset($data['assignment34']['S18'])&&$data['assignment34']['S18']==2?'selected':'' ?>>Landlord</option>
                                                                <option value="3" <?= isset($data['assignment34']['S18'])&&$data['assignment34']['S18']==3?'selected':'' ?>>Owner-occupier</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-7">
                                                            <label for="C20">Company authorised signatory name</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C20" name="C20" data-cell="C20"
                                                            value="<?= isset($data['assignment34']['C20'])?$data['assignment34']['C20']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <label for="T20">Position in company</label>
                                                            <input type="text" class="form-control input-sm yellow" id="T20" name="T20" data-cell="T20"
                                                            value="<?= isset($data['assignment34']['T20'])?$data['assignment34']['T20']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-4">
                                                            <label for="C22">Phone</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C20" name="C20" data-cell="C20"
                                                            value="<?= isset($data['assignment34']['C20'])?$data['assignment34']['C20']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="H22">Mobile</label>
                                                            <input type="text" class="form-control input-sm" id="H22" name="H22" data-cell="H22"
                                                            value="<?= isset($data['assignment34']['H22'])?$data['assignment34']['H22']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="O22">Email</label>
                                                            <input type="text" class="form-control input-sm yellow" id="P22" name="P22" data-cell="P22"
                                                            value="<?= isset($data['assignment34']['P22'])?$data['assignment34']['P22']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-6">
                                                            <label for="C25">Site contact name (if different)</label>
                                                            <input type="text" class="form-control input-sm" id="C25" name="C25" data-cell="C25"
                                                            value="<?= isset($data['assignment34']['C25'])?$data['assignment34']['C25']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="R25">Phone</label>
                                                            <input type="text" class="form-control input-sm" id="R25" name="R25" data-cell="R25"
                                                            value="<?= isset($data['assignment34']['R25'])?$data['assignment34']['R25']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up3" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button3" class="btn btn-danger btn-xs row_up_button" data-button="3"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down3" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button3" class="btn btn-success btn-xs row_down_button" data-button="3"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Upgrade eligibility factors</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class3">
                                                <div class="col-sm-12 box_desc">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input id="H28" name="H28"  data-cell="H28" value="1" type="checkbox"
                                                            <?= isset($data['assignment34']['H28'])&&$data['assignment34']['H28']==1?'checked':'' ?>>The site is not compulsorily registered on the EREPs register under the Environment and Resource Efficiency Plans Program administered by the Environment Protection Authority.
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input id="H29" name="H29"  data-cell="H29" value="1" type="checkbox"
                                                            <?= isset($data['assignment34']['H29'])&&$data['assignment34']['H29']==1?'checked':'' ?>>The site is a Class 3, 4, 5, 6, 7, 8, 9 or 10(b) building, or the common area of a Class 2 building, as defined by the Building Code of Australia (BCA) 2008.
                                                        </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-10">
                                                            <label for="">Is the lighting upgrade at this site undertaken as part of building work which required a building permit according to the Building Act 1993 and the Building Regulations 2006 (permit must be attached)? </label>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="text" class="form-control input-sm text-center" id="X33" name="X33" data-cell="X33" data-formula="IF(#survey!AW9=0,' ',#survey!AW9)"
                                                            value="<?= isset($data['assignment34']['X33'])?$data['assignment34']['X33']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up4" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button4" class="btn btn-danger btn-xs row_up_button" data-button="4"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down4" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button4" class="btn btn-success btn-xs row_down_button" data-button="4"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Certificate calculation</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class4">
                                                <div class="col-sm-12 box_desc">
                                                    <p class="text-justify">
                                                        The number of certificates depends on the lamp circuit power (LCP), the number of lamps, the presence of control and air-conditioning systems and the asset lifetime. In order to calculate the number of certificates, please use the lighting survey sheet. 
                                                    </p>
                                                    <div class="form-group">
                                                        <div class="col-sm-3 col-xs-12">
                                                            <label for="AE8">Number of certificates</label>
                                                            <input type="text" class="form-control input-sm" id="AE8" name="AE8" data-cell="AE8" data-formula="IF(#survey!AV51>0.01,#survey!AV51,' ')" data-format="0,0[.]00"
                                                            value="<?= isset($data['assignment34']['AE8'])?$data['assignment34']['AE8']:'' ?>">
                                                        </div>
                                                        <div class="text-center col-sm-1 col-xs-12">
                                                            <label for="" class="hidden-xs">&nbsp;</label> <br>
                                                            <label for=""><strong>X</strong></label>
                                                        </div>
                                                        <div class="col-sm-3 col-xs-12">
                                                            <label for="AM8">Certificate price (excluding GST)</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AM8" name="AM8" data-cell="AM8" data-formula="IF(#quote!H66>0,#quote!H66,' ')" data-format="$ 0,0.00"
                                                            value="<?= isset($data['assignment34']['AM8'])?$data['assignment34']['AM8']:'' ?>">
                                                        </div>
                                                        <div class="text-center col-sm-1 col-xs-12">
                                                            <label for="" class="hidden-xs">&nbsp;</label> <br>
                                                            <label for=""><strong>=</strong></label>
                                                        </div>
                                                        <div class="col-sm-4 col-xs-12">
                                                            <label for="AW8">Number of certificates</label>
                                                            <input type="text" class="form-control input-sm" id="AW8" name="AW8" data-cell="AW8" data-formula="IFERROR(IF(AE8>0,AM8*AE8,' '),' ')" data-format="$ 0,0.00"
                                                            value="<?= isset($data['assignment34']['AW8'])?$data['assignment34']['AW8']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <p class="text-justify">
                                                        Notes : <br>
                                                        (i).  The number of certificates must be rounded to the nearest whole number. <br>
                                                        (ii). Certificate prices can fluctuate and are valid on the date this asssignment form is received at Green Energy Trading, subject to installed product approval.
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up5" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button5" class="btn btn-danger btn-xs row_up_button" data-button="5"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down5" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button5" class="btn btn-success btn-xs row_down_button" data-button="5"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Upgrade Manager's details</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class5 box_desc">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label for="AE14">First name</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AE14" name="AE14" data-cell="AE14"
                                                            value="<?= isset($data['assignment34']['AE14'])?$data['assignment34']['AE14']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="AN14">Surename</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AN14" name="AN14" data-cell="AN14"
                                                            value="<?= isset($data['assignment34']['AN14'])?$data['assignment34']['AN14']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label for="AZ14">Your job reference</label>
                                                            <input type="text" class="form-control input-sm" id="AZ14" name="AZ14" data-cell="AZ14"
                                                            value="<?= isset($data['assignment34']['AZ14'])?$data['assignment34']['AZ14']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label for="AE16">Company name</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AE16" name="AE16" data-cell="AE16"
                                                            value="<?= isset($data['assignment34']['AE16'])?$data['assignment34']['AE16']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="AT16">Email</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AT16" name="AT16" data-cell="AT16"
                                                            value="<?= isset($data['assignment34']['AT16'])?$data['assignment34']['AT16']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-7">
                                                            <label for="AE18">Company address</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AE18" name="AE18" data-cell="AE18"
                                                            value="<?= isset($data['assignment34']['AE18'])?$data['assignment34']['AE18']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <label for="AZ18">Phone number</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AZ18" name="AZ18" data-cell="AZ18"
                                                            value="<?= isset($data['assignment34']['AZ18'])?$data['assignment34']['AZ18']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label for="">The following benefit has been provided to the energy consumer:</label>
                                                    <div class="form-group">
                                                        <div class="col-sm-5">
                                                            <div class="radio">
                                                                <label>
                                                                    <input name="conducted" id="conducted1" value="1" type="radio"
                                                                    <?= isset($data['assignment34']['conducted'])&&$data['assignment34']['conducted']==1?'checked':'' ?>>
                                                                     Direct employees of the Upgrade Manager
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label>
                                                                    <input name="conducted" id="conducted2" value="2" type="radio"
                                                                    <?= isset($data['assignment34']['conducted'])&&$data['assignment34']['conducted']==2?'checked':'' ?>>
                                                                    All works subcontracted
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <div class="radio">
                                                                <label>
                                                                    <input name="conducted" id="conducted3" value="3" type="radio"
                                                                    <?= isset($data['assignment34']['conducted'])&&$data['assignment34']['conducted']==3?'checked':'' ?>>
                                                                    Employees of the Energy Consumer
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <div class="col">
                                                                    <label class="col-sm-3">
                                                                        <input name="conducted" id="conducted4" value="4" type="radio"
                                                                        <?= isset($data['assignment34']['conducted'])&&$data['assignment34']['conducted']==4?'checked':'' ?>>
                                                                             Other - please specify:
                                                                    </label>
                                                                    <div class="col-sm-4">
                                                                        <input class="form-control input-sm" id="AG190" name="AG190" data-cell="AG190" type="text"
                                                                        value="<?= isset($data['assignment34']['AG190'])?$data['assignment34']['AG190']:'' ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                            <label for="AE25">Is a Certificate of Electrical Safety (CoES) required?</label>
                                                            <select type="text" class="form-control input-sm yellow" id="AE25" name="AE25" data-cell="AE25">
                                                                <option value="0" <?= isset($data['assignment34']['AE25'])&&$data['assignment34']['AE25']==0?'selected':'' ?>>NO</option>
                                                                <option value="1" <?= isset($data['assignment34']['AE25'])&&$data['assignment34']['AE25']==1?'selected':'' ?>>YES</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6 hidden" id="coes">
                                                            <label for="AV25">CoES number</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AV25" name="AV25" data-cell="AV25"
                                                            value="<?= isset($data['assignment34']['AV25'])?$data['assignment34']['AV25']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up6" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button6" class="btn btn-danger btn-xs row_up_button" data-button="6"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down6" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button6" class="btn btn-success btn-xs row_down_button" data-button="6"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Electrician's details</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class6">
                                                <div class="col-sm-12 box_desc">
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label for="AE29">Electrican's name</label>
                                                            <input type="text" class="form-control input-sm yellow installer" id="AE29" name="AE29" data-cell="AE29"
                                                            value="<?= isset($data['assignment34']['AE29'])?$data['assignment34']['AE29']:'' ?>">
                                                            <input type="hidden" name="iid" />
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="AX29">Electrican's lincence number</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AX29" name="AX29" data-cell="AX29"
                                                            value="<?= isset($data['assignment34']['AX29'])?$data['assignment34']['AX29']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="electrician_sign">
                                                        <div class="col-sm-6">
                                                            <label for="AE32">Signature (Required)</label>
                                                            <div id="sign1"></div>
                                                            <button type="button" class="btn btn-xs" id="resetSign1">reset</button>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="AX32">Date</label>
                                                            <input type="text" class="form-control input-sm datepicker" id="AX32" name="AX32" data-cell="AX32"
                                                            value="<?= isset($data['assignment34']['AX32'])?$data['assignment34']['AX32']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up7" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button7" class="btn btn-danger btn-xs row_up_button" data-button="7"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down7" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button7" class="btn btn-success btn-xs row_down_button" data-button="7"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Data summary</h5>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up8" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button8" class="btn btn-danger btn-xs row_up_button" data-button="8"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down8" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button8" class="btn btn-success btn-xs row_down_button" data-button="8"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Installation summary</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class8 box_desc">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                            <label for="C92">Date of project commencement</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C92" name="C92" data-cell="C92"
                                                            value="<?= isset($data['assignment34']['C92'])?$data['assignment34']['C92']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="K92">Date of project completion</label>
                                                            <input type="text" class="form-control input-sm yellow" id="K92" name="K92" data-cell="K92"
                                                            value="<?= isset($data['assignment34']['K92'])?$data['assignment34']['K92']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="U92">Number of floor levels included</label>
                                                            <input type="text" class="form-control input-sm yellow" id="U92" name="U92" data-cell="U92"
                                                            value="<?= isset($data['assignment34']['U92'])?$data['assignment34']['U92']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                            <label for="C94">Floor space of the premises (m2)</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C94" name="C94" data-cell="C94"
                                                            value="<?= isset($data['assignment34']['C94'])?$data['assignment34']['C94']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="K94">Total lamps installed</label>
                                                            <input type="text" class="form-control input-sm" id="K94" name="K94" data-cell="K94" data-formula="IF(#survey!AS50='','',#survey!AS50)"
                                                            value="<?= isset($data['assignment34']['K94'])?$data['assignment34']['K94']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="U94">Total ballasts/drivers installed</label>
                                                            <input type="text" class="form-control input-sm yellow" id="U94" name="U94" data-cell="U94"
                                                            value="<?= isset($data['assignment34']['U94'])?$data['assignment34']['U94']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                            <label for="C96">Floor space of the (m2)</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C96" name="C96" data-cell="C96"
                                                            value="<?= isset($data['assignment34']['C96'])?$data['assignment34']['C96']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="K96">Total T5 adaptors installed</label>
                                                            <input type="text" class="form-control input-sm" id="K96" name="K96" data-cell="K96" data-formula="IF(AZ82='','',AZ82)"
                                                            value="<?= isset($data['assignment34']['K96'])?$data['assignment34']['K96']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="U96">Total lighting control</label>
                                                            <input type="text" class="form-control input-sm yellow" id="U96" name="U96" data-cell="U96"
                                                            value="<?= isset($data['assignment34']['U96'])?$data['assignment34']['U96']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label for="Y98">Is this the first lighting upgrade claiming VEECs to be carried out at these premises?</label>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <select type="text" class="form-control input-sm yellow" id="Y98" name="Y98" data-cell="Y98">
                                                                <option value="0" <?= isset($data['assignment34']['Y98'])&&$data['assignment34']['Y98']==0?'selected':'' ?>>NO</option>
                                                                <option value="1" <?= isset($data['assignment34']['Y98'])&&$data['assignment34']['Y98']==1?'selected':'' ?>>YES</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up9" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button9" class="btn btn-danger btn-xs row_up_button" data-button="9"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down9" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button9" class="btn btn-success btn-xs row_down_button" data-button="9"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Decomissioning summary</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class9">
                                                <div class="col-sm-12 box_desc">
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label for="D102">Total lamps removed</label>
                                                            <input type="text" class="form-control input-sm" id="D102" name="D102" data-cell="D102" data-formula="IF(#survey!W50=' ',' ',#survey!W50)"
                                                            value="<?= isset($data['assignment34']['D102'])?$data['assignment34']['D102']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="K102">Decommissioning method</label>
                                                            <input type="text" class="form-control input-sm" id="K102" name="K102" data-cell="K102" data-formula="IF(D102<>'','Recycled - receipt attached','')"
                                                            value="<?= isset($data['assignment34']['K102'])?$data['assignment34']['K102']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label for="D104">Total ballasts/drivers removed</label>
                                                            <input type="text" class="form-control input-sm" id="D104" name="D104" data-cell="D104"
                                                            value="<?= isset($data['assignment34']['D104'])?$data['assignment34']['D104']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="K104">Decommissioning method</label>
                                                            <input type="text" class="form-control input-sm" id="K104" name="K104" data-cell="K104" data-formula="IF(D104>0,'Recycled - receipt attached','')"
                                                            value="<?= isset($data['assignment34']['K104'])?$data['assignment34']['K104']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up10" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button10" class="btn btn-danger btn-xs row_up_button" data-button="10"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down10" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button10" class="btn btn-success btn-xs row_down_button" data-button="10"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Design and verification</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class10">
                                                <div class="col-sm-12 box_desc">
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label for="C108">Lighting designer's name</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C108" name="C108" data-cell="C108"
                                                            value="<?= isset($data['assignment34']['C108'])?$data['assignment34']['C108']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="O108">Lighting design process</label>
                                                            <select type="text" class="form-control input-sm yellow" id="O108" name="O108" data-cell="O108">
                                                                <option value="0" <?= isset($data['assignment34']['O108'])&&$data['assignment34']['O108']==0?'selected':'' ?>></option>
                                                                <option value="1" <?= isset($data['assignment34']['O108'])&&$data['assignment34']['O108']==1?'selected':'' ?>>Lighting design software</option>
                                                                <option value="2" <?= isset($data['assignment34']['O108'])&&$data['assignment34']['O108']==2?'selected':'' ?>>None undertaken</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label for="C110">Lighting designer's qualifications</label>
                                                            <select type="text" class="form-control input-sm" id="C110" name="C110" data-cell="C110">
                                                                <option value="0" <?= isset($data['assignment34']['C110'])&&$data['assignment34']['C110']==0?'selected':'' ?>></option>
                                                                <option value="1" <?= isset($data['assignment34']['C110'])&&$data['assignment34']['C110']==1?'selected':'' ?>>Registered Lighting Practictioner - Illuminating Engineering Society of Australia and New Zealand (RLP)</option>
                                                                <option value="2" <?= isset($data['assignment34']['C110'])&&$data['assignment34']['C110']==2?'selected':'' ?>>Member - Illuminating Engineering Society of Australia and New Zealand (MIES)</option>
                                                                <option value="3" <?= isset($data['assignment34']['C110'])&&$data['assignment34']['C110']==3?'selected':'' ?>>Relevant engineering qualifications - provide details in adjacent field </option>
                                                                <option value="4" <?= isset($data['assignment34']['C110'])&&$data['assignment34']['C110']==4?'selected':'' ?>>Other relevant qualification or experience - provide details in adjacent field</option>
                                                                <option value="5" <?= isset($data['assignment34']['C110'])&&$data['assignment34']['C110']==5?'selected':'' ?>>No lighting design undertaken</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="O109" id="O109" name="O109" data-cell="O109" data-formula="IF(C110=0,'Qualification details (if required)',IF(C110=4,'Qualification details',IF(C110=3,'Qualification details','')))"></label>
                                                            <input type="text" class="form-control input-sm" id="O110" name="O110" data-cell="O110"
                                                            value="<?= isset($data['assignment34']['O110'])?$data['assignment34']['O110']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label for="C113" id="C113" name="C113" data-cell="C113" data-formula="IF(AE8=0,'Light level verifiers name',IF(AE8>99,'Light level verifiers name',''))"></label>
                                                            <input type="text" class="form-control input-sm" id="C114" name="C114" data-cell="C114" data-formula="IF(C113>0,(AE14&' '&AN14),'')"
                                                            value="<?= isset($data['assignment34']['C114'])?$data['assignment34']['C114']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="O113" id="O113" name="O113" data-cell="O113" data-formula="IF(C113=0,' ','Light level verification process')"></label>
                                                            <select type="text" class="form-control input-sm" id="O114" name="O114" data-cell="O114">
                                                                <option value="0" <?= isset($data['assignment34']['O114'])&&$data['assignment34']['O114']==0?'selected':'' ?>></option>
                                                                <option value="1" <?= isset($data['assignment34']['O114'])&&$data['assignment34']['O114']==1?'selected':'' ?>>Lighting design software & Lux level report</option>
                                                                <option value="2" <?= isset($data['assignment34']['O114'])&&$data['assignment34']['O114']==2?'selected':'' ?>>Maintained illuminance/depreciation table & Lux level report</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label for="C115" id="C115" name="C115" data-cell="C115" data-formula="IF(C113=0,' ','Light level verifiers qualifications')"></label>
                                                            <select type="text" class="form-control input-sm" id="C116" name="C116" data-cell="C116">
                                                                <option value="0" <?= isset($data['assignment34']['C116'])&&$data['assignment34']['C116']==0?'selected':'' ?>></option>
                                                                <option value="1" <?= isset($data['assignment34']['C116'])&&$data['assignment34']['C116']==1?'selected':'' ?>>Registered Lighting Practictioner - Illuminating Engineering Society of Australia and New Zealand (RLP)</option>
                                                                <option value="2" <?= isset($data['assignment34']['C116'])&&$data['assignment34']['C116']==2?'selected':'' ?>>Member - Illuminating Engineering Society of Australia and New Zealand (MIES)</option>
                                                                <option value="3" <?= isset($data['assignment34']['C116'])&&$data['assignment34']['C116']==3?'selected':'' ?>>Relevant engineering qualifications - provide details in adjacent field </option>
                                                                <option value="4" <?= isset($data['assignment34']['C116'])&&$data['assignment34']['C116']==4?'selected':'' ?>>Other relevant qualification or experience - provide details in adjacent field</option>
                                                                <option value="5" <?= isset($data['assignment34']['C116'])&&$data['assignment34']['C116']==5?'selected':'' ?>>Green Energy Trading Illumination Training</option>
                                                                <option value="6" <?= isset($data['assignment34']['C116'])&&$data['assignment34']['C116']==6?'selected':'' ?>>None - under 100 VEECs to be created in respect of this upgrade</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="O115" id="O115" name="O115" data-cell="O115" data-formula="IF(C113=0,' ',IF(C116=0,'Qualification details (if required)',IF(C116=4,'Qualification details',IF(C116=3,'Qualification details',' '))))"></label>
                                                            <input type="text" class="form-control input-sm" id="O116" name="O116" data-cell="O116"
                                                            value="<?= isset($data['assignment34']['O116'])?$data['assignment34']['O116']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up11" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button11" class="btn btn-danger btn-xs row_up_button" data-button="11"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down11" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button11" class="btn btn-success btn-xs row_down_button" data-button="11"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Upgrade Manager declaration</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class11">
                                                <div class="col-sm-12 box_desc">
                                                    <div class="form-group">
                                                        <label for="" class="col-xs-2">Installation address</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="AN91" name="AN91" data-cell="AN91" data-formula=""
                                                            value="<?= isset($data['assignment34']['AN91'])?$data['assignment34']['AN91']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-xs-offset-1 col-xs-1">I,</label>
                                                        <div class="col-sm-2">
                                                          <input class="form-control input-sm" id="AE92" name="AE92" data-cell="AE92" data-formula="" type="text"
                                                          value="<?= isset($data['assignment34']['AE92'])?$data['assignment34']['AE92']:'' ?>">
                                                        </div>
                                                        <label for="" class="">(upgrade manager), hereby declare that:</label>
                                                    </div>
                                                    <ul>
                                                        <li><p class="text-justify">The above lighting upgrade was undertaken in the stated premises in accordance with the requirements of the Victorian Energy Efficiency Target Act 2007 (the Act) and the Victorian Energy Efficiency Target Regulations 2008 (the Regulations).</p></li>
                                                        <li><p class="text-justify">I confirm that the above lighting upgrade was not undertaken in a premise compulsorily registered on the EREPs register under the Environment and Resource Efficiency Plans Program administered by the Environment Protection Authority.</p></li>
                                                        <li><p class="text-justify">All of the above information (and the information contained in the associated upgrade data summary and AS/NZS 1680 compliance declaration) relating to the lighting upgrade at the nominated address is true and accurate.</p></li>
                                                        <li><p class="text-justify">Any installed lighting control device, including any voltage reduction unit (VRU), is appropriate for use with the type of lamps it is required to control.</p></li>
                                                        <li><p class="text-justify">This assignment form does not record information about any VRU that was installed during this upgrade in conjunction with electronic ballasts (such installations being ineligible for VEECs).</p></li>
                                                        <li><p class="text-justify">All products listed above have been installed according to the manufacturer’s guidelines.</p></li>
                                                        <li><p class="text-justify">The above installation was undertaken by someone who is appropriately licensed, for which evidence has been provided (license number, certificate of electrical safety).</p></li>
                                                        <li><p class="text-justify">This installation, including any wiring work, meets all the relevant standards and guidelines.</p></li>
                                                        <li><p class="text-justify">All of the documentation relating to this lighting upgrade will be kept in accordance with the record keeping requirements of the Act and the Regulations.</p></li>
                                                        <li><p class="text-justify">The information provided is complete and accurate and that I am aware that penalties can be applied for providing misleading information in this form under the Act.</p></li>
                                                    </ul>
                                                    <p class="text-justify">Further, with regards to the minimum required illuminance (brightness) and maintained illuminance requirements of AS/NZS 1680.0: </p>
                                                    <ol type="i">
                                                        <li><p class="text-justify">the lighting upgrade meets all the requirements of AS/NZS 1680 which are relevant to this activity,</p></li>
                                                        <li><p class="text-justify">any records maintained (or submitted to the ESC) in connection with this lighting upgrade are   sufficient to substantiate compliance with the requirements of AS/NZS 1680,</p></li>
                                                        <li><p class="text-justify">the requirements of AS/NZS 1680 have been explained to the consumer or authorised   signatory, including but not limited to maintained illuminance and the recommended replacement  point, </p></li>
                                                        <li><p class="text-justify">the light measuring apparatus used complies with the international standard IEC62341-6-1 and    is calibrated to the manufacturer's recommended standards, and</p></li>
                                                        <li><p class="text-justify">the information contained in this declaration and any attachments is true and accurate.</p></li>
                                                    </ol>
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label for="">Signature</label>
                                                            <div id="sign2"></div>
                                                            <button type="button" class="btn btn-xs" id="resetSign2">reset</button>
                                                            
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="">Date</label>
                                                            <input type="text" class="form-control input-sm datepicker" id="AU115" name="AU115" data-cell="AU115"
                                                            value="<?= isset($data['assignment34']['AU115'])?$data['assignment34']['AU115']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label for="">Name</label>
                                                            <input type="text" class="form-control input-sm" id="AE118" name="AE118" data-cell="AE118" data-formula="IF(AE14=0,' ',AE14&' '&AN14)"
                                                            value="<?= isset($data['assignment34']['AE118'])?$data['assignment34']['AE118']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="">Company</label>
                                                            <input type="text" class="form-control input-sm datepicker" id="AU118" name="AU118" data-cell="AU118" data-formula="IF(AE16=0,' ',AE16)"
                                                            value="<?= isset($data['assignment34']['AU118'])?$data['assignment34']['AU118']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up12" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button12" class="btn btn-danger btn-xs row_up_button" data-button="12"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down12" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button12" class="btn btn-success btn-xs row_down_button" data-button="12"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Energy Consumer declaration</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class12">
                                                <div class="col-sm-12 box_desc">
                                                    <p class="text-justify">* I am the Energy Consumer or I have been authorised to assign the right to create Victorian energy efficiency certificates (VEECs) on behalf of the owner of the right to create certificates. I have provided proof of this authority. </p>
                                                    <p class="text-justify" id="C129" name="C129" data-cell="C129" data-formula="IF(S18=0,'*    The above entity is the [tenant/landlord/owner] of the premises at the above installation address.','*    The above entity is the '&S18&' of the premises at the above installation address.')"></p>
                                                    <p class="text-justify">* I confirm that the above commercial lighting upgrade was not undertaken in a premise compulsorily registered on the EREPs register under the Environment and Resource Efficiency Plans Program administered by the Environment Protection Authority. </p>
                                                    <p class="text-justify">* I have signed this assignment form and confirm that the information provided by the installer on this form and in any associated appendices is correct and complete. </p>
                                                    <p class="text-justify">* I understand that by signing this form I am assigning the right to create VEECs for the above lighting upgrade to Green Energy Trading Pty Ltd.</p>
                                                    <p class="text-justify">* The above entity has received an identifiable benefit from the Upgrade Manager/Lighting Supplier and/or Green Energy Trading Pty Ltd in exchange for assigning its right to create the VEECs for the above lighting upgrade.</p>
                                                    <p class="text-justify">* The Essential Services Commission has the right to inspect any documentation which relates to the undertaking of a lighting upgrade at the above listed premises, according to the Victorian Energy Efficiency Target Act 2007 (the Act) and the Victorian Energy Efficiency Target Regulations 2008 (the Regulations).</p>
                                                    <p class="text-justify">* All of the documentation relating to this lighting upgrade will be kept in accordance with the record keeping requirements of the Act and the Regulations.</p>
                                                    <p class="text-justify">* I understand that information on this form will be disclosed to the Essential Services Commission for the purpose of creating VEECs under the Act and for related verification, audit and scheme monitoring purposes.</p>
                                                    <p class="text-justify">* I am aware that penalties can be applied for providing misleading information in this form under the Act.</p>
                                                    <p class="text-justify">Further, with regards to the minimum required illuminance (brightness) and maintained illuminance requirements of AS/NZS 1680.0: </p>
                                                    <ol type="i">
                                                        <li>
                                                            <p class="text-justify">Where the installation generates more than 100 VEECs: I have sighted the lumen  depreciation data/graph, lux report AND either the maintained illuminance table OR the  output  report of approriate lighting design software, and the information contained therein    is true and accurate,</p>
                                                        </li>
                                                        <li>
                                                            <p class="text-justify">The  Upgrade Manager has explained the requirements of AS/NZS 1680 including but not    limited to the required maintained illuminance and recommended replacement point,   and</p>
                                                        </li>
                                                        <li>
                                                            <p class="text-justify">The lighting outcome of the upgrade project meets the expectations of the energy    consumer and is fit for purpose.</p>
                                                        </li>
                                                    </ol>
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label for="">Signature</label>
                                                            <div id="sign3"></div>
                                                            <button type="button" class="btn btn-xs" id="resetSign3">reset</button>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="">Date</label>
                                                            <input type="text" class="form-control input-sm datepicker" id="AV136" name="AV136" data-cell="AV136"
                                                            value="<?= isset($data['assignment34']['AV136'])?$data['assignment34']['AV136']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label for="">Authorised signatory name</label>
                                                            <input type="text" class="form-control input-sm" id="AE139" name="AE139" data-cell="AE139" data-formula="IF(C20=0,' ',C20)"
                                                            value="<?= isset($data['assignment34']['AE139'])?$data['assignment34']['AE139']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="">Position</label>
                                                            <input type="text" class="form-control input-sm datepicker" id="AV139" name="AV139" data-cell="AV139" data-formula="IF(S20=0,' ',S20)"
                                                            value="<?= isset($data['assignment34']['AV139'])?$data['assignment34']['AV139']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up13" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button13" class="btn btn-danger btn-xs row_up_button" data-button="13"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down13" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button13" class="btn btn-success btn-xs row_down_button" data-button="13"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">GST implications of this assignment</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class13">
                                                <div class="col-sm-12 box_desc">
                                                    <p class="text-justify">For the purposes of the Goods and Services Tax, this transaction is characterised as the supply of the right to create Environmental Certificates from the System Owner to Green Energy Trading (GET).   </p> <br>
                                                    <p class="text-justify">Where the System Owner is registered for GST and has acquired the System for business purposes, the supply will attract GST, and the Parties agree that the system owner will issue a tax invoice to GET in respect of this supply.</p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up14" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button14" class="btn btn-danger btn-xs row_up_button" data-button="14"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down14" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button14" class="btn btn-success btn-xs row_down_button" data-button="14"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Form of benefit provided to Energy Consumer</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class14">
                                                <div class="col-sm-12 box_desc">
                                                    <label for="">The following benefit has been provided to the energy consumer:</label>
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <div class="radio">
                                                                <label>
                                                                    <input name="benefit" id="benefit1" value="1" type="radio"
                                                                    <?= isset($data['assignment34']['benefit'])&&$data['assignment34']['benefit']==1?'checked':'' ?>>
                                                                    Upfront cash
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label>
                                                                    <input name="benefit" id="benefit2" value="2" type="radio"
                                                                    <?= isset($data['assignment34']['benefit'])&&$data['assignment34']['benefit']==2?'checked':'' ?>>
                                                                    Delayed cash
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="radio">
                                                                <label>
                                                                    <input name="benefit" id="benefit3" value="3" type="radio"
                                                                    <?= isset($data['assignment34']['benefit'])&&$data['assignment34']['benefit']==3?'checked':'' ?>>
                                                                    Price reduction
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label>
                                                                    <input checked name="benefit" id="benefit4" value="4" type="radio"
                                                                    <?= isset($data['assignment34']['benefit'])&&$data['assignment34']['benefit']==4?'checked':'' ?>>
                                                                    Free installation
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                                <div class="radio">
                                                                    <label>
                                                                        <input name="benefit" id="benefit5" value="5" type="radio"
                                                                    <?= isset($data['assignment34']['benefit'])&&$data['assignment34']['benefit']==5?'checked':'' ?>>
                                                                            other (please describe)
                                                                    </label>
                                                                </div>
                                                                <input class="form-control input-sm col-sm-4" id="K159" name="K159" data-cell="K159" type="text">
                                                        </div>
                                                        <div class="col-sm-6">
                                                                    <label for="">$ amount of benefit provided :</label>
                                                                    <input class="form-control input-sm yellow col-sm-4" id="D161" name="D161" data-cell="D161" data-format="$ 0,0[.]00" type="text" data-formula="AW8"
                                                                    value="<?= isset($data['assignment34']['D161'])?$data['assignment34']['D161']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up15" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button15" class="btn btn-danger btn-xs row_up_button" data-button="15"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down15" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button15" class="btn btn-success btn-xs row_down_button" data-button="15"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Payment details</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class15">
                                                <div class="col-sm-12 box_desc">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="">Make payment to :</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C165" name="C165" data-cell="C165"
                                                            value="<?= isset($data['assignment34']['C165'])?$data['assignment34']['C165']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                            <label for="">Account name</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C167" name="C167" data-cell="C167"
                                                            value="<?= isset($data['assignment34']['C167'])?$data['assignment34']['C167']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="">BSB</label>
                                                            <input type="text" class="form-control input-sm yellow" id="M167" name="M167" data-cell="M167"
                                                            value="<?= isset($data['assignment34']['M167'])?$data['assignment34']['M167']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="">Account number</label>
                                                            <input type="text" class="form-control input-sm yellow" id="S167" name="S167" data-cell="S167"
                                                            value="<?= isset($data['assignment34']['S167'])?$data['assignment34']['S167']:'' ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="">Email address for remittance advice</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C169" name="C169" data-cell="C169" data-formula="IF(C165=BT15,P22,IF(C165=BT16,AT16,' '))"
                                                            value="<?= isset($data['assignment34']['C169'])?$data['assignment34']['C169']:'' ?>">
                                                            <input type="hidden" class="form-control input-sm" id="BT15" name="BT15" data-cell="BT15" data-formula="'Direct to the Energy Consumer'">
                                                            <input type="hidden" class="form-control input-sm" id="BT16" name="BT16" data-cell="BT16" data-formula="'The upgrade manager (Energy Consumer has received VEEC benefit)'">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label class="col-xs-10 text-right">
                                                                Is the Energy Consumer a GST registered business?</label>
                                                            <div class="col-xs-2">
                                                                <input id="W171" name="W171"  data-cell="W171" value="1" type="checkbox"
                                                                <?= isset($data['assignment34']['W171'])&&$data['assignment34']['W171']==1?'checked':'' ?>>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up16" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button16" class="btn btn-danger btn-xs row_up_button" data-button="16"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down16" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button16" class="btn btn-success btn-xs row_down_button" data-button="16"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Document checklist (complete after printing)</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class16">
                                                <div class="col-sm-12 box_desc">
                                                    <label for="">Key documents</label>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input id="AE158" name="AE158"  data-cell="AE158" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE158'])&&$data['assignment34']['AE158']==1?'checked':'' ?>>
                                                                    <strong>A1</strong> Green Energy Trading Assignment Form (all five pages)
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input id="AE159" name="AE159"  data-cell="AE159" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE159'])&&$data['assignment34']['AE159']==1?'checked':'' ?>> 
                                                                    <strong>A2</strong> Supplier/Installer Invoice for works undertaken (copy)
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input id="AE160" name="AE160"  data-cell="AE160" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE160'])&&$data['assignment34']['AE160']==1?'checked':'' ?>> 
                                                                    <strong>A3</strong> Evidence of decommissioning (e.g. recycling receipt/certificate)
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input id="AE161" name="AE161"  data-cell="AE161" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE161'])&&$data['assignment34']['AE161']==1?'checked':'' ?>> 
                                                                    <strong>A4</strong> Copy of installation agreement signed by the Energy Consumer's authorised signatory (as declared in this assignment form)
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input id="AE162" name="AE162"  data-cell="AE162" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE162'])&&$data['assignment34']['AE162']==1?'checked':'' ?>> 
                                                                    <strong>A5</strong> A Tax invoice from the Energy Consumer to Green Energy Trading for sale of the VEECs
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input id="AE163" name="AE163"  data-cell="AE163" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE163'])&&$data['assignment34']['AE163']==1?'checked':'' ?>> 
                                                                    <strong>A6</strong> A copy of the Energy Consumer's electricity bill
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label for="" class="bold">Evidence of baseline lighting</label>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input id="AE167" name="AE167"  data-cell="AE167" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE167'])&&$data['assignment34']['AE167']==1?'checked':'' ?>> 
                                                                    <strong>B1</strong> Computer drawn reflective ceiling plans for baseline lighting configuration (if any fittings have been moved during installation)
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input id="AE168" name="AE168"  data-cell="AE168" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE168'])&&$data['assignment34']['AE168']==1?'checked':'' ?>> 
                                                                    <strong>B2</strong> Photos showing the installation site prior to installation, existing equipment which verifies lamp type, ballast type, wattage and any originally installed control system
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label for="" class="bold">Evidence of upgraded lighting</label>
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input id="AE174" name="AE174"  data-cell="AE174" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE174'])&&$data['assignment34']['AE174']==1?'checked':'' ?>> 
                                                                    <strong>U1</strong> Computer drawn reflective ceiling plans for upgraded lighting configuration. Please also indicate where lux level readings were taken.
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                <label>
                                                                    <input id="AE175" name="AE175"  data-cell="AE175" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE175'])&&$data['assignment34']['AE175']==1?'checked':'' ?>> 
                                                                    <strong>U2</strong> Photos showing the installation site post installation, newly installed equipment which verifies lamp type and ballast type, wattage and any installed control system
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox">
                                                            <div class="col-sm-12">
                                                                
                                                                <label id="AI178" name="AI178" data-cell="AI178" data-formula="IF(AE8=0,'Combined Maintained Illuminace Table and Post-installation Lux Report, with measurements taken in accordance with Appendix B of AS1680.1 and demonstrating that post-installation lighting levels exceed the threshold level as specified for the specific installation (>99 certificates only)',IF(AE8>99,'Combined Maintained Illuminace Table and Post-installation Lux Report,  with measurements taken in accordance with Appendix B of AS1680.1 and demonstrating that post-installation lighting levels exceed the threshold level as specified for the specific installation',' '))">
                                                                     <input id="AE176" name="AE176"  data-cell="AE176" value="1" type="checkbox"
                                                                    <?= isset($data['assignment34']['AE176'])&&$data['assignment34']['AE176']==1?'checked':'' ?>>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="col-sm-offset-1">
                                                        <p class="text-jusfity bold" id="AI181" name="AI181" data-cell="AI181" data-formula="IF(AI178=0,' ','Note: Lux report test sites must be noted on the reflective ceiling plan, and copies of the individual test results (in your preferred format) must be submitted')"></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div id="row_up17" class="col-xs-1 box_label row_up hidden"><button type="button" id="row_up_button17" class="btn btn-danger btn-xs row_up_button" data-button="17"><span class="glyphicon glyphicon-chevron-up"></span></button></div>
                                                <div id="row_down17" class="col-xs-1 box_label row_down"><button type="button" id="row_down_button17" class="btn btn-success btn-xs row_down_button" data-button="17"><span class="glyphicon glyphicon-chevron-down"></span></button></div>
                                                <div class="col-xs-11 box_label">
                                                    <h5 style="padding-top:5px">Verification detail</h5>
                                                </div>
                                            </div>
                                            <div class="row row_class17 box_desc">
                                                <div class="col-sm-12">
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-3">
                                                            <label for="C189">Energy Consumer company name</label>
                                                            <input type="text" class="form-control input-sm" id="C189" name="C189" data-cell="C189" data-formula="C14"
                                                            value="<?= isset($data['assignment34']['C189'])?$data['assignment34']['C189']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="O189">Job Reference</label>
                                                            <input type="text" class="form-control input-sm" id="O189" name="O189" data-cell="O189" data-formula="AZ14"
                                                            value="<?= isset($data['assignment34']['O189'])?$data['assignment34']['O189']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label for="W189">Name of light level verifier / tester</label>
                                                            <input type="text" class="form-control input-sm" id="W189" name="W189" data-cell="W189" data-formula="C114"
                                                            value="<?= isset($data['assignment34']['W189'])?$data['assignment34']['W189']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="AR189">Light meter apparatus</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AR189" name="AR189" data-cell="AR189"
                                                            value="<?= isset($data['assignment34']['AR189'])?$data['assignment34']['AR189']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-4">
                                                            <label for="C191">Installation address</label>
                                                            <input type="text" class="form-control input-sm" id="C191" name="C191" data-cell="C191" data-formula="C16"
                                                            value="<?= isset($data['assignment34']['C191'])?$data['assignment34']['C191']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="W191">Qualification of light level verifier/tester</label>
                                                            <input type="text" class="form-control input-sm" id="W191" name="W191" data-cell="W191" data-formula="C116"
                                                            value="<?= isset($data['assignment34']['W191'])?$data['assignment34']['W191']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="AR191">Date of test</label>
                                                            <input type="text" class="form-control input-sm yellow datepicker" id="AR191" name="AR191" data-cell="AR191"
                                                            value="<?= isset($data['assignment34']['AR191'])?$data['assignment34']['AR191']:'' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="BB191">Time of test (24 HR)</label>
                                                            <input type="text" class="form-control input-sm yellow" id="BB191" name="BB191" data-cell="BB191"
                                                            value="<?= isset($data['assignment34']['BB191'])?$data['assignment34']['BB191']:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="table-responsive" style="overflow-x:auto;margin-top:10px">
                                                        <table id="illuminance_tbl" cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                            <thead>
                                                                <tr>
                                                                    <th class="small center" ></th>
                                                                    <th class="small center" ><label for="">Lighting space</label></th>
                                                                    <th class="small center" ><label for="">Class of task</label></th>
                                                                    <th class="small rotate center" ><label for="">Required  Illuminance (lm)</label></th>
                                                                    <th class="small rotate center" ><label for="">Size of test area  (m2)</label></th>
                                                                    <th class="small rotate center" ><label for=""># Readings taken</label></th>
                                                                    <th class="small rotate center" ><label for="">Distance between readings (m)</label></th>
                                                                    <th class="small center" ><label for="">Height readings taken (m)</label></th>
                                                                    <th class="small center" ><label for="">Installed product</label></th>
                                                                    <th class="small center" ><label for="">Product type</label></th>
                                                                    <th class="small center" ><label for="">Rec. relamping point (Hrs)</label></th>
                                                                    <th class="small center" ><label for="">Lumen depreciation factor at RRP</label></th>
                                                                    <th class="small center" ><label for="">Space cleanliness</label></th>
                                                                    <th class="small center" ><label for="">Other light loss factor</label></th>
                                                                    <th class="small center" ><label for="">Total light loss factor at RRP</label></th>
                                                                    <th class="small center" ><label for="">target lux level required at installation</label></th>
                                                                    <th class="small center" ><label for="">Average Lux test result for space</label></th>
                                                                    <th class="small center" ><label for="">Apparent Compliance?</label></th>
                                                                    <th class="small center" ><label for="">Explanation if non-compliant?</label></th>

                                                                </tr>
                                                            </thead>
                                                            <tbody id="illuminance_body">
                                                                <tr id="row1" class="">
                                                                    <td class="text-center"><label for="">1</label></td>
                                                                    <td class=""><input type="text" class="input-sm form-control text-center width-150" id="D196" name="D196" data-cell="D196" data-formula="IF(#survey!C15=0,' ',#survey!C15)"></td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control width-350" name="H196" id="H196" data-cell="H196">
                                                                            <option value="0" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==0?'selected':'' ?></option>
                                                                            <option value="40" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==40?'selected':'' ?>>Movement and orientation (e.g. corridors, walkways)</option>
                                                                            <option value="80" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==80?'selected':'' ?>>Rough intermittent use (e.g. change rooms, loading bays)</option>
                                                                            <option value="160" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==160?'selected':'' ?>>Simple tasks and work places (e.g. waiting rooms, rough machine work)</option>
                                                                            <option value="240" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==240?'selected':'' ?>>Moderately easy tasks (e.g. food preparation, medium woodworking)</option>
                                                                            <option value="320" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==320?'selected':'' ?>>Office spaces with routine tasks (e.g. reading, typing)</option>
                                                                            <option value="400" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==400?'selected':'' ?>>Moderately difficult tasks (e.g. fine woodwork, car assembly)</option>
                                                                            <option value="600" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==600?'selected':'' ?>>Difficult tasks (e.g. drawing boards, inspection tasks)</option>
                                                                            <option value="800" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==800?'selected':'' ?>>Very difficult tasks (e.g. paint retouching, colour matching of dyes)</option>
                                                                            <option value="1200" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==1200?'selected':'' ?>>Extremely difficult tasks (e.g.hand tailoring, extra-fine bench work)</option>
                                                                            <option value="1600" <?= isset($data['assignment34']['H196'])&&$data['assignment34']['H196']==1600?'selected':'' ?>>Exceptionally difficult tasks (e.g. jewellery and watchmaking)</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class=""><input type="text" class="input-sm form-control text-center width-50" id="O196" name="O196" data-cell="O196" data-formula="H196"></td>
                                                                    <td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="Q196" name="Q196" data-cell="Q196"></td>
                                                                    <td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="S196" name="S196" data-cell="S196"></td>
                                                                    <td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="U196" name="U196" data-cell="U196"></td>
                                                                    <td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="W196" name="W196" data-cell="W196"></td>
                                                                    <td class=""><input type="text" class="input-sm form-control text-center width-150" id="Y196" name="Y196" data-cell="Y196"></td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow width-250" name="AD196" id="AD196" data-cell="AD196">
                                                                            <option value="0" <?= isset($data['assignment34']['AD196'])&&$data['assignment34']['AD196']==0?'selected':'' ?>></option>
                                                                            <option value="0.89" <?= isset($data['assignment34']['AD196'])&&$data['assignment34']['AD196']==0.89?'selected':'' ?>>Fluorescent</option>
                                                                            <option value="0.89" <?= isset($data['assignment34']['AD196'])&&$data['assignment34']['AD196']==0.89?'selected':'' ?>>Induction</option>
                                                                            <option value="0.89" <?= isset($data['assignment34']['AD196'])&&$data['assignment34']['AD196']==0.89?'selected':'' ?>>MH/MV</option>
                                                                            <option value="0.89" <?= isset($data['assignment34']['AD196'])&&$data['assignment34']['AD196']==0.89?'selected':'' ?>>T5-adapter</option>
                                                                            <option value="0.95" <?= isset($data['assignment34']['AD196'])&&$data['assignment34']['AD196']==0.95?'selected':'' ?>>LED</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="AI196" name="AI196" data-cell="AI196" data-formula="IF(Y196=0,' ',30000)"></td>
                                                                    <td class=""><input type="text" class="input-sm form-control text-center width-50" id="AK196" name="AK196" data-cell="AK196" data-formula="AD196"></td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow width-150" name="AN196" id="AN196" data-cell="AN196">
                                                                            <option value="0" <?= isset($data['assignment34']['AN196'])&&$data['assignment34']['AN196']==0?'selected':'' ?>></option>
                                                                            <option value="0.91" <?= isset($data['assignment34']['AN196'])&&$data['assignment34']['AN196']==0.91?'selected':'' ?>>Clean (e.g. hospital, clean room)</option>
                                                                            <option value="0.85" <?= isset($data['assignment34']['AN196'])&&$data['assignment34']['AN196']==0.85?'selected':'' ?>>Normal (e.g. office, warehouse)</option>
                                                                            <option value="0.78" <?= isset($data['assignment34']['AN196'])&&$data['assignment34']['AN196']==0.78?'selected':'' ?>>Dirty (steelworks, welding areas)</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center width-250" id="AR196" name="AR196" data-cell="AR196" data-formula="AN196"
                                                                      value="<?= isset($data['assignment34']['AR196'])?$data['assignment34']['AR196']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center width-50" id="AT196" name="AT196" data-cell="AT196" data-formula="IF(AR196=0,' ',(AK196*AR196))" data-format="0,0[.]00"
                                                                      value="<?= isset($data['assignment34']['AT196'])?$data['assignment34']['AT196']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center width-50" id="AV196" name="AV196" data-cell="AV196" data-formula="IF(O196=0,' ',INT(O196/AT196))"
                                                                      value="<?= isset($data['assignment34']['AV196'])?$data['assignment34']['AV196']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-50" id="AX196" name="AX196" data-cell="AX196"
                                                                      value="<?= isset($data['assignment34']['AX196'])?$data['assignment34']['AX196']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center widht-50" id="AZ196" name="AZ196" data-cell="AZ196" data-formula="IF(AK196=0,' ',IF(AX196>=AV196,'Yes','No'))"
                                                                      value="<?= isset($data['assignment34']['AZ196'])?$data['assignment34']['AZ196']:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-50" id="BB196" name="BB196" data-cell="BB196"
                                                                      value="<?= isset($data['assignment34']['BB196'])?$data['assignment34']['BB196']:'' ?>">
                                                                    </td>

                                                                </tr>
                                                                
                                                            </tbody>
                                                            
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div><!-- close page -->
                                        
                                    </form> 
                                </div>
                                <div class="tab-pane" id="doc-require">
                                    <p>veec doucment requirement</p>
                                </div>
                            </div>
                        </div><!-- close panel-body -->
                    </div><!-- close panel -->
                </div>
            </div><!-- close content -->   
        </div><!-- close container -->

        <!--
        <footer>
          <div class="container">
            <div class="row" style="padding-top:5px">
              <div class="col-md-12 text-right"><h5>©Eminent Service Group PL T/A My Electrician 2015</h5></div>
            </div>
          </div>
        </footer>
        -->


        <!-- script references -->
          <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js') ?>"></script>-->
        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/moment.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/jstat.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/numeral.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-calx-2.2.5.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/select2.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/industry.json') ?>"></script>
        <script src="<?= base_url('asset/js/street.json') ?>"></script>
        <script src="<?= base_url('asset/js/existing_lamp.json') ?>"></script>
        <script src="<?= base_url('asset/js/postcode.json') ?>"></script>
        <!-- <script src="<?= base_url('asset/js/product.json') ?>"></script> -->
        <!-- <script src="<?= base_url('asset/js/installer.json') ?>"></script> -->
        <script type="text/javascript">
            var installer = [<?php if(isset($settings_app['installer']))foreach($settings_app['installer'] as $installer)echo json_encode($installer).','; ?>];
            var product = [<?php if(isset($settings_app['item']))foreach($settings_app['item'] as $item)echo json_encode($item).','; ?>];
            var base_url = '<?= base_url() ?>';
            var site_url = '<?= site_url() ?>';
            var mapsrc = '<?= isset($stored['mapsrc'])?$stored['mapsrc']:'' ?>';
            var signatureData = []
            <?php for ($sign=1; $sign<=3; $sign++): ?>
              signatureData[<?= $sign ?>] = '<?= isset($data['assignment34']["sign$sign"])?$data['assignment34']["sign$sign"]:'' ?>'
            <?php endfor ?>
            var product34 = <?= json_encode($product34) ?>;
        </script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.ui.touch-punch.min.js') ?>" type="text/javascript"></script>
        
        <script src="<?= base_url('asset/js/flashcanvas.js') ?>"></script>
        <script src="<?= base_url('asset/js/jSignature.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/jquery.flot.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/jquery.flot.resize.min.js') ?>"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
        <script src="<?= base_url('asset/js/34form.js') ?>" type="text/javascript"></script>
        
    </body>
</html>