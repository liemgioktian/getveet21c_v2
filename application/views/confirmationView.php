<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>assignment list</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                        <input type="hidden" name="action" id="action">
                        <input name="action" id="action" type="hidden">
                        <!--page 2-->
                        <div class="page" id="page2">
                            <div class="row">
                                <div class="col-sm-4 hidden-xs">
                                    <img src="<?= $logo ?>" style="height:80px" alt="">
                                </div>
                                <div class="col-sm-4">
                                    <h2 class="text-center bold">VEEC 21</h2>
                                    <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                </div>
                                <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                <div class="col-sm-3 hidden-xs">
                                    <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 box_label">
                                    <h5>CONFIRMATION PAGE</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 box_desc">
                                    <div class="row">
                                        <div class="text-center col-xs-12">
                                            <H4>ARE YOU SURE ?</h4>
                                            <?php if(isset($post_url)): ?>
                                            	<form action="<?= $post_url ?>" method="POST" style="display:inline">
                                            		<?php foreach($post_params as $key => $value): ?>
                                            			<input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
                                            		<?php endforeach; ?>
                                            		<input type="submit" name="confirm" class="btn btn-sm btn-success" value="YES" />
                                            	</form>
                                            <?php else : ?>
                                            	<a href="<?= str_replace('/confirm', '', current_url()) ?>" class="btn btn-sm btn-success">YES</a>
                                            <?php endif; ?>
                                            <a href="javascript:window.history.back()" class="btn btn-sm btn-warning">NO</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--close page2-->
                </div>
            </div>      
        </div>
    </body>
</html>