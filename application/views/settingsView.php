<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Setting</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/js/summernote/dist/summernote.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/js/summernote/dist/summernote-bs3.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
    </head>
    <body>
        <div class="container" style="margin-bottom:10px">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <input type="hidden" name="action" id="action">
                            <input name="action" id="action" type="hidden">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>SETTINGS</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page setting-->
                            <div class="page" id="">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <form method="POST" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <div class="col-sm-12 text-center"><h4>Electricians Details</h4></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Electricians Company Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-sm" name="electricians_company_name" size="50" value="<?= isset($electricians_company_name)?$electricians_company_name:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">ABN Number:</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-sm" name="abn_number" size="25" value="<?= isset($abn_number)?$abn_number:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Company Address</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-sm" name="company_address" size="50" value="<?= isset($company_address)?$company_address:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-3">
                                                    <label for="" class="bold col-sm-12">Suburb</label>
                                                    <div class="col-sm-12" style="margin-bottom:10px">
                                                        <input type="text" class="form-control input-sm" name="suburb" size="25" value="<?= isset($suburb)?$suburb:'' ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <label for="" class="bold col-sm-12">postal code</label>
                                                    <div class="col-sm-7" style="margin-bottom:10px">
                                                        <input type="text" <?= $current_user['is_admin']!=1?'disabled="disabled"':'' ?>
                                                        class="form-control input-sm" name="postal_code" size="25" value="<?= isset($postal_code)?$postal_code:'' ?>" />
                                                    </div>
                                                <div class="clearfix"></div>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Telephone Number</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-sm" name="telephone_number" size="25" value="<?= isset($telephone_number)?$telephone_number:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Web Site</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-sm" name="website" size="50" value="<?= isset($website)?$website:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Email Address</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-sm" name="email_address" size="50" value="<?= isset($email_address)?$email_address:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">R.E.C Number</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control input-sm" name="rec_number" size="25" value="<?= isset($rec_number)?$rec_number:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Registered for GST</label>
                                                <div class="col-sm-6">
                                                    <label class="radio-inline">
                                                        <input type="radio" class="" name="gst" id="gst1" <?= isset($gst) && $gst == 1? 'checked':'' ?> value="1"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" class="" name="gst" id="gst2" <?= isset($gst) && $gst == 0? 'checked':'' ?> value="0"> No
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Activity 21C Certificate Price</label>
                                                <div class="col-sm-6">
                                                    <input type="text" <?= !isset($non_super_admin_company)?'disabled="disabled"':'' ?>
                                                    class="form-control input-sm" name="c_certificate" size="25" value="<?= isset($c_certificate)?$c_certificate:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Activity 21D Certificate Price</label>
                                                <div class="col-sm-6">
                                                    <input type="text" <?= !isset($non_super_admin_company)?'disabled="disabled"':'' ?>
                                                    class="form-control input-sm" name="d_certificate" size="25" value="<?= isset($d_certificate)?$d_certificate:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Activity 21E Certificate Price</label>
                                                <div class="col-sm-6">
                                                    <input type="text" <?= !isset($non_super_admin_company)?'disabled="disabled"':'' ?>
                                                    class="form-control input-sm" name="e_certificate" size="25" value="<?= isset($e_certificate)?$e_certificate:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <?php if(isset($non_super_admin_company)): ?>
	                                            <div class="form-group">
	                                                <label for="" class="bold col-sm-3">Client's Activity 21C Certificate Price</label>
	                                                <div class="col-sm-6">
	                                                    <input type="text" class="form-control input-sm" name="client_c_certificate" size="25"
	                                                    value="<?= isset($non_super_admin_company->c_certificate)?$non_super_admin_company->c_certificate:'' ?>" />
	                                                </div>
	                                                <div class="clearfix"></div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label for="" class="bold col-sm-3">Client's Activity 21D Certificate Price</label>
	                                                <div class="col-sm-6">
	                                                    <input type="text" class="form-control input-sm" name="client_d_certificate" size="25"
	                                                    value="<?= isset($non_super_admin_company->d_certificate)?$non_super_admin_company->d_certificate:'' ?>" />
	                                                </div>
	                                                <div class="clearfix"></div>
	                                            </div>
                                                <div class="form-group">
                                                    <label for="" class="bold col-sm-3">Client's Activity 21E Certificate Price</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-sm" name="client_e_certificate" size="25"
                                                        value="<?= isset($non_super_admin_company->e_certificate)?$non_super_admin_company->e_certificate:'' ?>" />
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            <?php endif; ?>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Create New Installer URL</label>
                                                <div class="col-sm-9">
                                                		<?php $installer_signup = site_url($cid . '-' . $this->sluggify->slug($electricians_company_name) . '/installer-signup') ?>
                                                    <a target="_blank" href="<?= $installer_signup ?>"><?= $installer_signup ?></a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Upload Logo</label>
                                                <div class="col-sm-6">
                                                    <input type="file" name="company_logo" />
                                                    <small>*) don't forget to backup existing logo</small>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="checkbox">
                                                <label for="" class="col-sm-3" style="font-weight: bold !important">Lead Manager / Landing Page</label>
                                                <label class="col-sm-6">
                                                    <input type="checkbox" value="1" name="lead_manager_enabled" <?= isset($lead_manager_enabled) && $lead_manager_enabled == 1? 'checked':'' ?>> Enabled
                                                </label>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Lead Manager Url</label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="lead_manager_url" class="input-sm form-control"
                                                        value="<?= isset($lead_manager_url)?$lead_manager_url:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Lead Form Url</label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="lead_form_url" class="input-sm form-control"
                                                        value="<?= isset($lead_form_url)?$lead_form_url:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Landing Page Url</label>
                                                <div class="col-sm-9">
                                                    <a target="_blank" href="<?php echo site_url('form/' . $cid . '-' . $this->sluggify->slug($electricians_company_name)) ?>"><?php echo site_url('form/' . $cid . '-' . $this->sluggify->slug($electricians_company_name)) ?></a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Landing Page Text</label>
                                                <div class="col-sm-9">
                                                    <textarea id="landing_text" type="text" name="landing_text" class="input-sm form-control" style="height: 350px"><?= isset($landing_text)?$landing_text:'' ?></textarea>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Header Image</label>
                                                <div class="col-sm-6">
                                                    <input type="file" name="landing_header_image" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Footer Image</label>
                                                <div class="col-sm-6">
                                                    <input type="file" name="landing_footer_image" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>



                                            <br/>
                                            <div class="col-xs-12">
                                                <br/><b>Installers</b>
                                                <small>*) installer with blank first name will not be saved</small>
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                <table cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                    <thead>
                                                        <tr class="skyblue">
                                                            <th></th>
                                                            <th>First name</th>
                                                            <th>Last name</th>
                                                            <th>A Class Licence No.</th>
                                                            <th>Issue/ Renewal Date</th>
                                                            <th>Expiry Date</th>
                                                            <th>Email address</th>
                                                            <th>Phone number</th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $table_loop=1; if(isset($installer)):foreach($installer as $ins): ?>
                                                        <tr class="">
                                                            <td><?= $table_loop ?><input type="hidden" name="installer[<?= $table_loop ?>][iid]" value="<?= $ins['iid'] ?>" /></td>
                                                            <td><input type="text" <?= $current_user['is_admin']!=1?'disabled="disabled"':'' ?> name="installer[<?= $table_loop ?>][first_name]" value="<?= $ins['first_name'] ?>" /></td>
                                                            <td><input type="text" <?= $current_user['is_admin']!=1?'disabled="disabled"':'' ?> name="installer[<?= $table_loop ?>][last_name]" value="<?= $ins['last_name'] ?>" /></td>
                                                            <td><input type="text" <?= $current_user['is_admin']!=1?'disabled="disabled"':'' ?> name="installer[<?= $table_loop ?>][license_number]" value="<?= $ins['license_number'] ?>" /></td>
                                                            <td><input type="text" <?= $current_user['is_admin']!=1?'disabled="disabled"':'' ?> name="installer[<?= $table_loop ?>][renewal_date]"  class="datepicker" value="<?= $ins['renewal_date'] ?>" /></td>
                                                            <td><input type="text" <?= $current_user['is_admin']!=1?'disabled="disabled"':'' ?> name="installer[<?= $table_loop ?>][expiry_date]"  class="datepicker" value="<?= $ins['expiry_date'] ?>" /></td>
                                                            <td><input type="text" <?= $current_user['is_admin']!=1?'disabled="disabled"':'' ?> name="installer[<?= $table_loop ?>][email_address]" value="<?= $ins['email_address'] ?>" /></td>
                                                            <td><input type="text" <?= $current_user['is_admin']!=1?'disabled="disabled"':'' ?> name="installer[<?= $table_loop ?>][phone_number]" value="<?= $ins['phone_number'] ?>" /></td>
                                                            <td>
                                                            	Upload ID Photo<input type="file" name="installer[<?= $table_loop ?>][photo]" <?= $current_user['is_admin']!=1?'disabled="disabled"':'' ?>/>
                                                            </td>
                                                            <td>
                                                              Upload ID Photo 2<input type="file" name="installer[<?= $table_loop ?>][photo2]" <?= $current_user['is_admin']!=1?'disabled="disabled"':'' ?>/>
                                                            </td>
                                                            <td>
                                                            	<a href="<?= site_url('settingscontroller/delete_installer/'.$ins['iid'].'/confirm') ?>" class="btn btn-sm btn-danger">DELETE</a>
                                                            </td>
                                                        </tr>
                                                        <?php $table_loop++; endforeach; endif; ?>
                                                    </tbody>
                                                    <!--
                                                    <tfoot>
                                                        <tr class="">
                                                            <th>Add New Installer</th>
                                                            <th><input type="text" name="installer[<?= $table_loop ?>][first_name]" /></th>
                                                            <th><input type="text" name="installer[<?= $table_loop ?>][last_name]" /></th>
                                                            <th><input type="text" name="installer[<?= $table_loop ?>][license_number]" /></th>
                                                            <th><input type="text"  class="datepicker" name="installer[<?= $table_loop ?>][renewal_date]" /></th>
                                                            <th><input type="text" class="datepicker" name="installer[<?= $table_loop ?>][expiry_date]" /></th>
                                                            <th><input type="text" name="installer[<?= $table_loop ?>][email_address]" /></th>
                                                            <th><input type="text" name="installer[<?= $table_loop ?>][phone_number]" /></th>
                                                            <th colspan="2">Upload ID Photo<input type="file" name="installer[<?= $table_loop ?>][photo]" /></th>
                                                        </tr>
                                                    </tfoot>
                                                -->
                                                    </table>
                                                </div>
                                            </div>

                                            <br/>
                                            <div class="col-xs-12">
                                                <br/><b>Invoice Item</b>
                                                <small>*) item with blank description will not be saved</small>
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                <table cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                    <thead>
                                                        <tr class="skyblue">
                                                            <th width="75%">Description</th>
                                                            <th>Rate</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $last_item=1; if(isset($item)) :foreach($item as $it): ?>
                                                        <tr>
                                                            <td>
                                                            	<input type="hidden" name="item[<?= $last_item ?>][item_id]" value="<?= $it['item_id'] ?>" />
                                                            	<input type="text" name="item[<?= $last_item ?>][id]" value="<?= $it['id'] ?>" size="100" />
                                                            </td>
                                                            <td><input type="text" name="item[<?= $last_item ?>][rate]" value="<?= $it['rate'] ?>" /></td>
                                                            <td><a href="<?= site_url('settingscontroller/delete_item/'.$it['item_id'].'/confirm') ?>" class="btn btn-sm btn-danger">DELETE</a></td>
                                                        </tr>
                                                        <?php $last_item++; endforeach; endif; ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td><input type="text" name="item[<?= $last_item ?>][id]" size="100" /></td>
                                                            <td colspan="2"><input type="text" name="item[<?= $last_item ?>][rate]" /></td>
                                                        </tr>
                                                        <!--
                                                        <tr>
                                                            <td><button type="button" id="btn_add_invoice" class="btn btn-default btn-xs" data-count="1" style="width:30px"><img style="width:25px" src="<?= base_url('asset/css/add.png') ?>" alt="add">add Item</button></td>
                                                            <td></td>
                                                        </tr>
                                                        -->
                                                    </tfoot>
                                                 </table>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 text-center" style="margin-bottom:20px">
                                            <input type="submit" value="save settings" class="btn btn-primary" />
                                            <a href="<?= $current_user['is_admin'] == 1 ? site_url('settingscontroller') : site_url('settingscontroller/edit/'.$current_user['cid']) ?>" class="btn btn-primary">cancel</a>
                                            </div>
                                            <br/>
                                        </form>

<!--
                                        &nbsp;<hr/>
                                        <h3 class="text-center">ADD USER</h3>
                                        <form action="" method="POST" class="form-horizontal">
			                            <div class="form-group">
			                                <label class="col-lg-3 control-label" for="email">Email</label>
			                                <div class="col-lg-8">
			                                    <input type="hidden" name="cid" value="<?= str_replace('settingscontroller/edit/', '', $this->uri->uri_string()) ?>" />
			            						<input type="email" name="email" class="form-control validate[required]" id="email" placeholder="Email">
			                                </div>
			                            </div>
			                            <div class="form-group">
			                                <label class="col-lg-3 control-label" for="password">Password</label>
			                                <div class="col-lg-8">
			                                    <input type="password" name="password" class="form-control validate[required]" id="password" placeholder="Password">
			                                </div>
			                            </div>
			                            <div class="form-group">
			                                <label class="col-lg-3 control-label" for="confirm_password">Confirm Password</label>
			                                <div class="col-lg-8">
			                                    <input type="password" class="form-control validate[required,equals[password]] " name="confirm_password" id="confirm_password" placeholder="Confirm Password">
			                                </div>
			                            </div>
			                            <div class="form-group">
			                                <div class="col-lg-12 text-center">
			                                	<input type="reset" class="btn btn-danger" />
			                                	<input type="submit" class="btn btn-warning" name="add_user" />
			                                </div>
			                            </div>
                                        </form>
                                    -->
                                    </div>
                                </div>

                            </div>
                            <!--close page setting-->
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!--
        <footer>
          <div class="container">
            <div class="row" style="padding-top:5px">
              <div class="col-md-12 text-right"><h5>©Eminent Service Group PL T/A My Electrician 2015</h5></div>
            </div>
          </div>
        </footer>
        -->
        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.ui.touch-punch.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/setting.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/summernote/dist/summernote.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
        <script>
            $('#landing_text').summernote({

                height: 300,
                iconPrefix: 'glyphicon glyphicon-',
                icons: {
                   lists: {
                        unordered: 'list',
                        ordered: 'list'
                   },
                    link: {
                        link: 'link',
                        edit: 'pencil',
                        unlink: 'link'
                    },
                    image: {
                        image: 'picture'
                    }
               }
            });
        </script>
    </body>
</html>