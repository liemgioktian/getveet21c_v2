<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Profit Calculator</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/select2.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/select2-bootstrap.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('asset/css/jquery.signaturepad.css') ?>" />
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">

    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="content">
                    <div class="panel">
                        <div class="box_label text-center">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>PROFIT CALCULATOR</strong></h4>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form enctype="multipart/form-data" action="" class="form-horizontal" method="post" role="form" id="profitCalc">
                               
                                <div class="page" id="">
                                    <div class="row">
                                        <div class="col-sm-4 hidden-xs">
                                            <img src="<?= $logo ?>" style="height:80px" alt="">
                                        </div>
                                        <div class="col-sm-4">
                                            <h2 class="text-center bold">VEEC 21</h2>
                                            <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                        </div>
                                        <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                        <div class="col-sm-3 hidden-xs">
                                            <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                        </div>
                                    </div>
                                    <div class="row box_label text-center">
                                        <div class="col-xs-12">
                                            <h4 style="margin:5px 0px;"><strong>Revenue</strong></h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Select Item to upgrade</label>
                                                <div class="col-xs-6">
                                                    <select class="input-sm form-control yellow" name="F25" id="F25" data-cell="F25">
                                                        <option value="12V Halogen lamp only" selected>12V Halogen lamp only</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Existing transformet Type</label>
                                                <div class="col-xs-6">
                                                    <select class="input-sm form-control yellow" name="F26" id="F26" data-cell="F26">
                                                        <option value=""></option>
                                                        <option value="Electroninc">Electroninc</option>
                                                        <option value="Magnetic">Magnetic</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Number of Lamps</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm yellow" id="F27" name="F27" data-cell="F27" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Lamp Brand and Model Number</label>
                                                <div class="col-xs-6">
                                                    <select class="input-sm form-control yellow" name="F28" id="F28" data-cell="F28">
                                                        <option value="Primsal PMR166WWHPFB" selected>Primsal PMR166WWHPFB</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Post Code</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm postcode select2-offscreen" id="F29" name="F29" data-cell="F29" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Current VEEC Price</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" value="<?= $setting['c_certificate'] ?>" id="F30" name="F30" data-cell="F30" type="text" data-format="$ 0.00">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Total VEECS</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F31" name="F31" data-cell="F31" type="text" data-formula="F27*0.8715*D31" data-format="0,0[.]" disabled>
                                                    <input class="form-control input-sm" id="E31" name="E31" data-cell="E31" type="hidden">
                                                    <input class="form-control input-sm" id="D31" name="D31" data-cell="D31" type="hidden" data-formula="IF(E31='Regional',1.04,IF(E31='Metropolitan',0.98,''))" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">VEEC Value</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F32" name="F32" data-cell="F32" type="text" data-format="$ 0.00" data-formula="F31*F30" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">VEEC Value less GST</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F33" name="F33" data-cell="F33" type="text" data-format="$ 0.00" data-formula="F32/1.1" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Customer contribution Per Lamp</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F34" name="F34" data-cell="F34" type="text" data-format="$ 0.00">
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="row box_label text-center">
                                        <div class="col-xs-12">
                                            <h4 style="margin:5px 0px;"><strong>Costs</strong></h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Lamp Cost</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F35" name="F35" data-cell="F35" value="6.8" type="text" data-format="$ 0.00">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Lead Cost</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F36" name="F36" data-cell="F36" value="0" type="text" data-format="$ 0.00">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Labour cost price per hr.</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F37" name="F37" data-cell="F37" value="65" type="text" data-format="$ 0.00">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Labour per lamp (allowing 1.5 min per lamp)</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F38" name="F38" data-cell="F38" type="text" data-formula="(F37/60)*1.5" data-format="$ 0.00" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group hidden">
                                                <label for="" class="col-xs-6 bold">Labour per lamp (allowing 12.5 min per lamp)</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F39" name="F39" data-cell="F39" type="text" data-formula="(F37/60)*12.5" data-format="$ 0.00" disabled>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="row box_label text-center">
                                        <div class="col-xs-12">
                                            <h4 style="margin:5px 0px;"><strong>Profit calculation</strong></h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Cost Per Lamp</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F40" name="F40" data-cell="F40" type="text" data-formula="F35+F36+F38" data-format="$ 0.00" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Revenue Per Lamp</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F41" name="F41" data-cell="F41" type="text" data-formula="F33+F34" data-format="$ 0.00" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Job Cost</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F42" name="F42" data-cell="F42" type="text" data-formula="F40*F27" data-format="$ 0.00" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Job Revenue</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F43" name="F43" data-cell="F43" type="text" data-formula="F41*F27" data-format="$ 0.00" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Job Profit</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F44" name="F44" data-cell="F44" type="text" data-formula="F43-F42" data-format="$ 0.00" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Profit Margin %</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F45" name="F45" data-cell="F45" type="text" data-formula="(F44/F43)" data-format="0[.]00 %" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-6 bold">Mark up %</label>
                                                <div class="col-xs-6">
                                                    <input class="form-control input-sm" id="F46" name="F46" data-cell="F46" type="text" data-formula="(F44/F42)" data-format="0[.]00 %" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div><!-- close page-->
                            </form>
                        </div>
                    </div>
                </div>
            </div>      
        </div>


        <!-- script references -->
          <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js') ?>"></script>-->
        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/moment.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/jstat.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/numeral.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-calx-2.1.1.js') ?>"></script>
        <script src="<?= base_url('asset/js/select2.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.ui.touch-punch.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/postcode.json') ?>"></script>
        <script src="<?= base_url('asset/js/profit.js') ?>" type="text/javascript"></script>
    </body>
</html>