<form enctype="multipart/form-data" action="" class="form-horizontal" method="post" role="form" id="survey">
                                        <input type="hidden" id="action1" name="action1">
                                        <div class="page">
                                            <div class="row">
                                                <div class="col-sm-4 hidden-xs">
                                                    <img src="<?= $logo ?>" style="height:80px" alt="">
                                                </div>
                                                <div class="col-sm-4">
                                                    <h2 class="text-center bold">CL VEEC</h2>
                                                    <h4 class="text-center bold" style="margin-bottom:10px">Lighting Survey Form</h4>
                                                </div>
                                                <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                                <div class="col-sm-3 hidden-xs">
                                                    <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 box_label">
                                                    <h5 style="padding-top:5px">Energy Consumer's details</h5>
                                                </div>
                                            </div>
                                            <div class="row box_desc">
                                                <div class="col-sm-12">
                                                    <div class="form-group small_space">
                                                        <div class="col-sm-4">
                                                            <label for="C6">Company name</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C6" name="C6" data-cell="C6"
                                                            value="<?= isset($data['survey']['C6']) ? $data['survey']['C6'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="U6">Contact person</label>
                                                            <input type="text" class="form-control input-sm yellow" id="U6" name="U6" data-cell="U6"
                                                            value="<?= isset($data['survey']['U6']) ? $data['survey']['U6'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="AA6">Contact phone</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AA6" name="AA6" data-cell="AA6"
                                                            value="<?= isset($data['survey']['AA6']) ? $data['survey']['AA6'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label for="AI6">Contact email</label>
                                                            <input type="text" class="form-control input-sm yellow" id="AI6" name="AI6" data-cell="AI6"
                                                            value="<?= isset($data['survey']['AI6']) ? $data['survey']['AI6'] : '' ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group small_space">
                                                        <div class="col-sm-3">
                                                            <label for="C9">Installation street address</label>
                                                            <input type="text" class="form-control input-sm yellow" id="C9" name="C9" data-cell="C9"
                                                            value="<?= isset($data['survey']['C9']) ? $data['survey']['C9'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label for="M9">Installation subrb</label>
                                                            <input type="text" class="form-control input-sm yellow" id="M9" name="M9" data-cell="M9"
                                                            value="<?= isset($data['survey']['M9']) ? $data['survey']['M9'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="W9">State</label>
                                                            <input type="text" value="VIC" class="form-control input-sm text-center" id="W9" name="W9" data-cell="W9"
                                                            value="<?= isset($data['survey']['W9']) ? $data['survey']['W9'] : '' ?>">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label for="Y9">Postcode</label>
                                                            <input type="text" class="form-control input-sm yellow postcode text-center" id="Y9" name="Y9" data-cell="Y9"
                                                            value="<?= isset($data['survey']['Y9']) ? $data['survey']['Y9'] : '' ?>">
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="" class="col-sm-10 col-xs-9">Is the lighting upgrade at this site undertaken as part of building work which required a building permit according to the Building Act 1993 and the Building Regulations 2006 (i.e. a J6 upgrade)? </label>
                                                            <div class="col-sm-2 col-xs-3">
                                                                <select type="text" class="form-control input-sm" id="AW9" name="AW9" data-cell="AW9">
                                                                    <option value="0" <?= isset($data['survey']['AW9']) && $data['survey']['AW9'] == 0?'selected':'' ?>>NO</option>
                                                                    <option value="1" <?= isset($data['survey']['AW9']) && $data['survey']['AW9'] == 1?'selected':'' ?>>YES</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="table-responsive" style="">
                                                        <table id="survey_tbl" cellspasing="0" class="table table-bordered table-condensed table-hover" border="0" cellpadding="0" >
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="2">&nbsp;</th>
                                                                    <th class="skyblue"colspan="6">Existing lighting (baseline)</th>
                                                                    <th class="warm" colspan="6">Upgraded lighting</th>
                                                                    <th colspan="2">&nbsp;</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="small center" style="width:50px !important"></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Room/area</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Building class</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Space type</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Lamp type & ballast/control gear</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Nom. watts</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Control ystem</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Air-Con?</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">No of lamps</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Activy type</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Brand & model number</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">LCP</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Rate Lifetime(hrs)</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Control ystem</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">No of lamps</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">Annual operating hours</label></th>
                                                                    <th class="small center" style="width:50px !important"><label for="">No. VEECs</label></th>

                                                                </tr>
                                                            </thead>
                                                            <tbody id="survey_body" >
                                                              <?php $svbd=15; while($svbd==15 || isset($data['survey']["A$svbd"])): ?>
                                                                <tr id="row<?= $svbd-14 ?>">
                                                                    <td class="text-center">
                                                                        <label for="" id="B<?= $svbd ?>" name="B<?= $svbd ?>" data-cell="B<?= $svbd ?>" data-formula="A<?= $svbd ?>"></label>
                                                                        <input type="hidden" id="A<?= $svbd ?>" name="A<?= $svbd ?>" data-cell="A<?= $svbd ?>" value="<?= $svbd-14 ?>"
                                                                        value="<?= isset($data['survey']["A$svbd"])?$data['survey']["A$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-150" id="C<?= $svbd ?>" name="C<?= $svbd ?>" data-cell="C<?= $svbd ?>"
                                                                      value="<?= isset($data['survey']["C$svbd"])?$data['survey']["C$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow width-250" name="H<?= $svbd ?>" id="H<?= $svbd ?>" data-cell="H<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="Class2_Common" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class2_Common'?'selected':'' ?>>2 - Apartment building \ Block of flats (Common areas)</option>
                                                                            <option value="Class3_Other" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class3_Other'?'selected':'' ?>>3 - Hotel / Motel or Dormitory room (Other than common areas)</option>
                                                                            <option value="Class3_Common" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class3_Common'?'selected':'' ?>>3 - Hotel / Motel or Dormitory common area (common areas) </option>
                                                                            <option value="Class5" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class5'?'selected':'' ?>>5  - Office or professional services building (includes medical consulting rooms) </option>
                                                                            <option value="Class6" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class6'?'selected':'' ?>>6  - Retail shop / Restaurant / Café/ Hairdressers / Service station / Bar (excluding dance or live music venues) </option>
                                                                            <option value="Class7a_open" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class7a_open'?'selected':'' ?>>7a - Carpark - Open air  (open air carparks) </option>
                                                                            <option value="Class7a_undercover" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class7a_undercover'?'selected':'' ?>>7a - Carpark - Undercover  (carparks other than open air) </option>
                                                                            <option value="Class7b" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class7b'?'selected':'' ?>>7b - Warehouse or Wholesale premises  </option>
                                                                            <option value="Class8_DivC" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class8_DivC'?'selected':'' ?>>8  - Factory / Manufacturing facility (Division C ANZSIC) </option>
                                                                            <option value="Class8_Other" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class8_Other'?'selected':'' ?>>8  - Laboratory or factory - Non Manufacturing  (i.e. sawmill) (other than Division C ANZSIC) </option>
                                                                            <option value="Class9a" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class9a'?'selected':'' ?>>9A - Hospital, day surgery or similar healthcare building  </option>
                                                                            <option value="Class9b" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class9b'?'selected':'' ?>>9B - Any public assembly building (e.g. a School, Library, Church, Theatre, etc.)  </option>
                                                                            <option value="Class9c" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class9c'?'selected':'' ?>>9C - Aged care facility (excludes detached retirement units)  </option>
                                                                            <option value="Class10" <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='Class10'?'selected':'' ?>>10 - Non-habitable structures including Fences, Retaining walls, Swimming pools B </option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow width-250" name="J<?= $svbd ?>" id="J<?= $svbd ?>" data-cell="J<?= $svbd ?>">
                                                                            <!--
                                                                            <option value="0" <?= isset($data['survey']["J$svbd"])&&$data['survey']["J$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="A non-habitable structure being a fence, mast, antenna, retaining or free standing wall, swimming pool or the like." <?= isset($data['survey']["H$svbd"])&&$data['survey']["H$svbd"]=='A non-habitable structure being a fence, mast, antenna, retaining or free standing wall, swimming pool or the like.'?'selected':'' ?>>A non-habitable structure being a fence, mast, antenna, retaining or free standing wall, swimming pool or the like.</option>
                                                                            -->
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                        <input type="text" class="input-sm form-control text-center yellow existing_lamp width-250" id="L<?= $svbd ?>" name="L<?= $svbd ?>" data-cell="L<?= $svbd ?>" data-count="<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["L$svbd"])?$data['survey']["L$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="hidden">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="K<?= $svbd ?>" name="K<?= $svbd ?>" data-cell="K<?= $svbd ?>" data-formula="I<?= $svbd ?>&' '&J<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["K$svbd"])?$data['survey']["K$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="I<?= $svbd ?>" name="I<?= $svbd ?>" data-cell="I<?= $svbd ?>" data-formula="H<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["I$svbd"])?$data['survey']["I$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="HA<?= $svbd ?>" name="HA<?= $svbd ?>" data-cell="HA<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["HA$svbd"])?$data['survey']["HA$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="HB<?= $svbd ?>" name="HB<?= $svbd ?>" data-cell="HB<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["HB$svbd"])?$data['survey']["HB$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="HC<?= $svbd ?>" name="HC<?= $svbd ?>" data-cell="HC<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["HC$svbd"])?$data['survey']["HC$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="HD<?= $svbd ?>" name="HD<?= $svbd ?>" data-cell="HD<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["HD$svbd"])?$data['survey']["HD$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="T<?= $svbd ?>" name="T<?= $svbd ?>" data-cell="T<?= $svbd ?>" data-formula="(R<?= $svbd ?>+0)*HA<?= $svbd ?>+HB<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["T$svbd"])?$data['survey']["T$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="AP<?= $svbd ?>" name="AP<?= $svbd ?>" data-cell="AP<?= $svbd ?>" data-formula="IF(AC<?= $svbd ?>='NJ6-AB',0,AC<?= $svbd ?>)"
                                                                        value="<?= isset($data['survey']["AP$svbd"])?$data['survey']["AP$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="R<?= $svbd ?>" name="R<?= $svbd ?>" data-cell="R<?= $svbd ?>" data-formula="IF(HD<?= $svbd ?> = 'yes',IF(S<?= $svbd ?>>37,37,S<?= $svbd ?>),S<?= $svbd ?>)"
                                                                        value="<?= isset($data['survey']["R$svbd"])?$data['survey']["R$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="X<?= $svbd ?>" name="X<?= $svbd ?>" data-cell="X<?= $svbd ?>" data-formula="U<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["T$svbd"])?$data['survey']["T$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="Z<?= $svbd ?>" name="Z<?= $svbd ?>" data-cell="Z<?= $svbd ?>" data-formula="Y<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["Z$svbd"])?$data['survey']["Z$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="AD<?= $svbd ?>" name="AD<?= $svbd ?>" data-cell="AD<?= $svbd ?>" data-formula="IF(AA<?= $svbd ?>=' ',0,IF(AP<?= $svbd ?>=0,(T<?= $svbd ?>*AN<?= $svbd ?>*X<?= $svbd ?>*Z<?= $svbd ?>*AA<?= $svbd ?>)/1000000,(T<?= $svbd ?>*AN<?= $svbd ?>*X<?= $svbd ?>*Z<?= $svbd ?>*AA<?= $svbd ?>)/1000000))"
                                                                        value="<?= isset($data['survey']["AD$svbd"])?$data['survey']["AD$svbd"]:'' ?>">
                                                                        <input type="text" class="input-sm form-control text-center yellow" id="AT<?= $svbd ?>" name="AT<?= $svbd ?>" data-cell="AT<?= $svbd ?>" data-formula="AQ<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["AT$svbd"])?$data['survey']["AT$svbd"]:'' ?>">
                                                                        <input type="text" class="input-sm form-control text-center yellow" id="AV<?= $svbd ?>" name="AV<?= $svbd ?>" data-cell="AV<?= $svbd ?>" data-formula="Y<?= $svbd ?>" data-format="0.0000"
                                                                        value="<?= isset($data['survey']["AV$svbd"])?$data['survey']["AV$svbd"]:'' ?>">
                                                                        <input type="text" class="input-sm form-control text-center yellow" id="AX<?= $svbd ?>" name="AX<?= $svbd ?>" data-cell="AX<?= $svbd ?>" data-formula="IF(AW<?= $svbd ?>=' ',0,IF(AP<?= $svbd ?>=1,((T<?= $svbd ?>*AN<?= $svbd ?>*AT<?= $svbd ?>*AV<?= $svbd ?>*AW<?= $svbd ?>)/1000000),IF(AP<?= $svbd ?>=2,((AM<?= $svbd ?>*AN<?= $svbd ?>*AT<?= $svbd ?>*AV<?= $svbd ?>*AW<?= $svbd ?>)/1000000),IF(AP<?= $svbd ?>=3,((AM<?= $svbd ?>*AN<?= $svbd ?>*AT<?= $svbd ?>*AV<?= $svbd ?>*AW<?= $svbd ?>)/1000000),((AM<?= $svbd ?>*AN<?= $svbd ?>*AT<?= $svbd ?>*AV<?= $svbd ?>*AW<?= $svbd ?>)/1000000)))))"
                                                                        value="<?= isset($data['survey']["AX$svbd"])?$data['survey']["AX$svbd"]:'' ?>">
                                                                        <input type="text" class="input-sm form-control text-center yellow" id="AZ<?= $svbd ?>" name="AZ<?= $svbd ?>" data-cell="AZ<?= $svbd ?>" data-formula="IF(AW<?= $svbd ?>=' ',' ',(AD<?= $svbd ?>-AX<?= $svbd ?>))"
                                                                        value="<?= isset($data['survey']["AZ$svbd"])?$data['survey']["AZ$svbd"]:'' ?>">
                                                                        <?php if($svbd==15): ?>
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="Y10" name="Y10" data-cell="Y10"
                                                                        value="<?= isset($data['survey']['Y10'])?$data['survey']['Y10']:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" id="BS106" name="BS106" data-cell="BS106" data-formula="IF(Y10='Metropolitan',0.98,IF(Y10='Regional',1.04,0))"
                                                                        value="<?= isset($data['survey']['BS106'])?$data['survey']['BS106']:'' ?>">
                                                                        <?php endif; ?>

                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-50" id="S<?= $svbd ?>" name="S<?= $svbd ?>" data-cell="S<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["S$svbd"])?$data['survey']["S$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow width-250" name="U<?= $svbd ?>" id="U<?= $svbd ?>" data-cell="U<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="1" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==1?'selected':'' ?>>No control system</option>
                                                                            <option value="0.7" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.7?'selected':'' ?>>Occupancy sensor</option>
                                                                            <option value="0.7" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.7?'selected':'' ?>>Day-light linked control</option>
                                                                            <option value="0.85" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.85?'selected':'' ?>>Programmable dimming</option>
                                                                            <option value="0.9" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.9?'selected':'' ?>>Manual dimming</option>
                                                                            <option value="0.744" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.744?'selected':'' ?>>VRU</option>
                                                                            <option value="0.6" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.6?'selected':'' ?>>Occupancy & Daylight Control</option>
                                                                            <option value="0.6" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.6?'selected':'' ?>>Occupancy & Program. Dimmer</option>
                                                                            <option value="0.63" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.63?'selected':'' ?>>Occupancy & Manual Dimmer</option>
                                                                            <option value="0.6" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.6?'selected':'' ?>>Occupancy & VRU</option>
                                                                            <option value="0.6" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.6?'selected':'' ?>>Daylight Control & Program. Dimmer</option>
                                                                            <option value="0.63" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.63?'selected':'' ?>>Daylight Control & Manual Dimmer</option>
                                                                            <option value="0.6" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.6?'selected':'' ?>>Daylight Control & VRU</option>
                                                                            <option value="0.765" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.765?'selected':'' ?>>Program. Dimmer & Manual Dimmer</option>
                                                                            <option value="0.6324" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.6324?'selected':'' ?>>Program. Dimmer & VRU</option>
                                                                            <option value="0.6696" <?= isset($data['survey']["U$svbd"])&&$data['survey']["U$svbd"]==0.6696?'selected':'' ?>>Manual Dimmer & VRU</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow" name="Y<?= $svbd ?>" id="Y<?= $svbd ?>" data-cell="Y<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["Y$svbd"])&&$data['survey']["Y$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="1" <?= isset($data['survey']["Y$svbd"])&&$data['survey']["Y$svbd"]==1?'selected':'' ?>>No</option>
                                                                            <option value="1.05" <?= isset($data['survey']["Y$svbd"])&&$data['survey']["Y$svbd"]==1.05?'selected':'' ?>>Yes</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-50" id="AA<?= $svbd ?>" name="AA<?= $svbd ?>" data-cell="AA<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["AA$svbd"])?$data['survey']["AA$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow width-250" name="AC<?= $svbd ?>" id="AC<?= $svbd ?>" data-cell="AC<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["AC$svbd"])&&$data['survey']["AC$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="1" <?= isset($data['survey']["AC$svbd"])&&$data['survey']["AC$svbd"]==1?'selected':'' ?>>Delamping from multiple lamp fittings (NJ6-A)</option>
                                                                            <option value="2" <?= isset($data['survey']["AC$svbd"])&&$data['survey']["AC$svbd"]==2?'selected':'' ?>>Replace lamp only (NJ6-D)</option>
                                                                            <option value="3" <?= isset($data['survey']["AC$svbd"])&&$data['survey']["AC$svbd"]==3?'selected':'' ?>>Replace lamp and control gear (NJ6-C)</option>
                                                                            <option value="3" <?= isset($data['survey']["AC$svbd"])&&$data['survey']["AC$svbd"]==3?'selected':'' ?>>Replace full luminaire (NJ6-C)</option>
                                                                            <option value="NJ6-AB" <?= isset($data['survey']["AC$svbd"])&&$data['survey']["AC$svbd"]=='NJ6-AB'?'selected':'' ?>>Install lighting control device or sensor only (NJ6-AB)</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-250 product34" id="AE<?= $svbd ?>" name="AE<?= $svbd ?>" data-cell="AE<?= $svbd ?>" data-count="<?= $svbd ?>"
                                                                      value="<?= isset($data['survey']["AA$svbd"])?$data['survey']["AA$svbd"]:'' ?>">
                                                                      <input type="hidden" class="input-sm form-control text-center yellow width-250" id="ZE<?= $svbd ?>" name="ZE<?= $svbd ?>" data-cell="ZE<?= $svbd ?>"
                                                                      value="<?= isset($data['survey']["ZE$svbd"])?$data['survey']["ZE$svbd"]:'' ?>">
                                                                      <input type="hidden" class="input-sm form-control text-center yellow width-250" id="ZF<?= $svbd ?>" name="ZF<?= $svbd ?>" data-cell="ZF<?= $svbd ?>"
                                                                      value="<?= isset($data['survey']["ZF$svbd"])?$data['survey']["ZF$svbd"]:'' ?>">
                                                                      <input type="hidden" class="input-sm form-control text-center yellow width-250" id="ZG<?= $svbd ?>" name="ZG<?= $svbd ?>" data-cell="ZG<?= $svbd ?>" data-formula="CONCATENATE(ZE<?= $svbd ?>,' ',ZF<?= $svbd ?>)"
                                                                      value="<?= isset($data['survey']["ZG$svbd"])?$data['survey']["ZG$svbd"]:'' ?>">
                                                                      <input type="hidden" id="PID<?= $svbd ?>" name="PID<?= $svbd ?>" data-cell="PID<?= $svbd ?>" value="<?= isset($data['survey']["PID$svbd"])?$data['survey']["PID$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-50" id="AM<?= $svbd ?>" name="AM<?= $svbd ?>" data-cell="AM<?= $svbd ?>" 
                                                                        value="<?= isset($data['survey']["AM$svbd"])?$data['survey']["AM$svbd"]:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow width-50" id="AN<?= $svbd ?>" name="AN<?= $svbd ?>" data-cell="AN<?= $svbd ?>" data-formula="IF(AP<?= $svbd ?>=1,AO<?= $svbd ?>,IF(AP<?= $svbd ?>=2,AO<?= $svbd ?>,IF(AP<?= $svbd ?>=3,(10*AY<?= $svbd ?>),5*AY<?= $svbd ?>)))"
                                                                        value="<?= isset($data['survey']["AN$svbd"])?$data['survey']["AN$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow" id="AO<?= $svbd ?>" name="AO<?= $svbd ?>" data-cell="AO<?= $svbd ?>" data-format="0,0[.]"
                                                                        value="<?= isset($data['survey']["AO$svbd"])?$data['survey']["AO$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow width-250" name="AQ<?= $svbd ?>" id="AQ<?= $svbd ?>" data-cell="AQ<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="1" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==1?'selected':'' ?>>No control system</option>
                                                                            <option value="0.7" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.7?'selected':'' ?>>Occupancy sensor</option>
                                                                            <option value="0.7" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.7?'selected':'' ?>>Day-light linked control</option>
                                                                            <option value="0.85" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.85?'selected':'' ?>>Programmable dimming</option>
                                                                            <option value="0.9" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.9?'selected':'' ?>>Manual dimming</option>
                                                                            <option value="0.744" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.744?'selected':'' ?>>VRU</option>
                                                                            <option value="0.6" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.6?'selected':'' ?>>Occupancy & Daylight Control</option>
                                                                            <option value="0.6" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.6?'selected':'' ?>>Occupancy & Program. Dimmer</option>
                                                                            <option value="0.63" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.63?'selected':'' ?>>Occupancy & Manual Dimmer</option>
                                                                            <option value="0.6" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.6?'selected':'' ?>>Occupancy & VRU</option>
                                                                            <option value="0.6" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.6?'selected':'' ?>>Daylight Control & Program. Dimmer</option>
                                                                            <option value="0.63" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.63?'selected':'' ?>>Daylight Control & Manual Dimmer</option>
                                                                            <option value="0.6" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.6?'selected':'' ?>>Daylight Control & VRU</option>
                                                                            <option value="0.765" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.765?'selected':'' ?>>Program. Dimmer & Manual Dimmer</option>
                                                                            <option value="0.6324" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.6324?'selected':'' ?>>Program. Dimmer & VRU</option>
                                                                            <option value="0.6696" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0.6696?'selected':'' ?>>Manual Dimmer & VRU</option>
                                                                        </select>
                                                                    </td>
                                                                    <!--
                                                                    <td class="">
                                                                        <select class="input-sm form-control yellow" name="AQ<?= $svbd ?>" id="AQ<?= $svbd ?>" data-cell="AQ<?= $svbd ?>">
                                                                            <option value="0" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==0?'selected':'' ?>></option>
                                                                            <option value="1" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==1?'selected':'' ?>>No</option>
                                                                            <option value="1.05" <?= isset($data['survey']["AQ$svbd"])&&$data['survey']["AQ$svbd"]==1.05?'selected':'' ?>>Yes</option>
                                                                        </select>
                                                                    </td>
                                                                    -->
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow width-50" id="AW<?= $svbd ?>" name="AW<?= $svbd ?>" data-cell="AW<?= $svbd ?>" data-formula="AA<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["AW$svbd"])?$data['survey']["AW$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow" id="AY<?= $svbd ?>" name="AY<?= $svbd ?>" data-cell="AY<?= $svbd ?>" data-formula="J<?= $svbd ?>"
                                                                        value="<?= isset($data['survey']["AY$svbd"])?$data['survey']["AY$svbd"]:'' ?>">
                                                                    </td>
                                                                    <td class="">
                                                                      <input type="text" class="input-sm form-control text-center yellow" id="BA<?= $svbd ?>" name="BA<?= $svbd ?>" data-cell="BA<?= $svbd ?>" data-formula="IFERROR(IF(AZ<?= $svbd ?>>0,IF(AW9=0,(AZ<?= $svbd ?>*BS106)*1.095,' '),' '),' ')" data-format="0.0"
                                                                        value="<?= isset($data['survey']["BA$svbd"])?$data['survey']["BA$svbd"]:'' ?>">
                                                                    </td>
                                                                </tr>
                                                                <?php $svbd++; endwhile; ?>
                                                                
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <td colspan="8" class="text-right"><label for="" class="text-right">Total existing lamp</label></td>
                                                                    <td>
                                                                        <input type="text" class="input-sm form-control text-center" id="W50" name="W50" data-cell="W50" data-formula="SUM(AA15:AA25)"
                                                                        value="<?= isset($data['survey']['W50'])?$data['survey']['W50']:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center" id="Z50" name="Z50" data-cell="Z50" data-formula="SUM(AD15:AD25)" data-format="0.00"
                                                                        value="<?= isset($data['survey']['Z50'])?$data['survey']['Z50']:'' ?>">
                                                                        <input type="hidden" class="input-sm form-control text-center" id="AT50" name="AT50" data-cell="AT50" data-formula="SUM(AX15:AX25)" data-format="0.00"
                                                                        value="<?= isset($data['survey']['AT50'])?$data['survey']['AT50']:'' ?>">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td colspan="4" class="text-right"><label for="" class="text-right">Total upgrade lamp</label></td>
                                                                    <td>
                                                                      <input type="text" class="input-sm form-control text-center" id="AS50" name="AS50" data-cell="AS50" data-formula="SUM(AW15:AW25)"
                                                                        value="<?= isset($data['survey']['AS50'])?$data['survey']['AS50']:'' ?>">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="15" class="text-right"><label for="">Total estimate VEECs</label></td>
                                                                    <td colspan="2">
                                                                      <input type="text" class="input-sm form-control text-center" id="AV51" name="AV51" data-cell="AV51" data-formula="ROUND(SUM(BA15:BA25),0)"
                                                                        value="<?= isset($data['survey']['AV51'])?$data['survey']['AV51']:'' ?>">
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 text-right">
                                                            <button type="button" class="btn btn-sm btn-primary" id="btn-add-survey">add row</button>
                                                        </div>

                                                <div class="col-sm-12 text-center" style="margin-top:20px">
                                                    <a href="<?= site_url('maincontroller/subform') ?>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
                                                    <a href="#quote-page" data-toggle="tab" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>