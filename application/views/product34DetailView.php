<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Product 34 Form</title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
    <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
  </head>
  <body>
    
    <div class="container">

      <div class="row">
        <div class="col-sm-12">
          <?php include APPPATH.'/views/menuView.php'; ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12" id="content">
          <div class="panel">
            <div class="panel-body">
              
              <form enctype="multipart/form-data" action="" class="form-horizontal" method="post" role="form" id="product34">
                <div class="page" id="page1">
                  
                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-2 col-sm-offset-3 col-xs-5">BRAND</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="brand" value="<?= isset($brand)?$brand:'' ?>" />
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-2 col-sm-offset-3 col-xs-5">MODEL NUMBER</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="modelNumber" value="<?= isset($modelNumber)?$modelNumber:'' ?>" />
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-2 col-sm-offset-3 col-xs-5">LCP</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="lcp" value="<?= isset($lcp)?$lcp:'' ?>" />
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="form-group">
                      <label for="" class="col-sm-2 col-sm-offset-3 col-xs-5">RATED LIFETIME</label>
                      <div class="col-sm-3 col-xs-7">
                        <input type="text" class="form-control input-sm" name="ratedLifetime" value="<?= isset($ratedLifetime)?$ratedLifetime:'' ?>" />
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="form-group text-right">
                      <label for="" class="col-sm-2 col-sm-offset-3 col-xs-5"></label>
                      <div class="col-sm-3 col-xs-7">
                        <a href="<?= site_url('product34controller') ?>" class="btn btn-sm">CANCEL</a>
                        <input type="submit" class="btn btn-sm" value="SAVE" />
                      </div>
                    </div>
                  </div>
                  
                  
                </div>
              </form>
              
            </div>
          </div>
        </div>
      </div>
      
    </div>

    </body>
</html>