<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Store</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
				
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>BUY PRIMSAL PRODUCT</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="page" id="">
                                <div>
<script type="text/javascript" src="https://app.ecwid.com/script.js?7303664"></script> <script type="text/javascript"> xProductBrowser("categoriesPerRow=3","views=grid(3,3) list(10) table(20)","categoryView=grid","searchView=list","style="); </script> <noscript>Your browser does not support JavaScript. Please proceed to <a href="http://mdemo.ecwid.com">HTML version of Eugene's store</a></noscript> 
​</div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>      
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
        <script type="text/javascript">
        	jQuery('.filter-date').datepicker();
        </script>
    </body>
</html>