<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Relationship Agreement</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
<style>
<!--
 /* Font Definitions */
 @font-face
    {font-family:Calibri;
    panose-1:2 15 5 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
    {margin-top:0cm;
    margin-right:0cm;
    margin-bottom:8.0pt;
    margin-left:0cm;
    line-height:107%;
    font-size:11.0pt;
    font-family:"Calibri","sans-serif";}
.MsoChpDefault
    {font-size:11.0pt;
    font-family:"Calibri","sans-serif";}
.MsoPapDefault
    {margin-bottom:8.0pt;
    line-height:107%;}
 /* Page Definitions */
 @page WordSection1
    {size:595.0pt 841.9pt;
    margin:71.6pt 72.0pt 21.9pt 72.0pt;}
div.WordSection1
    {page:WordSection1;}
@page WordSection2
    {size:595.0pt 841.9pt;
    margin:71.6pt 71.0pt 21.9pt 72.0pt;}
div.WordSection2
    {page:WordSection2;}
@page WordSection3
    {size:595.0pt 841.9pt;
    margin:72.0pt 76.0pt 21.9pt 72.0pt;}
div.WordSection3
    {page:WordSection3;}
@page WordSection4
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection4
    {page:WordSection4;}
@page WordSection5
    {size:595.0pt 841.9pt;
    margin:72.0pt 74.0pt 21.9pt 128.65pt;}
div.WordSection5
    {page:WordSection5;}
@page WordSection6
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection6
    {page:WordSection6;}
@page WordSection7
    {size:595.0pt 841.9pt;
    margin:71.6pt 72.0pt 21.9pt 128.65pt;}
div.WordSection7
    {page:WordSection7;}
@page WordSection8
    {size:595.0pt 841.9pt;
    margin:71.6pt 71.0pt 21.9pt 72.0pt;}
div.WordSection8
    {page:WordSection8;}
@page WordSection9
    {size:595.0pt 841.9pt;
    margin:72.0pt 72.0pt 21.9pt 100.35pt;}
div.WordSection9
    {page:WordSection9;}
@page WordSection10
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection10
    {page:WordSection10;}
@page WordSection11
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection11
    {page:WordSection11;}
@page WordSection12
    {size:595.0pt 841.9pt;
    margin:72.0pt 72.0pt 21.9pt 100.35pt;}
div.WordSection12
    {page:WordSection12;}
@page WordSection13
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection13
    {page:WordSection13;}
@page WordSection14
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection14
    {page:WordSection14;}
@page WordSection15
    {size:595.0pt 841.9pt;
    margin:72.0pt 72.0pt 21.9pt 72.0pt;}
div.WordSection15
    {page:WordSection15;}
@page WordSection16
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection16
    {page:WordSection16;}
@page WordSection17
    {size:595.0pt 841.9pt;
    margin:72.0pt 73.0pt 21.9pt 100.35pt;}
div.WordSection17
    {page:WordSection17;}
@page WordSection18
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection18
    {page:WordSection18;}
@page WordSection19
    {size:595.0pt 841.9pt;
    margin:71.6pt 80.0pt 21.9pt 72.0pt;}
div.WordSection19
    {page:WordSection19;}
@page WordSection20
    {size:595.0pt 841.9pt;
    margin:71.6pt 71.0pt 21.9pt 72.0pt;}
div.WordSection20
    {page:WordSection20;}
@page WordSection21
    {size:595.0pt 841.9pt;
    margin:71.6pt 72.0pt 21.9pt 72.0pt;}
div.WordSection21
    {page:WordSection21;}
@page WordSection22
    {size:595.0pt 841.9pt;
    margin:71.6pt 71.0pt 21.9pt 72.0pt;}
div.WordSection22
    {page:WordSection22;}
@page WordSection23
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection23
    {page:WordSection23;}
@page WordSection24
    {size:595.0pt 841.9pt;
    margin:71.6pt 72.0pt 21.9pt 72.0pt;}
div.WordSection24
    {page:WordSection24;}
@page WordSection25
    {size:595.0pt 841.9pt;
    margin:71.6pt 71.0pt 21.9pt 72.0pt;}
div.WordSection25
    {page:WordSection25;}
@page WordSection26
    {size:595.0pt 841.9pt;
    margin:72.0pt 76.0pt 21.9pt 100.0pt;}
div.WordSection26
    {page:WordSection26;}
@page WordSection27
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection27
    {page:WordSection27;}
@page WordSection28
    {size:595.0pt 841.9pt;
    margin:72.0pt 72.0pt 21.9pt 72.0pt;}
div.WordSection28
    {page:WordSection28;}
@page WordSection29
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection29
    {page:WordSection29;}
@page WordSection30
    {size:595.0pt 841.9pt;
    margin:72.0pt 73.0pt 21.9pt 72.0pt;}
div.WordSection30
    {page:WordSection30;}
@page WordSection31
    {size:595.0pt 841.9pt;
    margin:72.0pt 71.0pt 21.9pt 72.0pt;}
div.WordSection31
    {page:WordSection31;}
 /* List Definitions */
 ol
    {margin-bottom:0cm;}
ul
    {margin-bottom:0cm;}
-->
</style>
    </head>
    <body>
        
        <div class="container">
           
            <div class="row">
				
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>RELATIONSHIP AGREEMENT</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="page" id="">
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-10">
                                        <div class=WordSection1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><a name=page1></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>IN TERMS OF AN
AGREEMENT made this                     day of                                   2015.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>BETWEEN:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.15pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:190.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>My
Electrician</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:141.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>ABN
__________ (<b><i>client to insert</i></b>)</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:106.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>of
__________ (<b><i>client to insert physical address</i></b>)</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>


<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:190.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>(the
“Solution Provider”)</span></p>



<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.15pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:210.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>-
and -</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:126.0pt;margin-bottom:
0cm;margin-left:126.0pt;margin-bottom:.0001pt;text-indent:4.3pt;line-height:
96%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:96%;font-family:"Times New Roman","serif"'>_____________
(<b><i>client to insert name</i></b>) ABN/ACN __________ (<b><i>client to
insert</i></b>)</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.2pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:106.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>of
__________ (<b><i>client to insert physical address</i></b>)</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:311.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>(the
“Installation Company”)</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.15pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:137.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
none'><b><u><span lang=EN-AU style='font-size:14.0pt;font-family:"Times New Roman","serif"'>TERMS
AND CONDITIONS</span></u></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><b><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>INTRODUCTION:</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.55pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-indent:-28.0pt;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman","serif"'>A.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman","serif"'>The Certificate Creation Partner (“CCP”)
provides a range of services to participants in environmental markets,
including product accreditation and Environmental Certificate Creation and
monetisation and is to be nominated from time to time by and in the absolute
and sole discretion of My Electrician. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:6.0pt;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-indent:-28.0pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>B.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>My Electrician provides a range of
services to participants in environmental markets, including product supply,
Software systems for Environmental Certificate creation and monetisation. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.25pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:27.0pt;margin-bottom:
0cm;margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.0pt;line-height:97%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>C.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>The Installation Company operates a
business that is involved in the supply and/or installation of Systems for
System Owners. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:12.0pt;margin-bottom:
0cm;margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.0pt;line-height:104%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>D.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>The Installation Company wishes to work
with My Electrician on an ongoing basis in respect of a range of business
activities including but not limited to transactions with System Owners. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.25pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>E.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>From
time to time the Installation Company may: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:9.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:104%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:104%;font-family:
"Times New Roman","serif"'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>offer System Owners a discount in return
for the System Owner entering into an assignment agreement with My Electrician
or its nominated Certificate Creation Partner; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.45pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:99%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:99%;font-family:
"Times New Roman","serif"'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:99%;
font-family:"Times New Roman","serif"'>utilise My Electrician’s online
transaction portal (“MOP”). </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>



<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection3>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><a name=page2></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-indent:-28.0pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>F.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>This document specifies the terms on
which My Electrician and the Installation Company have agreed to work together
and additional agreements may be entered into in respect of individual
transactions and other things. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.7pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><b><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>TERMS:</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.65pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Definitions
and Interpretation </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Definitions:
For the purposes of this Agreement: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>“<b><i>Accept</i></b>”
means, in respect of: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:13.0pt;margin-bottom:
0cm;margin-left:128.0pt;margin-bottom:.0001pt;text-indent:-28.75pt;line-height:
102%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:11.5pt;line-height:102%;font-family:"Times New Roman","serif"'>i.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:11.5pt;line-height:102%;
font-family:"Times New Roman","serif"'>a Submitted Assignment Agreement, the
process by which My Electrician becomes bound by that Assignment Agreement; and
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.65pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:128.0pt;margin-bottom:.0001pt;text-indent:-28.75pt;line-height:
109%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:109%;font-family:"Times New Roman","serif"'>ii.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:109%;
font-family:"Times New Roman","serif"'>this Agreement the acceptance by My
Electrician of any Purchase Orders effective immediately upon My Electrician
transmitting electronically its acceptance of any Purchaser Order to any such
Electronic address as it may have received from the person who requests such
Purchase Order (“the purchaser”). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.6pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>“<b><i>Administrator</i></b>”
includes a liquidator, receiver or receiver and manager </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:normal;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>(whether
provisional or otherwise). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:8.0pt;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>1.1.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>“<b><i>Agreement</i></b>” means this
Agreement and governs all future dealings between My Electrician and the
Installation Company for the purposes of Introduction paragraph B. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>1.1.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>“<b><i>Assignment Agreement</i></b>”
means an agreement between My Electrician and a System Owner, in which the
System Owner assigns its rights to create a Parcel of Environmental
Certificates to My Electrician. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.4pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>“<b><i>CER</i></b>”
means the Clean Energy Regulator. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:5.0pt;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>1.1.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>“<b><i>Certificate Creation Partner</i></b>
(“<b><i>CCP</i></b>”)” means an accredited person under VEET who provides the
services described in paragraph A of the Introduction. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.2pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>1.1.7<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>“<b><i>Code of Conduct</i></b>” means
any set of rules or other obligations that are a condition of membership of an
industry association to which a Party belongs. </span></p>
</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>


<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection5>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><a name=page3></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
96%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:96%;font-family:"Times New Roman","serif"'>1.1.8<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>“<b><i>Confidential Information</i></b>”
means the confidential information described in Clause 11, My Electrician’s
Industrial Property, Intellectual </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.1pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:normal;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Property
and Know-How. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1.9<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>“<b><i>Data</i></b>”
means information contained within the MOP, including Form </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:normal;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Data
and Solution Data. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1.10<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>“<b><i>Defaulting
Party</i></b>” has the meaning given in clause 7. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1.11<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>“<b><i>Energy
Saving Certificate</i></b>” has the meaning given in the Electricity </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:normal;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Supply
Act 1995 (NSW). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
109%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:109%;font-family:"Times New Roman","serif"'>1.1.12<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:109%;
font-family:"Times New Roman","serif"'>“<b><i>Environmental Certificate</i></b>”
means a tradeable certificate that can be created to represent the
environmental improvement created through the deployment of a System, and
includes Small-scale Technology Certificates, Large Generation Certificates,
Victorian Energy Efficiency Certificates, and Energy Saving Certificates. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.65pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1.13<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>“<b><i>ESC</i></b>”
means Essential Services Commission. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:7.0pt;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>1.1.14<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>“<b><i>Form Data</i></b>” means Data
that is presented to the Installation Company by MOP to assist in the entry of
Solution Data. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:13.0pt;margin-bottom:
0cm;margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-42.35pt;line-height:97%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>1.1.15<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>“<b><i>My Electrician</i></b>” has the
meaning given at the commencement of this document. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>1.1.16<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>“<b><i>GST</i></b>” means Goods and
Services Tax, as that phrase is defined in the A New Tax System (Goods and
Services Tax) Act 1999 (Cth). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:6.0pt;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
111%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:111%;font-family:"Times New Roman","serif"'>1.1.17<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:111%;
font-family:"Times New Roman","serif"'>“<b><i>Industrial Property</i></b>”
means all My Electrician’s (whether or not in documentary visual, oral, machine
readable, electronic, digital, tangible or other forms) designs,
specifications, artwork, trademarks, copyright, business names, trade secrets,
methodologies, insignia, logogram, brochures, promotional material, advertising
layouts, bromides, illustrations, client lists, master files of all
administration forms, disseminated information and other industrial property
the interest in which is the property of My Electrician. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.6pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1.18<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>“<b><i>Injured
Party</i></b>” has the meaning given in clause 7. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:12.0pt;margin-bottom:
0cm;margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-42.35pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>1.1.19<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>“<b><i>Insolvent</i></b>” means the
occurrence of any one or more of the following events in respect of a person: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
4.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:13.0pt;margin-bottom:
0cm;margin-left:71.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.75pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>an application is made that it be wound
up, declared bankrupt or that an Administrator be appointed; </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection7>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:71.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.75pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><a name=page4></a><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>an
Administrator is appointed to any of its assets; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
4.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:13.0pt;margin-bottom:
0cm;margin-left:71.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.75pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>c)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>it enters into an arrangement with its
creditors (or proposes to do so); </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
5.1pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:40.0pt;margin-bottom:
0cm;margin-left:71.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.75pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>d)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>it makes an assignment to benefit one or
more creditors (or proposes to do so); </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
5.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:28.0pt;margin-bottom:
0cm;margin-left:71.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.75pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>e)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>it is insolvent, states that it is
insolvent or it is presumed to be insolvent under an applicable law; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
5.1pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:22.0pt;margin-bottom:
0cm;margin-left:71.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.75pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>f)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>it becomes an insolvent under
administration or action is taken which could result in that event; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:71.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.75pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>g)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a
writ of execution is levied against it or its property; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:71.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.75pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>h)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>it
ceases to carry on business or threatens to do so; or </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
5.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:71.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.75pt;line-height:102%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:11.5pt;line-height:102%;font-family:
"Times New Roman","serif"'>i)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:11.5pt;line-height:102%;
font-family:"Times New Roman","serif"'>anything occurs under the law of any
applicable jurisdiction which has a substantially similar effect to paragraphs
a) - h) above. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.55pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:6.0pt;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
115%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:11.5pt;line-height:115%;font-family:"Times New Roman","serif"'>1.1.20<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:11.5pt;line-height:115%;
font-family:"Times New Roman","serif"'>“<b><i>Intellectual Property</i></b>”
means all My Electrician’s rights and industrial property rights, titles,
interests in and to data, instructions, plans, formulae, technology, computer
software, designs, process descriptions, reports, results, technical advice and
trade secrets whether in documentary, visual, oral, machine, readable,
electronic or other forms. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.95pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:5.0pt;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
109%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:109%;font-family:"Times New Roman","serif"'>1.1.21<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:109%;
font-family:"Times New Roman","serif"'>“<b><i>Know-how</i></b>” means My
Electrician’s expertise in the supply of services described in Introduction
paragraph B, marketing of My Electrician’s products through the System,
techniques developed business systems and methods of operation and the business
trading name My Electric some of which is secret. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.65pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1.22<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>“<b><i>MOP</i></b>”
has the meaning given in paragraph E 2 of the Introduction. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>1.1.23<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>“<b><i>Offer Price</i></b>” means, on
any given day and in respect of a particular type of Environmental Certificate,
the price specified by My Electrician for that particular type of Environmental
Certificate on that day. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:42.35pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>1.1.24<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>“<b><i>Parcel of Environmental
Certificates</i></b>” means, in respect of a specific System, the Environmental
Certificates that may be created as a result of the installation or operation
of that System. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:16.0pt;margin-bottom:
0cm;margin-left:42.35pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>1.1.25<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>“<b><i>Parcel Volume</i></b>” means, in
respect of a particular Parcel of Environmental Certificates, the number of
Environmental Certificates contained in that parcel. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:43.0pt;margin-bottom:
0cm;margin-left:42.35pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-42.35pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>1.1.26<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>“<b><i>Parties</i></b>” means My
Electrician, Installation Company and their transferees, assigns and successors
in title. </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection9>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><a name=page5></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:11.0pt;margin-bottom:
0cm;margin-left:70.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-42.35pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>1.1.27<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>“<b><i>Penalty Interest Rate</i></b>”
means the rate specified in the Penalty Interest Rates Act 1983 (Vic). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:70.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.1.28<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>“<b><i>Privacy
Act</i></b>” means the Privacy Act 1988 (Cth). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:70.65pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
109%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:109%;font-family:"Times New Roman","serif"'>1.1.29<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:109%;
font-family:"Times New Roman","serif"'>“<b><i>Purchase Order</i></b>” means any
request from time to time by the Installation Company of My Electrician to
provide to System Owners the activities contained in paragraph B of the
Introduction to these Terms and Conditions. Upon acceptance by My Electrician
of such Purchase Order the Terms and Conditions of this Agreement come into
operation. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.65pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:4.0pt;margin-bottom:0cm;
margin-left:70.65pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
107%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman","serif"'>1.1.30<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman","serif"'>“<b><i>Regulator</i></b>” means a
government department or agency that has responsibility for the administration
of programs involving or associated with Environmental Certificates, and
includes the CER, the ESC and IPART. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:16.0pt;margin-bottom:
0cm;margin-left:70.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-42.35pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>1.1.31<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>“<b><i>Solution Data</i></b>” means Data
entered into the MOP by the Installation Company that is not Form Data. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:70.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>1.1.32<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>“<b><i>Solution Provider</i></b>” has
the meaning given at the commencement of this document. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:18.0pt;margin-bottom:
0cm;margin-left:70.65pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>1.1.33<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>“<b><i>Submit</i></b>” means, in respect
of an Assignment Agreement, delivery to My Electrician for Acceptance and
processing by either physical or electronic means. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.2pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:12.0pt;margin-bottom:
0cm;margin-left:70.65pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
110%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:110%;font-family:"Times New Roman","serif"'>1.1.34<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:110%;
font-family:"Times New Roman","serif"'>“<b><i>System</i></b>” means MOP portal,
the Assignment Agreement, this Agreement, the method of operation of each of
the Assignment Agreement, MOP and this Agreement whether in documentary,
visual, oral, machine readable, electronic, tangible or other forms and
equipment which can be installed or removed to enable the creation of
Environmental Certificates. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.7pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:17.0pt;margin-bottom:
0cm;margin-left:70.65pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>1.1.35<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>“<b><i>System Owner</i></b>”, in respect
of a specific System, means the owner of that System or other person entitled
to create the Environmental Certificates associated with the System. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:6.0pt;margin-bottom:0cm;
margin-left:70.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>1.1.36<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>“<b><i>Victorian Energy Efficiency Certificate</i></b>”
has the meaning given in the Victorian Energy Efficiency Target Act 2007 (Vic)
(“VEET”). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Interpretation:
For the purposes of this agreement: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:70.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.2.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Plural
and Singular: </span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:70.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:normal;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Words
importing the singular number include the plural and vice versa. </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection11>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
15.5pt;text-autospace:none'><a name=page6></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.2.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Persons:
</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
4.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:4.0pt;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;line-height:109%;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:109%;font-family:"Times New Roman","serif"'>References to persons
include references to individuals, companies, corporations, firms,
partnerships, joint ventures, associations, organisations, trusts, states or
agencies of state, government departments and local and municipal authorities,
whether or not having separate legal personality. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.2.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Headings:
</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
4.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:96%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:96%;font-family:"Times New Roman","serif"'>Clause
and other headings are for ease of reference only and do not form any part of
the context or to affect the interpretation of this Agreement. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-42.35pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>1.2.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Introduction
and Schedules: </span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
4.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:32.0pt;margin-bottom:
0cm;margin-left:99.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;line-height:96%;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:"Times New Roman","serif"'>The
Introduction and Schedules to this Agreement form part of the Agreement. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Assignment
Agreement: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman","serif"'>2.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman","serif"'>The Installation Company may contract
with a System Owner for the supply, deployment and/or removal of a System on
terms including a benefit conditional on System Owner entering into an
Assignment Agreement with My Electrician or its nominated Certificate Creation
Partner. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>2.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>When an Assignment Agreement executed by
the System Owner is Submitted to and Accepted by My Electrician or its
nominated Certificate Creation Partner, the Installation Company make the
representations and warranties set out below. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.2pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:14.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>2.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>On Submission of an Assignment Agreement
executed by a System Owner, the Installation Company represents and warrants to
My Electrician and My Electrician that it: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.2pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:24.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:97%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>is unaware of System Owner dealing with
its Environmental Certificate creation rights except through the Assignment
Agreement; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:10.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>has no claim over and will not interfere
with My Electrician’s rights under the Assignment Agreement; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:13.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-27.6pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>c)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>consents to My Electrician or its
nominated Certificate Creation Partner making statements to third parties in
reliance on the information provided under this Agreement by My Electrician,
its associates and customers; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:33.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
96%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:96%;font-family:"Times New Roman","serif"'>d)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>considers the information contained in
any Assignment Agreement submitted to My Electrician to be true and correct in
every particular; </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection12>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><a name=page7></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:104%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:104%;font-family:
"Times New Roman","serif"'>e)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>will inform My Electrician of any new
information it obtains in respect of a System that indicates that previously
supplied information in respect of that System is misleading, incorrect or
inaccurate; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>f)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>indemnifies My Electrician for any and
all losses that My Electrician may suffer if information described in 2.3 (e),
(g) and (i) is misleading, incorrect or inaccurate; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:22.0pt;margin-bottom:
0cm;margin-left:56.65pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>g)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>will provide My Electrician and My
Electrician any information or assistance required to create Environmental
Certificates or comply with Regulator requirements; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:16.0pt;margin-bottom:
0cm;margin-left:56.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:104%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>h)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>will assist My Electrician and My
Electrician to conduct an inspection of the System the subject of the
Assignment Agreement if requested by My Electrician; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:26.0pt;margin-bottom:
0cm;margin-left:56.65pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>i)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>has supplied a System that is fit for
purpose and in compliance with all regulatory requirements (not just those
relating to Environmental Certificates); </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>j)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>will refund to My Electrician erroneous
payments, or payments made where the Parcel of Environmental Certificates for a
System is less than that specified in the Assignment Agreement. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:27.0pt;margin-bottom:
0cm;margin-left:28.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>2.4<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>On Acceptance of an Assignment
Agreement, My Electrician represents and warrants to the Installation Company
that it will: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:34.0pt;margin-bottom:
0cm;margin-left:56.65pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>pay the amount specified in the
Assignment Agreement by electronic transfer to the Installation Company’s
account within the timeframe specified by My Electrician; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.15pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:12.0pt;margin-bottom:
0cm;margin-left:56.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:97%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>comply with usual Regulator requirements
associated with the creation of the Parcel of Environmental Certificates. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>2.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>The
Installation Company must: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:11.0pt;margin-bottom:
0cm;margin-left:70.65pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>2.5.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>provide to every System Owner evidence
of the system installed by it using and completing the purchase receipt
attached to the Assignment Agreement in MOP and handing that receipt to the
System Owner; and </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:70.65pt;margin-bottom:.0001pt;text-indent:-42.35pt;line-height:
107%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman","serif"'>2.5.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman","serif"'>ensure that the duplicate copy of the
purchase receipt attached to the Assignment Agreement in MOP and electronically
completed on MOP is identical to the purchase receipt provided to every System
Owner in terms of Clause 2.5.1. </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection14>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
15.5pt;text-autospace:none'><a name=page8></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>My
Electrician Online Portal (“MOP”): </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>3.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>At
any time, the Installation Company may request access to MOP. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:11.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>3.2<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>If the Installation Company requests
access to MOP, My Electrician may accept or decline the request in its absolute
discretion. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>3.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>If My Electrician allows the
Installation Company access to MOP, the Installation Company agrees that it
will comply with the terms of use that are in force at the time of access and
which are available on MOP. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:15.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>3.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>It is the Installation Company’s
responsibility to manage user access. The Installation Company must nominate
Authorised Representatives and advise of any user access changes by written
email to My Electrician. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:44.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>3.5<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>MOP incorporates two areas, an
Administrator’s Area and a User’s Area. Different rules apply to Data in the
two areas: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>in
both the Administrator’s Area and the User’s Area: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:56.0pt;margin-bottom:
0cm;margin-left:113.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-27.9pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>Form Data is owned by My Electrician and
licenced to My Electrician; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:41.0pt;margin-bottom:
0cm;margin-left:113.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-27.9pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>Solution Data is owned by My Electrician
and licenced to My Electrician; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:113.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-27.9pt;line-height:97%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:97%;font-family:
"Times New Roman","serif"'>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>Solution Data has been received by My
Electrician for the purposes of the Privacy Act. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:12.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>3.6<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>Solution Data is in the Private Area
until it is Lodged, when it enters the Shared Area. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:4.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>3.7<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>Form Data is in the Private Area if it
is associated with Solution Data that has not been Submitted. Otherwise it is
in the Shared Area. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:6.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:116%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
11.5pt;line-height:116%;font-family:"Times New Roman","serif"'>3.8<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:11.5pt;line-height:116%;
font-family:"Times New Roman","serif"'>The Installation Company has a licence
to use Form Data and Solution Data for the sole and exclusive purposes of
complying with its obligations under this Agreement and must not whether
directly or indirectly allow the Form Data and Solution Data to be used for any
other purpose. The Installation Company acknowledges that if any such licence
is breached in any manner whatsoever that licence terminates immediately upon
such breach and the Installation Company immediately becomes liable to My
Electrician for all and any loss and damage </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection15>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><a name=page9></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>they may suffer and My Electrician is
entitled in its absolute discretion to prevent access to MOP by the
Installation Company and without further notice.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>General
obligations: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>4.1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>During
the course of this Agreement, the Installation Company must: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:43.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>comply with directions and advice issued
by My Electrician and its associates; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:7.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:97%;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;line-height:97%;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>ensure that its employees, agents and
independent contractors comply with this Agreement and any Associated
Agreements; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:17.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman","serif"'>c)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman","serif"'>promptly advise My Electrician if its
becomes aware of any action that a third party is taking or may take that could
result in a claim being made against the Installation Company or result in a
requirement for the Installation Company to notify its insurer; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>d)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>not
represent that it can bind My Electrician or its associates in any way; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>e)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>comply
with all applicable privacy and confidentiality laws; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>f)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>submit
Assignment Agreements to My Electrician promptly and at least one </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:normal;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>(1)
month prior to any relevant regulatory deadline; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:17.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>g)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>take all steps to make it clear to all
third parties that it is not an agent, partner or joint venturer with My
Electrician or its nominated Certificate Creation Partner and that it is an
independent business entity; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:16.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>h)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>immediately it becomes or ought to
become aware thereof notify My Electrician of any non-compliance by the
Installation Company with any provision of this Agreement or associated
Agreements. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.4pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Payments:
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:6.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:97%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:97%;font-family:
"Times New Roman","serif"'>5.1<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>At any time the Parties may have payment
obligations to each other under this or any other Agreements. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>5.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Where
this is the case, My Electrician may in its sole and absolute discretion: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:11.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:102%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;line-height:102%;
font-family:"Times New Roman","serif"'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:11.5pt;line-height:102%;
font-family:"Times New Roman","serif"'>deduct amounts owed by the Installation
Company from amounts owed to the Installation Company, and pay a net amount as
a final settlement; or </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection17>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><a name=page10></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>in the event where outstanding payments
are referred to a collection agency and/or law firm, pass on all costs which
would be incurred as if the debt were collected in full, including legal demand
costs. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:5.0pt;margin-bottom:0cm;
margin-left:28.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>5.3<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>For the avoidance of doubt, these
provisions are applicable to any amounts owed to/claimed by My Electrician
including amounts resulting from: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>the
terms of this Agreement; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a
breach of this Agreement; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>c)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>the
invalidity of any Environmental Certificates; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>d)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>the
operation of the common law or applicable statutes; or </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>e)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>any
other actions taken by My Electrician. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:7.0pt;margin-bottom:0cm;
margin-left:28.65pt;margin-bottom:.0001pt;text-indent:-28.65pt;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman","serif"'>5.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman","serif"'>Where My Electrician has previously been
directed by the Installation Company to make a payment in a particular manner,
My Electrician is entitled to assume that future payments may be made in the
same manner until the Installation Company informs My Electrician otherwise. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>5.5.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>The
Installation Company must submit any payment claim to My Electrician: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:48.0pt;margin-bottom:
0cm;margin-left:56.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>in such format as My Electrician may
from time to time notify the Installation Company in writing; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>on
or before 4:00pm on any Monday that is not a public holiday in Victoria. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.65pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>5.5.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a)
Any payment claims shall be paid within fourteen (14) days reckoned from </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
4.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:14.0pt;margin-bottom:
0cm;margin-left:56.65pt;margin-bottom:.0001pt;line-height:96%;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:96%;font-family:"Times New Roman","serif"'>the first Monday after
the date on which the Installation Company lodges the payment claim with My
Electrician.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:7.0pt;margin-bottom:0cm;
margin-left:56.65pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:109%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:109%;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:109%;
font-family:"Times New Roman","serif"'>Despite anything to the contrary
contained in this Agreement unless and until the Installation Company complies
with clause 2.4 of this Agreement it is not entitled to and My Electrician is
not obliged to make any payment to the Installation Company for any transaction
in respect of which the requirements of clause 2.4 have not been met. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.55pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:10.0pt;margin-bottom:
0cm;margin-left:35.65pt;margin-bottom:.0001pt;text-indent:-28.0pt;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman","serif"'>5.5.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman","serif"'>Payment
rates, changes to certificate pricing and schedule updates will change from
time to time and will be notified to the Installation Company in terms of MOP.
By continuing to use MOP the Installation Company by its conduct accepts such
changes. </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection19>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><a name=page11></a><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Third
party requests and directions: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:26.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:102%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;line-height:102%;
font-family:"Times New Roman","serif"'>6.1<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:11.5pt;line-height:102%;
font-family:"Times New Roman","serif"'>In the course of running its business My
Electrician may interact with third parties including Certificate Creation
Partners, Regulators and Financiers. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.4pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>6.2<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>As a result of the interactions
described in clause 6.1, or otherwise, a third party may: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>make
requests of and issue directions to My Electrician; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>bring
proceedings against My Electrician; or </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>c)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>otherwise
impose costs on My Electrician. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:5.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>6.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>Where the actions described in clause
6.2 relate to the Installation Company’s actions (or failure to act) or those
of its subcontractors, customers or associates, the Installation Company must: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.15pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:97%;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;line-height:97%;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>provide any assistance that My
Electrician may require in order to respond to or comply with the Regulators’
requests and directions; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:31.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
109%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:109%;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:109%;
font-family:"Times New Roman","serif"'>indemnify My Electrician for all its
costs (both direct and indirect) associated with responding to and complying
with the requests and directions (for the avoidance of doubt, these costs may
include costs associated with the procurement of professional services
including consulting, legal, tax, accounting, and engineering). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.65pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Breach
and termination: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>7.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>A
Party will notify the other if it is in default of this Agreement. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>7.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>A
Party is in default of this Agreement if it: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>fails
to comply with any term of this Agreement; or </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>becomes
Insolvent. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:25.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>7.3<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>If a Party (Defaulting Party) is in
default of this Agreement, the other Party (Injured Party) is entitled to: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>terminate
this Agreement immediately by written notice; or </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>recover
damages in respect of its losses; or </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection21>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><a name=page12></a><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>c)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>both
a) and b). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman","serif"'>7.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman","serif"'>If the Defaulting Party does not pay any
damages within fourteen (14) days of the Injured Party making a written request
for payment, interest will accrue at 10% per annum or the current Penalty
Interest Rate (whichever is the greater) on the damages from the date of
default until the damages are paid. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:109%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:109%;font-family:"Times New Roman","serif"'>7.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:109%;
font-family:"Times New Roman","serif"'>If the Injured Party holds any monies,
Environmental Certificates or other property, rights or interests on behalf of
the Defaulting Party or is obliged to make payments to the Defaulting Party,
then the Injured Party is entitled to utilise these items to satisfy any
damages claimed under this Agreement. This right is additional to the rights
created elsewhere in this Agreement. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.65pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>7.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Either
Party may Terminate this Agreement by thirty (30) days written notice. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>7.7<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>If this Agreement is terminated then the
Parties’ obligations under clauses 2.2(d)-(j), 2.3(a)-(b), 3.8, 5, 7, 8, 10,
11, 12, 13, 15 and 16 remain in place. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>8.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Non-disclosure:
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:47.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>8.1<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>The Parties may not disclose information
relating to or shared under this Agreement to any person except: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>to
the extent that it is already in the public domain; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>with
the written consent of the other Party; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>c)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>to
its officers, employees and professional advisers; or </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>d)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>as required by an applicable law or Code
of Conduct after first consulting (to the extent lawful and reasonably
practical) with the other Party about the form and content of the disclosure. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:12.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>8.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>Where permitted disclosures are made by
a Party on any basis other than clause 8.1 (a), they will use reasonable
endeavours to ensure the disclosed material is kept confidential by the Party
to whom it has been shared. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.45pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>9.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Representations
and warranties </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:17.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:97%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>9.1<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>In respect of this Agreement the
Installation Company represents and warrants that: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>all information, documents and material
provided to My Electrician are and will be accurate and complete; and </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.4pt;text-autospace:none'><a name=page13></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:17.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>it will deal with My Electrician in good
faith and respond promptly to Solution Provider reasonable requests for
additional information or other support that may be required. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.25pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>10.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Relationship:
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:7.0pt;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;line-height:109%;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:109%;font-family:"Times New Roman","serif"'>The Installation
Company is not an agent, partner or joint venturer nor is it authorised to act
on behalf or bind My Electrician or its nominated Certificate Creation Partner
and hereby indemnifies My Electrician or its nominated Certificate Creation Partner
in relation to all and any liability or responsibility arising from or
connected in any manner whatsoever to the Installation Company’s dealings with
System Owners. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.65pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>11.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Confidentiality:
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:7.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>11.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>The
Installation Company must keep confidential during and after the end of this
Agreement the following information whether or not it is in material
electronic, digital or machine readable or other form: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:107%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman","serif"'>all confidential information including
but not limited to intellectual property, trade secrets, confidential know-how
(and the software) relating to this Agreement, MOP, data contained within MOP
and the Assignment Agreement; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:38.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>those parts of all notes and other
records prepared by the Installation Company based on or incorporating the
information referred to in paragraphs (a) and (b) above; and </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:11.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>c)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>all copies of the information and those
parts of the notes and other records referred to in paragraph (a), (b) and (c).
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:24.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman","serif"'>11.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman","serif"'>The
Installation Company must use Confidential Information solely and exclusively
for the purpose of complying with its obligations under this Agreement and must
not whether directly or indirectly allow the Confidential Information to be
used or disclosed for any other purpose. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>11.3<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>The Installation Company may disclose
Confidential Information only to those of its officers, employees and agents
who: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:9.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:97%;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;line-height:97%;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>have a need to know (and only to the
extent that each has a need to know); and </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>have
been directed to keep Confidential Information. </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection24>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><a name=page14></a><span lang=EN-AU style='font-size:12.0pt;font-family:
"Times New Roman","serif"'>11.4<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>The
Installation Company must at its own costs: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:97%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:97%;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>ensure at all times that each person to
whom Confidential Information has been disclosed complies with the agreement
contemplated by paragraph 11; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:19.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>notify My Electrician immediately if it
becomes aware of a suspected or actual breach of the Confidentiality Agreement;
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>c)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>take
all steps to prevent or stop the suspected or actual breach; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:5.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>d)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>comply with any direction issued by My
Electrician from time to time regarding the enforcement of Confidentiality
Agreement (including but not limited to starting, conducting and settling
enforcement proceedings); </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.2pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:15.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:97%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>e)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>assign any Confidential Agreement to My
Electrician at My Electrician’s request. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:46.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>11.5<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>The obligations of confidentiality under
this Agreement do not extend to information that (whether before or after this
Agreement is executed): </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:16.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>is rightfully known to or is in the
possession or control of the Installation Company and not subject to an
obligation of confidentiality on the Installation Company; or </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:51.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>is public knowledge (otherwise than as a
result of a breach of this Agreement). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:102%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
11.5pt;line-height:102%;font-family:"Times New Roman","serif"'>11.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:11.5pt;line-height:102%;font-family:"Times New Roman","serif"'>If
the Installation Company is required by law to disclose any Confidential
Information to a third party (including but not limited to a government
authority): </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.5pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>before
doing so it must notify My Electrician in writing; and </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:13.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>it must not do so until My Electrician
has had a reasonable opportunity to take any steps that My Electrician
considers necessary to protect the confidentiality of that information. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>11.7<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>The Installation Company acknowledges
that its knowledge concerning MOP and the intellectual property comes from My
Electrician. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>12.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Non-dealing
with users of the System: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>For
the purposes of this clause a user of the System (“the User”) means every
person or entity who uses the System or whose details or information is
recorded or appears in the System or any part thereof but excludes a
Certificate Creation Partner and excludes a </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>

<div class=WordSection26>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><a name=page15></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>person
or entity whose details or information is in the System solely by reason of
being a System Owner with whom the Installation Company first transacted any
business activities.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.45pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:29.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.65pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>12.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>The
Installation Company must not: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>other
than as permitted by this Agreement engage in the business: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:42.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-27.9pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>to secure directly or indirectly from
any User Environmental Certificates; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:11.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-27.9pt;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>to supply directly or indirectly
software systems for Environmental Certificate creation for any User. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>from
the date of ending or expiration of this Agreement for a period of two </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.15pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:normal;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>(2)
years within the State of Victoria: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:6.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-27.9pt;line-height:96%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>engage in the business of or to secure
directly or indirectly from any User Environmental Certificates; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-27.9pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>ii.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>directly or indirectly solicit, canvass,
approach or accept any approach from any person who was at any time during the
last twelve (12) months of the Agreement: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.45pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:114.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.6pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a
User; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:114.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.6pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>a
referral contact of My Electrician; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:114.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.6pt;line-height:109%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:109%;font-family:
"Times New Roman","serif"'>c)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:109%;
font-family:"Times New Roman","serif"'>any System Owner whose information
appears on MOP other than a System Owner whose details are first entered into
MOP solely as a result of the provision of services by the Installation Company
of the nature referred to in Introduction paragraph C to such Service Owner. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.55pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:8.0pt;margin-bottom:0cm;
margin-left:29.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman","serif"'>12.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman","serif"'>In
this clause “engaging” means to participate, assist or otherwise be directly or
indirectly involved as a member, shareholder, unit holder, director,
consultant, advisor, contractor, principal, agent, manager, employee,
beneficiary, partner, associate, trustee or financier. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:36.0pt;margin-bottom:
0cm;margin-left:29.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:97%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>12.3<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>The Installation Company acknowledges
that each of the prohibitions and restrictions contained in this Clause 12: </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>


<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<div class=WordSection28>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><a name=page16></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>a)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>should be read and construed and will
have the effect as a separate, severable and independent prohibition or
restriction and will be enforceable accordingly; and </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:"Times New Roman","serif"'>b)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>is reasonable as to period, territory or
limitations and subject matter in order to protect the legitimate commercial
interest of My Electrician. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
18.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>13.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Costs:
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:12.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-28.65pt;line-height:97%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>13.1<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>Each Party is responsible for its own
costs and expenses in connection with and incidental to the preparation and
execution of this Agreement. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>14.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Goods
and Services Tax: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.9pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:16.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:
110%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:11.5pt;line-height:110%;font-family:"Times New Roman","serif"'>14.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:11.5pt;line-height:110%;font-family:"Times New Roman","serif"'>To
the extent that GST is applicable to the transactions contemplated by this
Agreement, the Parties agree to apply it in a manner consistent with any public
rulings or other explanatory material published by the Australian Tax Office. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
16.55pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>15.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Dispute
resolution: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.75pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:9.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>15.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>If
a dispute arises between the Parties under this Agreement or Other Agreements,
the following steps must be taken by the Parties prior to taking any action to
commence legal proceedings: </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.2pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:43.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>the aggrieved Party must provide a
written notice to the other Party specifying their concerns. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.95pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:19.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>The Party receiving a notice under
clause 15.1(a) must provide a written response within fourteen (14) days. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman","serif"'>c)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman","serif"'>If the aggrieved party is not satisfied
with a response provided under clause 15.1(b) then they must provide a further
notice in writing to the other Party requesting that a meeting be held between
the chief executives of each Party. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
19.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:96%;punctuation-wrap:simple;text-autospace:none'><span
lang=EN-AU style='font-size:12.0pt;line-height:96%;font-family:"Times New Roman","serif"'>d)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:96%;
font-family:"Times New Roman","serif"'>The meeting between the chief executives
must be held within fourteen (14) days of the other Party receiving a notice
under clause 15.1(c). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.85pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman","serif"'>e)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman","serif"'>If the dispute remains unresolved after
the meeting described in clause 15.1(d) then either Party may by notice in
writing to the other direct that the matters raised in the documents referred
to in clauses 15.1(a) and 15.1(b) be resolved by mediation. </span></p>

</div>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>


<div class=WordSection30>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><a name=page17></a><span lang=EN-AU
style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:12.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-indent:-1.0cm;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>f)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>A party receiving a notice under clause
15.1(e) must co-operate with the other Party to hold a mediation within
fourteen (14) days of receipt of the notice. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:11.0pt;margin-bottom:
0cm;margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-1.0cm;line-height:104%;punctuation-wrap:simple;
text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>g)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>If the mediation concludes and the
dispute remains unresolved then either Party may bring legal proceedings in
respect of the dispute as specified in the documents referred to in clauses
15.1(a) and 15.1(b). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:8.0pt;margin-bottom:0cm;
margin-left:85.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-1.0cm;line-height:104%;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;line-height:104%;font-family:
"Times New Roman","serif"'>h)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;line-height:104%;
font-family:"Times New Roman","serif"'>If a Party does not comply with the
requirements of this clause 15 then the other party may bring legal proceedings
immediately notwithstanding that all of the steps set out in this clause have
not yet been completed. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
17.4pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:28.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-28.0pt;line-height:normal;punctuation-wrap:simple;text-autospace:
none'><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>16.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>General:
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.8pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:102%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
11.5pt;line-height:102%;font-family:"Times New Roman","serif"'>16.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:11.5pt;line-height:102%;font-family:"Times New Roman","serif"'>The
invalidity of any part or provision of this Agreement or Other Agreement will
not affect the enforceability of any other part or provision of this Agreement.
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.4pt;text-autospace:none'><span lang=EN-AU style='font-size:11.5pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:104%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>16.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>The
Parties will sign and execute all assurances, documents and deeds and do such
deeds, acts and things as are required by the provisions of this Agreement to
give effect to it. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.15pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:14.0pt;margin-bottom:
0cm;margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:
104%;punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU
style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>16.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;line-height:104%;font-family:"Times New Roman","serif"'>This
Agreement is governed by and construed in accordance with the laws applicable
in Victoria and the Parties submit to the jurisdiction of the Courts in that
State. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.35pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:57.0pt;margin-bottom:.0001pt;text-indent:-28.65pt;line-height:110%;
punctuation-wrap:simple;text-autospace:none'><span lang=EN-AU style='font-size:
12.0pt;line-height:110%;font-family:"Times New Roman","serif"'>16.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-AU style='font-size:12.0pt;line-height:110%;font-family:"Times New Roman","serif"'>This
Agreement is intended to be legally binding upon the parties. For the purpose
of this Clause 16.4 the Parties include but is not limited to legal successors,
agents, assigns, related entities and subcontractors and members, shareholders,
unit holders, directors, consultants, advisors, contractors, principals,
agents, managers, employees, beneficiaries, partners, associates, trustees or
financiers of each of the parties. </span></p>

<span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:auto'>
</span>

<div class=WordSection31>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.05pt;text-autospace:none'><span lang=EN-AU style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span lang=EN-AU style='font-size:6.0pt'>RA 4 -
250915</span><span lang=EN-AU style='font-size:12.0pt;font-family:"Times New Roman","serif"'>                                                                                                                       </span><span
lang=EN-AU>Page <b>17</b> of <b>17</b></span></p>

</div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>      
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
        <script type="text/javascript">
        	jQuery('.filter-date').datepicker();
        </script>
    </body>
</html>