<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>assignment list</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row main">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <input type="hidden" name="action" id="action">
                            <input name="action" id="action" type="hidden">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>STOCK REGISTER</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page 2-->
                            <div class="page" id="page2">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <div class="row">
                                            
                                            <form method="POST">
                                            	<!-- company -->
                                            	<div class="col-xs-12 col-md-6 col-md-offset-2">
                                            		<div class="form-group">
                                            			<label>Company</label>
                                            			<input type="text" class="form-control input-sm"
                                            			disabled="disabled" 
                                            			value="<?= $installer['electricians_company_name'] ?>" />
                                            		</div>
                                            	</div>
                                            	<!-- company -->
                                            	<!-- installer -->
                                            	<div class="col-xs-12 col-md-3 col-md-offset-2">
                                            		<div class="form-group">
                                            			<label>Installer</label>
                                            			<input type="text" disabled="disabled" 
                                            			value="<?= $installer['first_name'].' '.$installer['last_name'] ?>" 
                                            			class="form-control input-sm" />
                                            		</div>
                                            	</div>
                                            	<!-- installer -->
                                            	<!-- date-range -->
                                            	<div class="col-xs-12 col-md-3">
                                            		<div class="form-group">
                                            			<label>Date Range</label><br/>
                                            			<input type="text" readonly="readonly" style="width: 49%;display: inline"
                                            			value="<?= isset($since)?$since:'' ?>" name="since" placeholder="since" 
                                            			class="filter-date form-control input-sm" />
                                            			<input type="text" readonly="readonly" style="width: 49%;display: inline" 
                                            			value="<?= isset($until)?$until:'' ?>" name="until" placeholder="until"
                                            			class="filter-date form-control input-sm" />
                                            		</div>
                                            	</div>
                                            	<!-- date-range -->
                                            	<!-- submit -->
                                            	<div class="col-xs-12 col-md-2">
                                            		<label></label>
                                            		<div class="form-group">
                                            			<input type="submit" value="search" class="btn btn-warning btn-sm" />
                                            		</div>
                                            	</div>
                                            	<!-- submit -->
                                            </form>
                                            
                                            <div class="col-xs-12">
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                    <table id="tcodes_tbl" cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                        <thead>
                                                            <tr class="skyblue">
                                                            	<th>ITEM ID</th>
                                                            	<th>TOTAL COLLECTED</th>
                                                            	<th>TOTAL INSTALLED</th>
                                                            	<th>STOCK ON HAND</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        	<?php foreach($stocks as $stock): ?>
                                                        		<tr>
                                                        			<td><?= $stock->item_name ?></td>
                                                        			<td class="text-right"><?= 0 + $stock->collected ?></td>
                                                        			<td class="text-right"><?= 0 + $stock->installed ?></td>
                                                        			<td class="text-right"><?= $stock->collected - $stock->installed ?></td>
                                                        		</tr>
                                                        	<?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--close page2-->
                        </div>
                    </div>
                </div>
            </div>      
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
        <script type="text/javascript">
        	jQuery('.filter-date').datepicker();
        </script>
    </body>
</html>