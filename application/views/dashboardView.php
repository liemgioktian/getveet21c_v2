<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Dashboard</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/select2.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/select2-bootstrap.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('asset/css/jquery.signaturepad.css') ?>" />
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/dashboard.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/landing-page.css') ?>" rel="stylesheet">

    </head>
    <body>

        <div class="container" style="padding:0px 5px;">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-8">

            <nav class="navbar" role="navigation" class="shadow" style="margin-bottom:0px">
            <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle glyphicon glyphicon-list" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= base_url() ?>" style="padding:10px"><img src="<?= base_url('asset/images/logo-green.png') ?>" class="img-responsive" style="width:150px" alt=""></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <?php if(isset($menus)): foreach ($menus as $menu): ?>

                        <li><a href="<?= $menu['url'] ?>"><?= $menu['title'] ?></a></li>
                      <?php endforeach; endif; ?>
                    </ul>
                    <ul class="nav navbar-right navbar-nav">
                      <li class="dropdown">
                        <a href="<?= site_url('authcontroller/logout') ?>" data-toggle="tooltip" data-placement="left" title="logout" style="padding:5px 10px"><i class="glyphicon glyphicon-log-out"></i></a>
                      </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>


                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-2 col-sm-8" id="content">
                    <div class="panel box_label" style="padding-right:0px;padding-left:0px">
                        <div class="intro-header">
                            <div class="">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="">
                                            <form action="" id="dashboard">

                                            <h1 style="color:#828396">Today's VEEC Price</h1>
                                            <h1 style="color:#828396" class="bold" id="A2" name="A2" data-cell="A2" data-formula="A1" data-format="$ 0.00"></h1>
                                            <h2 style="color:#828396">Inc. GST</h2>
                                            <input type="hidden" id="A1" name="A1" data-cell="A1" value="<?= $setting['c_certificate'] ?>">
                                        </form>
                                            <hr class="intro-divider">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.container -->

                        </div>
<!--
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="veec-background img-responsive">
                                    <div class="box_veec vcenter text-center">
                                        <form action="" id="dashboard">
                                        <h3 class="bold" id="A2" name="A2" data-cell="A2" data-formula="A1" data-format="$ 0.00" style="color:#000"></h3>
                                            <input type="hidden" id="A1" name="A1" data-cell="A1" value="<?= $setting['c_certificate'] ?>">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>-->

                        <div class="row no-gutter">
                            <div class="col-xs-6" style="padding-right:0.5px">
                                <ul class="nav">
                                    <li>
                                        <a href="<?= site_url('maincontroller/create') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/new_form.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">NEW FORM</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('maincontroller/store') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/store.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">STORE</span>
                                        </a>
                                    </li>
                                    <?php if ($this->session->userdata('is_admin') == 1) : ?>
                                    <li>
                                        <a href="<?= site_url('maincontroller/user_list') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/user_list.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">MANAGE USER</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('stockcontroller/installer') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/stock.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">STOCK MANAGER</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('settingscontroller/profit') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/profit.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">PROFIT CALCULATOR</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('maincontroller/job_leads') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/lead_manager.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">JOB LEADS</span>
                                        </a>
                                    </li>

                                    <?php elseif ($this->session->userdata('is_admin') == 2): ?>
                                    <li>
                                        <a href="<?= site_url('maincontroller/user_list') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/user_list.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">MANAGE USER</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('stockcontroller/installer/'.$current_user['cid'].'/company') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/stock.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">STOCK MANAGER</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('maincontroller/job_leads') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/lead_manager.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">JOB LEADS</span>
                                        </a>
                                    </li>
                                    <?php else: ?>
                                    <li>
                                        <a href="<?= site_url() ?>" class="btn dashboard-menu">
                                            <span class="bold"></span>
                                        </a>
                                    </li>

                                    <?php endif ?>
                                    
                                    
                                </ul>
                            </div>
                            <div class="col-xs-6" style="padding-left:0.5px">
                                <ul class="nav">
                                    <li>
                                        <a href="<?= site_url('maincontroller') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/folder_open.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">SAVED FORMS</span>
                                        </a>
                                    </li>
                                    <?php if ($this->session->userdata('is_admin') == 1) : ?>
                                    <li>
                                        <a href="<?= site_url('lead-manager') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/lead_manager.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">LEAD MANAGER</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('settingscontroller') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/setting.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">SETTING</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('maincontroller/subform/product') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/stock.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">PRODUCTS</span>
                                        </a>
                                    </li>
                                    <?php elseif ($this->session->userdata('is_admin') == 2): ?>
                                    <li>
                                        <a href="<?= site_url('lead-manager') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/lead_manager.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">LEAD MANAGER</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('settingscontroller/edit/'.$this->session->userdata('cid')) ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/setting.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">SETTING</span>
                                        </a>
                                    </li>
                                    <?php else: ?>

                                    <?php endif ?>
                                    <li>
                                        <a href="<?= site_url('maincontroller/documents') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/resources.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">RESOURCES</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('authcontroller/logout') ?>" class="btn dashboard-menu">
                                            <img src="<?= base_url('asset/images/logout.png') ?>" class="dashboard-icon" alt=""> <br>
                                            <span class="bold">LOGOUT</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!--
        <footer>
          <div class="container">
            <div class="row" style="padding-top:5px">
              <div class="col-md-12 text-right"><h5>©Eminent Service Group PL T/A My Electrician 2015</h5></div>
            </div>
          </div>
        </footer>
        -->


        <!-- script references -->
          <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js') ?>"></script>-->
        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/moment.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/jstat.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/numeral.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-calx-2.1.1.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.ui.touch-punch.min.js') ?>" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('#dashboard').calx();
            });
        </script>
    </body>
</html>