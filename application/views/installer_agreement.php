<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Installer Agreement</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
       <style>
<!--
 /* Font Definitions */
 @font-face
    {font-family:Wingdings;
    panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
    {font-family:Wingdings;
    panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
    {font-family:Calibri;
    panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
    {font-family:"Segoe UI";
    panose-1:2 11 5 2 4 2 4 2 2 3;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
    {margin-top:0cm;
    margin-right:0cm;
    margin-bottom:10.0pt;
    margin-left:0cm;
    line-height:115%;
    font-size:11.0pt;
    font-family:"Calibri","sans-serif";}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
    {mso-style-link:"Comment Text Char";
    margin-top:0cm;
    margin-right:0cm;
    margin-bottom:10.0pt;
    margin-left:0cm;
    font-size:10.0pt;
    font-family:"Calibri","sans-serif";}
p.MsoHeader, li.MsoHeader, div.MsoHeader
    {mso-style-link:"Header Char";
    margin:0cm;
    margin-bottom:.0001pt;
    font-size:11.0pt;
    font-family:"Calibri","sans-serif";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
    {mso-style-link:"Footer Char";
    margin:0cm;
    margin-bottom:.0001pt;
    font-size:11.0pt;
    font-family:"Calibri","sans-serif";}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
    {mso-style-link:"Balloon Text Char";
    margin:0cm;
    margin-bottom:.0001pt;
    font-size:9.0pt;
    font-family:"Segoe UI","sans-serif";}
p.MsoRMPane, li.MsoRMPane, div.MsoRMPane
    {margin:0cm;
    margin-bottom:.0001pt;
    font-size:11.0pt;
    font-family:"Calibri","sans-serif";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
    {margin-top:0cm;
    margin-right:0cm;
    margin-bottom:10.0pt;
    margin-left:36.0pt;
    line-height:115%;
    font-size:11.0pt;
    font-family:"Calibri","sans-serif";}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
    {margin-top:0cm;
    margin-right:0cm;
    margin-bottom:0cm;
    margin-left:36.0pt;
    margin-bottom:.0001pt;
    line-height:115%;
    font-size:11.0pt;
    font-family:"Calibri","sans-serif";}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
    {margin-top:0cm;
    margin-right:0cm;
    margin-bottom:0cm;
    margin-left:36.0pt;
    margin-bottom:.0001pt;
    line-height:115%;
    font-size:11.0pt;
    font-family:"Calibri","sans-serif";}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
    {margin-top:0cm;
    margin-right:0cm;
    margin-bottom:10.0pt;
    margin-left:36.0pt;
    line-height:115%;
    font-size:11.0pt;
    font-family:"Calibri","sans-serif";}
span.CommentTextChar
    {mso-style-name:"Comment Text Char";
    mso-style-link:"Comment Text";}
span.BalloonTextChar
    {mso-style-name:"Balloon Text Char";
    mso-style-link:"Balloon Text";
    font-family:"Segoe UI","sans-serif";}
span.HeaderChar
    {mso-style-name:"Header Char";
    mso-style-link:Header;}
span.FooterChar
    {mso-style-name:"Footer Char";
    mso-style-link:Footer;}
.MsoChpDefault
    {font-family:"Calibri","sans-serif";}
.MsoPapDefault
    {margin-bottom:10.0pt;
    line-height:115%;}
 /* Page Definitions */
 @page WordSection1
    {size:595.3pt 841.9pt;
    margin:72.0pt 72.0pt 72.0pt 72.0pt;}
div.WordSection1
    {page:WordSection1;}
 /* List Definitions */
 ol
    {margin-bottom:0cm;}
ul
    {margin-bottom:0cm;}
-->
</style>
    </head>
    <body>
        
        <div class="container">
           
            <div class="row">
				
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>Installer AGREEMENT</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="page" id="">
                                <div class="row">
                                    <div class="col-sm-offset-1 col-sm-10">
                                       <div class=WordSection1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center'><b><u><span lang=EN-AU style='font-size:14.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>TERMS AND CONDITIONS</span></u></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><b><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>INTRODUCTION:</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><b><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>A.      The Certificate Creation Partner (“CCP”) provides a range
of services to participants in environmental markets, including product accreditation
and Environmental Certificate Creation and monetisation and is to be nominated
from time to time by and in the absolute and sole discretion of My Electrician.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>B.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>      My Electrician
provides a range of services to participants in environmental markets,
including product supply, Software systems for Environmental Certificate
creation and monetisation. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>C.      The Installation Company operates a business that is
involved in the supply and/or installation of Systems for System Owners.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>D.      The Installation Company wishes to work with My
Electrician on an ongoing basis in respect of a range of business activities
including but not limited to transactions with System Owners.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>E.      From time to time the Installation Company may:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.       offer System Owners a discount in return for the System
Owner entering into an assignment agreement with My Electrician or </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>its</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'> nominated Certificate
Creation Partner;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>2.       utilise My Electrician’s online transaction portal
(“MOP”).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>F.      This document specifies the terms on which My Electrician
and the Installation Company have agreed to work together and additional
agreements may be entered into in respect of individual transactions and other
things.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><b><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>TERMS:</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><b><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.       Definitions and Interpretation</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:
36.0pt'><span lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:
"Times New Roman","serif";color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1     Definitions: For the purposes of this Agreement:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.1      “<b><i>Accept</i></b>” means, in respect of</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>i.        </span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'>a Submitted
Assignment Agreement, the process by which My Electrician becomes bound by that
Assignment Agreement</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>; and</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>ii.       this Agreement the acceptance by My Electrician of any
Purchase Orders effective immediately upon My Electrician transmitting
electronically its acceptance of any Purchaser Order to any such Electronic
address as it may have received from the person who requests such Purchase
Order (“the purchaser”).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.2      “<b><i>Administrator</i></b>” includes a liquidator,
receiver or receiver and manager (whether provisional or otherwise).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.3      “<b><i>Agreement</i></b>” means this Agreement and
governs all future dealings between My Electrician and the Installation Company
for the purposes of Introduction paragraph B.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.4      “<b><i>Assignment Agreement</i></b>” means an agreement
between My Electrician and a System Owner, in which the System Owner assigns
its rights to create a Parcel of Environmental Certificates to My Electrician.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.5      “<b><i>CER</i></b>” means the Clean Energy Regulator.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.6      “<b><i>Certificate Creation Partner</i></b> (“<b><i>CCP</i></b>”)”
means an accredited person under VEET who provides the services described in
paragraph A of the Introduction.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.7</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>      “<b><i>Code of
Conduct</i></b>” means any set of rules or other obligations that are a
condition of membership of an industry association to which a Party belongs.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.8      “<b><i>Confidential Information</i></b>” means the
confidential information described in Clause 11, My Electrician’s Industrial
Property, Intellectual Property and Know-How.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.9</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>      “<b><i>Data</i></b>”
means information contained within the MOP, including Form Data and Solution
Data. </span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>10</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Defaulting Party</i></b>” has the meaning given in
clause 7.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>11</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Energy Saving Certificate</i></b>” has the meaning
given in the Electricity Supply Act 1995 (NSW).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>12</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Environmental Certificate</i></b>” means a tradeable
certificate that can be created to represent the environmental improvement
created through the deployment of a System, and includes Small-scale Technology
Certificates, Large Generation Certificates, Victorian Energy Efficiency
Certificates, and Energy Saving Certificates.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>13</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>ESC</i></b>” means Essential Services Commission.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>14</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Form Data</i></b>” means Data that is presented to the
Installation Company by MOP to assist in the entry of Solution Data.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>15</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>My Electrician</i></b>” has the meaning given at the
commencement of this document.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>16</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>GST</i></b>” means Goods and Services Tax, as that
phrase is defined in the A New Tax System (Goods and Services Tax) Act 1999
(Cth).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.17    “<b><i>Industrial Property</i></b>” means all My
Electric’s (whether or not in documentary visual, oral, machine readable,
electronic, digital, tangible or other forms) designs, specifications, artwork,
trademarks, copyright, business names, trade secrets, methodologies, insignia,
logogram, brochures, promotional material, advertising layouts, bromides,
illustrations, client lists, master files of all administration forms,
disseminated information and other industrial property the interest in which is
the property of My Electrician.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.18    “<b><i>Injured Party</i></b>” has the meaning given in
clause 7.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.19</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>    “<b><i>Insolvent</i></b>”
means the occurrence of any one or more of the following events in respect of a
person:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)       an application is made that it be wound up, declared
bankrupt or that an Administrator be appointed;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      an Administrator is appointed to any of its assets;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)       it enters into an arrangement with its creditors (or
proposes to do so);</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>d)      it makes an assignment to benefit one or more creditors
(or proposes to do so);</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>e)       it is insolvent, states that it is insolvent or it is
presumed to be insolvent under an applicable law;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>f)       it becomes an insolvent under administration or action is
taken which could result in that event;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>g)      a writ of execution is levied against it or its property;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>h)      it ceases to carry on business or threatens to do so; or</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:127.6pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>i)       anything occurs under the law of any applicable
jurisdiction which has a substantially similar effect to paragraphs a) - h)
above.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>20    “<b><i>Intellectual
Property</i></b>” means all My Electrician’s rights and industrial property
rights, titles, interests in and to data, instructions, plans, formulae,
technology, computer software, designs, process descriptions, reports, results,
technical advice and trade secrets whether in documentary, visual, oral,
machine, readable, electronic or other forms.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.21    “<b><i>Know-how</i></b>” means My Electrician’s
expertise in the supply of services described in Introduction paragraph B,
marketing of My Electrician’s products through the System, techniques developed
business systems and methods of operation and the business trading name My
Electric some of which is secret.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.22</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>    “<b><i>MOP</i></b>”
has the meaning given in paragraph E 2 of the Introduction.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>23</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Offer Price</i></b>” means, on any given day and in
respect of a particular type of Environmental Certificate, the price specified
by My Electrician for that particular type of Environmental Certificate on that
day.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>24</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Parcel of Environmental Certificates</i></b>” means, in
respect of a specific System, the Environmental Certificates that may be
created as a result of the installation or operation of that System.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>25</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Parcel Volume</i></b>” means, in respect of a
particular Parcel of Environmental Certificates, the number of Environmental
Certificates contained in that parcel. </span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>26</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Parties</i></b>” means My Electrician, Installation
Company and their transferees, assigns and successors in title.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>27</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Penalty Interest Rate</i></b>” means the rate specified
in the Penalty Interest Rates Act 1983 (Vic).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>28</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Privacy Act</i></b>” means the Privacy Act 1988 (Cth).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>29</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Purchase Order</i></b>” means any request from time to
time by the Installation Company of My Electrician to provide to System Owners
the activities contained in paragraph B of the Introduction to these Terms and
Conditions.  Upon acceptance</span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'> by My
Electrician</span><span lang=EN-AU style='font-size:12.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:black'> of such Purchase Order the
Terms and Conditions of this Agreement come into operation.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>30</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Regulator</i></b>” means a government department or
agency that has responsibility for the administration of programs involving or
associated with Environmental Certificates, and includes the CER, the ESC and
IPART.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>31</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Solution Data</i></b>” means Data entered into the MOP
by the Installation Company that is not Form Data.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>32</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Solution Provider</i></b>” has the meaning given at the
commencement of this document.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>33</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Submit</i></b>” means, in respect of an Assignment
Agreement, delivery to My Electrician for Acceptance and processing by either
physical or electronic means.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.34    “<b><i>System</i></b>” means MOP portal, the Assignment
Agreement, this Agreement, the method of operation of each of the Assignment
Agreement, MOP and this Agreement whether in documentary, visual, oral, machine
readable, electronic, tangible or other forms and</span><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'> equipment which can be installed or removed to enable the
creation of Environmental Certificates.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>35</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>System Owner</i></b>”, in respect of a specific System,
means the owner of that System or other person entitled to create the
Environmental Certificates associated with the System.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.1.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>36</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>    “<b><i>Victorian Energy Efficiency Certificate</i></b>” has
the meaning given in the Victorian Energy Efficiency Target Act 2007 (Vic</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>) (“VEET”).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.2     Interpretation: For the purposes of this agreement:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.2.1      <b>Plural and Singular: </b></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>Words
importing the singular number include the plural and vice versa.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.2.2      <b>Persons: </b></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>References
to persons include references to individuals, companies, corporations, firms,
partnerships, joint ventures, associations, organisations, trusts, states or
agencies of state, government departments and local and municipal authorities,
whether or not having separate legal personality.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.2.3      <b>Headings: </b></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>Clause
and other headings are for ease of reference only and do not form any part of
the context or to affect the interpretation of this Agreement.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>1.2.4      <b>Introduction and Schedules: </b></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>The Introduction
and Schedules to this Agreement form part of the Agreement.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>2.       Assignment Agreement: </span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>2.1     The Installation Company may contract with a System Owner
for the supply, deployment and/or removal of a System on terms including a
benefit conditional on System Owner entering into an Assignment Agreement with My
Electrician or </span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>its</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'> nominated Certificate Creation Partner.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>2.2     </span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'>When an
Assignment Agreement executed by the System Owner is Submitted to and Accepted
by My Electrician or </span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'>its</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'> nominated Certificate Creation Partner, the </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>Installation Company</span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'> make the
representations and warranties set out below.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>2.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>3</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>     On Submission of an Assignment Agreement executed by a System
Owner, the Installation Company represents and warrants to My Electrician and My
Electrician that it:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      is unaware of System Owner dealing with its Environmental
Certificate creation rights except through the Assignment Agreement;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      has no claim over and will not interfere with My
Electrician’s rights under the Assignment Agreement;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-27.75pt'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)      consents to My Electrician or </span><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>its</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'> nominated Certificate
Creation Partner making statements to third parties in reliance on the
information provided under this Agreement by My Electrician, its associates and
customers;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>d)      considers the information contained in any Assignment
Agreement submitted to My Electrician to be true and correct in every
particular;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>e)      will inform My Electrician of any new information it
obtains in respect of a System that indicates that previously supplied
information in respect of that System is misleading, incorrect or inaccurate;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>f)       indemnifies My Electrician for any and all losses that My
Electrician may suffer if information described in 2.</span><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>3</span><span lang=EN-AU style='font-size:12.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:black'> (e), (g) and (i) is misleading,
incorrect or inaccurate;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>g)      will provide My Electrician and My Electrician any
information or assistance required to create Environmental Certificates or comply
with Regulator requirements;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>h)      will assist My Electrician and My Electrician to conduct
an inspection of the System the subject of the Assignment Agreement if requested
by My Electrician;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>i)       has supplied a System that is fit for purpose and in
compliance with all regulatory requirements (not just those relating to
Environmental Certificates);</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>j)       will refund to My Electrician erroneous payments, or
payments made where the Parcel of Environmental Certificates for a System is
less than that specified in the Assignment Agreement.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>2.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>4</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>     On Acceptance of an Assignment Agreement, My Electrician represents
and warrants to the Installation Company that it will:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      pay the amount specified in the Assignment Agreement by
electronic transfer to the Installation Company’s account within the timeframe
specified by My Electrician;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      comply with usual Regulator requirements associated with
the creation of the Parcel of Environmental Certificates.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:64.35pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>2.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>5</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>     The Installation Company must:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:92.7pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>2.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>5</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>.1      provide to every System Owner evidence of the system
installed by it using and completing the purchase receipt attached to the
Assignment Agreement in MOP and handing that receipt to the System Owner; and</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:99.25pt;margin-bottom:.0001pt;text-indent:-42.55pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>2.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>5</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>.2      ensure that the duplicate copy of the purchase receipt
attached to the Assignment Agreement in MOP and electronically completed on MOP
is identical to the purchase receipt provided to every System Owner in terms of
Clause 2.</span><span lang=EN-AU style='font-size:12.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:black'>5</span><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>.1.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>3.       My Electrician Online Portal (“MOP”):</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>3.1     At any time, the Installation Company may request access
to MOP.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>3.2     If the Installation Company requests access to MOP, My
Electrician may accept or decline the request in its absolute discretion.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>3.3     If My Electrician allows the Installation Company access
to MOP, the Installation Company agrees that it will comply with the terms of
use that are in force at the time of access and which are available on MOP. </span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>3.4     It is the </span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'>Installation
Company’s</span><span lang=EN-AU style='font-size:12.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:black'> responsibility to manage
user access.  The Installation Company must nominate Authorised Representatives
and advise of any user access changes by written email to My Electrician.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>3.5     MOP incorporates two areas, an </span><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>Administrator’s</span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'> Area and a
User’s Area. Different rules apply to Data in the two areas:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      in both the </span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'>Administrator’s</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'> Area and the </span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'>User’s</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'> Area:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>i.        Form Data is owned by My Electrician and licenced to My
Electrician;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>ii.       Solution Data is owned by My Electrician and licenced to
My Electrician;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>iii.      Solution Data has been received by My Electrician for
the purposes of the Privacy Act.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>3.6     Solution Data is in the Private Area until it is Lodged,
when it enters the Shared Area.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>3.7     Form Data is in the Private Area if it is associated with
Solution Data that has not been Submitted.  Otherwise it is in the Shared Area.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>3.8     The Installation Company has a licence to use Form Data
and Solution Data for the sole and exclusive purposes of complying with its
obligations under this Agreement and must not whether directly or indirectly
allow the Form Data and Solution Data to be used for any other purpose.  The
Installation Company acknowledges that if any such licence is breached in any
manner whatsoever that licence terminates immediately upon such breach and the
Installation Company immediately becomes liable to My Electrician for all and
any loss and damage they may suffer and My Electrician is entitled in its
absolute discretion to prevent access to MOP by the Installation Company and
without further notice.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>4.       General obligations:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>4.1.    During the course of this Agreement, the Installation
Company must:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      comply with directions and advice issued by My Electrician
and its associates;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      ensure that its employees, agents and independent
contractors comply with this Agreement and any Associated Agreements;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)      promptly advise My Electrician if its becomes aware of any
action that a third party is taking or may take that could result in a claim being
made against the Installation Company or result in a requirement for the
Installation Company to notify its insurer;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>d)      not represent that it can bind My Electrician or its associates
in any way;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>e)      comply with all applicable privacy and confidentiality
laws;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>f)       submit Assignment Agreements to My Electrician promptly
and at least one (1) month prior to any relevant regulatory deadline;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>g)      take all steps to make it clear to all third parties that
it is not an agent, partner or joint venturer with My Electrician or </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>its</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'> nominated Certificate
Creation Partner</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'> </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>and that it is an independent business entity;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>h)      immediately it becomes or ought to become aware thereof
notify My Electrician of any non-compliance by the Installation Company with
any provision of this Agreement or associated Agreements.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>5.       Payments:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>5.1     At any time the Parties may have payment obligations to
each other under this or any other Agreements.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>5.2     Where this is the case, My Electrician may in its sole and
absolute discretion:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      deduct amounts owed by the Installation Company from
amounts owed to the Installation Company, and pay a net amount as a final
settlement; or</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      in the event where outstanding payments are referred to a
collection agency and/or law firm, pass on all costs which would be incurred as
if the debt were collected in full, including legal demand costs.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>5.3     For the avoidance of doubt, these provisions are
applicable to any amounts owed to/claimed by My Electrician including amounts
resulting from:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      the terms of this Agreement; </span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      a breach of this Agreement;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)      the invalidity of any Environmental Certificates; </span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>d)      the operation of the common law or applicable statutes; or</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>e)      any other actions taken by My Electrician.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>5.4     Where My Electrician has previously been directed by the
Installation Company to make a payment in a particular manner, My Electrician is
entitled to assume that future payments may be made in the same manner until
the Installation Company informs My Electrician otherwise.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>5.5.1  The Installation Company must submit any payment claim to My
Electrician:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      in such format as My Electrician may from time to time
notify the Installation Company in writing;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      on or before 4:00pm on any Monday that is not a public
holiday in Victoria.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-2.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>5.5.2  a)      Any payment claims shall be paid within fourteen
(14) days reckoned from the first Monday after the date on which the
Installation Company lodges the payment claim with My Electrician.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      Despite anything to the contrary contained in this
Agreement unless and until the Installation Company complies with clause 2.4 of
this Agreement it is not entitled to and My Electrician is not obliged to make
any payment to the Installation Company for any transaction in respect of which
the requirements of clause 2.4 have not been met.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:64.35pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:64.35pt;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>5.5.3  Payment rates, changes to certificate pricing and schedule
updates will change from time to time and will be notified to the Installation
Company in terms of MOP.  By continuing to use MOP the Installation Company by
its conduct accepts such changes.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>6.       Third party requests and directions:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>6.1     In the course of running its business My Electrician may
interact with third parties including Certificate Creation Partners, Regulators
and Financiers.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>6.2     As a result of the interactions described in clause 6.1,
or otherwise, a third party may:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      make requests of and issue directions to My Electrician;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      bring proceedings against My Electrician; or</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)      otherwise impose costs on My Electrician.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>6.3     Where the actions described in clause 6.2 relate to the
Installation Company’s actions (or failure to act) or those of its
subcontractors, customers or associates, the Installation Company must:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      provide any assistance that My Electrician may require in
order to respond to or comply with the Regulators’ requests and directions;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      indemnify My Electrician for all its costs (both direct
and indirect) associated with responding to and complying with the requests and
directions (for the avoidance of doubt, these costs may include costs
associated with the procurement of professional services including consulting,
legal, tax, accounting, and engineering).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>7.       Breach and termination:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>7.1     A Party will notify the other if it is in default of this
Agreement.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>7.2     A Party is in default of this Agreement if it: </span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      fails to comply with any term of this Agreement; or</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      becomes Insolvent.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>7.3     If a Party (Defaulting Party) is in default of this
Agreement, the other Party (Injured Party) is entitled to:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      terminate this Agreement immediately by written notice; or
</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      recover damages in respect of its losses; or </span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)      both a) and b).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>7.4     If the Defaulting Party does not pay any damages within fourteen
(14) days of the Injured Party making a written request for payment, interest
will accrue at 10% per annum or the current Penalty Interest Rate (whichever is
the greater) on the damages from the date of default until the damages are
paid.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>7.5     If the Injured Party holds any monies, Environmental
Certificates or other property, rights or interests on behalf of the Defaulting
Party or is obliged to make payments to the Defaulting Party, then the Injured
Party is entitled to utilise these items to satisfy any damages claimed under
this Agreement.  This right is additional to the rights created elsewhere in
this Agreement.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>7.6     Either Party may Terminate this Agreement by thirty (30)
days written notice.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>7.7     If this Agreement is terminated then the Parties’
obligations under clauses 2.2(d</span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'>)-(</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>j), 2.3(a)-(b), 3.8, 5, 7, 8, 10</span><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>, </span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>11</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>, 12, 13, 15</span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'> and </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>16</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'> remain in place.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>8.       Non-disclosure:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>8.1     The Parties may not disclose information relating to or
shared under this Agreement to any person except:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      to the extent that it is already in the public domain;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      with the written consent of the other Party;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)      to its officers, employees and professional advisers; or</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>d)      as required by an applicable law or Code of Conduct after
first consulting (to the extent lawful and reasonably practical) with the other
Party about the form and content of the disclosure.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>8.2     Where permitted disclosures are made by a Party on any
basis other than clause 8.1 (a), they will use reasonable endeavours to ensure
the disclosed material is kept confidential by the Party to whom it has been
shared.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>9.       Representations and warranties</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>9.1     In respect of this Agreement the Installation Company
represents and warrants that:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      all information, documents and material provided to My
Electrician are and will be accurate and complete; and</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      it will deal with My Electrician in good faith and respond
promptly to Solution Provider reasonable requests for additional information or
other support that may be required.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>10.</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>     Relationship</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt'><span lang=EN-AU style='font-size:
12.0pt;line-height:115%;font-family:"Times New Roman","serif";color:black'>The
Installation Company is not an agent, partner or joint venturer nor is it
authorised to act on behalf or bind My Electrician or its nominated Certificate
Creation Partner and hereby indemnifies My Electrician or its nominated
Certificate Creation Partner in relation to all and any liability or
responsibility arising from or connected in any manner whatsoever to the
Installation Company’s dealings with System Owners.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>11.     Confidentiality:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>11.1   The Installation Company must keep confidential during and after
the end of this Agreement the following information whether or not it is in
material electronic, digital or machine readable or other form:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      all confidential information including but not limited to
intellectual property, trade secrets, confidential know-how (and the software)
relating to this Agreement, MOP, data contained within MOP and the Assignment
Agreement;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      those parts of all notes and other records prepared by the
Installation Company based on or incorporating the information referred to in
paragraphs (a) and (b) above; and</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)      all copies of the information and those parts of the notes
and other records referred to in paragraph (a), (b) and (c). </span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>11.2   The Installation Company must use Confidential Information
solely and exclusively for the purpose of complying with its obligations under
this Agreement and must not whether directly or indirectly allow the
Confidential Information to be used or disclosed for any other purpose.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>11.3   The Installation Company may disclose Confidential Information
only to those of its officers, employees and agents who:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      have a need to know (and only to the extent that each has
a need to know); and</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      have been directed to keep Confidential Information.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>11.4   The Installation Company must at its own costs:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      ensure at all times that each person to whom Confidential
Information has been disclosed complies with the agreement contemplated by
paragraph 11;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      notify My Electrician immediately if it becomes aware of a
suspected or actual breach of the Confidentiality Agreement;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)      take all steps to prevent or stop the suspected or actual
breach;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>d)      comply with any direction issued by My Electrician from
time to time regarding the enforcement of Confidentiality Agreement (including
but not limited to starting, conducting and settling enforcement proceedings);</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>e)      assign any Confidential Agreement to My Electrician at My
Electrician’s request.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>11.5   The obligations of confidentiality under this Agreement do
not extend to information that (whether before or after this Agreement is
executed):</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      is rightfully known to or is in the possession or control
of the Installation Company and not subject to an obligation of confidentiality
on the Installation Company; or</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      is public knowledge (otherwise than as a result of a
breach of this Agreement).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>11.6   If the Installation Company is required by law to disclose
any Confidential Information to a third party (including but not limited to a
government authority):</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      before doing so it must notify My Electrician in writing;
and</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      it must not do so until My Electrician has had a
reasonable opportunity to take any steps that My Electrician considers
necessary to protect the confidentiality of that information.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>11.7   The Installation Company acknowledges that its knowledge
concerning MOP and the intellectual property comes from My Electrician.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>12.     Non-dealing with Certificate Creation Partner:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>12.1   The Installation Company must not:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      other than as permitted by this Agreement engage in the
business:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>i.        to secure directly or indirectly from any Certificate
Creation Partner Environmental Certificates;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>ii.       to supply directly or indirectly software systems for
Environmental Certificate creation.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      from the date of ending or expiration of this Agreement
for a period of two (2) years within the State of Victoria:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>i.        engage in the business of or to secure directly or
indirectly from any Certificate Creation Partner Environmental Certificates;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>ii.       directly or indirectly solicit, canvass, approach or
accept any approach from any person who was at any time during the last twelve
(12) months of the Agreement:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:4.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:5.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)       a Certificate Creation partner of My Electrician;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:5.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:5.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      a referral contact of My Electrician;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:5.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:5.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)       any System Owner whose information appears on MOP other
than a System Owner whose details are first entered into MOP solely as a result
of the provision of services by the Installation Company of the nature referred
to in Introduction paragraph C to such Service Owner.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>12.2   In this clause “engaging” means to participate, assist or
otherwise be directly or indirectly involved as a member, shareholder, unit
holder, director, consultant, advisor, contractor, principal, agent, manager,
employee, beneficiary, partner, associate, trustee or financier.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>12.3   The Installation Company acknowledges that each of the
prohibitions and restrictions contained in this Clause 12:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      should be read and construed and will have the effect as a
separate, severable and independent prohibition or restriction and will be
enforceable accordingly; and</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      is reasonable as to period, territory or limitations and
subject matter in order to protect the legitimate commercial interest of My
Electrician.</span></p>

<p class=MsoNormal><span lang=EN-AU style='font-size:12.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:black'>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-AU style='font-size:12.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>13.     Costs:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>13</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1   Each Party is
responsible for its own costs and expenses in connection with and incidental to
the preparation and execution of this Agreement.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>14</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.     Goods and
Services Tax:<br>
<br>
</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>14</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1   To the extent that
GST is applicable to the transactions contemplated by this Agreement, the
Parties agree to apply it in a manner consistent with any public rulings or
other explanatory material published by the Australian Tax Office.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt'><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>15</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.     Dispute
resolution:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>15</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1   If a dispute
arises between the Parties under this Agreement or Other Agreements, the
following steps must be taken by the Parties prior to taking any action to
commence legal proceedings:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>a)      the aggrieved Party must provide a written notice to the
other Party specifying their concerns.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>b)      The Party receiving a notice under clause </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>15</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1(a) must provide a
written response within fourteen (14) days.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>c)      If the aggrieved party is not satisfied with a response
provided under clause </span><span lang=EN-AU style='font-size:12.0pt;
line-height:115%;font-family:"Times New Roman","serif";color:black'>15</span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>.1(b) then they must provide a further notice in writing to the
other Party requesting that a meeting be held between the chief executives of
each Party.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>d)      The meeting between the chief executives must be held
within fourteen (14) days of the other Party receiving a notice under clause </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>15</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1(c).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>e)      If the dispute remains unresolved after the meeting described
in clause </span><span lang=EN-AU style='font-size:12.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:black'>15</span><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>.1(d) then either Party may by notice in writing to the other
direct that the matters raised in the documents referred to in clauses </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>15</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1(a) and </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>15</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1(b) be resolved by
mediation.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>f)       A party receiving a notice under clause </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>15</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1(e) must co-operate
with the other Party to hold a mediation within fourteen (14) days of receipt
of the notice.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>g)      If the mediation concludes and the dispute remains
unresolved then either Party may bring legal proceedings in respect of the
dispute as specified in the documents referred to in clauses </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>15</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1(a) and </span><span
lang=EN-AU style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>15</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1(b).</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>h)      If a Party does not comply with the requirements of this
clause </span><span lang=EN-AU style='font-size:12.0pt;line-height:115%;
font-family:"Times New Roman","serif";color:black'>15</span><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'> then the other party may bring legal proceedings immediately
notwithstanding that all of the steps set out in this clause have not yet been
completed.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:1.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>16</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.     General:</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>16</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.1   The invalidity of
any part or provision of this Agreement or Other Agreement will not affect the
enforceability of any other part or provision of this Agreement.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>16</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.2   The Parties will
sign and execute all assurances, documents and deeds and do such deeds, acts
and things as are required by the provisions of this Agreement to give effect
to it.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>16</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.3   This Agreement is
governed by and construed in accordance with the laws applicable in Victoria
and the Parties submit to the jurisdiction of the Courts in that State.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>16</span><span lang=EN-AU style='font-size:12.0pt;line-height:
115%;font-family:"Times New Roman","serif";color:black'>.4   This Agreement is
intended to be legally binding upon the parties.</span><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>  For the purpose of this Clause 16.4 the Parties include but is
not limited to legal successors, agents, assigns, related entities and
subcontractors and members, shareholders, unit holders, directors, consultants,
advisors, contractors, principals, agents, managers, employees, beneficiaries,
partners, associates, trustees or financiers of each of the parties.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:2.0cm;margin-bottom:.0001pt;text-indent:-1.0cm'><span lang=EN-AU
style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif";
color:black'>&nbsp;</span></p>

</div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>      
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
        <script type="text/javascript">
        	jQuery('.filter-date').datepicker();
        </script>
    </body>
</html>