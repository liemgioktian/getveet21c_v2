<!DOCTYPE html>
<html lang="en"  class="body-error"><head>
        <meta charset="utf-8">
        <title>My Electrician Online Portal</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/validationEngine.jquery.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading" style="margin-top:5px">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <h3 class="bold" style="color:#828396">My Electrician Online Portal Company Sign Up</h3>
                                </div>
                                <div class="col-sm-12 top">
                                    <div class="progress" style="margin-bottom:0px">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                                                page 1 of 4
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">

                            <!--signup Step1 setting company-->
                            <div class="signup" id="signup1">
                                <div class="row space">
                                    <div class="col-sm-12 text-center">
                                        <img src="<?= base_url('asset/images/logo-green.png') ?>" style="height:80px" alt="">
                                        <br>
                                    </div>
                                    <!--
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">
                                    </div>-->
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <div class="form-group">
                                            <form method="POST" action="<?= site_url('signup/2') ?>" enctype="multipart/form-data">
                                            <div class="col-sm-12">
                                                <p class="text-justify top">
                                                    Welcome to the My Electrician Online Portal Company sign up.
                                                </p>
                                                <p class="text-justify top">
                                                    The process will only take a few minutes and will allow you to log in to the portal and have access to information about how to access VEET incentives.
                                                </p>
                                                <p class="text-justify">
                                                    Be sure to have your Company and A Class licence details handy as only complete profiles will be granted access.
                                                </p>
                                                <p class="text-justify">
                                                    There are no upfront costs and credit card information is not required. <br>
                                                    We look forward to assisting your business in accessing VEET Scheme incentives.
                                                </p>
                                                
                                                <br>
                                                <p class="">
                                                    Thank's
                                                </p>
                                               <p>
                                                   The My Electrician Team
                                               </p>

                                            </div>
                                            <div class="col-sm-12 text-center">
                                                <input type="submit" class="btn btn-sm btn-primary" value="NEXT" />
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <!--close signup step 1 setting-->

                            <!--signup Step2 setting company-->
                            <div class="signup hidden" id="signup2">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= base_url('asset/images/logo-green.png') ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <form method="POST" action="<?= site_url('signup/3') ?>" enctype="multipart/form-data" id="signup_form1">
                                            <div class="form-group">
                                                <div class="col-sm-12 text-center"><h4>Electricians Details</h4></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Authorised Business Representative</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="author" size="50"  value="<?= isset($author)?$author:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Electricians Company Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="electricians_company_name" size="50"  value="<?= isset($electricians_company_name)?$electricians_company_name:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">ABN Number:</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="abn_number" size="25"  value="<?= isset($abn_number)?$abn_number:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Company Address</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="company_address" size="50"  value="<?= isset($company_address)?$company_address:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-3">
                                                    <label for="" class="bold col-sm-12">Suburb</label>
                                                    <div class="col-sm-12" style="margin-bottom:10px">
                                                        <input type="text" class="validate[required] form-control input-sm" name="suburb" size="25"  value="<?= isset($suburb)?$suburb:'' ?>" />
                                                    </div>
                                                </div>    
                                                <div class="col-sm-5">
                                                    <label for="" class="bold col-sm-12">postal code</label>
                                                    <div class="col-sm-7" style="margin-bottom:10px">
                                                        <input type="text" class="validate[required] form-control input-sm" name="postal_code" size="25"  value="<?= isset($postal_code)?$postal_code:'' ?>" />
                                                    </div>
                                                <div class="clearfix"></div>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Telephone Number</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="telephone_number" size="25"  value="<?= isset($telephone_number)?$telephone_number:'' ?>" /> 
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Web Site</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="website" size="50"  value="<?= isset($website)?$website:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Email Address</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required,custom[email]] form-control input-sm" name="email_address" size="50"  value="<?= isset($email_address)?$email_address:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">R.E.C Number</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="rec_number" size="25"  value="<?= isset($rec_number)?$rec_number:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Registered for GST</label>
                                                <div class="col-sm-6">
                                                    <label class="radio-inline">
                                                        <input type="radio" class="validate[required]" name="gst" id="gst1" <?= isset($gst) && $gst == 'Y'? 'checked':'' ?> value="Y"> Yes
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" class="validate[required]" name="gst" id="gst2" <?= isset($gst) && $gst == 'N'? 'checked':'' ?> value="N"> No
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <!--
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Activity 21C Certificate Price</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="c_certificate" size="25" value="<?= isset($c_certificate)?$c_certificate:'' ?>" /> 
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="bold col-sm-3">Activity 21D Certificate Price</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="d_certificate" size="25" value="<?= isset($d_certificate)?$d_certificate:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <!--
                                            <?php if(isset($non_super_admin_company)): ?>
                                                <div class="form-group">
                                                    <label for="" class="bold col-sm-3">Client's Activity 21C Certificate Price</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="validate[required] form-control input-sm" name="client_c_certificate" size="25" 
                                                        value="<?= isset($non_super_admin_company->c_certificate)?$non_super_admin_company->c_certificate:'' ?>" />
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="bold col-sm-3">Client's Activity 21D Certificate Price</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="validate[required] form-control input-sm" name="client_d_certificate" size="25" 
                                                        value="<?= isset($non_super_admin_company->d_certificate)?$non_super_admin_company->d_certificate:'' ?>" />
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            <?php endif; ?>
                                            -->
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Upload Logo</label>
                                                <div class="col-sm-6">
                                                    <input type="file" name="company_logo" />
                                                    <small>*) don't forget to backup existing logo</small>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <br/>
                                            <div class="col-xs-12">
                                                <br/><b>Installers</b>
                                                <small>*) installer with blank first name will not be saved</small>
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                <table cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                    <thead>
                                                        <tr class="skyblue">
                                                            <th></th>
                                                            <th>First name</th>
                                                            <th>Last name</th>
                                                            <th>A Class Licence No.</th>
                                                            <th>Issue/ Renewal Date</th>
                                                            <th>Expiry Date</th>
                                                            <th>Email address</th>
                                                            <th>Phone number</th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $table_loop=1; if(isset($installer)):foreach($installer as $ins): ?>
                                                        <tr class="">
                                                            <td><?= $table_loop ?><input type="hidden" name="installer[<?= $table_loop ?>][iid]" value="<?= $ins['iid'] ?>" /></td>
                                                            <td><input type="text"  name="installer[<?= $table_loop ?>][first_name]" /></td>
                                                            <td><input type="text"  name="installer[<?= $table_loop ?>][last_name]"  /></td>
                                                            <td><input type="text"  name="installer[<?= $table_loop ?>][license_number]" /></td>
                                                            <td><input type="text"  class="datepicker" name="installer[<?= $table_loop ?>][renewal_date]" /></td>
                                                            <td><input type="text"  class="datepicker" name="installer[<?= $table_loop ?>][expiry_date]" /></td>
                                                            <td><input type="text"  name="installer[<?= $table_loop ?>][email_address]" /></td>
                                                            <td><input type="text"  name="installer[<?= $table_loop ?>][phone_number]" /></td>
                                                            <td>
                                                                Upload ID Photo<input type="file"  name="installer[<?= $table_loop ?>][photo]" />
                                                            </td>
                                                            <td>
                                                                <a href="<?= site_url('settingscontroller/delete_installer/'.$ins['iid'].'/confirm') ?>" class="btn btn-sm btn-danger">DELETE</a>
                                                            </td>
                                                        </tr>
                                                        <?php $table_loop++; endforeach; endif; ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr class="">
                                                            <th>Add New Installer</th>
                                                            <th><input type="text" class="validate[required] installer" name="installer[<?= $table_loop ?>][first_name]" /></th>
                                                            <th><input type="text" class="validate[required] installer" name="installer[<?= $table_loop ?>][last_name]" /></th>
                                                            <th><input type="text" class="validate[required] installer" name="installer[<?= $table_loop ?>][license_number]" /></th>
                                                            <th><input type="text" class="validate[required] installer datepicker" name="installer[<?= $table_loop ?>][renewal_date]" /></th>
                                                            <th><input type="text" class="validate[required] installer datepicker" name="installer[<?= $table_loop ?>][expiry_date]" /></th>
                                                            <th><input type="text" class="validate[required] installer" name="installer[<?= $table_loop ?>][email_address]" /></th>
                                                            <th><input type="text" class="validate[required] installer" name="installer[<?= $table_loop ?>][phone_number]" /></th>
                                                            <th colspan="2">Upload ID Photo<input type="file" name="installer[<?= $table_loop ?>][photo]" /></th>
                                                            <th colspan="2">Upload ID Photo 2<input type="file" name="installer[<?= $table_loop ?>][photo2]" /></th>
                                                        </tr>
                                                    </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                            <br/>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-12 text-center">
                                                            <div class="checkbox">
                                                                <label>
                                                                  <input type="checkbox" id="agree_company"> ‘I agree to My Electrician’s <a href="<?= site_url('company_agreement') ?>" target="_blank" style="color:#0000FF">Terms and Conditions</a>’ 
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>

                                            
                                          <div class="col-sm-12 text-center space">
                                            <!-- <button type="button" class="btn btn-sm btn-primary next" id="save_setting" data-next="2">NEXT</button> -->
                                            <!--<input type="submit" class="btn btn-sm btn-primary hidden" value="NEXT" id="next2"/>-->
                                            <button type="button" id="next2" data-loading-text="please wait..." class="btn btn-sm btn-primary hidden" autocomplete="off">
                                              Next
                                            </button>
                                          </div>
                                         
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--close signup step 1 setting-->

                            <!--signup step3-->
                            <!--
                            <div class="signup hidden" id="signup3">
                                <div class="row guide" id="guide1">
                                    <div class="col-sm-offset-1 col-sm-10">
                                        <h3 class="bold">How to create VEECs when replacing halogens with LEDs</h3>
                                        <h3 class="bold">
                                            What you need to know
                                        </h3>
                                        <h4 class="italic">Please read these pages carefully, complete our online quiz and we will call you to progress your certificate creation.</h4>
                                        <br>
                                        <h4 class="bold">An overview of the Victorian Energy Efficiency Target</h4>
                                        <p class="text-justify">
                                            VEECs for halogen downlight replacements are produced under Prescribed Activity 21 of the Victorian Energy Efficiency Target (VEET). The scheme provides financial incentives for owners or tenants of both domestic and commercial premises to replace existing 12V halogen lamps (Activity 21C) or replace the lamps and the associated transformers (Activity 21D) with an approved energy efficient product to reduce their energy consumption and lower greenhouse gas emissions. The quantity of certificates each installation can produce varies, depending on the number of downlights replaced and the efficacy of the energy efficient products installed.
                                        </p>
                                        <p class="text-justify">
                                            Certificates are created on a ‘deemed basis’ (i.e. the lifetime savings are created upfront, typically for up to ten years), which reduces upfront costs and enables a quicker return on investment. The lighting upgrade will also result in significant savings in electricity on an ongoing basis.
                                        </p>
                                        <p class="text-justify">
                                            Each VEEC represents the equivalent of one tonne of carbon dioxide abated as a result of the prescribed activity. Mandatory scheme participants, (i.e. Victorian electricity and/or gas retailers) must surrender a number of VEECs each year based on the share of the energy market that they held in that year, or face a penalty for any shortfall. This obligation to acquire certificates creates a dynamic market driven by supply and demand, and the value of certificates fluctuates on a regular basis accordingly.
                                        </p>
                                        <br>
                                        <h4 class="bold">General guidelines for claiming VEECs</h4>
                                        <p class="text-justify">
                                            VEECs may only be claimed for eligible lighting activities once the activity has been implemented and commissioned. You must submit the  supporting documentation to allow Green Energy Trading to confirm scheme compliance and registration of certificates.
                                        </p>
                                        <p class="text-justify">
                                            The right to create VEECs must be assigned from the energy consumer to Green Energy Trading using an approved assignment form, which is signed by the energy consumer and the electrician responsible for the installation. This procedure forms the basis of Green Energy Trading’s authority to act.
                                        </p>
                                        <p class="text-justify">
                                            VEECs must be created before 30 June in the calendar year after the installation date. After this date it is not possible to claim VEECs for installations in previous calendar year(s).
                                        </p>
                                        <p class="text-justify">
                                                Underpinning all environmental certificate schemes is the requirement to pass on a demonstrable financial benefit to the energy consumer.
                                        </p>
                                        <br>
                                        
                                        <h4 class="bold">VEEC creation checklists</h4>
                                        <p class="text-justify">For your handy reference, all considerations for VEEC creation are summarised in the following checklists.</p>
                                        <ol>
                                            <li>
                                                <p class="bold">Prepare your business for VEEC creation</p>
                                                <p class="text-justify">
                                                    Complete the training and sign a Relationship Agreement with My Electrician prior to commencing any installations.
                                                </p>
                                                <p class="text-justify">
                                                    Provide copies of licences for each electrician that will be working on your business’ behalf, prior to (or at the time) of submitting your first VEEC 21 claim.
                                                </p>
                                                <p class="text-justify">
                                                    Be familiar with your obligations under the Victorian Occupational Health and Safety Act 2004. Obligations include, but not limited to:
                                                </p>
                                                <ul style="padding-left:20px">
                                                    <li><p class="text-justify">Provision of adequate supervision and training of employees.</p></li>
                                                    <li><p class="text-justify">Provision and maintenance of safe systems of work.</p></li>
                                                    <li><p class="text-justify">Implementation of robust safety management and risk management systems.</p></li>
                                                    <li><p class="text-justify">Ensuring appropriate contractor management systems are in place.</p></li>
                                                </ul>
                                                <p class="text-justify">
                                                    Be familiar with your obligations under the Australian Consumer Law and the Competition and Consumer Act 2010. Obligations include, but are not limited to:
                                                </p>
                                                <ul style="padding-left:20px">
                                                    <li><p class="text-justify">Adhering to telemarketing and door-to-door sales practices, (including compliance with the Federal Governments “Do Not Call Register” and “Do Not Knock” stickers, and the times of day that consumers can be contacted).</p></li>
                                                    <li><p class="text-justify">Ensuring that employees do not engage in misleading or deceptive conduct (including claiming the installation is mandatory or that the employee is working on behalf of the Government).</p></li>
                                                </ul>
                                                <p class="text-justify">
                                                    Brief your sales team in relation to important key messages:
                                                </p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">It is appropriate to say:</p>
                                                        <ul style="padding-left:15px">
                                                            <li><p class="text-justify">“The Victorian Energy Saver Incentive provides a financial incentive for your lighting upgrade.”</p></li>
                                                            <li>
                                                                <p class="text-justify">“This is a voluntary scheme and certain conditions need to be met to be eligible.”</p>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">It is not appropriate to say:</p>
                                                        <ul style="padding-left:15px">
                                                            <li>
                                                                <p class="text-justify">“I work for the Victorian Government.”</p>
                                                            </li>
                                                            <li>
                                                                <p class="text-justify">“This lighting upgrade is mandatory.”</p>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">
                                                    Brief your sales team in relation to appropriate conduct:
                                                </p>
                                                <ul style="margin-left:20px">
                                                    <li>
                                                        <p class="text-justify">It is appropriate to:</p>
                                                        <ul style="padding-left:15px">
                                                            <li>
                                                                <p class="text-justify">Leave appropriate contact details (e.g. business card) at the premises, even when a sale doesn’t go ahead.</p>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">It is not appropriate to:</p>
                                                        <ul style="padding-left:15px">
                                                            <li>
                                                                <p class="text-justify">Use aggressive sales techniques.</p>
                                                            </li>
                                                            <li>
                                                                <p class="text-justify">Give contact details of the Victorian Government / Essential Services Commission instead of your own name and company details.</p>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <p class="text-justify bold">Understand the eligibility requirements</p>
                                                <p class="text-justify">In order for a project to be eligible to create VEECs it must consist of either of the following:</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">Replacement of at least one 35W (or higher) halogen downlight MR16 ELV lamp only in a residential or commercial premises with an approved energy efficient lamp that is compatible with the existing transformer – Activity 21C</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Replacement of at least one 35W (or higher) halogen downlight MR16 ELV and the associated transformer in a residential or commercial premises with an approved energy efficient lamp and driver – Activity 21D</p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">Any other type of lamp, including 240V halogen downlights (e.g. GU10 or PAR38), cannot not be replaced as part of the VEEC Activity.</p>
                                                <br>
                                                <p class="text-justify">Check that all energy efficient lighting products to be installed are listed on the Approved Products list.</p>
                                                <br>
                                                <p class="text-justify">All existing lighting products that are removed from the premises must be decommissioned in an environmentally friendly manner. All lamps must be recycled by companies which have the facility to safely recover and reuse mercury, glass, phosphor and aluminium. For Activity 21D, all replaced transformers must also be recycled by a third party recycling agent. Evidence of the type and quantity of removed products can be achieved through two means:</p>
                                                <br>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">An itemised recycling receipt for the disposal of the lamps (and transformers), clearly defining the quantity and the installation address.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">OR, where small installations are bundled together on one itemised recycling receipt with a total quantity, a photo of the removed lamps and transformers laid out in a grid pattern is  to confirm quantities for each installation.</p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">An ‘A class’ licenced electrician must conduct a pre-installation inspection of existing wiring condition at the premises.</p>
                                                <p class="text-justify">Where the pre-installation inspection reveals any non-compliant or unsafe electrical conditions, then the lighting upgrade installation cannot commence. The energy consumer must be advised to rectify or remove any identified unsafe electrical condition in accordance with the Electricity Safety Act 1998.</p>
                                                <p class="text-justify">A licenced electrician must carry out the installation and where any electrical wiring work was involved, a Certificate of Electrical Safety is provided.</p>
                                                <p class="text-justify">The energy efficient products must not be installed onto dimmable circuits, unless they are approved by the manufacturer as suitable.</p>
                                                <p class="text-justify">The installation team must not leave any spare energy efficient products at the premises, all energy efficient lamps must be fully installed by the electrician and the existing lamps must be removed from the premises.</p>
                                                <br>
                                            </li>
                                            <li>
                                                <p class="bold text-justify">Prior to the installation</p>
                                                <p class="text-justify">Take a photo of the name and address of a piece of mail of the energy consumer for the installation address for part of the documentary evidence. This is proves that the consumer signing the assignment from is a resident of the address that is listed down.</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">This must contain the installation site address, the name of the energy consumer.</p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">Conduct pre-installation inspection of existing wiring condition and report any issues to the energy consumer.</p>
                                                <p class="text-justify">Determine quantity of existing halogen MR16 ELV lamps, and the type of existing transformers.</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">•   Make note of these details. For Activity 21C installations, collect the brand and model number of the existing transformer, so that compatibility can be verified.</p>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <p class="bold text-justify">During installation</p>
                                                <p class="text-justify">Carry out installation, adhering to all State Electrical Regulations and requirements.</p>
                                                <p class="text-justify">Collect photographic evidence of:</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">Each type of existing halogen lamp.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Each type of existing transformer, clearly showing the markings including brand and model number.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">The quantity of removed lamps by laying them out in a grid and taking a photo. Lamps are to be decommissioned by removal of /p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">If the installation involved electrical wiring work, a Certificate of Electrical Safety (CES) must be provided to the energy consumer.</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">Ensure the ‘completion of work’ date on the CES is accurate.</p>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <p class="text-justify bold">Post installation</p>
                                                <br>
                                                <p class="text-justify">Organise removal of all replaced lamps to a registered recycling depot and obtain itemised recycling receipt.</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">•   Ensure this details the product quantities, lamp type, transformer type and installation address.</p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">Issue a final installation tax invoice to the energy consumer. The invoice should include:</p>
                                                <ul style="padding-left:35px">
                                                    <li>
                                                        <p class="text-justify">Energy consumer name.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Installation address.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Installed products’ model number and quantity.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">VEEC benefit provided.</p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">The My Electrician online form contains an automated Proof of purchase feature which satisfies the above requirement.</p>
                                                <p class="text-justify">Complete the VEEC 21 Assignment Form and obtain the signatures of the energy consumer and the electrician.</p>
                                                <p class="text-justify">If the energy consumer is a business that is registered for GST, obtain a tax invoice from the energy consumer to Green Energy Trading for the value of the VEEC benefit supplied, inclusive of GST.</p>
                                                <p class="text-justify">Collate all  documentation and submit to Green Energy Trading:</p>
                                                <p class="text-justify">Completed VEEC 21 Assignment Form with handwritten signatures.</p>
                                                <ul style="padding-left:35px">
                                                    <li>
                                                        <p class="text-justify">Copy of electrician’s licence (if not provided previously).</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Electricity bill.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Installation invoice.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Photos of removed lamps, existing transformers.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Certificate of Electrical Safety (if wiring work was involved).</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Tax invoice to Green Energy Trading (only if energy consumer is registered for GST).</p>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ol>
                                        <br>
                                        <h4 class="bold">Efficient and compliant VEEC creation</h4>
                                        <p class="text-justify">Maintaining high standards of processing compliance is a hallmark of our approach. Our procedures involve rigorous checking of all documents and information so that they adhere to the Essential Services Commission’s guidelines. Green Energy Trading has one of the best records for accuracy in certificate schemes as measured by the low incidence of invalid certificate creation and a prompt certificate turnaround time, which improves with streamlining of processes. We are flexible in our approach and we work with our clients to facilitate continuous improvement.</p>
                                        <h4 class="bold">Audits</h4>
                                        <p class="text-justify">As an AP under the VEET, our supplier, Green Energy Trading is obligated to undertake periodic audits to ascertain that we have retained the necessary records and confirm the creation of VEECs in accordance with the VEET legislation. An audit may include a site inspection of the installation or a telephone audit with the energy consumer.</p>
                                        <h4 class="bold">Congratulations</h4>
                                        <p class="text-justify">You are one step closer to creating VEECs for halogen replacements. We will contact you within one business day to prepare you for your business’ development and certificate creation. In the meantime, with any queries please call us on <strong>1300 660 042</strong>.</p>
                                        <br><br><br>
                                        <small class="bold">The information in this document is believed to be accurate at the time of writing and we do not guarantee the accuracy of any information or data contained and accept no responsibility for any loss, injury or inconvenience sustained by users of this document.</small>
                                        
                                    </div>
                                </div>
                                 <div class="row space">
                                    <div class="col-sm-12 text-center">
                                        <button type="button" class="btn btn-sm btn-primary next" id="next4" name="next4" data-next="4">NEXT</button>
                                    </div>
                                </div>
                            </div>
                            <!--close signup3 -->

                            <!--signup step4-->
                            <!--
                            <div class="signup hidden" id="signup4">
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <iframe name='proprofs' id='proprofs' width='100%' height='1000' frameborder=0 marginwidth=0 marginheight=0 src='https://www.proprofs.com/quiz-school/story.php?title=ODUyNTMwC8OH&id=852269&ew=430'></iframe><div style='font-size:10px; font-family:Arial, Helvetica, sans-serif; text-align:center;'><a href='https://www.proprofs.com/quiz-school/story.php?title=ODUyNTMwC8OH' target='_blank' title='Illumination for the creation of VEECs under Activity 21 Development module (February 2015)'>Illumination for the creation of VEECs under Activity 21 Development module (February 2015)</a> » <a href='https://www.proprofs.com/quiz-school/' title='ProProfs Quiz Maker' target='_blank' rel=''>ProProfs Quiz Maker</a></div>
                                    </div>
                                </div>
                                <div class="row space">
                                    <div class="col-sm-12 text-center">
                                        <button type="button" class="btn btn-sm btn-primary back" id="back3" name="back3" data-back="3">BACK</button>
                                        <button type="button" class="btn btn-sm btn-primary next" id="next5" name="next5" data-next="5">NEXT</button>
                                    </div>
                                </div>
                            </div>
                            <!--close signup4 -->


                            <!--signup step3-->
                            <div class="signup hidden" id="signup3">
                                <form action="<?= site_url('signup/4') ?>" class="form-horizontal form-signup3" method="post"> 
                                    <div class="row box_desc" style="border:none;margin-top:5px">
                                        <div class="col-sm-12 text-center">
                                            <h3 class="bold text-center">Create Adminsrator log in</h3>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="row box_desc">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/images/avatar.png') ?>" class="text-center" style="width:150px" alt="">
                                        </div>
                                    </div>
                                -->
                                    <div class="row box_desc" style="border:none">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="col-sm-offset-4 col-sm-4">
                                                    <input required class="form-control input-md email" name="email" type="text"  placeholder="Email" />
                                                    <input required class="form-control input-md" name="password" type="password"  placeholder="Password" />
                                                    <input required class="form-control input-md" name="password2" type="password"  placeholder="Password" />
                                                    <div class="clear"></div>
                                                    <!--<div class="button-login"><input type="submit" value="Sign up"></div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row space">
                                        <div class="col-sm-12 text-center">
                                            <!--<button type="button" class="btn btn-sm btn-primary back" id="back2" name="back2" data-back="2">BACK</button>-->
                                            <!-- <button type="button" class="btn btn-sm btn-primary next" id="next6" name="next6" data-next="6">NEXT</button> -->
                                            <input type="submit" value="NEXT" class="btn btn-sm btn-primary" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--close signup3-->

                            <!--signup step4-->
                            <div class="signup hidden" id="signup4">
                                <div class="row box_desc">
                                    <div class="col-sm-offset-3 col-sm-6 space">
                                        


                                        <p class="justify">You have successfully created your company profile and are almost ready to create VEECs.</p>
                                        <p class="justify">Your log in will be activated within 24 hours.</p>
                                        <p class="justify">You will be notified by email when your account is active and further details will be provided about the final steps required to create VEECs</p>
                                        <br>
                                        <p class="">Thanks for signing up!</p>
                                        <p class="">
                                            Kind Regards,
                                            <br>
                                            The My Electrician Team
                                        </p>
                                        
                                        <!--<div class="button-login text-center top"><a href="<?= site_url()?>" class="btn-success btn-md btn">SIGN IN</a></div>-->
                                    </div>
                                </div>
                                <div class="row space">
                                    <div class="col-sm-12 text-center">
                                        <!--<button type="button" class="btn btn-sm btn-primary back" id="back3" name="back3" data-back="3">BACK</button>-->
                                        <a href="<?= site_url()?>" class="btn-success btn-sm btn">SIGN IN</a>
                                    </div>
                                </div>
                                <!--
                                <div class="row space">
                                    <div class="col-sm-12 text-center">
                                        <button type="button" class="btn btn-sm btn-primary back" id="back6" name="back6" data-back="6">BACK</button>
                                        <button type="button" class="btn btn-sm btn-primary next" id="next8" name="next8" data-next="8">NEXT</button>
                                    </div>
                                </div>
                            -->
                            </div>
                            <!--close signup step4-->

                            <!--signup step8
                            <div class="signup hidden" id="signup">
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <h3 class="bold">VEET installer approval documents</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 space text-center">
                                        Permanent Link To Your Form : <br>
                                        <a href="https://myelect.wufoo.com/forms/xjpu6gw03c2n93/" target="_blank">https://myelect.wufoo.com/forms/xjpu6gw03c2n93/</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <div id="wufoo-xjpu6gw03c2n93" class="space">
                                            Fill out my <a href="https://myelect.wufoo.com/forms/xjpu6gw03c2n93">online form</a>.
                                        </div>
                                    </div>
                                </div>
                                <div class="row space">
                                    <div class="col-sm-12 text-center">
                                        <button type="button" class="btn btn-sm btn-primary back" id="back7" name="back7" data-back="7">BACK</button>
                                        <a href="<?= site_url()?>" class="btn-success btn-sm btn">SIGN IN</a>
                                    </div>
                                </div>
                            </div>
                            <!--close signup8 -->


                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" style="" id="loading" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" style="">
                <div class="modal-content" style="box-shadow:none;border:none;background:transparent;">
                  
                    <div class="modal-body" >
                        <div style="text-align:center;background:white;width:140px;border-radius:5px;margin:150px auto">
                          <img src=<?php echo base_url('asset/images/loading.gif') ?> alt="">
                        </div>
                    </div>
                      
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


    
        <script type="text/javascript">var site_url = '<?= site_url() ?>';</script>
        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.ui.touch-punch.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/jquery.validationEngine.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.validationEngine-en.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/signup.js') ?>" type="text/javascript"></script>
    </body>
</html>

