<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>assignment form</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/select2.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/select2-bootstrap.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <link rel="stylesheet" type="text/css" href="<?= base_url('asset/css/jquery.signaturepad.css') ?>" />
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">

    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="content">
                    <div class="panel">
                        
                        <div class="panel-body">
                            <form enctype="multipart/form-data" action="" class="form-horizontal" method="post" role="form" id="assignment">
                                <input type="hidden" name="mapsrc" id="mapsrc">
                                <input name="action" id="action" type="hidden">
                        
                                <!--page 1-->
                                <div class="page" id="page1">
                                    <div class="" id="map-canvas"></div>
                                    <div class="row">
                                        <div class="col-sm-4 hidden-xs">
                                            <img src="<?= $logo ?>" style="height:80px" alt="">
                                        </div>
                                        <div class="col-sm-4">
                                            <h2 class="text-center bold">VEEC 21</h2>
                                            <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                        </div>
                                        <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                        <div class="col-sm-3 hidden-xs">
                                            <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <h5 id="A3" name="A3" data-cell="A3" data-formula="IF(AE16='Residential','Residential Assignment Form version 2.05 August 2014',IF(AE16='Commercial','Non Residential Assignment Form version 2.05 August 2014','Assignment Form version 2.05 August 2014'))">Assignment Form version 2.05 August 2014</h5>
                                            <input value="Assignment Form version 2.05 August 2014" id="input_A3" name="input_A3" data-cell="input_A3" data-formula="A3" type="hidden">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4" style="visibility: hidden">
                                            <div class="form-group">
                                                <label for="" class="col-sm-6 col-xs-8">Job Reference</label>
                                                <div class="col-sm-6 col-xs-4">
                                                    <input style="border:solid 1px grey" class="form-control input-sm text-right bold" 
                                                           id="H4" name="H4" data-cell="H4" type="text" 
                                                           value="<?= isset($stored['H4'])?$stored['H4']:$clientNumber ?>"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label for="" class="col-sm-6 col-xs-8">Client number</label>
                                                <div class="col-sm-6 col-xs-4">
                                                  <input type="text" style="border:solid 1px grey" class="form-control input-sm text-right bold text-center" id="client" name="client" data-cell="H5" data-formula="'C21448'">
                                                </div>
                                              </div>
                                        </div>
                                        <div class="col-sm-3 hidden-xs">
                                            <p class="text-justify">
                                                2 Domville Avenue, Hawthorn VIC 3122 <br>
                                                T:1300 077 784    F:03 9815 1066
                                            </p>
                                            <p class="text-justify" style="color:green">
                                                Forms@greenenergytrading.com.au <br>
                                            </p>
                                            <p class="text-justify">
                                                ABN 21 128 476 406
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>About the Victorian Energy Efficiency Target</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <p class="text-justify">
                                                Consumers in respect of whom a prescribed activity is undertaken can create Victorian Energy Efficiency Certificates (VEECs)
                                                under the Victorian Energy Efficiency Target Act 2007. One VEEC represents one tonne of carbon dioxide equivalent (CO2-e) to
                                                be reduced by the prescribed activity undertaken by the consumer.
                                                Consumers or their authorised signatories are able to assign their right to create VEECs to an Accredited Person. In assigning
                                                their right to an Accredited Person, the Accredited Person will be entitled to create and own certificates in respect of the
                                                prescribed activity undertaken by the consumer. In return, the Accredited Person should provide consumers with an identifiable
                                                benefit for the assignation, such as a price reduction on a product, free installation or a cash-back arrangement. Consumers
                                                should be aware that it is their responsibility to negotiate satisfactory terms with the Accredited Person in return for assigning their
                                                right to create VEECs.
                                            </p>
                                            <p class="text-center red bold">
                                                Yellow fields are mandatory. <br>
                                                Depending on answers, some fields may change
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>Energy consumer details (i.e.for whom the installation was performed)</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <div class="form-group">
                                                <label for="" class="col-xs-8">Was the installation carried out in a residential or commercial premises?</label>
                                                <div class="col-xs-4">
                                                    <select type="text" class="form-control input-sm yellow" id="AE16" name="AE16" data-cell="AE16">
                                                        <option value="" <?= isset($stored['AE16']) && $stored['AE16'] == '' ? 'selected="selected"' : '' ?>></option>
                                                        <option value="Residential" selected="selected" <?= isset($stored['AE16']) && $stored['AE16'] == 'Residential' ? 'selected="selected"' : '' ?>>Residential</option>
                                                        <option value="Commercial" <?= isset($stored['AE16']) && $stored['AE16'] == 'Commercial' ? 'selected="selected"' : '' ?>>Commercial</option>
                                                    </select>
                                                    <input value="1" class="form-control input-sm yellow" id="AW19" name="AW19" data-cell="AW19" data-formula="AW16*AW18" type="hidden">
                                                    <input value="1" class="form-control input-sm yellow" id="AW16" name="AW16" data-cell="AW16" data-formula="IF(AE16='Commercial',0,1)" type="hidden">
                                                    <input value="1" class="form-control input-sm yellow" id="AW18" name="AW18" data-cell="AW18" data-formula="IF(AE18='Business-business use',0,1)" type="hidden">
                                                </div>
                                            </div>

                                            <div id="C18" class="form-group">
                                                <label for="" class="col-xs-8 ">Is the lighting system owned by a business for business use or an individual for private use?</label>
                                                <div class="col-xs-4 ">
                                                    <select type="text" class="form-control input-sm" id="AE18" name="AE18" data-cell="AE18">
                                                        <option <?= isset($stored['AE18']) && $stored['AE18'] == '' ? 'selected="selected"' : '' ?> selected="selected" value=""></option>
                                                        <option <?= isset($stored['AE18']) && $stored['AE18'] == 'Individual-private use' ? 'selected="selected"' : '' ?> value="Individual-private use" selected="selected">Individual-private use</option>
                                                        <option <?= isset($stored['AE18']) && $stored['AE18'] == 'Business-business use' ? 'selected="selected"' : '' ?> value="Business-business use">Business-business use</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <!--
                                                    <input type="hidden" id="AW16" name="AW16" data-cell="AW16" data-formula="IF(AE16='Commercial',0,1)">
                                                    <input type="hidden" id="AW18" name="AW18" data-cell="AW18" data-formula="IF(AE18='Business-business use',0,1)">
                                                    <input type="hidden" id="AW20" name="AW20" data-cell="AW20" data-formula="AW16AW18">
                                                    -->
                                                    <label for="" class="" id="C21" name="C21" data-cell="C21" data-formula="IF(AW20=2,'Energy consumers name (title, first name and surname)', 'Energy consumers first name')">Authorised signatory name</label>
                                                    <input value="Authorised signatory name" data-cell="CALX1" id="input_C21" name="input_C21" data-formula="C21" type="hidden">

                                                </div>
                                                <div class="col-xs-4">
                                                    <label for="">Last Name</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label for="" class="" id="Y21" name="Y21" data-cell="Y21" data-formula="IF(AND(AE18='Individual-private use',AE16='Residential'),' ','Company name')">Company name</label>
                                                    <input value="Company name" data-cell="CALX2" id="input_Y21" name="input_Y21" data-formula="Y21" type="hidden">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <input value="<?= isset($stored['C22'])?$stored['C22']:'' ?>" class="form-control input-sm yellow" required id="C22" name="C22" data-cell="C22" type="text">
                                                </div>
                                                <div class="col-xs-4">
                                                    <input value="<?= isset($stored['C23'])?$stored['C23']:'' ?>" class="form-control input-sm yellow" required id="C23" name="C23" data-cell="C23" type="text">
                                                </div>
                                                <div class="col-xs-4">
                                                    <input value="<?= isset($stored['Y22'])?$stored['Y22']:'' ?>" class="form-control input-sm yellow private" id="Y22" name="Y22" data-cell="Y22" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-3 col-xs-6">
                                                    <label for="">Tenant/Landlord/Owner?</label>
                                                    <select type="text" class="form-control input-sm yellow" id="C25" name="C25" data-cell="C25">
                                                        <option <?= isset($stored['C25']) && $stored['C25'] == 'tenant' ? 'selected="selected"' : '' ?> value="tenant">tenant</option>
                                                        <option <?= isset($stored['C25']) && $stored['C25'] == 'landlord' ? 'selected="selected"' : '' ?> value="landlord">landlord</option>
                                                        <option <?= isset($stored['C25']) && $stored['C25'] == 'owner' ? 'selected="selected"' : '' ?> value="owner" selected>owner</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3 col-xs-6">
                                                    <label for="" class="" id="M24" name="M24" data-cell="M24" data-formula="IF(AND(AE18='Individual-private use',AE16='Residential'),' ','Position in company/title')">Position in company/title</label>
                                                    <input value="Position in company/title" class="form-control input-sm yellow" id="input_M24" name="input_M24" data-cell="input_M24" data-formula="M24" type="hidden">
                                                    <input value="<?= isset($stored['M25'])?$stored['M25']:'' ?>" class="form-control input-sm yellow private" id="M25" name="M25" data-cell="M25" type="text">
                                                </div>
                                                <div class="col-sm-3 col-xs-6">
                                                    <label for="" class="" id="Y24" name="Y24" data-cell="Y24" data-formula="IF(AND(AE18='Individual-private use',AE16='Residential'),' ','ABN/ACN')">ABN/ACN</label>
                                                    <input value="ABN/ACN" class="form-control input-sm yellow" id="input_Y24" name="input_Y24" data-cell="input_Y24" data-formula="Y24" type="hidden">
                                                    <input value="<?= isset($stored['Y25'])?$stored['Y25']:'' ?>" class="form-control input-sm yellow private" id="Y25" name="Y25" data-cell="Y25" type="text">
                                                </div>
                                                <div class="col-sm-3 col-xs-6">
                                                    <label for="" class="" id="AI24" name="AI24" data-cell="AI24" data-formula="IF(AND(AE18='Individual-private use',AE16='Residential'),' ','Is the business registered for GST?')">Is the business registered for GST?</label>
                                                    <input value="Is the business registered for GST?" class="form-control input-sm yellow" id="input_AI24" name="input_AI24" data-cell="input_AI24" data-formula="AI24" type="hidden">
                                                    <select type="text" class="form-control input-sm yellow private" id="AI25" name="AI25" data-cell="AI25">
                                                        <option <?= isset($stored['AI25']) && $stored['AI25'] == '' ? 'selected="selected"' : '' ?> value=""></option>
                                                        <option <?= isset($stored['AI25']) && $stored['AI25'] == 'Yes' ? 'selected="selected"' : '' ?> value="Yes">Yes</option>
                                                        <option <?= isset($stored['AI25']) && $stored['AI25'] == 'No' ? 'selected="selected"' : '' ?> value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <label for="">Phone</label>
                                                </div>
                                                <div class="col-xs-8">
                                                    <label for="">Postal address (if different from installation address below)</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <input value="<?= isset($stored['C28'])?$stored['C28']:'' ?>" class="form-control input-sm yellow" id="C28" name="C28" type="text" required>
                                                </div>
                                                <div class="col-xs-8">
                                                    <input value="<?= isset($stored['R28'])?$stored['R28']:'' ?>" class="form-control input-sm " id="R28" name="R28" data-cell="R28" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <label for="">Email</label>
                                                </div>
                                                <div class="col-xs-6">
                                                    <label for="" class="" id="Y30" name="Y30" data-cell="Y30" data-formula="IF(AND(AE18='Individual-private use',AE16='Residential'),' ','Industry type')">Industry type</label>
                                                    <input value="Industry type" class="form-control input-sm " id="input_Y30" name="input_Y30" data-cell="input_Y30" data-formula="Y30" type="hidden">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-4">
                                                    <input value="<?= isset($stored['C31'])?$stored['C31']:'' ?>" class="form-control input-sm yellow input_email" id="C31" name="C31" data-cell="C31" type="text" required>
                                                </div>
                                                <div class="col-xs-6 col-sm-7">
                                                    
                                                        <input tabindex="-1" value="<?= isset($stored['Y31'])?$stored['Y31']:'' ?>" class="form-control input-sm yellow industry private" id="Y31" name="Y31" data-cell="Y31" type="text">
                                                </div>
                                                <div class="col-xs-2 col-sm-1">
                                                    <input value="<?= isset($stored['AS31'])?$stored['AS31']:'' ?>" class="form-control input-sm private" id="AS31" name="AS31" data-cell="AS31" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5 style="display: inline" id="O33" name="O33" data-cell="O33" data-formula="IF(AE16='Commercial','Installation address - must be a commercial premises','Installation address')">Installation address</h5>
                                            <input value="Installation address" class="form-control input-sm " id="input_O33" name="input_O33" data-cell="input_O33" data-formula="O33" type="hidden">
                                            <a class="btn btn-xs pull-right retrieve-address">retrieve</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc map-retieve-inputs">
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label for="">Unit type</label>
                                                    <select type="text" class="form-control input-sm yellow" id="C36" name="C36" data-cell="C36">
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Apartment' ? 'selected="selected"' : '' ?> value="Apartment">Apartment</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Cottage' ? 'selected="selected"' : '' ?> value="Cottage">Cottage</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Factory' ? 'selected="selected"' : '' ?> value="Factory">Factory</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Flat' ? 'selected="selected"' : '' ?> value="Flat">Flat</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'House' ? 'selected="selected"' : '' ?> value="House" selected>House</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Office' ? 'selected="selected"' : '' ?> value="Office">Office</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Room' ? 'selected="selected"' : '' ?> value="Room">Room</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Shed' ? 'selected="selected"' : '' ?> value="Shed">Shed</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Shop' ? 'selected="selected"' : '' ?> value="Shop">Shop</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Showroom' ? 'selected="selected"' : '' ?> value="Showroom">Showroom</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Site' ? 'selected="selected"' : '' ?> value="Site">Site</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Suite' ? 'selected="selected"' : '' ?> value="Suite">Suite</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Townhouse' ? 'selected="selected"' : '' ?> value="Townhouse">Townhouse</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Unit' ? 'selected="selected"' : '' ?> value="Unit">Unit</option>
                                                        <option <?= isset($stored['C36']) && $stored['C36'] == 'Villa' ? 'selected="selected"' : '' ?> value="Villa">Villa</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="">Unit number</label>
                                                    <input value="<?= isset($stored['I36'])?$stored['I36']:'' ?>" class="form-control input-sm " id="I36" name="I36" data-cell="I36" type="text">
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="">Street number</label>
                                                    <input value="<?= isset($stored['P36'])?$stored['P36']:'' ?>" class="form-control input-sm yellow" id="P36" name="P36" data-cell="P36" type="text" required>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label for="">Street name</label>
                                                    <input value="<?= isset($stored['W36'])?$stored['W36']:'' ?>" class="form-control input-sm yellow" id="W36" name="W36" data-cell="W36" type="text" required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="">Street type</label>
                                                        <input tabindex="-1" value="<?= isset($stored['AO36'])?$stored['AO36']:'' ?>" class="form-control input-sm yellow street select2-offscreen yellow" id="AO36" name="AO36" data-cell="AO36" type="text" required>
                                                        <input type="hidden" value="<?= isset($stored['AO37'])?$stored['AO37']:'' ?>" class="form-control input-sm yellow" id="AO37" name="AO37" data-cell="AO37">
                                                    <input id="AP36" name="AP36" data-cell="AP36" type="hidden">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label for="">Town/suburb</label>
                                                    <input value="<?= isset($stored['C39'])?$stored['C39']:'' ?>" class="form-control input-sm yellow" id="C39" name="C39" data-cell="C39" type="text">
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="">State</label>
                                                    <input value="<?= isset($stored['U39'])?$stored['U39']:'victoria' ?>" class="form-control input-sm" id="U39" name="U39" data-cell="U39" type="text" value="victoria">
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="">Postcode</label>
                                                    
                                                    <input tabindex="-1" value="<?= isset($stored['Z39'])?$stored['Z39']:'' ?>" class="form-control input-sm postcode select2-offscreen" id="Z39" name="Z39" data-cell="Z39" type="text" required>
                                                    <input type="hidden" value="<?= isset($stored['Z40'])?$stored['Z40']:'' ?>" class="form-control input-sm " id="Z40" name="Z40" data-cell="Z40">
                                                    <input class="form-control input-sm" id="AW69" name="AW69" data-cell="AW69" type="hidden">
                                                    <input value="" class="form-control input-sm" id="AX69" name="AX69" data-cell="AX69" data-formula="IF(AW69='Regional',1.04,IF(AW69='Metropolitan',0.98,''))" type="hidden">
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="" class="" id="AE38" name="AE38" data-cell="AE38" data-formula="IF(AND(AE18='Individual-private use',AE16='Residential'),'','Number of levels')">Number of levels</label>
                                                    <input value="Number of levels" id="input_AE38" name="input_AE38" data-cell="input_AE38" data-formula="AE38" type="hidden">
                                                    <input value="<?= isset($stored['AE39'])?$stored['AE39']:'' ?>" class="form-control input-sm yellow private" id="AE39" name="AE39" data-cell="AE39" type="text">
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="" class="" id="AM38" name="AM38" data-cell="AM38" data-formula="IF(AND(AE18='Individual-private use',AE16='Residential'),'','Total floor space (m2)')"></label>
                                                    <input id="input_AM38" name="input_AM38" data-cell="input_AM38" data-formula="AM38" type="hidden">
                                                    <input value="<?= isset($stored['AM39'])?$stored['AM39']:'' ?>" class="form-control input-sm yellow private" id="AM39" name="AM39" data-cell="AM39" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label for="">Has there previously been a VEEC lighting installation done in the past at this address?</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <select type="text" class="form-control input-sm yellow" id="C42" name="C42" data-cell="C42">
                                                        <option <?= isset($stored['C42']) && $stored['C42'] == 'No' ? 'selected="selected"' : '' ?> value="No">No</option>
                                                        <option <?= isset($stored['C42']) && $stored['C42'] == 'Yes' ? 'selected="selected"' : '' ?> value="Yes">Yes</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div id="C43" class="form-group hidden">
                                                <label for="" class="col-xs-6">Please give reason for additional visit</label>
                                                <div class="col-xs-6">
                                                    <input value="<?= isset($stored['L43'])?$stored['L43']:'' ?>" class="form-control input-sm yellow" id="L43" name="L43" data-cell="L43" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>VEEC benefit calculation</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <label for="" class="bold">Activity 21C</label>

                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <label for="" class="col-xs-12">Number of VEECs</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold"></label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <label for="" class="col-xs-12">Certificate price</label>

                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold"></label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <input value="<?= isset($stored['C51'])?$stored['C51']:'' ?>" class="form-control input-sm" id="C51" name="C51" data-cell="C51" data-formula="ROUND(AS94,0)" data-format=" 0,0[.]" type="text">
                                                                    <input value="<?= isset($stored['O74'])?$stored['O74']:'' ?>" id="AV94" name="AV94" data-cell="AV94" data-formula="O74" type="hidden">
                                                                    <input value="" id="AV96" name="AV96" data-cell="AV96" data-format="$ 0,0[.]00" data-formula="AV100*10" type="hidden">
                                                                    <input value="" id="AV98" name="AV98" data-cell="AV98" data-format="$ 0,0[.]00" data-formula="AV96/AV94" type="hidden">
                                                                    <input value="" id="AV100" name="AV100" data-cell="AV100" data-format="$ 0,0[.]00" data-formula="AV102/11" type="hidden">
                                                                    <input value="" id="AV102" name="AV102" data-cell="AV102" data-format="$ 0,0[.]00" data-formula="AI53" type="hidden">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold">X</label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <input value="<?= isset($stored['M51'])?$stored['M51']: $settings_app['c_certificate'] ?>" class="form-control input-sm" id="M51" name="M51" data-cell="M51" data-format="$ 0,0[.]00" type="text" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold">=</label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <input value="<?= isset($stored['W51'])?$stored['W51']:'' ?>" class="form-control input-sm" id="W51" name="W51" data-cell="W51" data-format="$ 0,0[.]00" data-formula="C51*M51" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <label for="" class="bold">Activity 21D</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <label for="" class="col-xs-12">Number of VEECs</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold"></label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <label for="" class="col-xs-12">Certificate price</label>

                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold"></label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <input value="<?= isset($stored['C55'])?$stored['C55']:'' ?>" class="form-control input-sm" id="C55" name="C55" data-cell="C55" data-format=" 0,0[.]" data-formula="ROUND(AS96,0)" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold">X</label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <input value="<?= isset($stored['M55'])?$stored['M55']:$settings_app['d_certificate'] ?>" class="form-control input-sm" id="M55" name="M55" data-cell="M55" data-format="$ 0,0[.]00" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold">=</label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <input value="<?= isset($stored['W55'])?$stored['W55']:'' ?>" class="form-control input-sm" id="W55" name="W55" data-cell="W55" data-format="$ 0,0[.]00" data-formula="C55*M55" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <label for="" class="bold">Activity 21E</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <label for="" class="col-xs-12">Number of VEECs</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold"></label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <label for="" class="col-xs-12">Certificate price</label>

                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold"></label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <input value="<?= isset($stored['C56'])?$stored['C56']:'' ?>" class="form-control input-sm" id="C56" name="C56" data-cell="C56" data-format=" 0,0[.]" data-formula="ROUND(AS98,0)" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold">X</label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <input value="<?= isset($stored['M56'])?$stored['M56']:$settings_app['e_certificate'] ?>" class="form-control input-sm" id="M56" name="M56" data-cell="M56" data-format="$ 0,0[.]00" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 text-center">
                                                            <label for="" class="bold">=</label>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="form-group">
                                                                <div class="col-xs-12">
                                                                    <input value="<?= isset($stored['W56'])?$stored['W56']:'' ?>" class="form-control input-sm" id="W56" name="W56" data-cell="W56" data-format="$ 0,0[.]00" data-formula="C56*M56" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="" class="col-xs-12">Total amount payable</label>
                                                        <div class="col-xs-8">
                                                            <input value="<?= isset($stored['AI53'])?$stored['AI53']:'' ?>" class="form-control input-sm" id="AI53" name="AI53" data-cell="AI53" data-format="$ 0,0[.]00" data-formula="IF(OR(W51 > 0,W55 > 0, W56 > 0),W51+W55+W56,'')" type="text">
                                                        </div>
                                                        <label for="" class="col-xs-4">excl. GST</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row top">
                                                <div class="col-xs-12">
                                                    <p class="text-justify">Notes :</p>
                                                    <p class="text-justify">
                                                        (i). Certificate prices can fluctuate and are valid on the date this assignment from is received at Green Energy Trading, subject to installed product approval. <br>
                                                        (ii). VEECs can only be claimed within 6 months after the end of the year in which the activity was completed. GET will only accept form if they are received complete and correct, one month prior to this date
                                                    </p>

                                                </div>
                                            </div>

                                            <p class="text-right bold top"><small>ΘGreen Energy Trading Pty Ltd 2014</small></p>

                                        </div>
                                    </div>

                                    

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>Installation details</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">

                                            <div class="form-group">
                                                <label for="" class="col-xs-12 ">Installation date</label>
                                                <div class="col-xs-4 col-sm-2">
                                                    <input value="<?= isset($stored['C68'])?$stored['C68']:'' ?>" class="form-control input-sm datepicker" id="C68" name="C68" data-cell="C68" type="text">
                                                    <input class="form-control input-sm datepicker hidden" id="C69" name="C69" data-cell="C69" type="text" data-formula="DATEFORMAT(TODAY(), 'DD/MM/YYYY')">
                                                    <input value="<?= isset($stored['O67'])?$stored['O67']:'' ?>" class="form-control input-sm datepicker hidden " id="O67" name="O67" data-cell="O67" type="text">
                                                </div>
                                            </div>

                                            <label for="">Please complete the following table with details of the removed and installed lighting</label>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                        <table id="tcodes_tbl" cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 3%">&nbsp;</th>
                                                                    <th class="skyblue" style="width: 40%" colspan="4">What has been removed</th>
                                                                    <th class="warm" style="width: 57%" colspan="6">What has been installed</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class=" small center" style="width:3%"></th>
                                                                    <th class="skyblue small center" style="width:25%"><label for="">Select existing product</label></th>
                                                                    <th class="skyblue small center" style="width:10%"><label for="">Transformer type</label></th>
                                                                    <th class="skyblue small center" style="width:10%"><label for=""># of lamps</label></th>
                                                                    <th class="skyblue small center" style="width:5%"><label for="">Activity</label></th>
                                                                    <th class="warm small center" style="width:27%"><label for="">Lamp brand and model numer</label></th>
                                                                    <th class="warm small center" style="width:5%"><label for=""># of lamps</label></th>
                                                                    <th class="warm small center" style="width:10%"><label for="">Compatible with existing transfomer</label></th>
                                                                    <th class="warm small center" style="width:5%"><label for="">Output</label></th>
                                                                    <th class="warm small center" style="width:5%"><label for="">Efficacy</label></th>
                                                                    <th class="warm small center" style="width:5%"><label for="">Lifetime</label></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr id="row1" class="">
                                                                    <td class="text-center"><label for="">1</label></td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control existing" data-count="74" name="C74" id="C74" data-cell="C74">
                                                                            <option <?= isset($stored['C74']) && $stored['C74'] == '' ? 'selected' : '' ?> value=""></option>
                                                                            <option <?= isset($stored['C74']) && $stored['C74'] == '12V Halogen lamp only' ? 'selected' : '' ?> value="12V Halogen lamp only" selected>12V Halogen lamp only</option>
                                                                            <option <?= isset($stored['C74']) && $stored['C74'] == '12V Halogen lamp &amp; transfomer' ? 'selected' : '' ?> value="12V Halogen lamp &amp; transfomer">12V Halogen lamp &amp; transfomer</option>
                                                                            <option <?= isset($stored['C74']) && $stored['C74'] == '230V Halogen GU10 lamp only' ? 'selected' : '' ?> value="230V Halogen GU10 lamp only">230V Halogen GU10 lamp only</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control" name="K74" id="K74" data-cell="K74">
                                                                            <option <?= isset($stored['K74']) && $stored['K74'] == '' ? 'selected' : '' ?> value=""></option>
                                                                            <option <?= isset($stored['K74']) && $stored['K74'] == '1' ? 'selected' : '' ?> value="1">Electronic</option>
                                                                            <option <?= isset($stored['K74']) && $stored['K74'] == '2' ? 'selected' : '' ?> value="2">Magnetic</option>
                                                                            <option <?= isset($stored['K74']) && $stored['K74'] == '3' ? 'selected' : '' ?> value="3">N/A</option>
                                                                        </select>
                                                                        <input value="" id="L74" name="L74" data-cell="L74" data-formula="IF(K74=1,'Electronic',IF(K74=2,'Magnetic',IF(K74=3,'N/A',' ')))" type="hidden">
                                                                    </td>
                                                                    <td class=""><input value="<?= isset($stored['O74'])?$stored['O74']:'' ?>" class="input-sm form-control text-center yellow" id="O74" name="O74" data-cell="O74" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['R74'])?$stored['R74']:'' ?>" class="input-sm form-control text-center" id="R74" name="R74" data-cell="R74" data-formula="IF(C74='12V Halogen lamp only','21C',IF(C74='12V Halogen lamp &amp; transfomer','21D',IF(C74='230V Halogen GU10 lamp only','21E',' ')))" type="text"></td>
                                                                    <td class="">
                                                                        <input value="<?= isset($stored['T74'])?$stored['T74']:'' ?>" class="input-sm form-control text-center yellow c_product" data-count="74" id="T74" name="T74" data-cell="T74" type="text">
                                                                        <input value="<?= isset($stored['T73'])?$stored['T73']:'' ?>" class="input-sm form-control text-center yellow d_product hidden" data-count="74" id="T73" name="T73" data-cell="T73" type="text">
                                                                        <input value="<?= isset($stored['TT73'])?$stored['TT73']:'' ?>" class="input-sm form-control text-center yellow e_product hidden" data-count="74" id="TT73" name="TT73" data-cell="TT73" type="text">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" data-count="74" id="S74" name="S74" data-cell="S74">
                                                                    </td>
                                                                    <td class=""><input value="<?= isset($stored['AI74'])?$stored['AI74']:'' ?>" class="input-sm form-control text-center" id="AI74" name="AI74" data-cell="AI74" data-formula="O74" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AK74'])?$stored['AK74']:'' ?>" class="input-sm form-control text-center" id="AK74" name="AK74" data-cell="AK74" data-formula="IF(AI74 > O74,O74,AI74)" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AL74'])?$stored['AL74']:'' ?>" class="input-sm form-control text-center" id="AL74" name="AL74" data-cell="AL74" data-formula="IF(AI74='','',IF(AND(C74='12V Halogen lamp only',AM74=K74),'Yes',IF(AND(C74='12V Halogen lamp only',AI74 < > K74),'No','NA')))" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AM74'])?$stored['AM74']:'' ?>" class="input-sm form-control text-center" id="AM74" name="AM74" data-cell="AM74" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AN74'])?$stored['AN74']:'' ?>" class="input-sm form-control text-center" id="AN74" name="AN74" data-cell="AN74" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AP74'])?$stored['AP74']:'' ?>" class="input-sm form-control text-center" id="AP74" name="AP74" data-cell="AP74" readonly="readonly" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AR74'])?$stored['AR74']:'' ?>" class="input-sm form-control text-center" id="AR74" name="AR74" data-cell="AR74" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AS74'])?$stored['AS74']:'' ?>" class="input-sm form-control text-center" id="AS74" name="AS74" data-cell="AS74" readonly="readonly" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AT74'])?$stored['AT74']:'' ?>" class="input-sm form-control text-center" id="AT74" name="AT74" data-cell="AT74" data-formula="IF(OR(AL74='Yes', AL74='NA'),AK74*AR74*AX69,'')" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AU74'])?$stored['AU74']:'' ?>" class="input-sm form-control text-center" id="AU74" name="AU74" data-cell="AU74" data-formula="IF(R74='21C',AT74,0)" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AV74'])?$stored['AV74']:'' ?>" class="input-sm form-control text-center" id="AV74" name="AV74" data-cell="AV74" data-formula="IF(R74='21D',AT74,0)" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AY74'])?$stored['AY74']:'' ?>" class="input-sm form-control text-center" id="AY74" name="AY74" data-cell="AY74" data-formula="IF(R74='21E',AT74,0)" type="text"></td>
                                                                </tr>
                                                                <tr id="row1" class="">
                                                                    <td class="text-center"><label for="">2</label></td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control existing" data-count="76" name="C76" id="C76" data-cell="C76">
                                                                            <option <?= isset($stored['C76']) && $stored['C76'] == '' ? 'selected' : '' ?> value=""></option>
                                                                            <option <?= isset($stored['C76']) && $stored['C76'] == '12V Halogen lamp only' ? 'selected' : '' ?> value="12V Halogen lamp only" selected>12V Halogen lamp only</option>
                                                                            <option <?= isset($stored['C76']) && $stored['C76'] == '12V Halogen lamp &amp; transfomer' ? 'selected' : '' ?> value="12V Halogen lamp &amp; transfomer">12V Halogen lamp &amp; transfomer</option>
                                                                            <option <?= isset($stored['C76']) && $stored['C76'] == '230V Halogen GU10 lamp only' ? 'selected' : '' ?> value="230V Halogen GU10 lamp only">230V Halogen GU10 lamp only</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control" name="K76" id="K76" data-cell="K76">
                                                                            <option <?= isset($stored['K76']) && $stored['K76'] == '' ? 'selected' : '' ?> value=""></option>
                                                                            <option <?= isset($stored['K76']) && $stored['K76'] == '1' ? 'selected' : '' ?> value="1">Electronic</option>
                                                                            <option <?= isset($stored['K76']) && $stored['K76'] == '2' ? 'selected' : '' ?> value="2">Magnetic</option>
                                                                            <option <?= isset($stored['K76']) && $stored['K76'] == '3' ? 'selected' : '' ?> value="3">N/A</option>
                                                                        </select>
                                                                        <input value="" id="L76" name="L76" data-cell="L76" data-formula="IF(K76=1,'Electronic',IF(K76=2,'Magnetic',IF(K76=3,'N/A',' ')))" type="hidden">
                                                                    </td>
                                                                    <td class=""><input value="<?= isset($stored['O76'])?$stored['O76']:'' ?>" class="input-sm form-control text-center yellow" id="O76" name="O76" data-cell="O76" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['R76'])?$stored['R76']:'' ?>" class="input-sm form-control text-center" id="R76" name="R76" data-cell="R76" data-formula="IF(C76='12V Halogen lamp only','21C',IF(C76='12V Halogen lamp &amp; transfomer','21D',IF(C76='230V Halogen GU10 lamp only','21E',' ')))" type="text"></td>
                                                                    <td class="">
                                                                        <input value="<?= isset($stored['T76'])?$stored['T76']:'' ?>" class="input-sm form-control text-center yellow c_product" data-count="76" id="T76" name="T76" data-cell="T76" type="text">
                                                                        <input value="<?= isset($stored['T75'])?$stored['T75']:'' ?>" class="input-sm form-control text-center yellow d_product hidden" data-count="76" id="T75" name="T75" data-cell="T75" type="text">
                                                                        <input value="<?= isset($stored['TT75'])?$stored['TT75']:'' ?>" class="input-sm form-control text-center yellow e_product hidden" data-count="76" id="TT75" name="TT75" data-cell="TT75" type="text">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" data-count="76" id="S76" name="S76" data-cell="S76">
                                                                    </td>
                                                                    <td class=""><input value="<?= isset($stored['AI76'])?$stored['AI76']:'' ?>" class="input-sm form-control text-center" id="AI76" name="AI76" data-cell="AI76" data-formula="O76" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AK76'])?$stored['AK76']:'' ?>" class="input-sm form-control text-center" id="AK76" name="AK76" data-cell="AK76" data-formula="IF(AI76 > O76,O76,AI76)" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AL76'])?$stored['AL76']:'' ?>" class="input-sm form-control text-center" id="AL76" name="AL76" data-cell="AL76" data-formula="IF(AI76='','',IF(AND(C76='12V Halogen lamp only',AM76=K76),'Yes',IF(AND(C76='12V Halogen lamp only',AI76 < > K76),'No','NA')))" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AM76'])?$stored['AM76']:'' ?>" class="input-sm form-control text-center" id="AM76" name="AM76" data-cell="AM76" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AN76'])?$stored['AN76']:'' ?>" class="input-sm form-control text-center" id="AN76" name="AN76" data-cell="AN76" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AP76'])?$stored['AP76']:'' ?>" class="input-sm form-control text-center" id="AP76" name="AP76" data-cell="AP76" readonly="readonly" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AR76'])?$stored['AR76']:'' ?>" class="input-sm form-control text-center" id="AR76" name="AR76" data-cell="AR76" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AS76'])?$stored['AS76']:'' ?>" class="input-sm form-control text-center" id="AS76" name="AS76" data-cell="AS76" readonly="readonly" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AT76'])?$stored['AT76']:'' ?>" class="input-sm form-control text-center" id="AT76" name="AT76" data-cell="AT76" data-formula="IF(OR(AL76='Yes', AL76='NA'),AK76*AR76*AX69,'')" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AU76'])?$stored['AU76']:'' ?>" class="input-sm form-control text-center" id="AU76" name="AU76" data-cell="AU76" data-formula="IF(R76='21C',AT76,0)" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AV76'])?$stored['AV76']:'' ?>" class="input-sm form-control text-center" id="AV76" name="AV76" data-cell="AV76" data-formula="IF(R76='21D',AT76,0)" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AY76'])?$stored['AY76']:'' ?>" class="input-sm form-control text-center" id="AY76" name="AY76" data-cell="AY76" data-formula="IF(R76='21E',AT76,0)" type="text"></td>
                                                                </tr>
                                                                <tr id="row1" class="">
                                                                    <td class="text-center"><label for="">3</label></td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control existing" data-count="78" name="C78" id="C78" data-cell="C78">
                                                                            <option <?= isset($stored['C78']) && $stored['C78'] == '' ? 'selected' : '' ?> value=""></option>
                                                                            <option <?= isset($stored['C78']) && $stored['C78'] == '12V Halogen lamp only' ? 'selected' : '' ?> value="12V Halogen lamp only" selected>12V Halogen lamp only</option>
                                                                            <option <?= isset($stored['C78']) && $stored['C78'] == '12V Halogen lamp &amp; transfomer' ? 'selected' : '' ?> value="12V Halogen lamp &amp; transfomer">12V Halogen lamp &amp; transfomer</option>
                                                                            <option <?= isset($stored['C78']) && $stored['C78'] == '230V Halogen GU10 lamp only' ? 'selected' : '' ?> value="230V Halogen GU10 lamp only">230V Halogen GU10 lamp only</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control" name="K78" id="K78" data-cell="K78">
                                                                            <option <?= isset($stored['K78']) && $stored['K78'] == '' ? 'selected' : '' ?> value=""></option>
                                                                            <option <?= isset($stored['K78']) && $stored['K78'] == '1' ? 'selected' : '' ?> value="1">Electronic</option>
                                                                            <option <?= isset($stored['K78']) && $stored['K78'] == '2' ? 'selected' : '' ?> value="2">Magnetic</option>
                                                                            <option <?= isset($stored['K78']) && $stored['K78'] == '3' ? 'selected' : '' ?> value="3">N/A</option>
                                                                        </select>
                                                                        <input value="" id="L78" name="L78" data-cell="L78" data-formula="IF(K78=1,'Electronic',IF(K78=2,'Magnetic',IF(K78=3,'N/A',' ')))" type="hidden">
                                                                    </td>
                                                                    <td class=""><input value="<?= isset($stored['O78'])?$stored['O78']:'' ?>" class="input-sm form-control text-center yellow" id="O78" name="O78" data-cell="O78" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['R78'])?$stored['R78']:'' ?>" class="input-sm form-control text-center" id="R78" name="R78" data-cell="R78" data-formula="IF(C78='12V Halogen lamp only','21C',IF(C78='12V Halogen lamp &amp; transfomer','21D',IF(C78='230V Halogen GU10 lamp only','21E',' ')))" type="text"></td>
                                                                    <td class="">
                                                                        <input value="<?= isset($stored['T78'])?$stored['T78']:'' ?>" class="input-sm form-control text-center yellow c_product" data-count="78" id="T78" name="T78" data-cell="T78" type="text">
                                                                        <input value="<?= isset($stored['T77'])?$stored['T77']:'' ?>" class="input-sm form-control text-center yellow d_product hidden" data-count="78" id="T77" name="T77" data-cell="T77" type="text">
                                                                        <input value="<?= isset($stored['TT77'])?$stored['TT77']:'' ?>" class="input-sm form-control text-center yellow e_product hidden" data-count="78" id="TT77" name="TT77" data-cell="TT77" type="text">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" data-count="78" id="S78" name="S78" data-cell="S78">
                                                                    </td>
                                                                    <td class=""><input value="<?= isset($stored['AI78'])?$stored['AI78']:'' ?>" class="input-sm form-control text-center" id="AI78" name="AI78" data-cell="AI78" data-formula="O78" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AK78'])?$stored['AK78']:'' ?>" class="input-sm form-control text-center" id="AK78" name="AK78" data-cell="AK78" data-formula="IF(AI78 > O78,O78,AI78)" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AL78'])?$stored['AL78']:'' ?>" class="input-sm form-control text-center" id="AL78" name="AL78" data-cell="AL78" data-formula="IF(AI78='','',IF(AND(C78='12V Halogen lamp only',AM78=K78),'Yes',IF(AND(C78='12V Halogen lamp only',AI78 < > K78),'No','NA')))" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AM78'])?$stored['AM78']:'' ?>" class="input-sm form-control text-center" id="AM78" name="AM78" data-cell="AM78" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AN78'])?$stored['AN78']:'' ?>" class="input-sm form-control text-center" id="AN78" name="AN78" data-cell="AN78" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AP78'])?$stored['AP78']:'' ?>" class="input-sm form-control text-center" id="AP78" name="AP78" data-cell="AP78" readonly="readonly" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AR78'])?$stored['AR78']:'' ?>" class="input-sm form-control text-center" id="AR78" name="AR78" data-cell="AR78" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AS78'])?$stored['AS78']:'' ?>" class="input-sm form-control text-center" id="AS78" name="AS78" data-cell="AS78" readonly="readonly" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AT78'])?$stored['AT78']:'' ?>" class="input-sm form-control text-center" id="AT78" name="AT78" data-cell="AT78" data-formula="IF(OR(AL78='Yes', AL78='NA'),AK78*AR78*AX69,'')" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AU78'])?$stored['AU78']:'' ?>" class="input-sm form-control text-center" id="AU78" name="AU78" data-cell="AU78" data-formula="IF(R78='21C',AT78,0)" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AV78'])?$stored['AV78']:'' ?>" class="input-sm form-control text-center" id="AV78" name="AV78" data-cell="AV78" data-formula="IF(R78='21D',AT78,0)" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AY78'])?$stored['AY78']:'' ?>" class="input-sm form-control text-center" id="AY78" name="AY78" data-cell="AY78" data-formula="IF(R78='21E',AT78,0)" type="text"></td>
                                                                </tr>
                                                                <tr id="row1" class="">
                                                                    <td class="text-center"><label for="">4</label></td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control existing" data-count="80" name="C80" id="C80" data-cell="C80">
                                                                            <option <?= isset($stored['C80']) && $stored['C80'] == '' ? 'selected' : '' ?> value=""></option>
                                                                            <option <?= isset($stored['C80']) && $stored['C80'] == '12V Halogen lamp only' ? 'selected' : '' ?> value="12V Halogen lamp only" selected>12V Halogen lamp only</option>
                                                                            <option <?= isset($stored['C80']) && $stored['C80'] == '12V Halogen lamp &amp; transfomer' ? 'selected' : '' ?> value="12V Halogen lamp &amp; transfomer">12V Halogen lamp &amp; transfomer</option>
                                                                            <option <?= isset($stored['C80']) && $stored['C80'] == '230V Halogen GU10 lamp only' ? 'selected' : '' ?> value="230V Halogen GU10 lamp only">230V Halogen GU10 lamp only</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="">
                                                                        <select class="input-sm form-control" name="K80" id="K80" data-cell="K80">
                                                                            <option <?= isset($stored['K80']) && $stored['K80'] == '' ? 'selected' : '' ?> value=""></option>
                                                                            <option <?= isset($stored['K80']) && $stored['K80'] == '1' ? 'selected' : '' ?> value="1">Electronic</option>
                                                                            <option <?= isset($stored['K80']) && $stored['K80'] == '2' ? 'selected' : '' ?> value="2">Magnetic</option>
                                                                            <option <?= isset($stored['K80']) && $stored['K80'] == '3' ? 'selected' : '' ?> value="3">N/A</option>
                                                                        </select>
                                                                        <input value="" id="L80" name="L80" data-cell="L80" data-formula="IF(K80=1,'Electronic',IF(K80=2,'Magnetic',IF(K80=3,'N/A',' ')))" type="hidden">
                                                                    </td>
                                                                    <td class=""><input value="<?= isset($stored['O80'])?$stored['O80']:'' ?>" class="input-sm form-control text-center yellow" id="O80" name="O80" data-cell="O80" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['R80'])?$stored['R80']:'' ?>" class="input-sm form-control text-center" id="R80" name="R80" data-cell="R80" data-formula="IF(C80='12V Halogen lamp only','21C',IF(C80='12V Halogen lamp &amp; transfomer','21D',IF(C80='230V Halogen GU10 lamp only','21E',' ')))" type="text"></td>
                                                                    <td class="">
                                                                        <input value="<?= isset($stored['T80'])?$stored['T80']:'' ?>" class="input-sm form-control text-center yellow c_product" data-count="80" id="T80" name="T80" data-cell="T80" type="text">
                                                                        <input value="<?= isset($stored['T79'])?$stored['T79']:'' ?>" class="input-sm form-control text-center yellow d_product hidden" data-count="80" id="T79" name="T79" data-cell="T79" type="text">
                                                                        <input value="<?= isset($stored['TT79'])?$stored['TT79']:'' ?>" class="input-sm form-control text-center yellow e_product hidden" data-count="80" id="TT79" name="TT79" data-cell="TT79" type="text">
                                                                        <input type="hidden" class="input-sm form-control text-center yellow" data-count="80" id="S80" name="S80" data-cell="S80">
                                                                    </td>
                                                                    <td class=""><input value="<?= isset($stored['AI80'])?$stored['AI80']:'' ?>" class="input-sm form-control text-center" id="AI80" name="AI80" data-cell="AI80" data-formula="O80" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AK80'])?$stored['AK80']:'' ?>" class="input-sm form-control text-center" id="AK80" name="AK80" data-cell="AK80" data-formula="IF(AI80 > O80,O80,AI80)" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AL80'])?$stored['AL80']:'' ?>" class="input-sm form-control text-center" id="AL80" name="AL80" data-cell="AL80" data-formula="IF(AI80='','',IF(AND(C80='12V Halogen lamp only',AM80=K80),'Yes',IF(AND(C80='12V Halogen lamp only',AI80 < > K80),'No','NA')))" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AM80'])?$stored['AM80']:'' ?>" class="input-sm form-control text-center" id="AM80" name="AM80" data-cell="AM80" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AN80'])?$stored['AN80']:'' ?>" class="input-sm form-control text-center" id="AN80" name="AN80" data-cell="AN80" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AP80'])?$stored['AP80']:'' ?>" class="input-sm form-control text-center" id="AP80" name="AP80" data-cell="AP80" readonly="readonly" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AR80'])?$stored['AR80']:'' ?>" class="input-sm form-control text-center" id="AR80" name="AR80" data-cell="AR80" readonly="readonly" type="text"></td>
                                                                    <td class=""><input value="<?= isset($stored['AS80'])?$stored['AS80']:'' ?>" class="input-sm form-control text-center" id="AS80" name="AS80" data-cell="AS80" readonly="readonly" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AT80'])?$stored['AT80']:'' ?>" class="input-sm form-control text-center" id="AT80" name="AT80" data-cell="AT80" data-formula="IF(OR(AL80='Yes', AL80='NA'),AK80*AR80*AX69,'')" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AU80'])?$stored['AU80']:'' ?>" class="input-sm form-control text-center" id="AU80" name="AU80" data-cell="AU80" data-formula="IF(R80='21C',AT80,0)" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AV80'])?$stored['AV80']:'' ?>" class="input-sm form-control text-center" id="AV80" name="AV80" data-cell="AV80" data-formula="IF(R80='21D',AT80,0)" type="text"></td>
                                                                    <td class="hidden"><input value="<?= isset($stored['AY80'])?$stored['AY80']:'' ?>" class="input-sm form-control text-center" id="AY80" name="AY80" data-cell="AY80" data-formula="IF(R80='21E',AT80,0)" type="text"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6 col-xs-6">
                                                    <label for="">Maximum of 5 different energy efficient lighting products can be installed for each activity</label>
                                                </div>
                                                <div class="col-sm-4 col-xs-3 text-right">
                                                    <label for="">Number of VEECs for 21C</label>
                                                </div>
                                                <div class="col-sm-2 col-xs-3">
                                                    <input value="<?= isset($stored['AS94'])?$stored['AS94']:'' ?>" class="form-control input-sm" id="AS94" name="AS94" data-cell="AS94" data-format="0,0[.]" data-formula="SUM(AU74:AU92)" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6 col-xs-6">
                                                    <label for=""></label>
                                                </div>
                                                <div class="col-sm-4 col-xs-3 text-right">
                                                    <label for="">Number of VEECs for 21D</label>
                                                </div>
                                                <div class="col-sm-2 col-xs-3">
                                                    <input value="<?= isset($stored['AS96'])?$stored['AS96']:'' ?>" class="form-control input-sm" id="AS96" name="AS96" data-cell="AS96" data-format="0,0[.]" data-formula="SUM(AV74:AV92)" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-6 col-xs-6">
                                                    <label for=""></label>
                                                </div>
                                                <div class="col-sm-4 col-xs-3 text-right">
                                                    <label for="">Number of VEECs for 21E</label>
                                                </div>
                                                <div class="col-sm-2 col-xs-3">
                                                    <input value="<?= isset($stored['AS98'])?$stored['AS98']:'' ?>" class="form-control input-sm" id="AS98" name="AS98" data-cell="AS98" data-format="0,0[.]" data-formula="SUM(AY74:AY92)" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label for="">*Schedule 21D activities require replacement of a 12 volt fitting. The (pre-existing) halogen transformer(s) must not be used as part of the new installation.</label>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>Pre-installation inspection completed by electrician</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <p class="text-justify">I have performed the pre-installation assessment and physically inspected the existing equipment:</p>
                                            <div class="checkbox">
                                                <label>
                                                    <input <?= isset($stored['D101'])?'checked':'' ?> name="D101" id="D101" data-cell="D101" value="1" type="checkbox" checked>   Any safety hazards identified have been resolved and the installation meets all relevant standards
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input <?= isset($stored['D102'])?'checked':'' ?> name="D102" id="D102" data-cell="D102" value="1" type="checkbox" checked>   The existing transformers are compatible with the new lamps to which they will be connected
                                                </label>
                                            </div>
                                            <p class="tex-justify bold"><i>Please note, where safety and compatibility requirements are not met the installation is not eligible for VEEC creation.</i></p>

                                            <p class="tex-justify bold top">Give the brand and model number of the existing transformers:</p>
                                            <div class="row">
                                                <div class="col-sm-offset-1 col-sm-3 col-xs-4 blue text-center bold">
                                                    <label for="" class=" text-center">Brand</label>
                                                </div>
                                                <div class="col-sm-4 col-xs-4 blue text-center bold">
                                                    <label for="" class=" text-center">Model</label>
                                                </div>
                                                <div class="col-sm-3 col-xs-4 blue text-center bold">
                                                    <label for="" class=" text-center">Transformer Type</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-offset-1 col-sm-3 col-xs-4 blue text-center bold">
                                                    <input value="<?= isset($stored['D108'])?$stored['D108']:'' ?>" class="form-control input-sm yellow" id="D108" name="D108" data-cell="D108" type="text">
                                                    <input value="<?= isset($stored['D110'])?$stored['D110']:'' ?>" class="form-control input-sm yellow" id="D110" name="D110" data-cell="D110" type="text">
                                                    <input value="<?= isset($stored['D112'])?$stored['D112']:'' ?>" class="form-control input-sm yellow" id="D112" name="D112" data-cell="D112" type="text">
                                                </div>
                                                <div class="col-sm-4 col-xs-4 blue text-center bold">
                                                    <input value="<?= isset($stored['R108'])?$stored['R108']:'' ?>" class="form-control input-sm yellow" id="R108" name="R108" data-cell="R108" type="text">
                                                    <input value="<?= isset($stored['R110'])?$stored['R110']:'' ?>" class="form-control input-sm yellow" id="R110" name="R110" data-cell="R110" type="text">
                                                    <input value="<?= isset($stored['R112'])?$stored['R112']:'' ?>" class="form-control input-sm yellow" id="R112" name="R112" data-cell="R112" type="text">
                                                </div>
                                                <div class="col-sm-3 col-xs-4 blue text-center bold">
                                                    <select class="input-sm form-control yellow" name="AI108" id="AI108" data-cell="AI108">
                                                        <option <?= isset($stored['AI108']) && $stored['AI108'] == '' ? 'selected="selected"' : '' ?> value=""></option>
                                                        <option <?= isset($stored['AI108']) && $stored['AI108'] == 'Electronic' ? 'selected="selected"' : '' ?> value="Electronic">Electronic</option>
                                                        <option <?= isset($stored['AI108']) && $stored['AI108'] == 'Magnetic' ? 'selected="selected"' : '' ?> value="Magnetic">Magnetic</option>
                                                    </select>
                                                    <select class="input-sm form-control yellow" name="AI110" id="AI110" data-cell="AI110">
                                                        <option <?= isset($stored['AI110']) && $stored['AI110'] == '' ? 'selected="selected"' : '' ?> value=""></option>
                                                        <option <?= isset($stored['AI110']) && $stored['AI110'] == 'Electronic' ? 'selected="selected"' : '' ?> value="Electronic">Electronic</option>
                                                        <option <?= isset($stored['AI110']) && $stored['AI110'] == 'Magnetic' ? 'selected="selected"' : '' ?> value="Magnetic">Magnetic</option>
                                                    </select>
                                                    <select class="input-sm form-control yellow" name="AI112" id="AI112" data-cell="AI112">
                                                        <option <?= isset($stored['AI112']) && $stored['AI112'] == '' ? 'selected="selected"' : '' ?> value=""></option>
                                                        <option <?= isset($stored['AI112']) && $stored['AI112'] == 'Electronic' ? 'selected="selected"' : '' ?> value="Electronic">Electronic</option>
                                                        <option <?= isset($stored['AI112']) && $stored['AI112'] == 'Magnetic' ? 'selected="selected"' : '' ?> value="Magnetic">Magnetic</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group top">
                                                <div class="col-sm-4">
                                                    <label for="">Were any lamps installed on dimmable circuits?</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <select class="form-control input-sm" id="V115" name="V115" data-cell="V115">
                                                        <option <?= isset($stored['V115']) && $stored['V115'] == 'No' ? 'selected="selected"' : '' ?> value="No">No</option>
                                                        <option <?= isset($stored['V115']) && $stored['V115'] == 'Yes' ? 'selected="selected"' : '' ?> value="Yes">Yes</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2"><label for="" id="AA115" name="AA115" data-cell="AA115" data-formula="IF(V115='Yes','If yes, how many?','')"></label></div>
                                                <div class="col-sm-2"><input value="<?= isset($stored['AJ115'])?$stored['AJ115']:'' ?>" class="form-control input-sm yellow hidden" id="AJ115" name="AJ115" data-cell="AJ115" type="text"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label for="">Was wiring work required as part of this upgrade?</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <select class="form-control input-sm" id="V117" name="V117" data-cell="V117">
                                                        <option <?= isset($stored['V117']) && $stored['V117'] == 'No' ? 'selected="selected"' : '' ?> value="No">No</option>
                                                        <option <?= isset($stored['V117']) && $stored['V117'] == 'Yes' ? 'selected="selected"' : '' ?> value="Yes">Yes</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3"><label for="" id="AL117" name="AL117" data-cell="AL117" data-formula="IF(V117='Yes','Electrical Safety Certificate number:','')"></label></div>
                                                <div class="col-sm-2"><input value="<?= isset($stored['AM117'])?$stored['AM117']:'' ?>" class="form-control input-sm yellow hidden" id="AM117" name="AM117" data-cell="AM117" type="text"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-7">
                                                    <label for="">Were there any 240V halogen downlights replaced for the purposes of the VEET during this installation?</label>

                                                </div>
                                                <div class="col-sm-2">
                                                    <select class="form-control input-sm" id="AO119" name="AO119" data-cell="AO119">
                                                        <option <?= isset($stored['AO119']) && $stored['AO119'] == 'No' ? 'selected="selected"' : '' ?> value="No">No</option>
                                                        <option <?= isset($stored['AO119']) && $stored['AO119'] == 'Yes' ? 'selected="selected"' : '' ?> value="Yes">Yes</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <p class="text-justify red" id="C121" name="C121" data-cell="C121" data-formula="IF(AO119='Yes','240V products are not eligible to be replaced under this activity',' ')"> </p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>Electrician details</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 box_desc">
                                            <div class="form-group">
                                                <div class="col-sm-5">
                                                    <label for="">Electrician's company name</label>
                                                    <input value="<?= isset($stored['C124'])?$stored['C124']:'' ?>" class="form-control input-sm yellow" id="C124" name="C124" data-cell="C124" 
                                                    data-formula="'<?= $settings_app['electricians_company_name'] ?>'" type="text">
                                                </div>
                                                <div class="col-sm-7">
                                                    <label for="">Company address</label>
                                                    <input value="<?= isset($stored['U124'])?$stored['U124']:'' ?>" class="form-control input-sm yellow" id="U124" name="U124" data-cell="U124" 
                                                    data-formula="'<?= $settings_app['company_full_address'] ?>'" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label for="">Electrician's name</label>
                                                    <input type="hidden" value="<?= isset($stored['iid'])?$stored['iid']:'' ?>" name="iid" />
                                                    <input value="<?= isset($stored['C127'])?$stored['C127']:'' ?>" class="form-control input-sm yellow installer" id="C127" name="C127" data-cell="C127" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label for="">Phone number</label>
                                                    <input value="<?= isset($stored['O127'])?$stored['O127']:'' ?>" class="form-control input-sm yellow" id="O127" name="O127" data-cell="O127" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label for="">Email</label>
                                                    <input value="<?= isset($stored['Z127'])?$stored['Z127']:'' ?>" class="form-control input-sm yellow input_email" id="Z127" name="Z127" data-cell="Z127" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label for="">Licence type</label>
                                                    <input value="<?= isset($stored['C130'])?$stored['C130']:'' ?>" class="form-control input-sm " id="C130" name="C130" data-cell="C130"  type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="">Licence number</label>
                                                    <input value="<?= isset($stored['K130'])?$stored['K130']:'' ?>" class="form-control input-sm yellow" id="K130" name="K130" data-cell="K130" type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="">Issue/renewal date</label>
                                                    <input value="<?= isset($stored['V130'])?$stored['V130']:'' ?>" class="form-control input-sm yellow" id="V130" name="V130" data-cell="V130" type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="" class="text-center">Date of expiry</label>
                                                    <input value="<?= isset($stored['AI130'])?$stored['AI130']:'' ?>" class="form-control input-sm yellow" id="AI130" name="AI130" data-cell="AI130" type="text">
                                                </div>
                                            </div>


                                            <p class="text-right bold top"><small>ΘGreen Energy Trading Pty Ltd 2014</small></p>

                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>Declaration by electrician</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <div class="form-group top">
                                                <label for="" class="col-xs-offset-1 col-xs-1">I,</label>
                                                <div class="col-sm-2"><input value="<?= isset($stored['C138'])?$stored['C138']:'' ?>" class="form-control input-sm" id="C138" name="C138" data-cell="C138" data-formula="C127" type="text"></div>
                                                <label for="" class="">hereby declare that:</label>
                                            </div>

                                            
                                                <p class="text-justify" id="C139" name="C139" data-cell="C139" data-formula="IF(AE16='Residential',' ','*   I confirm that the above installation was not undertaken in a premises compulsorily registered on the EREPs register under the Environment and Resource Efficiency Plans Program administered by the Environment Protection Authority.')">I confirm that the above installation was not undertaken in a premises compulsorily registered on the EREPs register under the Environment and Resource Efficiency Plans Program administered by the Environment Protection Authority.</p>
                                                <p class="text-justify" id="C141" name="C141" data-cell="C141" data-formula="IF(AE16='Residential','*   The above product(s) have been installed in a residential premises','*  The above product(s) have been installed at the stated premises.')">The above product(s) have been installed at the stated premises.</p>
                                                <p class="text-justify">*   Where applicable, the consumer has been informed that a Certificate of Electrical Safety is required for the work undertaken and will be provided a copy of the relevant certificate within five working days of installation.</p>
                                                <p class="text-justify">*   The lamps which were removed from the premises will be decommissioned through recycling.</p>
                                                <p class="text-justify">*   Where any product was installed in a dimmable circuit, the product is approved by the manufacturer as suitable for such a circuit.</p>
                                                <p class="text-justify">*   No damage, fault or hazard was identified during the mandatory compatibility &amp; safety inspection, and I determined that it was safe to proceed with the installation.</p>
                                                <p class="text-justify">*   I am a licensed electrician and the installation meets all mandatory industry standards.</p>
                                                <p class="text-justify">*   In the case of Schedule 21C, lamps installed are compatible with pre-existing transformers.</p>
                                                <p class="text-justify">*   The information provided is complete and accurate and I am aware that penalties can be applied for providing misleading information in this form under <i>the Victorian Energy Efficiency Target Act 2007.</i> </p>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="sigPad col-sm-6">
                                                        <p class="text-center">Signature</p>
                                                        <?php if(isset($stored['E153']) && file_exists($stored['E153'])): ?>
                                                            <img src="<?= base_url($stored['E153']) ?>" />
                                                        <?php else: ?>
                                                            <div style="display: block;padding-left:0px" class="sig sigWrapper col-sm-2 current">
                                                                <div style="display: none;" class="typed"></div>
                                                                <canvas class="pad" width="320px" height="175px"></canvas>
                                                                <input value="" class="output" name="E153" id="E153" type="hidden">
                                                            </div>
                                                            <div style="margin-top:190px;padding-left:20px">
                                                                <input class="clearButton" value="clear" type="button"><br><br>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-sm-1">&nbsp;</div>
                                                    <div class="col-sm-3">
                                                        &nbsp;
                                                        
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label for="assignment_date" class="control-label" style="">Date</label>
                                                        <input value="<?= isset($stored['Z153'])?$stored['Z153']:'' ?>" class="form-control input-sm text-center datepicker" id="Z153" name="Z153" data-cell="Z153" type="text" data-formula="C68">
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label for="">Name</label>
                                                    <input value="<?= isset($stored['C156'])?$stored['C156']:'' ?>" class="form-control input-sm" id="C156" name="C156" data-cell="C156" data-formula="IF(C127='','',C127)" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label for="">Company name</label>
                                                    <input value="<?= isset($stored['R156'])?$stored['R156']:'' ?>" class="form-control input-sm" id="R156" name="R156" data-cell="R156" data-formula="IF(C124='','',C124)" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label for="">Phone</label>
                                                    <input value="<?= isset($stored['AI156'])?$stored['AI156']:'' ?>" class="form-control input-sm" id="AI156" name="AI156" data-cell="AI156" data-formula="IF(O127='','',O127)" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    
                                    <div class="row hidden">
                                        <div class="col-xs-12 box_label">
                                            <h5>Document checklist</h5>
                                        </div>
                                    </div>
                                    <div class="row hidden">
                                        <div class="col-xs-12 box_desc">
                                            <p class="text-justify">Please attach the following required documentation upon submission of this form:</p>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input <?= isset($stored['D160'])?'checked':'' ?> name="D160" id="D160" data-cell="D160" value="1" type="checkbox">
                                                            A1. Completed and signed VEEC Assignment Form <br>
                                                            Ensure all yellow fields have been completed.
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input <?= isset($stored['D163'])?'checked':'' ?> name="D163" id="D163" data-cell="D163" value="1" type="checkbox">
                                                            A2. Proof of purchase (customer's tax invoice)
                                                            This must detail :
                                                            <ul>
                                                                <li>Energy consumers name and installation address</li>
                                                                <li>Model and quantity of each lighting product supplied</li>
                                                                <li>Value of any VEEC benefit provided</li>
                                                            </ul>
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input <?= isset($stored['D168'])?'checked':'' ?> name="D168" id="D168" data-cell="D168" value="1" type="checkbox">
                                                            A3. Itemised recycling receipt
                                                            <p class="text-justify">A receipt issued by a recycler or collect responsible for the disposal of the original lamps. The receipt must show the installation address and the type and quantity of each removed product. <i> A recycling receipt showing only "weight" is not accepted.</i></p>
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input <?= isset($stored['D174'])?'checked':'' ?> name="D174" id="D174" data-cell="D174" value="1" type="checkbox">
                                                            A4. Energy consumer's electricity bill
                                                            This must clearly name the energy consumer as the party responsible for paying the bill.
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input <?= isset($stored['AB160'])?'checked':'' ?> name="AB160" id="AB160" data-cell="AB160" value="1" type="checkbox">
                                                            A5. Installation photographs
                                                            <ul>
                                                                <li>One of each type of halogen lamp, showing wattage</li>
                                                                <li>One of each type of existing transformer, showing model</li>
                                                                <li>One of each type of installed energy efficient lamp, showing model</li>
                                                                <li>One of each type of installed LED driver (for Activity 21D)</li>
                                                                <li>Photo(s) of the space where lighting was upgraded</li>
                                                                <li>Removed lamps laid out in a grid, showing quantity</li>
                                                            </ul>
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input <?= isset($stored['AB167'])?'checked':'' ?> name="AB167" id="AB167" data-cell="AB167" value="1" type="checkbox">

                                                            <p class="text-justify" name="AB168" id="AB168" data-cell="AB168" data-formula="IF(V117='Yes','A6. Certificate of Electrical Safety','A6. Certificate of Electrical Safety - not required')">A6. Certificate of Electrical Safety - not required</p>
                                                            <p class="text-justify" name="AC169" id="AC169" data-cell="AC169" data-formula="IF(V117='Yes','This is only required when wiring work was carried out as part of the upgrade. Must clearly show the installation address, date of completion of work and be signed by the electrician.','')"></p>
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input <?= isset($stored['AB172'])?'checked':'' ?> name="AB172" id="AB172" data-cell="AB172" value="1" type="checkbox">

                                                            <p class="text-justify" name="AB173" id="AB173" data-cell="AB173" data-formula="IF(AI25='Yes','A7. Tax invoice to Green Energy Trading','A7. Tax invoice to Green Energy Trading - not required')">A7. Tax invoice to Green Energy Trading - not required</p>
                                                            <p class="text-justify" name="AC174" id="AC174" data-cell="AC174" data-formula="IF(AI25='Yes','An invoice issued to Green Energy Trading for payment of the correct value of certificates - only required when the energy consumer is a business registered for GST.','')"></p>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <!--

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>How to submit this form</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <p class="text-center">Please email the completed VEEC 21 Assigment Form and accompanying required documentation to :</p>
                                            <p class="text-center bold" style="color:blue"><u>forms@greenenegytrading.com.au</u></p>
                                            <p class="text-center top" style="color:green">With all enquiries:</p>
                                            <h3 style="color:green" class="text-center">1300 077 784</h3>


                                            <p class="text-right bold top"><small>ΘGreen Energy Trading Pty Ltd 2014</small></p>


                                        </div>
                                    </div>
                                -->



                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>Form of benefit provided to energy consumer</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <label for="">The following benefit has been provided to the energy consumer:</label>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <div class="radio">
                                                        <label>
                                                            <input <?= isset($stored['benefit']) && $stored['benefit']==1 ?'checked':'' ?> name="benefit" id="benefit1" value="1" type="radio">
                                                            upfront cash
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input <?= isset($stored['benefit']) && $stored['benefit']==2 ?'checked':'' ?> name="benefit" id="benefit2" value="2" type="radio">
                                                            Price reduction
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="radio">
                                                        <label>
                                                            <input <?= isset($stored['benefit']) && $stored['benefit']==3 ?'checked':'' ?> name="benefit" id="benefit3" value="3" type="radio">
                                                            Delayed cash
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input <?= isset($stored['benefit']) && $stored['benefit']==4 ?'checked':'' ?> checked name="benefit" id="benefit4" value="4" type="radio">
                                                            Free installation
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                            <div class="radio">
                                                                <label>
                                                                    <input <?= isset($stored['benefit']) && $stored['benefit']==5 ?'checked':'' ?> name="benefit" id="benefit5" value="5" type="radio">
                                                                    other (please describe)
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <input value="<?= isset($stored['AG190'])?$stored['AG190']:'' ?>" class="form-control input-sm" id="AG190" name="AG190" data-cell="AG190" type="text">
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="form-group">
                                                                <label for="" class="col-xs-4">$ amount of benefit provided :</label>
                                                                <div class="col-xs-4">
                                                                    <input value="<?= isset($stored['AJ192'])?$stored['AJ192']:'' ?>" class="form-control input-sm yellow" id="AJ192" name="AJ192" data-cell="AJ192" data-format="$ 0,0[.]00" type="text" data-formula="AI53">
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>Payment details</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <div class="form-group">
                                                <div class="col-xs-6">
                                                    <label for="">Make payment to :</label>
                                                    <input value="<?= isset($stored['C196'])?$stored['C196']:'' ?>" class="form-control input-sm yellow col-sm-6" id="C196" name="C196" data-cell="C196" data-formula="'Lighting solution provider - energy consumer has received VEEC benefit'" type="text">
                                                    <!--
                                                      <option  value="Installation company - energy consumer has received VEEC benefit">Installation company - energy consumer has received VEEC benefit</option>
                                                      <option  value="Direct to energy consumer">Direct to energy consumer</option>
                                                      <option  value="Lighting solution provider - energy consumer has received VEEC benefit">Lighting solution provider - energy consumer has received VEEC benefit</option>
                                                      <option  value=""></option>
                                                    </select>
                                                    -->
                                                </div>
                                                <div class="col-xs-6">
                                                    <label for="" id="AA195" name="AA195" data-cell="AA195" data-formula="IF(C196='Other - please describe arrangement in adjacent field','Other arrangement:',IF(C196='Lighting solution provider - energy consumer has received VEEC benefit','Who is the lighting solution provider?',''))">Who is the lighting solution provider?</label>
                                                    <input value="<?= isset($stored['AA196'])?$stored['AA196']:'' ?>" class="form-control input-sm col-sm-6 hidden" id="AA196" name="AA196" data-cell="AA196" data-formula="'Eminent Services Group Pty Ltd T/A My Electrician'" type="text">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label for="">Account name</label>
                                                    <input value="<?= isset($stored['C198'])?$stored['C198']:'' ?>" class="form-control input-sm yellow" id="C198" name="C198" data-cell="C198" data-formula="'Eminent Services Group Pty Ltd'" type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="">BSB</label>
                                                    <input value="<?= isset($stored['M198'])?$stored['M198']:'' ?>" class="form-control input-sm yellow" id="M198" name="M198" data-cell="M198" data-formula="'063 549'" type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="">Acount number</label>
                                                    <input value="<?= isset($stored['U198'])?$stored['U198']:'' ?>" class="form-control input-sm yellow" id="U198" name="U198" data-cell="U198" data-formula="'10569974'" type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label for="">Email address for remittance advice</label>
                                                    <input value="<?= isset($stored['AC198'])?$stored['AC198']:'' ?>" class="form-control input-sm yellow input_email" id="AC198" name="AC198" data-cell="AC198" data-formula="'reception@myelec.net.au'" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>Declaration by energy consumer</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <div class="form-group top">
                                                <label for="" class="col-xs-offset-1 col-xs-1">I,</label>
                                                <div class="col-sm-2"><input value="<?= isset($stored['C202'])?$stored['C202']:'' ?>" class="form-control input-sm" id="C202" name="C202" data-cell="C202" data-formula="CONCATENATE(C22, ' ' ,C23)" type="text"></div>
                                                <label for="" class="">hereby declare that:</label>
                                            </div>

                                            
                                                <p class="text-justify" id="C203" name="C203" data-cell="C203" data-formula="IF(OR(AW19=0,AW19=2),'*    I am authorised to sign on behalf of '&Y22&'.','')"></p>
                                                <p class="text-justify" id="C204" name="C204" data-cell="C204" data-formula="IF(OR(AW19=0,AW19=2), '*   '&Y22&' is the '&C25&' of the premises at the above installation address.' ,'*  I am the '&C25&' of the residence at the above installation')"></p>
                                                <p class="text-justify" id="C205" name="C205" data-cell="C205" data-formula="IF(OR(AW19=0,AW19=2), '*   '&Y22&' has received or will receive an identifiable benefit from '&c196&' in exchange for assigning the rights to create the VEECs for the above system.','*   I have received, or will receive an identifiable benefit from '&AA196&' in exchange for assigning my rights to create the certificates for the above installation.')"></p>
                                                
                                                    <p class="text-justify">The replaced lamps have been removed from the premises.</p>
                                                    <p class="text-justify">The information provided by the installer on page 3 of the assignment form is correct and complete.</p>
                                                    <p class="text-justify">I understand that by signing this form I am assigning the right to create VEECs for the installation to Green Energy Trading Pty Ltd.</p>
                                                    <p class="text-justify">The Essential Services Commission has the right to inspect the installation with reasonable notice.</p>
                                                    <p class="text-justify">Green Energy Trading has the right to inspect the installation with reasonable notice and to conduct a phone audit of the installation if necessary.</p>
                                                    <p class="text-justify">I understand that information on this form will be disclosed to the Essential Services Commission for the purpose of creating VEECs under the Victorian Energy Efficiency Target Act 2007 and for related verification, audit and scheme monitoring purposes.</p>
                                                    <p class="text-justify bold">I understand that this form cannot be signed before the latter of decommissioning or installation date, as required by the Activity undertaken by the Installer.</p>
                                                    <p class="text-justify">I am aware that penalties can be applied for providing misleading information in this form under <i> the Victorian Energy Efficiency Target Act 2007.</i></p>

                                            <p class="text-justify" id="C217" name="C217" data-cell="C217" data-formula="IF(AE16='Commercial','I confirm that the above installation was not undertaken in a premises compulsorily registered on the EREPs register under the Environment and Resource Efficiency Plans Program administered by the Environment Protection Authority.','')"></p>

                                            <div class="form-group">
                                                <label for="" class="col-xs-8">I have been advised whether a Certificate of Electrical Safety is required for the work undertaken and, if one is required, that I will be provided a copy of the relevant certificate</label>
                                                <div class="col-xs-4">
                                                    <select type="text" class="form-control input-sm yellow" id="AP220" name="AP220" data-cell="AP220">
                                                        <option <?= isset($stored['AP220']) && $stored['AP220'] == '' ? 'selected="selected"' : '' ?> value=""></option>
                                                        <option <?= isset($stored['AP220']) && $stored['AP220'] == 'Yes' ? 'selected="selected"' : '' ?> value="Yes" selected>Yes</option>
                                                        <option <?= isset($stored['AP220']) && $stored['AP220'] == 'No' ? 'selected="selected"' : '' ?> value="No">No</option>
                                                        <option <?= isset($stored['AP220']) && $stored['AP220'] == 'N/A' ? 'selected="selected"' : '' ?> value="N/A">N/A</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-xs-8">I declare the above named electrician physically attended the premises and left a form of identification at the completion of the upgrade.</label>
                                                <div class="col-xs-4">
                                                    <select type="text" class="form-control input-sm yellow" id="AP222" name="AP222" data-cell="AP222">
                                                        <option <?= isset($stored['AP222']) && $stored['AP222'] == '' ? 'selected="selected"' : '' ?> value=" "></option>
                                                        <option <?= isset($stored['AP222']) && $stored['AP222'] == 'Yes' ? 'selected="selected"' : '' ?> value="Yes" selected>Yes</option>
                                                        <option <?= isset($stored['AP222']) && $stored['AP222'] == 'No' ? 'selected="selected"' : '' ?> value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                              <div class="col-sm-12" >
                                                <img id="selected_installer_photo2_placeholder" style="width: 3cm; height: 4cm;" class="hidden" />
                                                <input type="hidden" name="installerphoto2" />
                                                <br>
                                                <p>This photo depicts the installer who undertook the replacement of halogen down lights in the above address.</p>
                                              </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="sigPad col-sm-6">
                                                        <p class="text-center">Signature</p>
                                                        <?php if(isset($stored['E225']) && file_exists($stored['E225'])): ?>
                                                            <img src="<?= base_url($stored['E225']) ?>" />
                                                        <?php else: ?>
                                                            <div style="display: block;padding-left:0px" class="sig sigWrapper col-sm-2 current">
                                                                <div style="display: none;" class="typed"></div>
                                                                <canvas class="pad" width="320px" height="175px"></canvas>
                                                                <input value="" class="output" name="E225" id="E225" type="hidden">
                                                            </div>
                                                            <div style="margin-top:190px;padding-left:20px">
                                                                <input class="clearButton" value="clear" type="button"><br><br>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-sm-2"></div>
                                                    <div class="col-sm-2">
                                                        <label for="assignment_date" class="control-label" style="">Date</label>
                                                        <input value="<?= isset($stored['Z225'])?$stored['Z225']:'' ?>" class="form-control input-sm text-center datepicker" id="Z225" name="Z225" data-cell="Z225" type="text" data-formula="C68">
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label for="" id="C227" name="C227" data-cell="C227" data-formula="IF(OR(AW19=0,AW19=2),'Energy consumers name','Energy consumers name')">Energy consumers name</label>
                                                    <input value="<?= isset($stored['C228'])?$stored['C228']:'' ?>" class="form-control input-sm" id="C228" name="C228" data-cell="C228" data-formula="IF(C202='','',C202)" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label for="" id="V227" name="V227" data-cell="V227" data-formula="IF(AW19=0,'Position','')"></label>
                                                    <input value="<?= isset($stored['V228'])?$stored['V228']:'' ?>" class="form-control input-sm private" id="V228" name="V228" data-cell="V228" data-formula="IF(M25='','',M25)" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label for="">Phone number</label>
                                                    <input value="<?= isset($stored['AI228'])?$stored['AI228']:'' ?>" class="form-control input-sm" id="AI228" name="AI228" type="text">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 box_label">
                                            <h5>GST implications of this assignment</h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 box_desc">
                                            <p class="text-justify">For the purposes of the Goods and Services Tax (GST), this transaction is characterised as the supply of the right to create Environmental Certificates from the energy consumer to Green Energy Trading.  </p>
                                            <p class="text-justify">Where the energy consumer is registered for GST and has acquired the lighting products for business purposes, the supply will attract GST, and the Parties agree that the energy consumer will issue a tax invoice to Green Energy Trading in respect of this supply.</p>

                                            <p class="text-right bold top"><small>ΘGreen Energy Trading Pty Ltd 2014</small></p>
                                        </div>
                                    </div>

                               
                                  <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                      &nbsp;
                                    </div>
                                    <div class="col-sm-4">
                                        <h1 class="text-right bold">Proof of Purchase</h1>
                                        <!--<img src="<?= base_url('asset/css/images/tax.png') ?>" alt="">-->
                                    </div>
                                  </div>

                                  <div class="row top">
                                    <div class="col-sm-4">
                                        <input type="hidden" value="<?= isset($stored['SP1'])?$stored['SP1']:'' ?>" class="form-control input-sm yellow" id="SP1" name="SP1" data-cell="SP1" data-formula="'<?= $settings_app['electricians_company_name'] ?>'">
                                        <input type="hidden" value="<?= isset($stored['SP2'])?$stored['SP2']:'' ?>" class="form-control input-sm yellow" id="SP2" name="SP2" data-cell="SP2" data-formula="'<?= $settings_app['company_address'] ?>'">
                                        <input type="hidden" value="<?= isset($stored['SP3'])?$stored['SP3']:'' ?>" class="form-control input-sm yellow" id="SP3" name="SP3" data-cell="SP3" data-formula="'<?= $settings_app['suburb'] ?>'">
                                        <input type="hidden" value="<?= isset($stored['SP4'])?$stored['SP4']:'' ?>" class="form-control input-sm yellow" id="SP4" name="SP4" data-cell="SP4" data-formula="'<?= $settings_app['postal_code'] ?>'">
                                        <input type="hidden" value="<?= isset($stored['SP5'])?$stored['SP5']:'' ?>" class="form-control input-sm yellow" id="SP5" name="SP5" data-cell="SP5" data-formula="'<?= $settings_app['rec_number'] ?>'">
                                        <input type="hidden" value="<?= isset($stored['SP6'])?$stored['SP6']:'' ?>" class="form-control input-sm yellow" id="SP6" name="SP6" data-cell="SP6" data-formula="'<?= $settings_app['abn_number'] ?>'">
                                        <input type="hidden" value="<?= isset($stored['SP7'])?$stored['SP7']:'' ?>" class="form-control input-sm yellow" id="SP7" name="SP7" data-cell="SP7" data-formula="'<?= $settings_app['telephone_number'] ?>'">
                                        <input type="hidden" value="<?= isset($stored['SP8'])?$stored['SP8']:'' ?>" class="form-control input-sm yellow" id="SP8" name="SP8" data-cell="SP8" data-formula="'<?= $settings_app['website'] ?>'">
                                        <input type="hidden" value="<?= isset($stored['SP9'])?$stored['SP9']:'' ?>" class="form-control input-sm yellow" id="SP9" name="SP9" data-cell="SP9" data-formula="'<?= $settings_app['email_address'] ?>'">
                                    <p class="bold" id="SP21" name="SP21" data-cell="SP21" data-formula="SP1" style="margin-bottom:0px"></p>
                                    <p class="bold" id="SP22" name="SP22" data-cell="SP22" data-formula="SP2" style="margin-bottom:0px"></p>
                                    <p class="bold" id="SP23" name="SP23" data-cell="SP23" data-formula="' '&SP3&' '&SP4" style="margin-bottom:0px"></p>
                                    <!--<p class="bold" id="SP24" name="SP24" data-cell="SP24" data-formula="SP4" style="margin-bottom:0px"></p>-->
                                    <p class="bold" id="SP25" name="SP25" data-cell="SP25" data-formula="SP5" style="margin-bottom:0px"></p>
                                    <p class="bold" id="SP26" name="SP26" data-cell="SP26" data-formula="SP6" style="margin-bottom:0px"></p>
                                    <p class="bold" id="SP27" name="SP27" data-cell="SP27" data-formula="SP7" style="margin-bottom:0px"></p>
                                    <p class="bold" id="SP28" name="SP28" data-cell="SP28" data-formula="SP8" style="margin-bottom:0px"></p>
                                    <p class="bold" id="SP29" name="SP29" data-cell="SP29" data-formula="SP9" style="margin-bottom:0px"></p>
                                    <!--
                                      <p class="bold">
                                        Eminent Services Group Pty Ltd T/A My Electrician <br>
                                        PO Box 332, Webb Street <br>
                                        Narre Warren 3805</p>
                                      <p class="bold">
                                        ABN: 95 149 996 958 <br>
                                        1300 660 042
                                      </p> 
                                      <a href="www.myelec.net.au">www.myelec.net.au</a>
                                      <p class="bold">reception@myelec.net.au</p>
                                  -->
                                    </div>

                                    <div class="col-sm-4">&nbsp;</div>

                                    <div class="col-sm-4">
                                      <div class="form-group">
                                        <label for="" class="col-xs-8 control-label">Invoice Number :</label>
                                        <div class="col-xs-4">
                                          <input type="text" id="ZL5" name="ZL5" data-cell="ZL5" data-formula="H4" class="form-control input-sm bold">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label for="" class="col-xs-8 control-label">Date :</label>
                                        <div class="col-xs-4">
                                          <input type="text" value="<?= isset($stored['ZL6'])?$stored['ZL6']:'' ?>" id="ZL6" name="ZL6" data-cell="ZL6" class="form-control input-sm bold" data-formula="C68">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label for="" class="col-xs-8 control-label">Terms :</label>
                                        <div class="col-xs-4">
                                          <input type="text" value="<?= isset($stored['ZL7'])?$stored['ZL7']:'' ?>" id="ZL7" name="ZL7" data-cell="ZL7" data-formula="'14 Days'" class="form-control input-sm bold">
                                        </div>
                                      </div>
                                    </div>

                                  </div>

                                  <div class="row" style="border-bottom:solid 2px #000">
                                    <div class="col-sm-12">
                                      <p>&nbsp;</p>
                                    </div>
                                  </div>
                                  
                                  <div class="row top box_address">
                                    <div class="col-sm-2">
                                      <label for="" class="Bold">Bill To :</label>
                                    </div>
                                    <div class="col-sm-10">
                                      <div class="form-group">
                                        <p class="bold" id="ZB17" name="ZB17" data-cell="ZB17" data-formula="' '&C22&'  '&C23&' , '&P36&' , '&W36&' , '&AO37&' '"></p>
                                        <p class="bold" id="ZB18" name="ZB18" data-cell="ZB18" data-formula="' '&C39&' , '&Z40 "></p>
                                        <p class="bold" id="ZB19" name="ZB19" data-cell="ZB19" data-formula="U39"></p>
                                        <p class="bold" id="ZB20" name="ZB20" data-cell="ZB20" data-formula="C28"></p>
                                        <p class="bold" id="ZB21" name="ZB21" data-cell="ZB21" data-formula="C31"></p>
                                        
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12">

                                      <div class="row">
                                        <div class="col-xs-12">
                                          <div class="table-responsive"  style="overflow-x:auto;max-width:100%;border:solid 1px #000">
                                            <table id="tcodes_tbl" cellpadding="0" cellspasing="0" border="0" class="table table-bordered table-condesed table-hover" >
                                              <thead>
                                                <tr>
                                                  <th class="grey text-center" style="width:55%"><label for="">Description</label></th>
                                                  <th class="grey text-center" style="width:10%"><label for="">Quantity</label></th>
                                                  <th class="grey text-center" style="width:10%"><label for="">Veecs</label></th>
                                                  <th class="grey text-center" style="width:10%"><label for="">Rate</label></th>
                                                  <th class="grey text-center" style="width:15%"><label for="">Amount incld GST</label></th>
                                                  <!--<th class="grey text-center" style="width:10%"><label for="">GST</label></th>-->
                                                </tr>
                                              </thead>
                                              <tbody>
                                                <tr id="row1" class="">
                                                  <td class=""><input type="text" class="input-sm form-control" value="<?= isset($stored['ZA23'])?$stored['ZA23']:'' ?>" id="ZA23" name="ZA23" data-cell="ZA23" data-formula="S74" data-count="23"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZI23'])?$stored['ZI23']:'' ?>" id="ZI23" name="ZI23" data-cell="ZI23" data-formula="AI74"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZG23'])?$stored['ZG23']:'' ?>" id="ZG23" name="ZG23" data-cell="ZG23" data-formula="ROUND(AT74,0)" data-format="0,0[.]"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZJ23'])?$stored['ZJ23']:'' ?>" id="ZJ23" name="ZJ23" data-cell="ZJ23" data-formula="IF(AND(C74='12V Halogen lamp only',ZA23 <> 0),M51,IF(AND(C74='12V Halogen lamp & transfomer',ZA23 <> 0),M55,IF(AND(C74='230V Halogen GU10 lamp only',ZA23 <> 0),M56,'')))" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZH23'])?$stored['ZH23']:'' ?>" id="ZH23" name="ZH23" data-cell="ZH23" data-formula="8" data-format="$ 0,0[.]00"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZK23'])?$stored['ZK23']:'' ?>" id="ZK23" name="ZK23" data-cell="ZK23" data-formula="ZJ23*ZG23" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZL23'])?$stored['ZL23']:'' ?>" id="ZL23" name="ZL23" data-cell="ZL23" data-formula="(ZJ23/10)*ZI23" data-format="$ 0,0[.]00"></td>
                                                </tr>
                                                <tr id="row2" class="">
                                                  <td class=""><input type="text" class="input-sm form-control" value="<?= isset($stored['ZA24'])?$stored['ZA24']:'' ?>" id="ZA24" name="ZA24" data-cell="ZA24" data-formula="S76" data-count="24"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZI24'])?$stored['ZI24']:'' ?>" id="ZI24" name="ZI24" data-cell="ZI24" data-formula="AI76"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZG24'])?$stored['ZG24']:'' ?>" id="ZG24" name="ZG24" data-cell="ZG24" data-formula="ROUND(AT76,0)" data-format="0,0[.]"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZJ24'])?$stored['ZJ24']:'' ?>" id="ZJ24" name="ZJ24" data-cell="ZJ24" data-formula="IF(AND(C74='12V Halogen lamp only',ZA24 <> 0),M51,IF(AND(C74='12V Halogen lamp & transfomer',ZA24 <> 0),M55,IF(AND(C74='230V Halogen GU10 lamp only',ZA24 <> 0),M56,'')))" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZH24'])?$stored['ZH24']:'' ?>" id="ZH24" name="ZH24" data-cell="ZH24" data-formula="25" data-format="$ 0,0[.]00"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZK24'])?$stored['ZK24']:'' ?>" id="ZK24" name="ZK24" data-cell="ZK24" data-formula="ZJ24*ZG24" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZL24'])?$stored['ZL24']:'' ?>" id="ZL24" name="ZL24" data-cell="ZL24" data-formula="(ZJ24/10)*ZI24" data-format="$ 0,0[.]00"></td>
                                                </tr>
                                                <tr id="row3" class="">
                                                  <td class=""><input type="text" class="input-sm form-control" value="<?= isset($stored['ZA25'])?$stored['ZA25']:'' ?>" id="ZA25" name="ZA25" data-cell="ZA25" data-formula="S78" data-count="25"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZI25'])?$stored['ZI25']:'' ?>" id="ZI25" name="ZI25" data-cell="ZI25" data-formula="AI78"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZG25'])?$stored['ZG25']:'' ?>" id="ZG25" name="ZG25" data-cell="ZG25" data-formula="ROUND(AT78,0)" data-format="0,0[.]"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZJ25'])?$stored['ZJ25']:'' ?>" id="ZJ25" name="ZJ25" data-cell="ZJ25" data-formula="IF(AND(C74='12V Halogen lamp only',ZA25 <> 0),M51,IF(AND(C74='12V Halogen lamp & transfomer',ZA25 <> 0),M55,IF(AND(C74='230V Halogen GU10 lamp only',ZA25 <> 0),M56,'')))" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZH25'])?$stored['ZH25']:'' ?>" id="ZH25" name="ZH25" data-cell="ZH25" data-formula="" data-format="$ 0,0[.]00"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZK25'])?$stored['ZK25']:'' ?>" id="ZK25" name="ZK25" data-cell="ZK25" data-formula="ZJ25*ZG25" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZL25'])?$stored['ZL25']:'' ?>" id="ZL25" name="ZL25" data-cell="ZL25" data-formula="0" data-format="$ 0,0[.]00"></td>
                                                </tr>
                                                <tr id="row4" class="">
                                                  <td class=""><input type="text" class="input-sm form-control" value="<?= isset($stored['ZA26'])?$stored['ZA26']:'' ?>" id="ZA26" name="ZA26" data-cell="ZA26" data-formula="S80" data-count="26"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZI26'])?$stored['ZI26']:'' ?>" id="ZI26" name="ZI26" data-cell="ZI26" data-formula="AI80"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZG26'])?$stored['ZG26']:'' ?>" id="ZG26" name="ZG26" data-cell="ZG26" data-formula="ROUND(AT80,0)" data-format="0,0[.]"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZJ26'])?$stored['ZJ26']:'' ?>" id="ZJ26" name="ZJ26" data-cell="ZJ26" data-formula="IF(AND(C74='12V Halogen lamp only',ZA26 <> 0),M51,IF(AND(C74='12V Halogen lamp & transfomer',ZA26 <> 0),M55,IF(AND(C74='230V Halogen GU10 lamp only',ZA26 <> 0),M56,'')))" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZH26'])?$stored['ZH26']:'' ?>" id="ZH26" name="ZH26" data-cell="ZH26" data-formula="" data-format="$ 0,0[.]00"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZK26'])?$stored['ZK26']:'' ?>" id="ZK26" name="ZK26" data-cell="ZK26" data-formula="ZJ26*ZG26" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZL26'])?$stored['ZL26']:'' ?>" id="ZL26" name="ZL26" data-cell="ZL26" data-formula="(ZJ26/10)*ZI26" data-format="$ 0,0[.]00"></td>
                                                </tr>
                                                <!--
                                                <tr id="row5" class="">
                                                  <td class=""><input type="text" class="input-sm form-control" value="<?= isset($stored['ZA27'])?$stored['ZA27']:'' ?>" id="ZA27" name="ZA27" data-cell="ZA27" data-formula="S82" data-count="27"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZI27'])?$stored['ZI27']:'' ?>" id="ZI27" name="ZI27" data-cell="ZI27" data-formula="AI82"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZG27'])?$stored['ZG27']:'' ?>" id="ZG27" name="ZG27" data-cell="ZG27" data-formula="ROUND(AT82,0)" data-format="0,0[.]"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZJ27'])?$stored['ZJ27']:'' ?>" id="ZJ27" name="ZJ27" data-cell="ZJ27" data-formula="IF(AND(C74='12V Halogen lamp only',ZA27 <> 0),M51,IF(AND(C74='12V Halogen lamp & transfomer',ZA27 <> 0),M55,''))" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZH27'])?$stored['ZH27']:'' ?>" id="ZH27" name="ZH27" data-cell="ZH27" data-formula="" data-format="$ 0,0[.]00"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZK27'])?$stored['ZK27']:'' ?>" id="ZK27" name="ZK27" data-cell="ZK27" data-formula="ZJ27*ZG27" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZL27'])?$stored['ZL27']:'' ?>" id="ZL27" name="ZL27" data-cell="ZL27" data-formula="(ZJ27/10)*ZI27" data-format="$ 0,0[.]00"></td>
                                                </tr>
                                                -->
                                                <tr id="row6" class="">
                                                  <input type="hidden" name="item_id_28" value="<?= isset($stored['item_id_28'])?$stored['item_id_28']:'' ?>" />
                                                  <td class=""><input type="text" class="invoice_desc product input-sm form-control" value="<?= isset($stored['ZA28'])?$stored['ZA28']:'' ?>" id="ZA28" name="ZA28" data-cell="ZA28" data-count="28"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZI28'])?$stored['ZI28']:'' ?>" id="ZI28" name="ZI28" data-cell="ZI28" ></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZG28'])?$stored['ZG28']:'' ?>" id="ZG28" name="ZG28" data-cell="ZG28" data-format="0,0[.]"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZJ28'])?$stored['ZJ28']:'' ?>" id="ZJ28" name="ZJ28" data-cell="ZJ28" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZH28'])?$stored['ZH28']:'' ?>" id="ZH28" name="ZH28" data-cell="ZH28" data-formula="" data-format="$ 0,0[.]00"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZK28'])?$stored['ZK28']:'' ?>" id="ZK28" name="ZK28" data-cell="ZK28" data-formula="ZJ28*ZI28" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZL28'])?$stored['ZL28']:'' ?>" id="ZL28" name="ZL28" data-cell="ZL28" data-formula="(ZJ28/10)*ZI28" data-format="$ 0,0[.]00"></td>
                                                </tr>
                                                <tr id="row7" class="">
                                                  <input type="hidden" name="item_id_29" value="<?= isset($stored['item_id_29'])?$stored['item_id_29']:'' ?>" />
                                                  <td class=""><input type="text" class="invoice_desc product input-sm form-control" value="<?= isset($stored['ZA29'])?$stored['ZA29']:'' ?>" id="ZA29" name="ZA29" data-cell="ZA29" data-count="29"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZI29'])?$stored['ZI29']:'' ?>" id="ZI29" name="ZI29" data-cell="ZI29" ></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZG29'])?$stored['ZG29']:'' ?>" id="ZG29" name="ZG29" data-cell="ZG29" data-format="0,0[.]"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZJ29'])?$stored['ZJ29']:'' ?>" id="ZJ29" name="ZJ29" data-cell="ZJ29" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZH29'])?$stored['ZH29']:'' ?>" id="ZH29" name="ZH29" data-cell="ZH29" data-formula="" data-format="$ 0,0[.]00"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZK29'])?$stored['ZK29']:'' ?>" id="ZK29" name="ZK29" data-cell="ZK29" data-formula="ZJ29*ZI29" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZL29'])?$stored['ZL29']:'' ?>" id="ZL29" name="ZL29" data-cell="ZL29" data-formula="(ZJ29/10)*ZI29" data-format="$ 0,0[.]00"></td>
                                                </tr>
                                                <tr id="row8" class="">
                                                  <input type="hidden" name="item_id_30" value="<?= isset($stored['item_id_30'])?$stored['item_id_30']:'' ?>" />
                                                  <td class=""><input type="text" class="invoice_desc product input-sm form-control" value="<?= isset($stored['ZA30'])?$stored['ZA30']:'' ?>" id="ZA30" name="ZA30" data-cell="ZA30" data-count="30"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZI30'])?$stored['ZI30']:'' ?>" id="ZI30" name="ZI30" data-cell="ZI30" ></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZG30'])?$stored['ZG30']:'' ?>" id="ZG30" name="ZG30" data-cell="ZG30" data-format="0,0[.]"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZJ30'])?$stored['ZJ30']:'' ?>" id="ZJ30" name="ZJ30" data-cell="ZJ30" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZH30'])?$stored['ZH30']:'' ?>" id="ZH30" name="ZH30" data-cell="ZH30" data-formula="" data-format="$ 0,0[.]00"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZK30'])?$stored['ZK30']:'' ?>" id="ZK30" name="ZK30" data-cell="ZK30" data-formula="ZJ30*ZI30" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZL30'])?$stored['ZL30']:'' ?>" id="ZL30" name="ZL30" data-cell="ZL30" data-formula="(ZJ30/10)*ZI30" data-format="$ 0,0[.]00"></td>
                                                </tr>
                                                <tr id="row9" class="">
                                                  <input type="hidden" name="item_id_31" value="<?= isset($stored['item_id_31'])?$stored['item_id_31']:'' ?>" />
                                                  <td class=""><input type="text" class="invoice_desc product input-sm form-control" value="<?= isset($stored['ZA31'])?$stored['ZA31']:'' ?>" id="ZA31" name="ZA31" data-cell="ZA31" data-count="31"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZI31'])?$stored['ZI31']:'' ?>" id="ZI31" name="ZI31" data-cell="ZI31" ></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZG31'])?$stored['ZG31']:'' ?>" id="ZG31" name="ZG31" data-cell="ZG31" data-format="0,0[.]"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZJ31'])?$stored['ZJ31']:'' ?>" id="ZJ31" name="ZJ31" data-cell="ZJ31" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZH31'])?$stored['ZH31']:'' ?>" id="ZH31" name="ZH31" data-cell="ZH31" data-formula="" data-format="$ 0,0[.]00"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZK31'])?$stored['ZK31']:'' ?>" id="ZK31" name="ZK31" data-cell="ZK31" data-formula="ZJ31*ZI31" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZL31'])?$stored['ZL31']:'' ?>" id="ZL31" name="ZL31" data-cell="ZL31" data-formula="(ZJ31/10)*ZI31" data-format="$ 0,0[.]00"></td>
                                                </tr>
                                                <tr id="row10" class="">
                                                  <input type="hidden" name="item_id_32" value="<?= isset($stored['item_id_32'])?$stored['item_id_32']:'' ?>" />
                                                  <td class=""><input type="text" class="invoice_desc product input-sm form-control" value="<?= isset($stored['ZA32'])?$stored['ZA32']:'' ?>" id="ZA32" name="ZA32" data-cell="ZA32" data-count="32"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZI32'])?$stored['ZI32']:'' ?>" id="ZI32" name="ZI32" data-cell="ZI32" ></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZG32'])?$stored['ZG32']:'' ?>" id="ZG32" name="ZG32" data-cell="ZG32" data-format="0,0[.]"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZJ32'])?$stored['ZJ32']:'' ?>" id="ZJ32" name="ZJ32" data-cell="ZJ32" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZH32'])?$stored['ZH32']:'' ?>" id="ZH32" name="ZH32" data-cell="ZH32" data-formula="" data-format="$ 0,0[.]00"></td>
                                                  <td class=""><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZK32'])?$stored['ZK32']:'' ?>" id="ZK32" name="ZK32" data-cell="ZK32" data-formula="ZJ32*ZI32" data-format="$ 0,0[.]00"></td>
                                                  <td class="hidden"><input type="text" class="input-sm form-control text-right" value="<?= isset($stored['ZL32'])?$stored['ZL32']:'' ?>" id="ZL32" name="ZL32" data-cell="ZL32" data-formula="(ZJ32/10)*ZI32" data-format="$ 0,0[.]00"></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                      </div>


                                    </div>
                                  </div>

                                  <div class="row top" style="border-bottom:solid 2px #000">
                                    <div class="col-sm-12">
                                      <p class="top bold">* Indicates Non Taxable item</p>
                                    </div>
                                  </div>

                                  <div class="row top">
                                    <div class="col-sm-12">

                                      <div class="row">
                                        <div class="col-xs-6">&nbsp;</div>
                                        <div class="col-xs-6">
                                          <div class="row">
                                            <div class="form-group">
                                              <div class="col-sm-6 text-right">
                                                <label for="" class="control-label bold text-right">Sub Total</label>
                                              </div>
                                              <div class="col-sm-6">
                                                <input value="<?= isset($stored['ZL36'])?$stored['ZL36']:'' ?>" type="text" class="input-sm form-control skyblue text-right" id="ZL36" name="ZL36" data-cell="ZL36" data-format="$ 0,0[.]00" data-formula="((SUM(ZK23:ZK32))/11)*10">
                                              </div>
                                            </div>
                                          </div>
                                          
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-xs-6">&nbsp;</div>
                                        <div class="col-xs-6">
                                          <div class="row">
                                            <div class="form-group">
                                              <div class="col-sm-6 text-right">
                                                <label for="" class="control-label bold text-right">GST 10%</label>
                                              </div>
                                              <div class="col-sm-6">
                                                <input value="<?= isset($stored['ZL38'])?$stored['ZL38']:'' ?>" type="text" class="input-sm form-control skyblue text-right" id="ZL38" name="ZL38" data-cell="ZL38" data-format="$ 0,0[.]00" data-formula="ZL36*0.1">
                                              </div>
                                            </div>
                                          </div>
                                          
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-xs-6">&nbsp;</div>
                                        <div class="col-xs-6">
                                          <div class="row">
                                            <div class="form-group">
                                              <div class="col-sm-6 text-right">
                                                <label for="" class="control-label bold text-right">Total</label>
                                              </div>
                                              <div class="col-sm-6">
                                                <input value="<?= isset($stored['ZL40'])?$stored['ZL40']:'' ?>" type="text" class="input-sm form-control skyblue text-right" id="ZL40" name="ZL40" data-cell="ZL40" data-format="$ 0,0[.]00" data-formula="ZL36+ZL38">
                                              </div>
                                            </div>
                                          </div>
                                          
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-xs-6">&nbsp;</div>
                                        <div class="col-xs-6">
                                          <div class="row">
                                            <div class="form-group">
                                              <div class="col-sm-6 text-right">
                                                <label for="" class="control-label bold text-right">VEEC Incentive</label>
                                              </div>
                                              <div class="col-sm-6">
                                                <input value="<?= isset($stored['ZL42'])?$stored['ZL42']:'' ?>" type="text" class="input-sm form-control skyblue text-right" id="ZL42" name="ZL42" data-cell="ZL42" data-format="$ 0,0[.]00" data-formula="ZL41*ZJ23">
                                                <input value="<?= isset($stored['ZL41'])?$stored['ZL41']:'' ?>" type="hidden" class="input-sm form-control skyblue text-right" id="ZL41" name="ZL41" data-cell="ZL41" data-format="$ 0,0[.]00" data-formula="SUM(ZG23:ZG32)">
                                              </div>
                                            </div>
                                          </div>
                                          
                                        </div>
                                      </div>

                                      <div class="row top">
                                        <div class="col-xs-6">&nbsp;</div>
                                        <div class="col-xs-6">
                                          <div class="row">
                                            <div class="form-group">
                                              <div class="col-sm-6 text-right">
                                                <h3 class="control-label bold text-right">Balance Due</h3>
                                              </div>
                                              <div class="col-sm-6">
                                                <h3 class="bold text-right" id="ZL44" name="ZL44" data-cell="ZL44" data-format="$ 0.00" data-formula="ZL40-ZL42" value="0"></h3>
                                                <input value="<?= isset($stored['ZL46'])?$stored['ZL46']:'' ?>" type="hidden" class="input-sm form-control skyblue text-right" id="ZL46" name="ZL46" data-cell="ZL46" data-format="$ 0.00" data-formula="ZL44">
                                              </div>
                                            </div>
                                          </div>
                                          
                                        </div>
                                      </div>

                                    </div>
                                  </div>

                                  <div class="row space space">
                                        <div class="col-sm-4 col-sm-offset-4 text-center">
                                        <div class="input-group">
                                            <input type="text"class="form-control" id="ZZ1" data-cell="ZZ1"  name="file_name" data-formula="C202" placeholder="Put a name here and" 
                                                <?= isset($stored['file_name'])?'disabled="disabled" value="'.$stored['file_name'].'"':'' ?> />
                                            <span class="input-group-btn">
                                                <button class="savetocloud btn btn-primary" name="save">SAVE</button>
                                            </span>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <!--close page5-->
                                <!--page 6-->
                                <div class="page hidden" id="page6">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <div class="box_photo">
                                                <h4>Certificate of electrical safety (if applicable)</h4>
                                                <?php if(isset($stored['safety']) && file_exists($stored['safety'])): ?>
                                                <img class="img_upload" style="height:125px;text-align:center" src="<?= base_url($stored['safety']) ?>" />
                                                <?php endif; ?>
                                                <div class="input-group">
                                                  <span class="input-group-btn">
                                                    <button type="button" class="delete-photo btn btn-xs btn-default"><img src="<?= base_url('asset/css/delete.png') ?>" alt="" style="width:19px"></button>
                                                  </span>
                                                    <input class="text-center" name="safety" id="safety" type="file">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Electricity Bill Front</h4>
                                                
                                                <?php if(isset($stored['bill_front']) && file_exists($stored['bill_front'])): ?>
                                                <img class="img_upload" style="height:125px;text-align:center" src="<?= base_url($stored['bill_front']) ?>" />
                                                <?php endif; ?>
                                                
                                                <div class="input-group">
                                                  <span class="input-group-btn">
                                                    <button type="button" class="delete-photo btn btn-xs btn-default"><img src="<?= base_url('asset/css/delete.png') ?>" alt="" style="width:19px"></button>
                                                  </span>
                                                    <input class="text-center" name="bill_front" id="bill_front" type="file">
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Installer Photo</h4>
                                                <!--
                                                <?php if(isset($stored['installer']) && file_exists($stored['installer'])): ?>
                                                <img class="img_upload" style="height:125px;text-align:center" src="<?= base_url($stored['installer']) ?>" />
                                                <?php endif; ?> -->
                                                <input name="installer" id="installer" type="hidden">
                                                <img src="" class="hidden" style="height:125px;text-align:center" id="selected_installer_photo_placeholder" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Lamp Photo</h4>
                                                <?php if(isset($stored['evidence1']) && file_exists($stored['evidence1'])): ?>
                                                <img class="img_upload" style="height:125px;text-align:center" src="<?= base_url($stored['evidence1']) ?>" />
                                                <?php endif; ?>
                                                
                                                <div class="input-group">
                                                  <span class="input-group-btn">
                                                    <button type="button" class="delete-photo btn btn-xs btn-default"><img src="<?= base_url('asset/css/delete.png') ?>" alt="" style="width:19px"></button>
                                                  </span>
                                                    <input class="text-center" name="evidence1" id="evidence1" type="file">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Transformer Photos (21d only)</h4>
                                                <?php if(isset($stored['evidence2']) && file_exists($stored['evidence2'])): ?>
                                                <img class="img_upload" style="height:125px;text-align:center" src="<?= base_url($stored['evidence2']) ?>" />
                                                <?php endif; ?>
                                                
                                                <div class="input-group">
                                                  <span class="input-group-btn">
                                                    <button type="button" class="delete-photo btn btn-xs btn-default"><img src="<?= base_url('asset/css/delete.png') ?>" alt="" style="width:19px"></button>
                                                  </span>
                                                    <input class="text-center" name="evidence2" id="evidence2" type="file">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php if($current_user['is_admin'] == 1): ?>
                                    	<div class="row">
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Signature 1</h4>
                                                <input type="file" name="signature1" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Signature 2</h4>
                                                <input type="file" name="signature2" />
                                            </div>
                                        </div>
                                      </div>
                                    <?php endif; ?>
                                    <!--
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Installation Evidence Photo 3</h4>
                                                <?php if(isset($stored['evidence3']) && file_exists($stored['evidence3'])): ?>
                                                <img class="img_upload" style="height:125px;text-align:center" src="<?= base_url($stored['evidence3']) ?>" />
                                                <?php endif; ?>
                                                <div class="input-group">
                                                  <span class="input-group-btn">
                                                    <button type="button" class="delete-photo btn btn-xs btn-default"><img src="<?= base_url('asset/css/delete.png') ?>" alt="" style="width:19px"></button>
                                                  </span>
                                                    <input class="text-center" name="evidence3" id="evidence3" type="file">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Installation Evidence Photo 4</h4>
                                                <?php if(isset($stored['evidence4']) && file_exists($stored['evidence4'])): ?>
                                                <img class="img_upload" style="height:125px;text-align:center" src="<?= base_url($stored['evidence4']) ?>" />
                                                <?php endif; ?>
                                                
                                                <div class="input-group">
                                                  <span class="input-group-btn">
                                                    <button type="button" class="delete-photo btn btn-xs btn-default"><img src="<?= base_url('asset/css/delete.png') ?>" alt="" style="width:19px"></button>
                                                  </span>
                                                    <input class="text-center" name="evidence4" id="evidence4" type="file">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Installation Evidence Photo 5</h4>
                                                <?php if(isset($stored['evidence5']) && file_exists($stored['evidence5'])): ?>
                                                <img src="<?= base_url($stored['evidence5']) ?>" />
                                                <?php endif; ?>
                                                <input class="text-center" name="evidence5" id="evidence5" type="file">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Installation Evidence Photo 6</h4>
                                                <?php if(isset($stored['evidence6']) && file_exists($stored['evidence6'])): ?>
                                                <img src="<?= base_url($stored['evidence6']) ?>" />
                                                <?php endif; ?>
                                                <input class="text-center" name="evidence6" id="evidence6" type="file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Installation Evidence Photo 7</h4>
                                                <?php if(isset($stored['evidence7']) && file_exists($stored['evidence7'])): ?>
                                                <img src="<?= base_url($stored['evidence7']) ?>" />
                                                <?php endif; ?>
                                                <input class="text-center" name="evidence7" id="evidence7" type="file">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Installation Evidence Photo 8</h4>
                                                <?php if(isset($stored['evidence8']) && file_exists($stored['evidence8'])): ?>
                                                <img src="<?= base_url($stored['evidence8']) ?>" />
                                                <?php endif; ?>
                                                <input class="text-center" name="evidence8" id="evidence8" type="file">
                                            </div>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Installation Evidence Photo 9</h4>
                                                <?php if(isset($stored['evidence9']) && file_exists($stored['evidence9'])): ?>
                                                <img src="<?= base_url($stored['evidence9']) ?>" />
                                                <?php endif; ?>
                                                <input class="text-center" name="evidence9" id="evidence9" type="file">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="box_photo">
                                                <h4>Installation Evidence Photo 10</h4>
                                                <?php if(isset($stored['evidence10']) && file_exists($stored['evidence10'])): ?>
                                                <img src="<?= base_url($stored['evidence10']) ?>" />
                                                <?php endif; ?>
                                                <input class="text-center" name="evidence10" id="evidence10" type="file">
                                            </div>
                                        </div>
                                    </div>
                                    -->
                                    <!--

                                    <div class="row space">
                                        <div class="col-sm-12 text-center">
                                            <button type="button" class="btn btn-sm btn-primary back" id="back5" name="back5" data-back="5">BACK</button>
                                            <!--<button type="button" class="btn btn-sm btn-primary" id="pdf" name="pdf">Download PDF</button>
                                        </div>
                                    </div>
                                -->
                                    <div class="row space">
                                        <div class="col-sm-4 col-sm-offset-4 text-center">
                                        <div class="input-group">
                                            <input type="text"class="form-control" id="ZZ2" data-cell="ZZ2"  name="file_name" data-formula="C202" placeholder="Put a name here and" 
                                                <?= isset($stored['file_name'])?'disabled="disabled" value="'.$stored['file_name'].'"':'' ?> />
                                            <span class="input-group-btn">
                                                <button class="savetocloud btn btn-primary" name="save">SAVE</button>
                                            </span>
                                        </div>
                                        <div class="input-group draft-lodge hidden" style="margin-left:auto;margin-right:auto">
                                        	<input type="submit" name="save draft" value="SAVE DRAFT" class="btn btn-warning" />
                                        	<input type="submit" name="lodge form" value="LODGE FORM" class="btn btn-primary" />
                                        </div>
                                        </div>
                                    </div>

                                </div>
                                <!--close page 6-->
                            </form>
                        </div>
                    </div>
                </div>
            </div>      
        </div>

        <!--
        <footer>
          <div class="container">
            <div class="row" style="padding-top:5px">
              <div class="col-md-12 text-right"><h5>©Eminent Service Group PL T/A My Electrician 2015</h5></div>
            </div>
          </div>
        </footer>
        -->


        <!-- script references -->
          <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js') ?>"></script>-->
        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/moment.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/jstat.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/numeral.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-calx-2.1.1.js') ?>"></script>
        <script src="<?= base_url('asset/js/select2.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/industry.json') ?>"></script>
        <script src="<?= base_url('asset/js/street.json') ?>"></script>
        <script src="<?= base_url('asset/js/c_product.json') ?>"></script>
        <script src="<?= base_url('asset/js/new_c_product.json') ?>"></script>
        <script src="<?= base_url('asset/js/d_product.json') ?>"></script>
        <script src="<?= base_url('asset/js/e_product.json') ?>"></script>
        <script src="<?= base_url('asset/js/new_d_product.json') ?>"></script>
        <script src="<?= base_url('asset/js/postcode.json') ?>"></script>
        <!-- <script src="<?= base_url('asset/js/product.json') ?>"></script> -->
        <!-- <script src="<?= base_url('asset/js/installer.json') ?>"></script> -->
        <script type="text/javascript">
            var installer = 
            [
              <?php 
                $filter_installer = array();
                if ($this->session->userdata('is_admin') == 2) 
                  $filter_installer[] = $this->session->userdata('email');
                if(isset($settings_app['installer'])) {
                  foreach($settings_app['installer'] as $installer) {
                    if (!empty($filter_installer) && !in_array($installer['email_address'], $filter_installer)) continue;
                    else echo json_encode($installer).',';
                  }
                }
              ?>
            ];
            var product = [<?php if(isset($settings_app['item']))foreach($settings_app['item'] as $item)echo json_encode($item).','; ?>];
            var base_url = '<?= base_url() ?>';
            var mapsrc = '<?= isset($stored['mapsrc'])?$stored['mapsrc']:'' ?>';
        </script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.ui.touch-punch.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.signaturepad.min.js') ?>"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
        <script src="<?= base_url('asset/js/daniel.js') ?>" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                var sigOpt = {
                    lineTop: 175,
                    drawOnly: true,
                    validateFields: false
                }
                $('.sigPad').each(function () {
                	if (jQuery(this).find('canvas').length > 0) {
                		jQuery(this).signaturePad(sigOpt)	
                	}
                })
            });
            var stored = <?= isset($stored) ? json_encode($stored) : '{}' ?>;
            var site_url = '<?= site_url() ?>';
            $('ul.nav li.dropdown').click(function(){$(this).toggleClass('open')});
        </script>
        <script src="<?= base_url('asset/js/getveet-db.js') ?>" type="text/javascript"></script>
    </body>
</html>