<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Job Leads</title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
    <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
  </head>
  <body>
    
    <div class="container">

      <div class="row">
        <div class="col-sm-12">
          <?php include APPPATH.'/views/menuView.php'; ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12" id="content">
          <div class="panel">
            <div class="panel-body">
              
              <form enctype="multipart/form-data" action="" class="form-horizontal" method="post" role="form" id="job_leads">
                <?php if($current_user['is_admin'] == 1): ?>
                <div class="row">
                    <div class="col-sm-2 hidden-xs nopadding">
                        &nbsp;
                    </div>
                    <div class="col-sm-1 nopadding">
                        <div class="row">
                            <div class="col-sm-12 col-xs-6">
                                <label for="" class="box_label btn-block text-center">Date</label>
                            </div>
                        
                            <div class="col-sm-12 col-xs-6">
                                <input type="text" class="form-control input-sm text-center datepicker" name="date_leads" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 nopadding">
                        <div class="row">
                            <div class="col-sm-12 col-xs-6">
                                <label for="" class="box_label btn-block text-center">Suburb</label>
                            </div>
                        
                            <div class="col-sm-12 col-xs-6">
                                  <input type="text" class="form-control input-sm text-center" name="suburb_leads" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 nopadding">
                        <div class="row">
                            <div class="col-sm-12 col-xs-6">
                                <label for="" class="box_label btn-block text-center">estimated lamp quantity</label>
                            </div>
                        
                            <div class="col-sm-12 col-xs-6">
                                  <input type="text" class="form-control input-sm text-center" name="qty_leads"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 nopadding">
                        <div class="row">
                            <div class="col-sm-12 col-xs-6">
                                <label for="" class="box_label btn-block text-center">Action</label>
                            </div>
                        
                            <div class="col-sm-12 col-xs-6 text-center">
                                <input type="submit" class="btn btn-xs" value="save leads" name="save_leads">
                            </div>
                        </div>
                    </div>

                </div>

                <?php endif; ?>
                
                <div class="page top" id="leads_list">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="bold text-center">Job Leads List</h3>
                        </div>
                    </div>

                    <?php if (isset ($status)): ?>
                    <div class="row">
                      <div class="col-xs-10 col-xs-offset-1 bg-info">
                        <?= $status ?>
                        <a onclick="$(this).parent('div').slideUp()" style="cursor:pointer; float:right">x</a>
                      </div>
                    </div>
                    <?php endif; ?>                    

                    <div class="row visible-lg">
                        <div class="col-sm-offset-1 col-sm-8 col-xs-8" style="border:solid 1px #000">
                            <div class="row">
                                <div class="col-sm-2 nopadding">
                                    <label for="" class="btn-block box_label visible-lg text-center" style="margin-bottom:0px">No</label>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <label for="" class="btn-block box_label visible-lg text-center" style="margin-bottom:0px">Date</label>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <label for="" class="btn-block box_label visible-lg text-center" style="margin-bottom:0px">Suburb</label>
                                </div>
                                <div class="col-sm-4 nopadding">
                                    <label for="" class="btn-block box_label visible-lg text-center" style="margin-bottom:0px">Estimated Lamp Quantity</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 col-xs-4 nopadding text-center" style="border:solid 1px #000;border-left:none">
                            <div class="col-sm-12 visible-lg nopadding">
                                <label for="" class="btn-block box_label" style="margin-bottom:0px">Action</label>
                            </div>
                        </div>
                    </div>

                    <?php foreach ($items as $item): ?>
                    <div class="row">
                        <div class="col-xs-12 hidden-lg nopadding">
                            <label for="" class="box_label btn-block text-center" style="margin-bottom:0px;margin-top:10px">job leads</label>
                        </div>
                        
                        <div class="col-sm-offset-1 col-sm-8 col-xs-8">
                            <div class="row">
                                <div class="col-sm-2 nopadding">
                                    <input type="text" class="form-control input-sm text-center" value="<?= $item->lid ?>"/>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <input type="text" class="form-control input-sm text-center" value="<?= $item->date_leads ?>"/>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <input type="text" class="form-control input-sm text-center" value="<?= $item->suburb_leads ?>"/>
                                </div>
                                <div class="col-sm-4 nopadding">
                                    <input type="text" class="form-control input-sm text-center" value="<?= $item->qty_leads ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 col-xs-4 nopadding text-center">
                            <div class="col-sm-6 nopadding">
                              <?php 
                                $cids = explode(',', $item->cids);
                                if ($item->companyRequest < 3 && !in_array($current_user['cid'], $cids)): 
                              ?>
                              <form action="" method="POST">
                                  <input type="hidden" name="lid" value="<?= $item->lid ?>">
                                  <input type="submit" class="btn btn-xs btn-success" value="Request Leads" name="req_leads">
                              </form>
                              <?php endif; ?>
                            </div>
                            <?php if($current_user['is_admin'] == 1): ?>
                            <div class="col-sm-6 nopadding">
                                <a href="<?= site_url("maincontroller/leadsDelete/$item->lid/confirm") ?>" class="btn btn-xs btn-danger">DELETE</a>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                  
                </div>
              </form>
              
            </div>
          </div>
        </div>
      </div>
      
    </div>
        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.ui.touch-punch.min.js') ?>" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('.datepicker').datepicker({
                    'dateFormat' : 'dd-mm-yy'
                });
            });
        </script>
    </body>
</html>