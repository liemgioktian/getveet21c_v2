<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Development Pack Document</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
				
                <div class="col-md-12">
                    <div class="panel">
                        <?php include APPPATH.'/views/resourcesSubmenu.php'; ?>
                        <div class="box_label text-center">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>DEVELOPMENT PACK DOCUMENT</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="page" id="">
                                <div class="row">
                                     <div class="col-sm-offset-1 col-sm-10">
                                        <h3 class="bold">How to create VEECs when replacing halogens with LEDs</h3>
                                        <h3 class="bold">
                                            What you need to know
                                        </h3>
                                        <h4 class="italic">Please read these pages carefully, complete our online quiz and we will call you to progress your certificate creation.</h4>
                                        <br>
                                        <h4 class="bold">An overview of the Victorian Energy Efficiency Target</h4>
                                        <p class="text-justify">
                                            VEECs for halogen downlight replacements are produced under Prescribed Activity 21 of the Victorian Energy Efficiency Target (VEET). The scheme provides financial incentives for owners or tenants of both domestic and commercial premises to replace existing 12V halogen lamps (Activity 21C) or replace the lamps and the associated transformers (Activity 21D) with an approved energy efficient product to reduce their energy consumption and lower greenhouse gas emissions. The quantity of certificates each installation can produce varies, depending on the number of downlights replaced and the efficacy of the energy efficient products installed.
                                        </p>
                                        <p class="text-justify">
                                            Certificates are created on a ‘deemed basis’ (i.e. the lifetime savings are created upfront, typically for up to ten years), which reduces upfront costs and enables a quicker return on investment. The lighting upgrade will also result in significant savings in electricity on an ongoing basis.
                                        </p>
                                        <p class="text-justify">
                                            Each VEEC represents the equivalent of one tonne of carbon dioxide abated as a result of the prescribed activity. Mandatory scheme participants, (i.e. Victorian electricity and/or gas retailers) must surrender a number of VEECs each year based on the share of the energy market that they held in that year, or face a penalty for any shortfall. This obligation to acquire certificates creates a dynamic market driven by supply and demand, and the value of certificates fluctuates on a regular basis accordingly.
                                        </p>
                                        <br>
                                        <h4 class="bold">General guidelines for claiming VEECs</h4>
                                        <p class="text-justify">
                                            VEECs may only be claimed for eligible lighting activities once the activity has been implemented and commissioned. You must submit the required supporting documentation to allow Green Energy Trading to confirm scheme compliance and registration of certificates.
                                        </p>
                                        <p class="text-justify">
                                            The right to create VEECs must be assigned from the energy consumer to Green Energy Trading using an approved assignment form, which is signed by the energy consumer and the electrician responsible for the installation. This procedure forms the basis of Green Energy Trading’s authority to act.
                                        </p>
                                        <p class="text-justify">
                                            VEECs must be created before 30 June in the calendar year after the installation date. After this date it is not possible to claim VEECs for installations in previous calendar year(s).
                                        </p>
                                        <p class="text-justify">
                                                Underpinning all environmental certificate schemes is the requirement to pass on a demonstrable financial benefit to the energy consumer.
                                        </p>
                                        <br>
                                        
                                        <h4 class="bold">VEEC creation checklists</h4>
                                        <p class="text-justify">For your handy reference, all considerations for VEEC creation are summarised in the following checklists.</p>
                                        <ol>
                                            <li>
                                                <p class="bold">Prepare your business for VEEC creation</p>
                                                <p class="text-justify">
                                                    Complete the training and sign a Relationship Agreement with My Electrician prior to commencing any installations.
                                                </p>
                                                <p class="text-justify">
                                                    Provide copies of licences for each electrician that will be working on your business’ behalf, prior to (or at the time) of submitting your first VEEC 21 claim.
                                                </p>
                                                <p class="text-justify">
                                                    Be familiar with your obligations under the Victorian Occupational Health and Safety Act 2004. Obligations include, but not limited to:
                                                </p>
                                                <ul style="padding-left:20px">
                                                    <li><p class="text-justify">Provision of adequate supervision and training of employees.</p></li>
                                                    <li><p class="text-justify">Provision and maintenance of safe systems of work.</p></li>
                                                    <li><p class="text-justify">Implementation of robust safety management and risk management systems.</p></li>
                                                    <li><p class="text-justify">Ensuring appropriate contractor management systems are in place.</p></li>
                                                </ul>
                                                <p class="text-justify">
                                                    Be familiar with your obligations under the Australian Consumer Law and the Competition and Consumer Act 2010. Obligations include, but are not limited to:
                                                </p>
                                                <ul style="padding-left:20px">
                                                    <li><p class="text-justify">Adhering to telemarketing and door-to-door sales practices, (including compliance with the Federal Governments “Do Not Call Register” and “Do Not Knock” stickers, and the times of day that consumers can be contacted).</p></li>
                                                    <li><p class="text-justify">Ensuring that employees do not engage in misleading or deceptive conduct (including claiming the installation is mandatory or that the employee is working on behalf of the Government).</p></li>
                                                </ul>
                                                <p class="text-justify">
                                                    Brief your sales team in relation to important key messages:
                                                </p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">It is appropriate to say:</p>
                                                        <ul style="padding-left:15px">
                                                            <li><p class="text-justify">“The Victorian Energy Saver Incentive provides a financial incentive for your lighting upgrade.”</p></li>
                                                            <li>
                                                                <p class="text-justify">“This is a voluntary scheme and certain conditions need to be met to be eligible.”</p>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">It is not appropriate to say:</p>
                                                        <ul style="padding-left:15px">
                                                            <li>
                                                                <p class="text-justify">“I work for the Victorian Government.”</p>
                                                            </li>
                                                            <li>
                                                                <p class="text-justify">“This lighting upgrade is mandatory.”</p>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">
                                                    Brief your sales team in relation to appropriate conduct:
                                                </p>
                                                <ul style="margin-left:20px">
                                                    <li>
                                                        <p class="text-justify">It is appropriate to:</p>
                                                        <ul style="padding-left:15px">
                                                            <li>
                                                                <p class="text-justify">Leave appropriate contact details (e.g. business card) at the premises, even when a sale doesn’t go ahead.</p>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">It is not appropriate to:</p>
                                                        <ul style="padding-left:15px">
                                                            <li>
                                                                <p class="text-justify">Use aggressive sales techniques.</p>
                                                            </li>
                                                            <li>
                                                                <p class="text-justify">Give contact details of the Victorian Government / Essential Services Commission instead of your own name and company details.</p>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <p class="text-justify bold">Understand the eligibility requirements</p>
                                                <p class="text-justify">In order for a project to be eligible to create VEECs it must consist of either of the following:</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">Replacement of at least one 35W (or higher) halogen downlight MR16 ELV lamp only in a residential or commercial premises with an approved energy efficient lamp that is compatible with the existing transformer – Activity 21C</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Replacement of at least one 35W (or higher) halogen downlight MR16 ELV and the associated transformer in a residential or commercial premises with an approved energy efficient lamp and driver – Activity 21D</p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">Any other type of lamp, including 240V halogen downlights (e.g. GU10 or PAR38), cannot not be replaced as part of the VEEC Activity.</p>
                                                <br>
                                                <p class="text-justify">Check that all energy efficient lighting products to be installed are listed on the Approved Products list.</p>
                                                <br>
                                                <p class="text-justify">All existing lighting products that are removed from the premises must be decommissioned in an environmentally friendly manner. All lamps must be recycled by companies which have the facility to safely recover and reuse mercury, glass, phosphor and aluminium. For Activity 21D, all replaced transformers must also be recycled by a third party recycling agent. Evidence of the type and quantity of removed products can be achieved through two means:</p>
                                                <br>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">An itemised recycling receipt for the disposal of the lamps (and transformers), clearly defining the quantity and the installation address.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">OR, where small installations are bundled together on one itemised recycling receipt with a total quantity, a photo of the removed lamps and transformers laid out in a grid pattern is required to confirm quantities for each installation.</p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">An ‘A class’ licenced electrician must conduct a pre-installation inspection of existing wiring condition at the premises.</p>
                                                <p class="text-justify">Where the pre-installation inspection reveals any non-compliant or unsafe electrical conditions, then the lighting upgrade installation cannot commence. The energy consumer must be advised to rectify or remove any identified unsafe electrical condition in accordance with the Electricity Safety Act 1998.</p>
                                                <p class="text-justify">A licenced electrician must carry out the installation and where any electrical wiring work was involved, a Certificate of Electrical Safety is provided.</p>
                                                <p class="text-justify">The energy efficient products must not be installed onto dimmable circuits, unless they are approved by the manufacturer as suitable.</p>
                                                <p class="text-justify">The installation team must not leave any spare energy efficient products at the premises, all energy efficient lamps must be fully installed by the electrician and the existing lamps must be removed from the premises.</p>
                                                <br>
                                            </li>
                                            <li>
                                                <p class="bold text-justify">Prior to the installation</p>
                                                <p class="text-justify">Take a photo of the name and address of a piece of mail of the energy consumer for the installation address for part of the documentary evidence. This is proves that the consumer signing the assignment from is a resident of the address that is listed down.</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">This must contain the installation site address, the name of the energy consumer.</p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">Conduct pre-installation inspection of existing wiring condition and report any issues to the energy consumer.</p>
                                                <p class="text-justify">Determine quantity of existing halogen MR16 ELV lamps, and the type of existing transformers.</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">•   Make note of these details. For Activity 21C installations, collect the brand and model number of the existing transformer, so that compatibility can be verified.</p>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <p class="bold text-justify">During installation</p>
                                                <p class="text-justify">Carry out installation, adhering to all State Electrical Regulations and requirements.</p>
                                                <p class="text-justify">Collect photographic evidence of:</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">Each type of existing halogen lamp.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Each type of existing transformer, clearly showing the markings including brand and model number.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">The quantity of removed lamps by laying them out in a grid and taking a photo. Lamps are to be decommissioned by removal of /p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">If the installation involved electrical wiring work, a Certificate of Electrical Safety (CES) must be provided to the energy consumer.</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">Ensure the ‘completion of work’ date on the CES is accurate.</p>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <p class="text-justify bold">Post installation</p>
                                                <br>
                                                <p class="text-justify">Organise removal of all replaced lamps to a registered recycling depot and obtain itemised recycling receipt.</p>
                                                <ul style="padding-left:20px">
                                                    <li>
                                                        <p class="text-justify">•   Ensure this details the product quantities, lamp type, transformer type and installation address.</p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">Issue a final installation tax invoice to the energy consumer. The invoice should include:</p>
                                                <ul style="padding-left:35px">
                                                    <li>
                                                        <p class="text-justify">Energy consumer name.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Installation address.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Installed products’ model number and quantity.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">VEEC benefit provided.</p>
                                                    </li>
                                                </ul>
                                                <p class="text-justify">The My Electrician online form contains an automated Proof of purchase feature which satisfies the above requirement.</p>
                                                <p class="text-justify">Complete the VEEC 21 Assignment Form and obtain the signatures of the energy consumer and the electrician.</p>
                                                <p class="text-justify">If the energy consumer is a business that is registered for GST, obtain a tax invoice from the energy consumer to Green Energy Trading for the value of the VEEC benefit supplied, inclusive of GST.</p>
                                                <p class="text-justify">Collate all required documentation and submit to Green Energy Trading:</p>
                                                <p class="text-justify">Completed VEEC 21 Assignment Form with handwritten signatures.</p>
                                                <ul style="padding-left:35px">
                                                    <li>
                                                        <p class="text-justify">Copy of electrician’s licence (if not provided previously).</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Electricity bill.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Installation invoice.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Photos of removed lamps, existing transformers.</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Certificate of Electrical Safety (if wiring work was involved).</p>
                                                    </li>
                                                    <li>
                                                        <p class="text-justify">Tax invoice to Green Energy Trading (only if energy consumer is registered for GST).</p>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ol>
                                        <br>
                                        <h4 class="bold">Efficient and compliant VEEC creation</h4>
                                        <p class="text-justify">Maintaining high standards of processing compliance is a hallmark of our approach. Our procedures involve rigorous checking of all documents and information so that they adhere to the Essential Services Commission’s guidelines. Green Energy Trading has one of the best records for accuracy in certificate schemes as measured by the low incidence of invalid certificate creation and a prompt certificate turnaround time, which improves with streamlining of processes. We are flexible in our approach and we work with our clients to facilitate continuous improvement.</p>
                                        <h4 class="bold">Audits</h4>
                                        <p class="text-justify">As an AP under the VEET, our supplier, Green Energy Trading is obligated to undertake periodic audits to ascertain that we have retained the necessary records and confirm the creation of VEECs in accordance with the VEET legislation. An audit may include a site inspection of the installation or a telephone audit with the energy consumer.</p>
                                        <h4 class="bold">Congratulations</h4>
                                        <p class="text-justify">You are one step closer to creating VEECs for halogen replacements. We will contact you within one business day to prepare you for your business’ development and certificate creation. In the meantime, with any queries please call us on <strong>1300 660 042</strong>.</p>
                                        <br><br><br>
                                        <small class="bold">The information in this document is believed to be accurate at the time of writing and we do not guarantee the accuracy of any information or data contained and accept no responsibility for any loss, injury or inconvenience sustained by users of this document.</small>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>      
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
        <script type="text/javascript">
        	jQuery('.filter-date').datepicker();
        </script>
    </body>
</html>