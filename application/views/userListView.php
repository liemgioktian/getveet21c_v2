<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>User list</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <input type="hidden" name="action" id="action">
                            <input name="action" id="action" type="hidden">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>REGISTERED USERS</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page user-->
                            <div class="page" id="">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                    <table id="tcodes_tbl" cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                        <thead>
                                                            <tr class="skyblue">
                                                            		<th>QUIZ</th>
                                                                <th>ID</th>
                                                                <th>USER ID</th>
                                                                <th>EMAIL</th>
                                                                <th>COMPANY</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($items as $item): ?>
                                                                <tr>
                                                                		<td class="text-center">
                                                                			<input onclick="window.location = '<?= site_url("maincontroller/mark_user/marked_quiz/$item->uid") ?>'" 
                                                                			type="checkbox" <?= $item->marked_quiz == 1 ? 'checked="checked"':'' ?> />
                                                                		</td>
                                                                    <td class="text-center">
                                                                    	<input onclick="window.location = '<?= site_url("maincontroller/mark_user/marked_id/$item->uid") ?>'"
                                                                    	type="checkbox" <?= $item->marked_id == 1 ? 'checked="checked"':'' ?> />
                                                                    </td>
                                                                    <td><?= $item->uid ?></td>
                                                                    <td><?= $item->email ?></td>
                                                                    <td><?= $item->electricians_company_name ?></td>
                                                                    <td>
                                                                        <a href="<?= site_url("maincontroller/userDelete/$item->uid/confirm") ?>" class="btn btn-sm btn-danger">DELETE</a>
                                                                        <form method="POST" style="display: inline">
                                                                        	<input type="hidden" name="uid" value="<?= $item->uid ?>" />
                                                                        	<input type="hidden" name="is_admin" value="<?= $item->is_admin == 0 ? '2':'0' ?>" />
                                                                        	<input type="submit" value="<?= $item->is_admin == 0 ? 'SET AS ADMIN':'SET AS USER' ?>" 
                                                                        	class="btn btn-sm btn-<?= $item->is_admin == 0 ? 'warning':'info' ?>" />
                                                                        </form>
                                                                        <a href="<?= site_url("maincontroller/userActive/$item->uid/confirm") ?>" 
                                                                        	class="btn btn-sm btn-<?= $item->active == 0 ? 'warning':'info' ?>">
                                                                        	<?= $item->active == 0 ? 'ACTIVATE':'DEACTIVATE' ?></a>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--close user-->
                        </div>
                    </div>
                        
                        
                </div>
            </div>      
        </div>


        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
    </body>
</html>