<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Primsal Training</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
				
                <div class="col-md-12">
                    <div class="panel">
                        <?php include APPPATH.'/views/resourcesSubmenu.php'; ?>
                        <div class="box_label text-center">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>INSTALATION GUIDE PRIMSAL_PMR166WWHPF</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page 1-->
                                <div class="page" id="page1">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide1.png') ?>"  class="img-responsive text-center" alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <!--<button type="button" class="btn btn-sm btn-info back" id="back4" name="back4" data-back="4">BACK</button>-->
                                              <button type="button" class="btn btn-sm btn-info next" id="next2" name="next2" data-next="2">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 1 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page1-->

                                <!--page 2-->
                                <div class="page hidden" id="page2">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide2.png') ?>"  class="img-responsive text-center" alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back1" name="back1" data-back="1">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next3" name="next3" data-next="3">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 2 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page2-->

                                <!--page 3-->
                                <div class="page hidden" id="page3">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide3.png') ?>"  class="img-responsive text-center" alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back2" name="back2" data-back="2">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next4" name="next4" data-next="4">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 3 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page3-->

                                <!--page 4-->
                                <div class="page hidden" id="page4">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide4.png') ?>"  class="img-responsive text-center" alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back3" name="back3" data-back="3">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next5" name="next5" data-next="5">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 4 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page4-->

                                <!--page 5-->
                                <div class="page hidden" id="page5">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide5.png') ?>"  class="img-responsive text-center" alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back4" name="back4" data-back="4">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next6" name="next6" data-next="6">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 5 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page5-->

                                <!--page 6-->
                                <div class="page hidden" id="page6">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide6.png') ?>"  class="img-responsive text-center" alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back5" name="back5" data-back="5">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next7" name="next7" data-next="7">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 6 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page6-->

                                <!--page 7-->
                                <div class="page hidden" id="page7">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide7.png') ?>"  class="img-responsive text-center" alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back6" name="back6" data-back="6">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next8" name="next8" data-next="8">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 7 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page7-->

                                <!--page 8-->
                                <div class="page hidden" id="page8">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide8.png') ?>"  class="img-responsive text-center" alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back7" name="back7" data-back="7">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next9" name="next9" data-next="9">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 8 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page8-->

                                <!--page 9-->
                                <div class="page hidden" id="page9">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide9.png') ?>"  class="img-responsive text-center" alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back8" name="back8" data-back="8">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next10" name="next10" data-next="10">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 9 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page9-->

                                <!--page 10-->
                                <div class="page hidden" id="page10">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide10.png') ?>" class="img-responsive text-center"  alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back9" name="back9" data-back="9">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next11" name="next11" data-next="11">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 10 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page10-->

                                <!--page 11-->
                                <div class="page hidden" id="page11">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide11.png') ?>" class="img-responsive text-center"  alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back10" name="back10" data-back="10">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next12" name="next12" data-next="12">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 11 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page11-->

                                <!--page 12-->
                                <div class="page hidden" id="page12">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide12.png') ?>" class="img-responsive text-center"  alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back11" name="back11" data-back="11">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next13" name="next13" data-next="13">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 12 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page12-->

                                <!--page 13-->
                                <div class="page hidden" id="page13">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide13.png') ?>" class="img-responsive text-center"  alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back12" name="back12" data-back="12">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next14" name="next14" data-next="14">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 13 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page13-->

                                <!--page 14-->
                                <div class="page hidden" id="page14">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide14.png') ?>" class="img-responsive text-center"  alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back13" name="back13" data-back="13">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next15" name="next15" data-next="15">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 14 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page14-->

                                <!--page 15-->
                                <div class="page hidden" id="page15">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide15.png') ?>" class="img-responsive text-center"  alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back14" name="back14" data-back="14">BACK</button>
                                              <button type="button" class="btn btn-sm btn-info next" id="next16" name="next16" data-next="16">NEXT</button>
                                          </div>
                                         
                                      </div>
                                      <div class="row page_foot">
                                          <div class="col-md-12">
                                              <p class="text-right bold">
                                                  (Page 15 of 16)
                                              </p>
                                          </div>
                                      </div>
                                </div>
                                <!--close page15-->

                                <!--page 16-->
                                <div class="page hidden" id="page16">
                                    
                                    <div class="row top">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/training/Slide16.png') ?>" class="img-responsive text-center"  alt="" style="width:100%">
                                        </div>
                                    </div>
                                    <div class="row space">
                                          <div class="col-sm-12 text-center">
                                              <button type="button" class="btn btn-sm btn-info back" id="back15" name="back15" data-back="15">BACK</button>
                                              <!--<button type="button" class="btn btn-sm btn-info next" id="next16" name="next16" data-next="16">NEXT</button>-->
                                          </div>
                                         
                                      </div>
                                    <div class="row page_foot">
                                        <div class="col-md-12">
                                            <p class="text-right bold">
                                                  (Page 16 of 16)
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--close page16-->
                        </div>
                    </div>
                </div>
                
            </div>      
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
            /*
            * button next and previous
            */
                $(function() {
                var clicks = 1;
                $('.next').on('click', function() {
                    clicks++;
                      var percent = Math.min(Math.round(clicks / 16 * 100), 100);
                      $('.progress-bar').width(percent + '%').text('Part ' +clicks+ ' of 16');
                    });
                    $('.back').on('click', function(){
                        clicks--
                        var percent = Math.min(Math.round(clicks / 16 *100), 100);
                        $('.progress-bar').width(percent + '%').text('Part ' +clicks+ ' of 16')
                    });
                });
                
                $('.back, .next').click(function(){
                    var $button = $(this);
                    var $prevPage = $button.attr('data-back');
                    var $nextPage = $button.attr('data-next');
                    $('.page').addClass('hidden');
                    if($button.hasClass('next')){
                        $('#page'+$nextPage).removeClass('hidden');
                    }else{

                        $('#page'+$prevPage).removeClass('hidden');
                    }
                });
            });
        </script>
        </script>
    </body>
</html>