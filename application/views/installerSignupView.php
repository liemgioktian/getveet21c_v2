<!DOCTYPE html>
<html lang="en"  class="body-error"><head>
        <meta charset="utf-8">
        <title>Get Veet21C Sign Up</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<?= base_url('asset/css/login.css') ?>" rel="stylesheet">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/validationEngine.jquery.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading" style="margin-top:5px">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <h3 class="bold" style="color:#828396">My Electrician VEET 21 Installer Sign Up</h3>
                                </div>
                                <div class="col-sm-12 top">
                                    <div class="progress" style="margin-bottom:0px">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 33%;">
                                                page 1 of 3
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form method="POST" action="<?= site_url('signup/2') ?>" enctype="multipart/form-data" class="installer-signup">
                            <!--signup Step1 setting company-->
                            <div class="signup" id="signup1">
                                <div class="row box_desc" id="welcome_installer">
                                    <div class="col-sm-12 text-center">
                                        <img src="<?= base_url('asset/images/logo-green.png') ?>" style="height:80px" alt="">
                                        <br>
                                    </div>
                                   
                                    <div class="col-sm-12">
                                        <p class="text-justify top">
                                            Welcome to the My Electrician Online Portal Installer sign up.
                                        </p>
                                        <p class="text-justify top">
                                            The process will only take a few minutes and will allow you to log in to the portal and have access to completing VEET 21 Assignment forms.
                                        </p>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                        <button type="button" class="btn btn-sm btn-primary next" id="next2" name="next2" data-next="2">NEXT</button>
                                    </div>
                                </div>
                               
                            </div>
                            <!--close signup step 1 setting-->
                            
                            <!--signup step2-->
                            <div class="signup hidden" id="signup2">
                                <form action="<?= site_url('signup/3') ?>" class="form-horizontal" method="post" id="instaler_details"> 
                                    <div class="row box_desc" style="border:none;margin-top:5px">
                                        <div class="col-sm-12 text-center">
                                            <h3 class="bold text-center">Installer Details</h3>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="row box_desc">
                                        <div class="col-sm-12 text-center">
                                            <img src="<?= base_url('asset/images/avatar.png') ?>" class="text-center" style="width:150px" alt="">
                                        </div>
                                    </div>
                                -->
                                    <div class="row box_desc" style="border:none">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">First Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="installer_first_name" size="50"  value="<?= isset($installer_first_name)?$installer_first_name:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Last Name</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="installer_last_name" size="50"  value="<?= isset($installer_last_name)?$installer_last_name:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">A Class Licence Number</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="licence_number" size="50"  value="<?= isset($licence_number)?$licence_number:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Issue/ Renewal Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm datepicker" name="issue_date" size="50"  value="<?= isset($issue_date)?$issue_date:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Expiry Date</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm datepicker" name="expaired_date" size="50"  value="<?= isset($expaired_date)?$expaired_date:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Email Address</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="email_address" size="50"  value="<?= isset($expaired_date)?$expaired_date:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Phone number</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="validate[required] form-control input-sm" name="phone_number" size="50"  value="<?= isset($expaired_date)?$expaired_date:'' ?>" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Upload ID Photo</label>
                                                <div class="col-sm-6">
                                                    <input type="file" name="installer_photo" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Upload ID Photo 2</label>
                                                <div class="col-sm-6">
                                                    <input type="file" name="installer_photo2" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Password</label>
                                                <div class="col-sm-6">
                                                    <input class="form-control input-sm" name="password" type="password" />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 bold">Confirm Password</label>
                                                <div class="col-sm-6">
                                                    <input class="form-control input-sm" name="password2" type="password"/>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            
                                            <!--
                                            <div class="form-group">
                                                <div class="col-sm-offset-4 col-sm-4">
                                                    <input class="form-control input-md" name="email" type="text"  placeholder="Email" />
                                                    <input class="form-control input-md" name="password" type="password"  placeholder="Password" />
                                                    <input class="form-control input-md" name="password2" type="password"  placeholder="Password" />
                                                    <div class="clear"></div>
                                                    <div class="button-login"><input type="submit" value="Sign up"></div>
                                                </div>
                                            </div>
                                            -->
                                        </div>
                                    </div>
                                    <div class="row space">
                                        <div class="col-sm-12 text-center">
                                            <!--<button type="button" class="btn btn-sm btn-primary back" id="back1" name="back1" data-back="1">BACK</button>-->
                                            <!--<button type="button" class="btn btn-sm btn-primary next" id="next3" name="next3" data-next="3">NEXT</button>-->
                                            <input type="submit" value="NEXT" class="btn btn-sm btn-primary" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--close signup2-->

                            <!--signup step3-->
                            <div class="signup hidden" id="signup3">
                                <div class="row box_desc">
                                    <div class="col-sm-offset-3 col-sm-6 space">
                                        <p class="">You have successfully created your Installer profile and are almost ready to create VEECs.</p>
                                        <p class="">Your log in will be activated within 24 hours.</p>
                                        <p class="">You will be notified by email when your account is active and further details will be provided about the final steps required to create VEECs</p>
                                        <p class="">Thanks for signing up!</p>
                                        
                                        <br>
                                        <p class="">
                                            Kind Regards, <br>
                                            The My Electrician Team
                                        </p>
                                        <!--<div class="button-login text-center top"><a href="<?= site_url()?>" class="btn-success btn-md btn">SIGN IN</a></div>-->
                                    </div>
                                </div>
                                <div class="row space">
                                    <div class="col-sm-12 text-center">
                                        <a href="<?= site_url()?>" class="btn-success btn-sm btn">SIGN IN</a>
                                    </div>
                                </div>
                            </div>
                            <!--close signup3 -->
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    
        <script src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.ui.touch-punch.min.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/jquery.validationEngine.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/jquery.validationEngine-en.js') ?>" type="text/javascript"></script>
        <script src="<?= base_url('asset/js/installerSignup.js') ?>" type="text/javascript"></script>
    </body>
</html>

