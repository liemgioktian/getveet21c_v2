<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Saved Forms</title>
        <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <link href="<?= base_url('asset/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('asset/css/jquery-ui.css') ?>" rel="stylesheet">
        <link rel="icon" type="image/png" href="<?= base_url('asset/images/favicon.png') ?>" sizes="32x32">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
        <link href="<?= base_url('asset/css/styles.css') ?>" rel="stylesheet">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php include APPPATH.'/views/menuView.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="box_label text-center">
                            <input type="hidden" name="action" id="action">
                            <input name="action" id="action" type="hidden">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 style="margin:5px 0px;"><strong>SAVED FORMS</strong></h4>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <!--page 2-->
                            <div class="page" id="page2">
                                <div class="row">
                                    <div class="col-sm-4 hidden-xs">
                                        <img src="<?= $logo ?>" style="height:80px" alt="">
                                    </div>
                                    <div class="col-sm-4">
                                        <h2 class="text-center bold">VEEC 21</h2>
                                        <h5 class="text-center bold"> 12V Halogen downlight replacement</h5>
                                    </div>
                                    <div class="col-sm-1 hidden-xs">&nbsp;</div>
                                    <div class="col-sm-3 hidden-xs">
                                        <!--<img src="<?= base_url('asset/css/images/logo.png') ?>" alt="">-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 box_desc">
                                        <div class="row">
                                            
                                            <form method="POST">
                                            <div class="col-xs-12 col-sm-4 col-sm-offset-1" style="margin-top:10px">
                                                <label>From</label>
                                                <input type="text" name="since" class="filter-date text-right filter-input pull-right" value="<?= isset($since)?$since:'' ?>" />
                                            </div>
                                            <div class="col-xs-12 col-sm-4" style="margin-top:10px">
                                                <label>To</label>
                                                <input type="text" name="until" class="filter-date text-right filter-input pull-right" value="<?= isset($until)?$until:'' ?>" />
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-sm-offset-1" style="margin-top:5px">
                                                <label>FILE NAME CONTAINING</label>
                                                <input type="text" class="filter-input pull-right" name="namelike" value="<?= isset($namelike)?$namelike:'' ?>" />
                                            </div>
                                            <div class="clearfix"></div>
                                            
                                            <?php if(isset($companies)): ?>
                                            <div class="col-xs-12 col-sm-4 col-sm-offset-1" style="margin-top:5px">
                                            	<label>SHOW FORM OF</label>
                                            	<select name="cid" class="col-xs-12">
                                            		<option value="all">All Companies</option>
                                            		<?php foreach($companies as $company): ?>
                                            			<option value="<?= $company->cid ?>"
                                            				<?php if(isset($selected_company)) if($company->cid==$selected_company) echo 'SELECTED="SELECTED"' ?>
                                            				><?= $company->electricians_company_name ?></option>
                                            		<?php endforeach; ?>
                                            	</select>
                                            </div>
                                            <?php endif; ?>
                                            
                                            <div class="col-xs-12 col-sm-4 col-sm-offset-1" style="margin-top:5px">
                                            	<!--<label>DROPDOWN</label>-->
                                            	<select name="file_list_dropdown">
                                            		<option value="0">NONE</option>
                                            		<option value="LODGE">LODGE FORM</option>
                                            		<option value="DRAFT">SAVE DRAFT</option>
                                            		<option value="COMPLETE">COMPLETE</option>
                                            		<option value="INCOMPLETE">INCOMPLETE</option>
                                            		<option value="CLIENTNUMBER">CLIENT NUMBER</option>
                                                    <option value="GET">GET Info Request</option>
                                            	</select>
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                            <!--
                                            <div class="col-xs-12 col-sm-2 col-sm-offset-1" style="margin-top:5px">
                                            	<label>SHOW FORM OF</label>
                                            	<select name="filter_lodge_draft">
                                            		<option value="ALL">ALL</option>
                                            		<option value="LODGE FORM">LODGE FORM</option>
                                            		<option value="SAVE DRAFT">SAVE DRAFT</option>
                                            	</select>
                                            </div>
                                            
                                            <div class="col-xs-12 col-sm-2 col-sm-offset-1" style="margin-top:5px">
                                            	<label>FORM COMPLETENESS</label>
                                            	<select name="filter_complete">
                                            		<option value="ALL">ALL</option>
                                            		<option value="1">COMPLETE</option>
                                            		<option value="0">INCOMPLETE</option>
                                            	</select>
                                            </div>
                                        -->
                                            
                                            <div class="col-xs-12 col-sm-3 col-sm-offset-1" style="margin-top:5px">
                                                <input type="submit" class="btn btn-sm btn-warning btn-search" value="SEARCH" />
                                                <input type="reset" class="btn btn-sm btn-danger" value="RESET" onclick="jQuery('.filter-input').val('').parent().parent('form').submit()" />
                                                <button class="btn btn-sm" name="removelimit">SHOW ALL</button>
                                            </div>
                                            </form>

                                            <?php if($this->session->userdata('is_admin')==1): ?>
                                            <div class="col-xs-12 col-sm-offset-1" style="margin-top:10px;margin-bottom:5px;">
                                                <a href="<?= site_url('maincontroller/csv') ?>" class="btn btn-info btn-sm">CSV ALL</a>
                                                <a href="javascript:download_csv_selected();" class="btn btn-info btn-sm">CSV SELECTED</a>
                                                <form action="<?= site_url('maincontroller/contractor_weekly_report') ?>" method="POST">

                                                <label class="pull-left" style="margin:6px 5px auto 0">Date Range To Export Weekly Report</label>
	                                            	<select name="cid" class="col-xs-12 col-sm-3" style="margin:5px 5px auto 0">
	                                            		<option value="0">All Companies</option>
	                                            		<?php foreach($companies as $company): ?>
	                                            			<option value="<?= $company->cid ?>"
	                                            				<?php if(isset($selected_company)) if($company->cid==$selected_company) echo 'SELECTED="SELECTED"' ?>
	                                            				><?= $company->electricians_company_name ?></option>
	                                            		<?php endforeach; ?>
	                                            	</select>
                                                    <input type="text" name="since" value="<?= date('m/d/Y', strtotime("-10 day")); ?>" class="filter-date" />
                                                    <input type="text" name="until" value="<?= date('m/d/Y', strtotime("-3 day")); ?>" class="filter-date" />
                                                    <input type="submit" value="WEEKLY REPORT" class="btn btn-sm btn-info" />
                                                </form>
                                            </div>
                                            <?php endif; ?>
                                            
                                            <div class="col-xs-12">
                                                <div class="table-responsive" style="overflow-x:auto;max-width:100%">
                                                    <table id="tcodes_tbl" cellspasing="0" class="table table-bordered table-condesed table-hover" border="0" cellpadding="0">
                                                        <thead>
                                                            <tr class="skyblue">
                                                                <?php if($this->session->userdata('is_admin')==1): ?>
                                                                <th class="text-center hidden-xs">CSV</th>
                                                                <th class="text-center hidden-xs">GET</th>
                                                                <!--<th class="text-center">PROCESSED</th>-->
                                                                <?php endif;?>
                                                                <th class="text-center"><span class="hidden-xs">COMPLETION STATUS</span></th>
                                                                <th class="text-center"><span class="hidden-xs">JOB REFERENCE</span></th>
                                                                <th class="text-center"><span class="hidden-xs">FILE NAME</span></th>
                                                                <th class="text-center hidden-xs">INSTALLATION DATE</th>
                                                                <th class="text-center hidden-xs">USER</th>
                                                                <th class="text-center hidden-xs">COMPANY</th>
                                                                <th class="text-center hidden-xs">ACTION</th>
                                                                <th class="text-center hidden-xs"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($items as $item): ?>
                                                                <tr <?= $item->out_of_stock==1?'class="bg-danger"':'' ?>>
                                                                    <?php if($this->session->userdata('is_admin')==1): ?>
                                                                    <td class="text-center hidden-xs"><input type="checkbox" value="<?= $item->fid ?>" /></td>
                                                                    <td class="text-center hidden-xs">
                                                                      <input onclick="$.get('<?= site_url("maincontroller/marked/$item->fid") ?>')" type="checkbox" 
                                                                      <?= $item->marked > 0 ? 'checked="checked"' : '' ?> />
                                                                    </td>
                                                                    
                                                                    <?php endif; ?>

                                                                    <td class="text-center" width="20%">
                                                                        <?php if($this->session->userdata('is_admin')==1): ?>
                                                                            <?php if($item->admin3_status=='confirmed'):?>
                                                                                <p class="bg-info">Form Checked and Clean</p>
                                                                                <?php echo $item->checked_date > 0 ? date('d.m.Y h:i:sa', $item->checked_date) : ''; ?>
                                                                            <?php endif; ?>
                                                                            <?php if(null!=$item->admin3_status): ?>
                                                                                <?php if ($item->processed==1) : 
                                                                                echo '<p class="bg-warning">Processed</p>' ; 
                                                                                echo $item->processed_date > 0 ? date('d.m.Y h:i:sa', $item->processed_date) : '';
                                                                                else : ?>

                                                                                <!--<button class="btn btn-sm" onclick="$.get('<?= site_url('maincontroller/processed/' . $item->fid) ?>', function () {window.location=window.location.href})">
                                                                                    process
                                                                                </button> <br>-->
                                                                                &nbsp;
                                                                                <?php endif; ?>
                                                                            <?php endif; ?>
                                                                        <?php endif; ?>

                                                                    	<?php
                                                                    	$lodge_draft = $item->lodge_draft;
                                                                    	if (strlen($lodge_draft) > 0) {
                                                                    		$lodge_draft = $lodge_draft === 'SAVE DRAFT' ? 'DRAFT' : 'LODGED';
                                                                    		$lodge_draft =  "<p class='bg-danger' style='margin-bottom:0'>$lodge_draft</p>";
                                                                    	}
                                                                        
                                                                        switch ($current_user['is_admin']) {
                                                                      	case '0':
                                                                      	case '2':
                                                                      		if ($item->marked > 0) {
                                                                      			echo $item->complete == 1 ?
                                                                      				"$lodge_draft<p class='bg-success'>COMPLETED</p>":
                                                                                    "$lodge_draft<p class='bg-warning'>INCOMPLETE</p><p class='text-left'>$item->incompletion_msg</p>";
                                                                                }else echo "$lodge_draft";

                                                                           break;
                                                                           
                                                                        case '1':
                                                                        	if ($item->marked > 0) {
                                                                        		echo $item->complete == 1 ?
                                                                        			"$lodge_draft<p class='bg-success'>COMPLETED</p>":
                                                                        			"$lodge_draft<p class='bg-warning'>INCOMPLETE</p>";
                                                                                echo $item->complete == 1 && $item->completed_date > 0 ? date('d.m.Y h:i:sa', $item->completed_date) : '';

																				}else echo "$lodge_draft";
																		    ?>
																			<form action="" method="POST">
																				<input type="hidden" name="fid" value="<?= $item->fid ?>" />
																				<?php if ($item->complete == 0): ?>
																					<textarea id="incompletion_msg" name="incompletion_msg" placeholder="Details…"><?= $item->incompletion_msg ?></textarea><br/>
																				<?php endif; ?>
																				<input type="submit" name="set_incomplete" value="incomplete" class="btn btn-sm btn-warning" />
																				<input type="submit" name="set_complete" value="complete" class="btn btn-sm btn-success" />
																			</form>
																		  <?php break; } ?>

                                                                            <?php if ($current_user['is_admin'] == 1 && $item->admin3_status == 'incomplete' && $item->processed != 1): ?>
                                                                                <p class="bg-danger">Information Requested:</p>
                                                                                                                                                
                                                                                <?php echo $item->request_date > 0 ? date('d.m.Y h:i:sa', $item->request_date) : ''; ?>
                                                                                                                                                
                                                                                <?php if ($item->info_submitted != 1): ?>
                                                                                    <form action="" method="POST">
                                                                                        <input type="hidden" name="fid" value="<?= $item->fid ?>" />
                                                                                        <textarea id="admin3_msg" name="admin3_msg" placeholder="Info Request Details"><?= $item->admin3_msg ?></textarea><br/>
                                                                                               
                                                                                        <input type="submit" name="set_msg_submitted" value="Information Submitted" class="btn btn-sm btn-warning" />
                                                                                    </form>
                                                                                <?php else: ?>
                                                                                    <p class="bg-danger">Information Submitted:</p>
                                                                                    <?php echo $item->submitted_date > 0 ? date('d.m.Y h:i:sa', $item->submitted_date) : ''; ?>
                                                                                    <form action="" method="POST">
                                                                                        <input type="hidden" name="fid" value="<?= $item->fid ?>" />
                                                                                        <textarea id="admin3_msg" name="admin3_msg" placeholder="Info Request Details"><?= $item->admin3_msg ?></textarea><br/>
                                                                                                                                                        
                                                                                    </form>
                                                                                <?php endif; ?>
                                                                            <?php endif; ?>
																																			
																																			
																																			
                                                                    </td>
                                                                    <td class="">
                                                                      <div class="hidden-xs"><?= $item->clientNumber ?></div>
                                                                      <table class="hidden-md hidden-lg mobile-21list-data">
                                                                        <tr>
                                                                          <td>Job Ref.</td>
                                                                          <td><?= $item->clientNumber ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td>File Name</td>
                                                                          <td><?= $item->name ?></td>
                                                                        </tr>
                                                                          <td>Installation Date</td>
                                                                          <td>
                                                                            <?php
                                                                              $data = json_decode($item->data, true);
                                                                              $item->installation_date = isset($data['C68']) && !empty($data['C68']) ? $data['C68'] : '';
                                                                              echo $item->installation_date;
                                                                             ?>
                                                                          </td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td>Company</td>
                                                                          <td><?= $item->electricians_company_name ?></td>
                                                                        </tr>
                                                                      </table>
                                                                    </td>
                                                                    <td class="">
                                                                      <div class="hidden-xs"><?= $item->name ?></div>
                                                                      <div class="hidden-md hidden-lg mobile-21list-action">
                                                                        <a href="<?= site_url("maincontroller/edit/$item->fid") ?>" class="btn btn-sm">Load / Edit</a> <br>
                                                                        <a href="<?= site_url("maincontroller/edit/$item->fid/upload") ?>" class="btn btn-sm">Upload Photos</a> <br>
                                                                        <a href="<?= site_url("maincontroller/pdf/$item->fid") ?>" class="btn btn-sm">Print PDF</a> <br>
                                                                        <a href="<?= site_url("maincontroller/delete/$item->fid/confirm") ?>" class="btn btn-sm">Delete</a>
                                                                      </div>
                                                                    </td>
                                                                    <td class="text-center hidden-xs"><?= $item->installation_date?></td>
                                                                    <td class="hidden-xs"><?= $item->email ?></td>
                                                                    <td class="hidden-xs"><?= $item->electricians_company_name ?></td>
                                                                    <td class="hidden-xs">
                                                                    	<?php if($current_user['is_admin'] == 1 || $item->complete != 1): ?>
                                                                        <a href="<?= site_url("maincontroller/edit/$item->fid") ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Load/Edit"><i class="glyphicon glyphicon-edit"></i></a>
                                                                        <a href="<?= site_url("maincontroller/edit/$item->fid/upload") ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Upload"><i class="glyphicon glyphicon-camera"></i></a>
                                                                        <?php endif; ?>

                                                                        <a href="<?= site_url("maincontroller/pdf/$item->fid") ?>" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Print PDF"><i class="glyphicon glyphicon-print"></i></a>

                                                                        <?php if($this->session->userdata('is_admin')==1): ?>
                                                                        <a href="<?= site_url("maincontroller/delete/$item->fid/confirm") ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>
                                                                            <form action="" method="POST">
                                                                                <input type="hidden" name="fid" value="<?= $item->fid ?>" />
                                                                                <input type="submit" name="reset_signature" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="remove signature" value="remove sign"/>
                                                                            </form>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                    <td class="hidden-xs">
                                                                    	<?php if ($item->lodge_draft !== 'LODGE FORM'): ?>
	                                                                    	<form action="" method="POST">
	                                                                    		<input type="hidden" name="fid" value="<?= $item->fid ?>" />
	                                                                    		<input type="submit" name="lodge_draft" class="btn btn-sm btn-primary"
	                                                                    		value="LODGE FORM" />
	                                                                    	</form>
                                                                    	<?php endif; ?>
	                                                                    	
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--close page2-->
                        </div>
                    </div>
                </div>
            </div>      
        </div>

        <script type="text/javascript" src="<?= base_url('asset/js/jquery-1.11.1.min.js') ?>"></script>
        <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('asset/js/jquery-ui.min.js') ?>"></script>
        <script type="text/javascript">
        	jQuery('.filter-date').datepicker();
        	function download_csv_selected(){
        		var download_csv_url = '<?= site_url('maincontroller/csv?ids=') ?>';
        		jQuery('table#tcodes_tbl tbody tr td :checkbox:checked').each(function(){
        			download_csv_url += jQuery(this).val()+'|';
        		});
        		download_csv_url = download_csv_url.slice(0,-1);
        		window.location = download_csv_url;
        	}
        </script>
    </body>
</html>