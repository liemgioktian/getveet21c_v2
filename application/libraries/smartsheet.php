<?php

class smartsheet {

  function open ($conlength = 0) {
    $this->curl = curl_init();
    curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    $header = array();
    $header[] = "Content-length: $conlength";
    $header[] = 'Content-type: application/json';
    $header[] = "Authorization: Bearer 3tyflplbuf46lupspip245g4br";
    curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);
  }

  function close () {
    curl_close($this->curl);
  }

  function buildUrl ($param) {
    $url = 'https://api.smartsheet.com/2.0';
    $url .= isset ($param['method']) ? '/' . $param['method'] : '';
    $url .= isset ($param['id']) ? '/' . $param['id'] : '';
    return $url;
  }

  function get ($param) {
    $this->open();
    $url = $this->buildUrl($param);
    curl_setopt($this->curl, CURLOPT_URL, $url);
    $result = curl_exec($this->curl);
    $this->close();
    $returntype = isset ($param['datatype']) ? $param['datatype'] : 'text';
    switch ($returntype) {
      case 'object': return json_decode($result); break;
      default: return $result;
    }
  }

  function update ($param) {
    $data = json_encode($param['object']);
    $this->open(strlen ($data));
    $url = $this->buildUrl($param);
    curl_setopt($this->curl, CURLOPT_URL, $url);
    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
    $result = curl_exec($this->curl);
    $this->close();
  }
}
