ALTER TABLE `company` ADD `lead_manager_enabled` TINYINT( 1 ) NOT NULL ,
ADD `lead_manager_url` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
ADD `lead_form_url` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
ADD `landing_text` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
ADD `landing_header_image` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
ADD `landing_footer_image` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;
