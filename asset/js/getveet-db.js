////$('input[type="text"]').each(function () {
//    var name = $(this).attr('name');
//    var value = $(this).attr('value');
//    value = (!$(this).is('[value]')) ? '\'\'' : '\'' + value + '\'';
//    name = '$stored[\'' + name + '\']';
//    $(this).attr('value', '<?= isset(' + name + ')?' + name + ':' + value + ' ?>');
//});
//
//$(':radio').each(function () {
//    var name = $(this).attr('name');
//    $(this).attr('henrisusanto', '<?= isset($stored[\'' + name + '\'])?\'checked\':\'\' ?>');
//});
$(function (){
    $('select').each(function () {
        var name = $(this).attr('name');
        var selected = stored[name];
        $('[name="'+name+'"] option[value="'+selected+'"]').attr('selected',true);
        $(this).change();
    });
    var currentURL = window.location.href;
    if(currentURL.indexOf('maincontroller/edit') > 0 && currentURL.indexOf('upload') > 0) {
    	$('#page6').removeClass('hidden').siblings('div.page').hide();
    	$('.draft-lodge').removeClass('hidden').siblings('div').addClass('hidden')
    }
});

jQuery('button.delete-photo').each(function(){
	var file_upload = jQuery(this).parent().siblings('input[type="file"]');
	var file_field = file_upload.attr('name');
	var stored_photo = jQuery(this).parent().parent().siblings('img');
	if(stored_photo.length>0){
		var photo_name = stored_photo.attr('src').replace(base_url,'');
		stored_photo.parent().append('<input type="hidden" name="delete-photos['+file_field+']">');
	}
	jQuery(this).bind('click',function(){
		if(!confirm('Are you sure ?')) return;
		file_upload.val('');
		if(stored_photo.length>0){
			stored_photo.hide();
			jQuery('[name="delete-photos['+file_field+']"]').val(photo_name);
		}
	});
});