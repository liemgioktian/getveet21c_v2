$(document).ready(function(){
  $('.btn-34-submit').unbind('click').bind('click', function() {
    var data = {}
    for (var s=1; s<=3; s++) {
      var elId = 'sign'+s
      var elObj = $('#'+elId)
      var base30 = elObj.jSignature('getData', 'base30')
      if (base30[1] !== '') elObj.append('<input type="hidden" name="'+elId+'" value="'+base30[1]+'">')
    }
    $('form').each(function() {data[$(this).attr('id')] = $(this).serialize()})
    $.post(window.location.href, data, function (fid) {
      // $('body').html(fid)
      window.location = site_url + '/form34controller/edit/' + fid
    })
  })
/*
*   init calx
*/
    var $calc = $('#assignment34').calx();
    var $calc1 = $('#survey').calx();
    var $calc2 = $('#quote').calx();
    select2Product34()
    specType()

    $('#sidebar').affix({
      offset: {
        top: 230,
        bottom: 100
      }
    }); 
    $('#midCol').affix({
          offset: {
            top: 230,
            bottom: 100
          }
    }); 
    
/** find next input when enter  */
    var $input = $('input');
    $input.on('keypress', function(e) {
        if (e.which === 13) {
            var ind = $input.index(this);
            $input.eq(ind + 1).focus();
            e.preventDefault();
        }
    });
/*
*   Hide each categories when page load
*/
    /*
    var $a;
    for ( $a=1; $a <=20; $a++) {
        $('.row_class'+$a).hide();

    };


/** initial date-picker */
    $('.datepicker').datepicker({
        'dateFormat' : 'dd-mm-yy'
    });

/*
*   up down button on each categories assignment form
*/

    $('.row_up_button').click(function(e){
        e.preventDefault();
        var $data_row_id = $(this).attr('data-button');
        $('.row_class'+$data_row_id).hide('');
        $('#row_down'+$data_row_id).removeClass('hidden');
        $('#row_up'+$data_row_id).addClass('hidden');
    });

    $('.row_down_button').click(function(e){
        e.preventDefault();
        var $data_row_id = $(this).attr('data-button');
        $('.row_class'+$data_row_id).show('');
        $('#row_down'+$data_row_id).addClass('hidden');
        $('#row_up'+$data_row_id).removeClass('hidden');
    });

/*
*   up down button on each categories quote page
*/

    $('.quote_up_button').click(function(e){
        e.preventDefault();
        var $data_row_id = $(this).attr('data-button');
        $('.quote_class'+$data_row_id).hide('');
        $('#quote_down'+$data_row_id).removeClass('hidden');
        $('#quote_up'+$data_row_id).addClass('hidden');
    });

    $('.quote_down_button').click(function(e){
        e.preventDefault();
        var $data_row_id = $(this).attr('data-button');
        $('.quote_class'+$data_row_id).show('');
        $('#quote_down'+$data_row_id).addClass('hidden');
        $('#quote_up'+$data_row_id).removeClass('hidden');
    });
/*
*   hide input field
*/  
    $('#AE25').change(function(){
        $AE25 = $('#AE25').val();
        if($(this).val() == 1 ){
            $('#coes').removeClass('hidden');
            $('#electrician_sign').addClass('hidden');
        }else{
            $('#coes').addClass('hidden');
            $('#electrician_sign').removeClass('hidden');
        }
    });

/** 
* select2 init
*/
    $(".industry").select2({
        data:{ results: industry, text: 'id' },
        formatSelection: function(item){
            
           
            //$('#AS31').val(item.value).trigger('change');
            return item.id;
        },
        formatResult: function(item){
            return item.id;
        }
    });

    $(".postcode").select2({
        data:{ results: postcode, text: 'id' },
        formatSelection: function(item){
            //var count = this.element.attr('data-count');
           
            //$('#survey').calx('getCell', 'AW69').setValue(item.value).calculate();
            $('#survey').calx('getCell', 'Y10').setValue(item.value).calculate();
            $('#survey').calx('getSheet').calculate();
            return item.id;

        },
        formatResult: function(item){
            return item.id;

        }
    });

    $(".existing_lamp").select2({
        data:{ results: existing_lamp, text: 'id' },
        formatSelection: function(item){
            var count = this.element.attr('data-count');
            $('#survey').calx('getCell', 'HA'+count).setValue(item.multi).calculate();
            $('#survey').calx('getCell', 'HB'+count).setValue(item.plus).calculate();
            $('#survey').calx('getCell', 'HC'+count).setValue(item.rate).calculate();
            $('#survey').calx('getCell', 'HD'+count).setValue(item.max).calculate();
            $('#survey').calx('getSheet').calculate();
            return item.id;

        },
        formatResult: function(item){
            return item.id;

        }
    });

    $(".installer").select2({
      data:{ results: installer, text: 'id' },
      formatSelection: function(item){
        $('input[name="iid"]').val(item.iid)
        return item.id;
      },
      formatResult: function(item){
        return item.id;
      }
    });
/*
*   add row lighting survey
*/  $row_num = $('#survey_body tr').length + 14;
    $('#btn-add-survey').click(function(e){
        e.preventDefault();
        $row_num++;
        if ($row_num==24) {
            $('#btn-add-survey').addClass('hidden');
        }else{
            $('#btn-add-survey').removeClass('hidden');
        };
            var $survey_body = $('#survey_body');
            $survey_body.append(
                /*
                '<tr id="row1">'+
                    '<td class="text-center">'+
                        '<label for="" id="B'+$row_num+'" name="B'+$row_num+'" data-cell="B'+$row_num+'" data-formula="A'+$row_num+'"></label>'+
                        '<input type="hidden" id="A'+$row_num+'" name="A'+$row_num+'" data-cell="A'+$row_num+'" data-formula="'+$row_num+'-14">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-'+$row_num+'0" id="C'+$row_num+'" name="C'+$row_num+'" data-cell="C'+$row_num+'"></td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow existing_lamp width-250" id="H'+$row_num+'" name="H'+$row_num+'" data-cell="H'+$row_num+'" data-count="'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HA'+$row_num+'" name="HA'+$row_num+'" data-cell="HA'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HB'+$row_num+'" name="HB'+$row_num+'" data-cell="HB'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HC'+$row_num+'" name="HC'+$row_num+'" data-cell="HC'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HD'+$row_num+'" name="HD'+$row_num+'" data-cell="HD'+$row_num+'">'+
                    '</td>'+
                    '<td class="hidden">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="N'+$row_num+'" name="N'+$row_num+'" data-cell="N'+$row_num+'" data-formula="IF(HD'+$row_num+' = '+'yes'+',IF(O'+$row_num+'>37,37,O'+$row_num+'),O'+$row_num+')">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="P'+$row_num+'" name="P'+$row_num+'" data-cell="P'+$row_num+'" data-formula="(N'+$row_num+'+0)*HA'+$row_num+'+HB'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="T'+$row_num+'" name="T'+$row_num+'" data-cell="T'+$row_num+'" data-formula="Q'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="V'+$row_num+'" name="V'+$row_num+'" data-cell="V'+$row_num+'" data-formula="U'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="Z'+$row_num+'" name="Z'+$row_num+'" data-cell="Z'+$row_num+'" data-formula="IF(W'+$row_num+'=0,0,IF(AL'+$row_num+'=0,(P'+$row_num+'*30000*T'+$row_num+'*V'+$row_num+'*W'+$row_num+')/1000000,(P'+$row_num+'*AK'+$row_num+'*T'+$row_num+'*V'+$row_num+'*W'+$row_num+')/1000000))">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="O'+$row_num+'" name="O'+$row_num+'" data-cell="O'+$row_num+'"></td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow width-250" name="Q'+$row_num+'" id="Q'+$row_num+'" data-cell="Q'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No control system</option>'+
                            '<option value="0.7">Occupancy sensor</option>'+
                            '<option value="0.7">Day-light linked control</option>'+
                            '<option value="0.85">Programmable dimming</option>'+
                            '<option value="0.9">Manual dimming</option>'+
                            '<option value="0.744">VRU</option>'+
                            '<option value="0.6">Occupancy & Daylight Control</option>'+
                            '<option value="0.6">Occupancy & Program. Dimmer</option>'+
                            '<option value="0.63">Occupancy & Manual Dimmer</option>'+
                            '<option value="0.6">Occupancy & VRU</option>'+
                            '<option value="0.6">Daylight Control & Program. Dimmer</option>'+
                            '<option value="0.63">Daylight Control & Manual Dimmer</option>'+
                            '<option value="0.6">Daylight Control & VRU</option>'+
                            '<option value="0.765">Program. Dimmer & Manual Dimmer</option>'+
                            '<option value="0.6324">Program. Dimmer & VRU</option>'+
                            '<option value="0.6696">Manual Dimmer & VRU</option>'+
                            '</select>'+
                        '</td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow" name="U'+$row_num+'" id="U'+$row_num+'" data-cell="U'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No</option>'+
                            '<option value="1.05">Yes</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="W'+$row_num+'" name="W'+$row_num+'" data-cell="W'+$row_num+'"></td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow width-250" name="Y'+$row_num+'" id="Y'+$row_num+'" data-cell="Y'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">Delamping from multiple fittings</option>'+
                            '<option value="2">Replace lamp only</option>'+
                            '<option value="replace_lamp">Replace lamp and control gear </option>'+
                            '<option value="replace_full">Replace full luminaire</option>'+
                            '<option value="1">Install VRU or sensor only (on existing lighting)</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class="hidden">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AL'+$row_num+'" name="AL'+$row_num+'" data-cell="AL'+$row_num+'" data-formula="IF(Y'+$row_num+'='+"'replace_lamp'"+',0,IF(Y'+$row_num+'='+"'replace_full'"+',0,Y'+$row_num+'))">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AP'+$row_num+'" name="AP'+$row_num+'" data-cell="AP'+$row_num+'" data-formula="AM'+$row_num+'">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AR'+$row_num+'" name="AR'+$row_num+'" data-cell="AR'+$row_num+'" data-formula="AQ'+$row_num+'">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AT'+$row_num+'" name="AT'+$row_num+'" data-cell="AT'+$row_num+'" data-formula="IF(AS'+$row_num+'=0,0,IF(AL'+$row_num+'=1,((P'+$row_num+'*AK'+$row_num+'*AP'+$row_num+'*AR'+$row_num+'*AS'+$row_num+')/1000000),IF(AL'+$row_num+'=2,((AI'+$row_num+'*AK'+$row_num+'*AP'+$row_num+'*AR'+$row_num+'*AS'+$row_num+')/1000000),((AI'+$row_num+'*30000*AP'+$row_num+'*AR'+$row_num+'*AS'+$row_num+')/1000000))))">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AV'+$row_num+'" name="AV'+$row_num+'" data-cell="AV'+$row_num+'" data-formula="IF(AS'+$row_num+'=0,0,(Z'+$row_num+'- AT'+$row_num+'))" data-format="0.0">'+
                    '</td>'+
                    '<td class="">'+
                      '<input type="text" class="input-sm form-control text-center yellow width-'+$row_num+'0 product34" id="AA'+$row_num+'" name="AA'+$row_num+'" data-cell="AA'+$row_num+'">'+
                      '<input type="hidden" id="PID'+$row_num+'" name="PID'+$row_num+'" data-cell="PID'+$row_num+'">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="AI'+$row_num+'" name="AI'+$row_num+'" data-cell="AI'+$row_num+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow" id="AK'+$row_num+'" name="AK'+$row_num+'" data-cell="AK'+$row_num+'" data-format="0,0[.]"></td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow width-250" name="AM'+$row_num+'" id="AM'+$row_num+'" data-cell="AM'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No control system</option>'+
                            '<option value="0.7">Occupancy sensor</option>'+
                            '<option value="0.7">Day-light linked control</option>'+
                            '<option value="0.85">Programmable dimming</option>'+
                            '<option value="0.9">Manual dimming</option>'+
                            '<option value="0.744">VRU</option>'+
                            '<option value="0.6">Occupancy & Daylight Control</option>'+
                            '<option value="0.6">Occupancy & Program. Dimmer</option>'+
                            '<option value="0.63">Occupancy & Manual Dimmer</option>'+
                            '<option value="0.6">Occupancy & VRU</option>'+
                            '<option value="0.6">Daylight Control & Program. Dimmer</option>'+
                            '<option value="0.63">Daylight Control & Manual Dimmer</option>'+
                            '<option value="0.6">Daylight Control & VRU</option>'+
                            '<option value="0.765">Program. Dimmer & Manual Dimmer</option>'+
                            '<option value="0.6324">Program. Dimmer & VRU</option>'+
                            '<option value="0.6696">Manual Dimmer & VRU</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow" name="AQ'+$row_num+'" id="AQ'+$row_num+'" data-cell="AQ'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No</option>'+
                            '<option value="1.05">Yes</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow width-50" id="AS'+$row_num+'" name="AS'+$row_num+'" data-cell="AS'+$row_num+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow" id="AU'+$row_num+'" name="AU'+$row_num+'" data-cell="AU'+$row_num+'" data-formula="IF(AS'+$row_num+'=0,0,3000)"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center yellow" id="AW'+$row_num+'" name="AW'+$row_num+'" data-cell="AW'+$row_num+'" data-formula="IFERROR(IF(AV'+$row_num+'>0,IF(AW9=0,(AV'+$row_num+'*BS106)*0.963,0),0),0)" data-format="0.0"></td>'+
               '</tr>'
               */
               '<tr id="row1">'+
                    '<td class="text-center">'+
                        '<label for="" id="B'+$row_num+'" name="B'+$row_num+'" data-cell="B'+$row_num+'" data-formula="A'+$row_num+'"></label>'+
                        '<input type="hidden" id="A'+$row_num+'" name="A'+$row_num+'" data-cell="A'+$row_num+'" data-formula="'+$row_num+'-14">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow width-150" id="C'+$row_num+'" name="C'+$row_num+'" data-cell="C'+$row_num+'">'+
                    '</td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow width-250" name="H'+$row_num+'" id="H'+$row_num+'" data-cell="H'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="Class2_Common">2 - Apartment building or Block of flats (Common areas)</option>'+
                            '<option value="Class3_Other">3 - Hotel / Motel or Dormitory room (Other than common areas)</option>'+
                            '<option value="Class3_Common">3 - Hotel / Motel or Dormitory common area (common areas) </option>'+
                            '<option value="Class5">5  - Office or professional services building (includes medical consulting rooms) </option>'+
                            '<option value="Class6">6  - Retail shop / Restaurant / Café/ Hairdressers / Service station / Bar (excluding dance or live music venues) </option>'+
                            '<option value="Class7a_open">7a - Carpark - Open air  (open air carparks) </option>'+
                            '<option value="Class7a_undercover">7a - Carpark - Undercover  (carparks other than open air) </option>'+
                            '<option value="Class7b">7b - Warehouse or Wholesale premises  </option>'+
                            '<option value="Class8_DivC">8  - Factory / Manufacturing facility (Division C ANZSIC) </option>'+
                            '<option value="Class8_Other">8  - Laboratory or factory - Non Manufacturing  (i.e. sawmill) (other than Division C ANZSIC) </option>'+
                            '<option value="Class9a">9A - Hospital, day surgery or similar healthcare building  </option>'+
                            '<option value="Class9b">9B - Any public assembly building (e.g. a School, Library, Church, Theatre, etc.)  </option>'+
                            '<option value="Class9c">9C - Aged care facility (excludes detached retirement units)  </option>'+
                            '<option value="Class10">10 - Non-habitable structures including Fences, Retaining walls, Swimming pools B </option>'+
                        '</select>'+
                    '</td>'+
                    '<td class="">'+
                        '<select class="input-sm form-control yellow width-250" name="J'+$row_num+'" id="J'+$row_num+'" data-cell="J'+$row_num+'">'+
                        '</select>'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow existing_lamp width-250" id="L'+$row_num+'" name="L'+$row_num+'" data-cell="L'+$row_num+'" data-count="'+$row_num+'">'+
                    '</td>'+
                    '<td class="hidden">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="K'+$row_num+'" name="K'+$row_num+'" data-cell="K'+$row_num+'" data-formula="I'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="I'+$row_num+'" name="I'+$row_num+'" data-cell="I'+$row_num+'" data-formula="H'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HA'+$row_num+'" name="HA'+$row_num+'" data-cell="HA'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HB'+$row_num+'" name="HB'+$row_num+'" data-cell="HB'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HC'+$row_num+'" name="HC'+$row_num+'" data-cell="HC'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="HD'+$row_num+'" name="HD'+$row_num+'" data-cell="HD'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="T'+$row_num+'" name="T'+$row_num+'" data-cell="T'+$row_num+'" data-formula="(R'+$row_num+'+0)*HA'+$row_num+'+HB'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="AP'+$row_num+'" name="AP'+$row_num+'" data-cell="AP'+$row_num+'" data-formula="IF(AC'+$row_num+'='+'NJ6-AB'+',0,AC'+$row_num+')">'+ 
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="R'+$row_num+'" name="R'+$row_num+'" data-cell="R'+$row_num+'" data-formula="IF(HD'+$row_num+' = '+'yes'+',IF(S'+$row_num+'>37,37,S'+$row_num+'),S'+$row_num+')">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="X'+$row_num+'" name="X'+$row_num+'" data-cell="X'+$row_num+'" data-formula="U'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="Z'+$row_num+'" name="Z'+$row_num+'" data-cell="Z'+$row_num+'" data-formula="Y'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow" id="AD'+$row_num+'" name="AD'+$row_num+'" data-cell="AD'+$row_num+'" data-formula="IF(AA'+$row_num+'=0,0,IF(AP'+$row_num+'=0,(T'+$row_num+'*AN'+$row_num+'*X'+$row_num+'*Z'+$row_num+'*AA'+$row_num+')/1000000,(T'+$row_num+'*AN'+$row_num+'*X'+$row_num+'*Z'+$row_num+'*AA'+$row_num+')/1000000))">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AT'+$row_num+'" name="AT'+$row_num+'" data-cell="AT'+$row_num+'" data-formula="AQ'+$row_num+'">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AV'+$row_num+'" name="AV'+$row_num+'" data-cell="AV'+$row_num+'" data-formula="Y'+$row_num+'" data-format="0.0000">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AX'+$row_num+'" name="AX'+$row_num+'" data-cell="AX'+$row_num+'" data-formula="IF(AW'+$row_num+'=0,0,IF(AP'+$row_num+'=1,((T'+$row_num+'*AN'+$row_num+'*AT'+$row_num+'*AV'+$row_num+'*AW'+$row_num+')/1000000),IF(AP'+$row_num+'=2,((AM'+$row_num+'*AN'+$row_num+'*AT'+$row_num+'*AV'+$row_num+'*AW'+$row_num+')/1000000),IF(AP'+$row_num+'=3,((AM'+$row_num+'*AN'+$row_num+'*AT'+$row_num+'*AV'+$row_num+'*AW'+$row_num+')/1000000),((AM'+$row_num+'*AN'+$row_num+'*AT'+$row_num+'*AV'+$row_num+'*AW'+$row_num+')/1000000)))))">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AZ'+$row_num+'" name="AZ'+$row_num+'" data-cell="AZ'+$row_num+'" data-formula="IF(AW'+$row_num+'=0,0,(AD'+$row_num+'-AX'+$row_num+'))">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow width-50" id="S'+$row_num+'" name="S'+$row_num+'" data-cell="S'+$row_num+'">'+
                    '</td>'+
                    '<td class="">'+
                    '<select class="input-sm form-control yellow width-250" name="U'+$row_num+'" id="U'+$row_num+'" data-cell="U'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No control system</option>'+
                            '<option value="0.7">Occupancy sensor</option>'+
                            '<option value="0.7">Day-light linked control</option>'+
                            '<option value="0.85">Programmable dimming</option>'+
                            '<option value="0.9">Manual dimming</option>'+
                            '<option value="0.744">VRU</option>'+
                            '<option value="0.6">Occupancy & Daylight Control</option>'+
                            '<option value="0.6">Occupancy & Program. Dimmer</option>'+
                            '<option value="0.63">Occupancy & Manual Dimmer</option>'+
                            '<option value="0.6">Occupancy & VRU</option>'+
                            '<option value="0.6">Daylight Control & Program. Dimmer</option>'+
                            '<option value="0.63">Daylight Control & Manual Dimmer</option>'+
                            '<option value="0.6">Daylight Control & VRU</option>'+
                            '<option value="0.765">Program. Dimmer & Manual Dimmer</option>'+
                            '<option value="0.6324">Program. Dimmer & VRU</option>'+
                            '<option value="0.6696">Manual Dimmer & VRU</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class="">'+
                    '<select class="input-sm form-control yellow" name="Y'+$row_num+'" id="Y'+$row_num+'" data-cell="Y'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No</option>'+
                            '<option value="1.05">Yes</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow width-50" id="AA'+$row_num+'" name="AA'+$row_num+'" data-cell="AA'+$row_num+'">'+
                    '</td>'+
                    '<td class="">'+
                    '<select class="input-sm form-control yellow width-250" name="AC'+$row_num+'" id="AC'+$row_num+'" data-cell="AC'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">Delamping from multiple lamp fittings (NJ6-A)</option>'+
                            '<option value="2">Replace lamp only (NJ6-D)</option>'+
                            '<option value="3">Replace lamp and control gear (NJ6-C)</option>'+
                            '<option value="3">Replace full luminaire (NJ6-C)</option>'+
                            '<option value="NJ6-AB">Install lighting control device or sensor only (NJ6-AB)</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow width-250 product34" id="AE'+$row_num+'" name="AE'+$row_num+'" data-cell="AE'+$row_num+'" data-count="'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow width-250" id="ZE<?= $svbd ?>" name="ZE<?= $svbd ?>" data-cell="ZE<?= $svbd ?>">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow width-250" id="ZF<?= $svbd ?>" name="ZF<?= $svbd ?>" data-cell="ZF<?= $svbd ?>">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow width-250" id="ZG<?= $svbd ?>" name="ZG<?= $svbd ?>" data-cell="ZG<?= $svbd ?>" data-formula="CONCATENATE(ZE<?= $svbd ?>,'+' '+',ZF<?= $svbd ?>)">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow width-50" id="AM'+$row_num+'" name="AM'+$row_num+'" data-cell="AM'+$row_num+'">'+
                        '<input type="hidden" class="input-sm form-control text-center yellow width-50" id="AN'+$row_num+'" name="AN'+$row_num+'" data-cell="AN'+$row_num+'" data-formula="IF(AP'+$row_num+'=1,AO'+$row_num+',IF(AP'+$row_num+'=2,AO'+$row_num+',IF(AP'+$row_num+'=3,(10*AY'+$row_num+'),5*AY'+$row_num+')))">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AO'+$row_num+'" name="AO'+$row_num+'" data-cell="AO'+$row_num+'" data-format="0,0[.]">'+
                    '</td>'+
                    '<td class="">'+
                    '<select class="input-sm form-control yellow width-250" name="AQ'+$row_num+'" id="AQ'+$row_num+'" data-cell="AQ'+$row_num+'">'+
                            '<option value="0"></option>'+
                            '<option value="1">No control system</option>'+
                            '<option value="0.7">Occupancy sensor</option>'+
                            '<option value="0.7">Day-light linked control</option>'+
                            '<option value="0.85"8>Programmable dimming</option>'+
                            '<option value="0.9">Manual dimming</option>'+
                            '<option value="0.744">VRU</option>'+
                            '<option value="0.6">Occupancy & Daylight Control</option>'+
                            '<option value="0.6">Occupancy & Program. Dimmer</option>'+
                            '<option value="0.63">Occupancy & Manual Dimmer</option>'+
                            '<option value="0.6">Occupancy & VRU</option>'+
                            '<option value="0.6">Daylight Control & Program. Dimmer</option>'+
                            '<option value="0.63">Daylight Control & Manual Dimmer</option>'+
                            '<option value="0.6">Daylight Control & VRU</option>'+
                            '<option value="0.765">Program. Dimmer & Manual Dimmer</option>'+
                            '<option value="0.6324">Program. Dimmer & VRU</option>'+
                            '<option value="0.6696">Manual Dimmer & VRU</option>'+
                        '</select>'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow width-50" id="AW'+$row_num+'" name="AW'+$row_num+'" data-cell="AW'+$row_num+'" data-formula="AA'+$row_num+'">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="AY'+$row_num+'" name="AY'+$row_num+'" data-cell="AY'+$row_num+'" data-formula="J'+$row_num+'">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow" id="BA'+$row_num+'" name="BA'+$row_num+'" data-cell="BA'+$row_num+'" data-formula="IFERROR(IF(AZ'+$row_num+'>0,IF(AW9=0,(AZ'+$row_num+'*BS106)*1.095,0),0),0)" data-format="0.0">'+
                    '</td>'+
                '</tr>'
            );

            var $quote_body = $('#quote_body');
            var $row_quote = $row_num - 2;
            $quote_body.append(
                '<tr>'+
                    '<td <label for="" id="B'+$row_quote+'" name="B'+$row_quote+'" data-cell="B'+$row_quote+'" data-formula="C'+$row_quote+'"></label>'+
                        '<input type="hidden" class="input-sm form-control text-center" id="C'+$row_quote+'" name="C'+$row_quote+'" data-cell="C'+$row_quote+'" data-formula="'+$row_num+'-14">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center width-250" id="D'+$row_quote+'" name="D'+$row_quote+'" data-cell="D'+$row_quote+'" data-formula="#survey!C'+$row_num+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center width-250" id="G'+$row_quote+'" name="G'+$row_quote+'" data-cell="G'+$row_quote+'" data-formula="#survey!AE'+$row_num+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center" id="M'+$row_quote+'" name="M'+$row_quote+'" data-cell="M'+$row_quote+'" data-formula="#survey!AW'+$row_num+'"></td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center yellow" data-format="$ 0,0[.]00" id="O'+$row_quote+'" name="O'+$row_quote+'" data-cell="O'+$row_quote+'">'+
                        '<input type="hidden" class="input-sm form-control text-center" id="N'+$row_quote+'" name="N'+$row_quote+'" data-cell="N'+$row_quote+'" data-formula="O'+$row_quote+'*M'+$row_quote+'">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="Q'+$row_quote+'" name="Q'+$row_quote+'" data-cell="Q'+$row_quote+'">'+
                        '<input type="hidden" class="input-sm form-control text-center" id="P'+$row_quote+'" name="P'+$row_quote+'" data-cell="P'+$row_quote+'" data-formula="Q'+$row_quote+'*M'+$row_quote+'">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="S'+$row_quote+'" name="S'+$row_quote+'" data-cell="S'+$row_quote+'">'+
                        '<input type="hidden" class="input-sm form-control text-center" id="R'+$row_quote+'" name="R'+$row_quote+'" data-cell="R'+$row_quote+'" data-formula="S'+$row_quote+'*M'+$row_quote+'">'+
                    '</td>'+
                    '<td class="">'+
                        '<input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="U'+$row_quote+'" name="U'+$row_quote+'" data-cell="U'+$row_quote+'">'+
                        '<input type="hidden" class="input-sm form-control text-center" id="T'+$row_quote+'" name="T'+$row_quote+'" data-cell="T'+$row_quote+'" data-formula="U'+$row_quote+'*M'+$row_quote+'">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center" data-format="$ 0,0[.]00" id="W'+$row_quote+'" name="W'+$row_quote+'" data-cell="W'+$row_quote+'" data-formula="IF(((O'+$row_quote+'+Q'+$row_quote+'+S'+$row_quote+'+U'+$row_quote+')*M'+$row_quote+')>0,((O'+$row_quote+'+Q'+$row_quote+'+S'+$row_quote+'+U'+$row_quote+')*M'+$row_quote+'),0)"></td>'+
                '</tr>'
            )
            
            $(".existing_lamp").select2({
                data:{ results: existing_lamp, text: 'id' },
                formatSelection: function(item){
                    var count = this.element.attr('data-count');
                    $('#survey').calx('getCell', 'HA'+count).setValue(item.multi).calculate();
                    $('#survey').calx('getCell', 'HB'+count).setValue(item.plus).calculate();
                    $('#survey').calx('getCell', 'HC'+count).setValue(item.rate).calculate();
                    $('#survey').calx('getCell', 'HD'+count).setValue(item.max).calculate();
                    $('#survey').calx('getSheet').calculate();
                    return item.id;

                },
                formatResult: function(item){
                    return item.id;

                }
            });
            specType()

            select2Product34()


            $('#survey, #quote').calx('update').calx('calculate');
            /** find next input when enter  */
            var $input = $('input');
            $input.on('keypress', function(e) {
                if (e.which === 13) {
                    var ind = $input.index(this);
                    $input.eq(ind + 1).focus();
                    e.preventDefault();
                }
            });
    });

/*
*   add row addtional cost
*/  $row_cost = 54;
    $('#btn-add-cost').click(function(e){
        e.preventDefault();
        $row_cost++;
        if ($row_cost==58) {
            $('#btn-add-cost').addClass('hidden');
        }else{
            $('#btn-add-cost').removeClass('hidden');
        };
            var $quote_body2 = $('#quote_body2');
            $quote_body2.append(
                '<tr>'+
                    '<td class="text-center">'+
                        '<label class="width-50" for="" id="C'+$row_cost+'" name="C'+$row_cost+'" data-cell="C'+$row_cost+'" data-formula="B'+$row_cost+'"></label>'+
                        '<input type="hidden" class="input-sm form-control text-center" id="B'+$row_cost+'" name="B'+$row_cost+'" data-cell="B'+$row_cost+'" data-formula="'+$row_cost+'-53">'+
                    '</td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center" id="D'+$row_cost+'" name="D'+$row_cost+'" data-cell="D'+$row_cost+'"></td>'+
                    '<td class=""><input type="text" class="input-sm form-control text-center width-150" data-format="$ 0,0[.]00" id="W'+$row_cost+'" name="W'+$row_cost+'" data-cell="W'+$row_cost+'"></td>'+
                '</tr>'
            );

     
            $('#quote').calx('update').calx('calculate');
            /** find next input when enter  */
            var $input = $('input');
            $input.on('keypress', function(e) {
                if (e.which === 13) {
                    var ind = $input.index(this);
                    $input.eq(ind + 1).focus();
                    e.preventDefault();
                }
            });
    });


/*
*   refresh graph 
*/


/*
*   jsignature
*/
    $("#sign1, #sign2, #sign3").jSignature({lineWidth:1, height:200, width:400})
    for (var s=1; s<=3; s++) {
      $('#sign'+s).jSignature('setData', signatureData[s], 'base30')
    }
    var $sign1 = $('#sign1');
    var $sign2 = $('#sign2');
    var $sign3 = $('#sign3');

    $('#resetSign2').click(function(e){
        e.preventDefault();
        $sign2.jSignature("reset");
    });

/*
* geo location
*/
    
    var map;

    function initialize() {
      var mapOptions = {
        zoom: 17
      };
      map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);

      // Try HTML5 geolocation
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          if('' !== mapsrc){
            mapsrc = mapsrc.split("&markers=");
            mapsrc = mapsrc[0].replace('https://maps.googleapis.com/maps/api/staticmap?center=','');
            mapsrc = mapsrc.split(',');
            var pos = new google.maps.LatLng(mapsrc[0],mapsrc[1]);
          }else{
            jQuery('[name="mapsrc"]').val(
                "https://maps.googleapis.com/maps/api/staticmap?center="+
                position.coords.latitude+","+position.coords.longitude+
                "&markers="+
                position.coords.latitude+","+position.coords.longitude+
                "&zoom=15&size=700x475");
            var pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
          }

          var infowindow = new google.maps.InfoWindow({
            map: map,
            position: pos,
            content: 'Installer location'
          });

          map.setCenter(pos);
        }, function() {
          handleNoGeolocation(true);
        });
      } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }
    }

    function handleNoGeolocation(errorFlag) {
      if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
      } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
      }

      var options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content
      };

      var infowindow = new google.maps.InfoWindow(options);
      map.setCenter(options.position);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
});


function select2Product34 () {
  $('.product34').select2({
    data : product34,
    formatSelection: function(item){
      var cell = this.element.attr('data-cell');
      $('#survey').calx('getCell', cell.replace('AA','AI')).setValue(item.lcp).calculate();
      $('#survey').calx('getCell', cell.replace('AA','AK')).setValue(item.ratedLifetime).calculate();
      $('#survey').calx('getCell', cell.replace('AA','PID')).setValue(item.pid).calculate();
      var count = this.element.attr('data-count');
      $('#survey').calx('getCell', 'AM'+count ).setValue(item.lcp).calculate();
      $('#survey').calx('getCell', 'AO'+count).setValue(item.ratedLifetime).calculate();
      $('#survey').calx('getCell', 'ZE'+count).setValue(item.brand).calculate();
      $('#survey').calx('getCell', 'ZF'+count).setValue(item.modelNumber).calculate();
      $('#survey').calx('getSheet').calculate();
      return item.text;
    },
    formatResult: function(item){
      return item.text;
    }
  });
}

/*
*   select space type dependent building class
*/  
function specType() {
        var Class2_Common = [
            {display:" ", value:""},
            {display: "Common rooms, spaces and corridors in a Class 2 building", value:7000},
            {display: "Entry lobby from outside the building", value:7000},
            {display: "Maintained Emergency Lighting", value:8500},
            {display: "Plant room", value:7000},
            {display: "Service area, cleaner's room etc.", value:7000},
            {display: "Toilet, locker room, staff room, rest room and the like", value:7000},
            {display: "Carpark - general (open air)", value:4500},
            {display: "Carpark - general (undercover)", value:7000},
            {display: "Unlisted space type", value:7000}
        ];
        var Class3_Other = [
            {display:" ", value:""},
            {display: "Sole-occupancy unit of Class 3 building", value: 3000},
            {display: "Dormitory of a Class 3 building used for sleeping only", value: 3000},
            {display: "Dormitory of a Class 3 building used for sleeping and study", value: 3000},
            {display: "Unlisted space type", value: 3000}
        ];
        var Class3_Common = [
            {display:" ", value:""},
            {display: "Common rooms, spaces and corridors in a Class 2 building ", value:7000},
            {display: "Entry lobby from outside the building", value:7000},
            {display: "Kitchen and food preparation area", value:7000},
            {display: "Maintained Emergency Lighting", value:8500},
            {display: "Plant room", value:7000},
            {display: "Service area, cleaner's room etc.", value:7000},
            {display: "Toilet, locker room, staff room, rest room and the like", value:7000},
            {display: "Restaurant, café, bar, hotel lounge and a space for the serving and consumption of food or drinks", value:5000},
            {display: "Lounge area for communal use in a Class 3 building or a Class 9c aged care building", value:7000},
            {display: "Retail space including a museum and gallery whose purpose is the sale of objects", value:5000},
            {display: "Unlisted space type", value:7000}
        ];
        var Class5 = [
            {display:" ", value:""},
            {display: "Office - artifically lit to an ambient level of 200lx or more", value:3000},
            {display: "Control room, switch room and the like", value:3000},
            {display: "Corridors", value:3000},
            {display: "Entry lobby from outside the building", value:3000},
            {display: "Kitchen and food preparation area", value:3000},
            {display: "Maintained Emergency Lighting", value:8500},
            {display: "Plant room", value:3000},
            {display: "Service area, cleaner's room etc.", value:3000},
            {display: "Toilet, locker room, staff room, rest room and the like", value:3000},
            {display: "Board room and conference room", value:3000},
            {display: "Carpark - general (open air)", value:4500},
            {display: "Carpark - general (undercover)", value:7000},
            {display: "Unlisted space type", value:3000}
        ];
        var Class6 = [
            {display:" ", value:""},
            {display: "Restaurant, café, bar, hotel lounge and a space for the serving and consumption of food or drinks", value:5000},
            {display: "Retail space including a museum and gallery whose purpose is the sale of objects", value:5000},
            {display: "Control room, switch room and the like", value:5000},
            {display: "Corridors", value:5000},
            {display: "Entry lobby from outside the building", value:5000},
            {display: "Kitchen and food preparation area", value:5000},
            {display: "Maintained Emergency Lighting", value:8500},
            {display: "Plant room", value:5000},
            {display: "Service area, cleaner's room etc.", value:5000},
            {display: "Toilet, locker room, staff room, rest room and the like", value:5000},
            {display: "Carpark - general (open air)", value:4500},
            {display: "Carpark - general (undercover)", value:7000},
            {display: "Unlisted space type", value:5000}
        ];
        var Class7a_open = [
            {display:" ", value:""},
            {display: "Carpark - general (open air)", value:4500}
        ];
        var Class7a_undercover = [
            {display:" ", value:""},
            {display: "Carpark - general (undercover)", value:7000},
            {display: "Control room, switch room and the like", value:7000},
            {display: "Corridors", value:7000},
            {display: "Maintained Emergency Lighting", value:8500},
            {display: "Plant room", value:7000},
            {display: "Service area, cleaner's room etc.", value:7000},
            {display: "Toilet, locker room, staff room, rest room and the like", value:7000},
            {display: "Unlisted space type", value:7000}
        ];
        var Class7b = [
            {display:" ", value:""},
            {display: "Wholesale storage and display area", value:5000},
            {display: "Storage with shelving no higher than 75% of the height of the aisle lighting", value:5000},
            {display: "Storage with shelving higher than 75% of the height of the aisle lighting", value:5000},
            {display: "Control room, switch room and the like", value:5000},
            {display: "Corridors", value:5000},
            {display: "Kitchen and food preparation area", value:5000},
            {display: "Maintained Emergency Lighting", value:8500},
            {display: "Plant room", value:5000},
            {display: "Service area, cleaner's room etc.", value:5000},
            {display: "Toilet, locker room, staff room, rest room and the like", value:5000},
            {display: "Carpark - general (open air)", value:4500},
            {display: "Carpark - general (undercover)", value:7000},
            {display: "Unlisted space type", value:5000}
        ];
        var Class8_DivC = [
            {display:" ", value:""},
            {display: "Plant room", value:5000},
            {display: "Office - artifically lit to an ambient level of 200lx or more", value:3000},
            {display: "Storage with shelving no higher than 75% of the height of the aisle lighting", value:5000},
            {display: "Storage with shelving higher than 75% of the height of the aisle lighting", value:5000},
            {display: "Control room, switch room and the like", value:5000},
            {display: "Corridors", value:5000},
            {display: "Kitchen and food preparation area", value:5000},
            {display: "Maintained Emergency Lighting", value:8500},
            {display: "Service area, cleaner's room etc.", value:5000},
            {display: "Toilet, locker room, staff room, rest room and the like", value:5000},
            {display: "Carpark - general (open air)", value:4500},
            {display: "Carpark - general (undercover)", value:7000},
            {display: "Unlisted space type", value:5000}
        ];
        var Class8_Other = [
            {display:" ", value:""},
            {display: "Laboratory - artifically lit to an ambient level of 400lx or more", value: 3000},
            {display: "Wholesale storage and display area", value: 5000},
            {display: "Storage with shelving no higher than 75% of the height of the aisle lighting", value: 5000},
            {display: "Storage with shelving higher than 75% of the height of the aisle lighting", value: 5000},
            {display: "Control room, switch room and the like", value: 3000},
            {display: "Corridors", value: 3000},
            {display: "Kitchen and food preparation area", value: 3000},
            {display: "Maintained Emergency Lighting", value: 8500},
            {display: "Plant room", value: 3000},
            {display: "Service area, cleaner's room etc.", value: 3000},
            {display: "Toilet, locker room, staff room, rest room and the like", value: 3000},
            {display: "Carpark - general (open air)", value: 4500},
            {display: "Carpark - general (undercover)", value: 7000},
            {display: "Unlisted space type", value: 3000}
        ];
        var Class9a = [
            {display:" ", value:""},
            {display: "Healthcare - children's ward & examination room", value:6000},
            {display: "Healthcare - patient ward  ", value:6000},
            {display: "Healthcare - all patient care areas including corridors where cyanosis lamps are used  ", value:6000},
            {display: "Laboratory - artifically lit to an ambient level of 400lx or more   ", value:3000},
            {display: "Storage with shelving no higher than 75% of the height of the aisle lighting    ", value:5000},
            {display: "Storage with shelving higher than 75% of the height of the aisle lighting   ", value:5000},
            {display: "Control room, switch room and the like  ", value:6000},
            {display: "Corridors   ", value:6000},
            {display: "Kitchen and food preparation area   ", value:6000},
            {display: "Maintained Emergency Lighting   ", value:8500},
            {display: "Plant room  ", value:6000},
            {display: "Service area, cleaner's room etc.   ", value:6000},
            {display: "Toilet, locker room, staff room, rest room and the like ", value:6000},
            {display: "Carpark - general (open air)    ", value:4500},
            {display: "Carpark - general (undercover)  ", value:7000},
            {display: "Unlisted space type ", value:6000}
        ];
        var Class9b = [
            {display:" ", value:""},
            {display: "Auditorium, church and public hall", value:2000},
            {display: "Board room and conference room", value:3000},
            {display: "Control room, switch room and the like", value:2000},
            {display: "Corridors", value:2000},
            {display: "Courtroom", value:2000},
            {display: "Entry lobby from outside the building", value:2000},
            {display: "Kitchen and food preparation area", value:2000},
            {display: "Library - reading room and general areas", value:3000},
            {display: "Library - stack and shelving areas", value:3000},
            {display: "Maintained Emergency Lighting", value:8500},
            {display: "Museum and gallery - circulation, cleaning and service lighting", value:2000},
            {display: "Office - artifically lit to an ambient level of 200lx or more", value:3000},
            {display: "Plant room", value:2000},
            {display: "Restaurant, café, bar, hotel lounge and a space for the serving and consumption of food or drinks", value:5000},
            {display: "Retail space including a museum and gallery whose purpose is the sale of objects", value:5000},
            {display: "School - general purpose learning areas and tutorial rooms", value:3000},
            {display: "Storage with shelving no higher than 75% of the height of the aisle lighting", value:5000},
            {display: "Storage with shelving higher than 75% of the height of the aisle lighting", value:5000},
            {display: "Service area, cleaner's room etc.", value:2000},
            {display: "Toilet, locker room, staff room, rest room and the like", value:2000},
            {display: "Carpark - general (open air)", value:4500},
            {display: "Carpark - general (undercover)", value:7000},
            {display: "Unlisted space type", value:2000},
        ];
        var Class9c = [
            {display:" ", value:""},
            {display: "Sole-occupancy unit fo a Class 9c aged care building", value:6000},
            {display: "Healthcare - patient ward", value:6000},
            {display: "Healthcare - all patient care areas including corridors where cyanosis lamps are used", value:6000},
            {display: "Laboratory - artifically lit to an ambient level of 400lx or more", value:3000},
            {display: "Lounge area for communal use in a Class 3 building or a Class 9c aged care building", value:7000},
            {display: "Storage with shelving no higher than 75% of the height of the aisle lighting", value:5000},
            {display: "Storage with shelving higher than 75% of the height of the aisle lighting", value:5000},
            {display: "Control room, switch room and the like", value:6000},
            {display: "Corridors", value:6000},
            {display: "Kitchen and food preparation area", value:6000},
            {display: "Maintained Emergency Lighting", value:8500},
            {display: "Plant room", value:6000},
            {display: "Restaurant, café, bar, hotel lounge and a space for the serving and consumption of food or drinks", value:5000},
            {display: "Service area, cleaner's room etc.", value:6000},
            {display: "Toilet, locker room, staff room, rest room and the like", value:6000},
            {display: "Carpark - general (open air)", value:4500},
            {display: "Carpark - general (undercover)", value:7000},
            {display: "Unlisted space type", value:6000},
        ];
        var Class10 = [
            {display:" ", value:""},
            {display: "A non-habitable structure being a fence, mast, antenna, retaining or free standing wall, swimming pool or the like.", value:1000}
        ];
        

        //If parent option is changed
        $("#H15").change(function() {
            var parent = $(this).val(); //get option value from parent
           
            switch(parent){ //using switch compare selected option and populate child
                case 'Class2_Common':
                    list(Class2_Common);
                    break;
                case 'Class3_Other':
                    list(Class3_Other);
                    break;
                case 'Class3_Common':
                    list(Class3_Common);
                    break;
                case 'Class5':
                    list(Class5);
                    break;
                case 'Class6':
                    list(Class6);
                    break;
                case 'Class7a_open':
                    list(Class7a_open);
                    break;
                case 'Class7a_undercover':
                    list(Class7a_undercover);
                    break;
                case 'Class7b':
                    list(Class7b);
                    break;
                case 'Class8_DivC':
                    list(Class8_DivC);
                    break;
                case 'Class8_Other':
                    list(Class8_Other);
                    break;
                case 'Class9a':
                    list(Class9a);
                    break;
                case 'Class9b':
                    list(Class9b);
                    break;
                case 'Class9c':
                    list(Class9c);
                    break;
                case 'Class10':
                    list(Class10);
                    break;
                default: //default child option is blank
                    $("#J15").html('');  
                    break;
               }
        });

        //function to populate child select box
        function list(array_list)
        {
            $("#J15").html(""); //reset child options
            $(array_list).each(function (i) { //populate child options
                $("#J15").append("<option value=\""+array_list[i].value+"\">"+array_list[i].display+"</option>");
            });
        }
}

