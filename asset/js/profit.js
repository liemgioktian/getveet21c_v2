$(document).ready(function(){

    var $calc = $('#profitCalc').calx();
/** find next input when enter  */
    var $input = $('input');
    $input.on('keypress', function(e) {
        if (e.which === 13) {
            var ind = $input.index(this);
            $input.eq(ind + 1).focus();
            e.preventDefault();
        }
    });


    $(".postcode").select2({
        data:{ results: postcode, text: 'id' },
        formatSelection: function(item){
            //var count = this.element.attr('data-count');
           
            $('#profitCalc').calx('getCell', 'E31').setValue(item.value).calculate();
            //$('#profitCalc').calx('getCell', 'Z40').setValue(item.id).calculate();
            $('#profitCalc').calx('getSheet').calculate();
            return item.id;

        },
        formatResult: function(item){
            return item.id;

        }
    });


    /** initial date-picker */
    $('.datepicker').datepicker({
        'dateFormat' : 'dd-mm-yy'
    });


});
