$(document).ready(function(){
	
  /** find next input when enter  */
  var $input = $('input');
  $input.on('keypress', function(e) {
    if (e.which === 13) {
      var ind = $input.index(this);
      $input.eq(ind + 1).focus();
      e.preventDefault();
    }
  });

  $("#agree_company").change(function() {
    if(this.checked) {
        $('#next2').removeClass('hidden');
    }else{
        $('#next2').addClass('hidden');;
    }
});

  /** initial date-picker */
  $('.datepicker').datepicker({
    'dateFormat' : 'dd-mm-yy'
  });

  var url = window.location.href;
  var segments = url.split('/')
  var clicks = segments[segments.length - 2];
  clicks = clicks % 1 == 0 ? clicks : segments[segments.length - 1];
  clicks = clicks/clicks !== 1 ? 1 : clicks;
  var percent = Math.min(Math.round(clicks / 4 * 100), 100);
  $('.progress-bar').width(percent + '%').text('Part ' +clicks+ ' of 4');
  $('.signup').addClass('hidden');
  $('#signup'+clicks).removeClass('hidden');
                          
  $('.back, .next').click(function(){
    var $button = $(this);
    var $prevPage = $button.attr('data-back');
    var $nextPage = $button.attr('data-next');
    $('.signup').addClass('hidden');
    if($nextPage >1){
      $('.welcome').addClass('hidden');
      $('.welcome1').removeClass('hidden');
    }
    if($button.hasClass('next')){
      window.location = url.slice(0,-1)+$nextPage
    }else{
      window.location = url.slice(0,-1)+$prevPage
    }
  });

/*
  $('#signup4 .next').addClass('hidden')
  var frameloaded = 0;
  $('#proprofs').load(function () {
  	frameloaded++;
  	if(frameloaded === 3){
  	  $('#signup4 .next').removeClass('hidden')
  	}
  });
*/


        $('.prev, .forward').click(function(){
            var $button = $(this);
            var $prevPage = $button.attr('data-prev');
            var $forwardPage = $button.attr('data-forward');
            $('.slide').addClass('hidden');
            if($button.hasClass('forward')){
                $('#slide'+$forwardPage).removeClass('hidden');
            }else{
                $('#slide'+$prevPage).removeClass('hidden');
            }
        });

        $('.prev_doc, .forward_doc').click(function(){
            var $button = $(this);
            var $prevPage = $button.attr('data-prev');
            var $forwardPage = $button.attr('data-forward');
            $('.guide').addClass('hidden');
            if($button.hasClass('forward')){
                $('#guide'+$forwardPage).removeClass('hidden');
            }else{
                $('#guide'+$prevPage).removeClass('hidden');
            }
        });
        $('#forward16').click(function(){
            $('#next6').removeClass('hidden');
        });

        $('#signup_form1').validationEngine();
        $('.installer').validationEngine();
/*
        $('body').on('click','#next2',function(event){
            event.preventDefault();

            $('#loading').modal('show');
            $('#signup_form1').submit();
            var $href = $(this).attr('href');
            $.get($href, function(){
                $('#loading').modal('hide');
            });

        });
*/

         $('#next2').on('click', function () {
						var input = $('input[name="electricians_company_name"]')
						var ecn = input.val()
						$.ajax({
							url:site_url+'/settingscontroller/ajax_company_name_exists',
							type:'POST',
							dataType:'text',
							data:{ecn:ecn},
							success: function (count) {
								if (count > 0) {
									input.after('<p class="bg-danger">*Company with above name, already exists</p>');
									return false;
								} else {
			          	if ($('#signup_form1').validationEngine('validate')) {
			              var $btn = $(this).button('loading')
			              $('#signup_form1').submit();
			            }									
								}
							}
						})
          });

        var xjpu6gw03c2n93;(function(d, t) {
        var s = d.createElement(t), options = {
        'userName':'myelect',
        'formHash':'xjpu6gw03c2n93',
        'autoResize':true,
        'height':'609',
        'async':true,
        'host':'wufoo.com',
        'header':'show',
        'ssl':true};
        s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'www.wufoo.com/scripts/embed/form.js';
        s.onload = s.onreadystatechange = function() {
        var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
        try { xjpu6gw03c2n93 = new WufooForm();xjpu6gw03c2n93.initialize(options);xjpu6gw03c2n93.display(); } catch (e) {}};
        var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
        });

	$('form.form-signup3 input[name="email"]').blur(function() {
		var input = $('input[name="email"]')
		var email = input.val()
		if (email=='') return
		if ($('p.invalid-email').length>0) $('p.invalid-email').remove()
		$.ajax({
			url:site_url+'/settingscontroller/ajax_user_email_exists',
			type:'POST',
			dataType:'text',
			data:{email:email},
			context: this,
			success: function (count) {
				if (count > 0) 
					input.after('<p class="bg-danger invalid-email">*User with above email, already exists</p>');
			}
		})
	})
});