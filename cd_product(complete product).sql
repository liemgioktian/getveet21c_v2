-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 03, 2015 at 07:17 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `getveet21c_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `cd_product`
--

CREATE TABLE IF NOT EXISTS `cd_product` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(255) NOT NULL,
  `type` char(1) NOT NULL DEFAULT 'C',
  `value` int(11) NOT NULL DEFAULT '1',
  `lifetime` varchar(255) NOT NULL DEFAULT '20000+',
  `output` int(11) NOT NULL DEFAULT '350',
  `efficacy` int(11) NOT NULL DEFAULT '62',
  `factors` float NOT NULL DEFAULT '0.8715',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `cd_product`
--

INSERT INTO `cd_product` (`item_id`, `id`, `type`, `value`, `lifetime`, `output`, `efficacy`, `factors`) VALUES
(1, 'PRIMSAL PMR166WWHPF (Electronic driver)', 'C', 1, '20000+', 350, 62, 0.8715),
(2, 'PRIMSAL PMR166WWHPF (Magnetic driver)', 'C', 2, '20000+', 350, 62, 0.8715),
(3, 'PRIMSAL PMR166WWHPFB (Electronic driver)', 'C', 1, '20000+', 350, 62, 0.8715),
(4, 'PRIMSAL PMR166WWHPFB (Magnetic driver)', 'C', 2, '20000+', 350, 62, 0.8715),
(5, 'Primsal PLV10WWK (Driver PD1000)', 'D', 1, '20000+', 350, 69, 0.924),
(6, 'Primsal PLV6WWK (Driver PD400)', 'D', 1, '20000+', 350, 69, 0.924),
(7, 'Primsal PGU106WWDHPF', 'D', 1, '20000+', 350, 69, 0.924);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
